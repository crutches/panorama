//@ts-check

//const fsp = require("fs").promises;
const { 
	lstat, 
	readdir, 
	readFile, 
	writeFile, 
	ensureFile
} = require("fs-extra");

const { join, extname } = require("path");
const obfuscator = require('javascript-obfuscator');
const cssminify = require("./cssminify");

let Settings;

let sourceFolder;
let outputFolder;

module.exports = async function (modSrc, ...args) {

	Settings = await LoadConfig(join(__dirname, "settings.json"));

	console.log("Obfuscating and minifying..");

	for (let i = 0; i < args.length; i++) {

		let arg = args[i];

		switch (arg) {
			case "-scripts":
				outputFolder = join(modSrc, "output", "scripts");

				FindFiles(join(modSrc, "scripts"));
				return;

			case "-source":
				sourceFolder = args[i + 1];

				if (sourceFolder === undefined)
					throw Error(`-source is not defined. Example.: -source "C:/test/test2"`);

				i++;
				break;

			case "-output":
				outputFolder = args[i + 1];

				if (outputFolder === undefined)
					throw Error(`-output is not defined. Example.: -source "C:/test/test2"`);

				i++;
				break;
			default: break;
		}
	}
	
	await FindFiles(sourceFolder);
};

async function LoadConfig(src) {
	let file = await readFile(src);

	return JSON.parse(file.toString());
}

async function FindFiles(oldPath, relativePath = "") {
	
	let paths = await readdir(oldPath);

	let promises = paths.map(async url => {

		let fullPath = join(oldPath, url);

		const stat = await lstat(fullPath);

		let relativePathUrl = join(relativePath, url);
		
		if (stat.isDirectory())
			return FindFiles(fullPath, relativePathUrl);

		let ext = extname(url);
			
		if (ext === ".js")
			await Obfuscate(fullPath, relativePathUrl);
		else if (ext === ".css")
			await MinifyCSS(fullPath, relativePathUrl);
	});

	return await Promise.all(promises);
}

async function Obfuscate(source, relativePath) {
	
	let sourceFileBuffer = await readFile(source);
	let sourceFile = sourceFileBuffer.toString();
	
	let obfs = obfuscator.obfuscate(sourceFile, Settings).getObfuscatedCode();
	
	let distPath = join(outputFolder, relativePath);
	
	await ensureFile(distPath);
	
	await writeFile(distPath, obfs);
	
	console.log("\t- Obfuscated JS: " + relativePath);
}

async function MinifyCSS(source, relativePath) {
	
	let sourceFileBuffer = await readFile(source);
	let sourceFile = sourceFileBuffer.toString();
	
	let min = cssminify(sourceFile);
	
	let distPath = join(outputFolder, relativePath);
	
	await ensureFile(distPath);

	await writeFile(distPath, min);
	
	console.log("\t- Minified CSS: " + relativePath);
}