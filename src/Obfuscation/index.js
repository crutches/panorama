let args = process.argv.slice(2);

if (args[0] === undefined)
	throw Error("Need set mod src!")

require("./obfuscator")(join(process.cwd(), "mods", args[0]), ...args.slice(1));