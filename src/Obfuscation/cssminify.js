module.exports = (str) => 
	str.replace(/\/\*(.|\n)*?\*\//g, "")
		.replace(/\s*(\{|\}|\[|\]|\(|\)|\:|\;|\,)\s*/g, "$1")
		.replace(/#([\da-fA-F])\1([\da-fA-F])\2([\da-fA-F])\3(;|\n)\4/g, "#$1$2$3$4")
		.replace(/:[\+\-]?0(rem|em|ec|ex|px|pc|pt|vh|vw|vmin|vmax|%|mm|cm|in)/g, ":0")
		.replace(/\n/g, "")
		.replace(/^\s+|\s+$/g, "");