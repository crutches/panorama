// @ts-check

const { join, basename } = require("path");
const find = require("find-process");
const exec = require("child_process").exec;
const request = require("request");
const { 
	copy, 
	move, 
	existsSync,
	ensureFile,
	readFile, 
	writeFile, 
	remove 
} = require("fs-extra");
const obfuscator = require("../Obfuscation/obfuscator");

const rootPath = process.cwd();

var args = process.argv.slice(2);

if (args[0] === undefined)
	throw Error("Need set mod src!")

const NAME = basename(args[0]);
const NAME_MOD = "crutches_" + NAME;

const MOD_SRC = join(rootPath, "mods", args[0]);
const MOD_SRC_COMPILER = join(MOD_SRC, "compiler");
const MOD_SRC_OUTPUT = join(MOD_SRC, "output");

const URL_TO_TRACKING = "https://raw.githubusercontent.com/SteamDatabase/GameTracking-Dota2/master/game/dota/pak01_dir/panorama/layout/";

const gameInfoFind = new RegExp("Game\\s+" + (NAME_MOD + "_TEST"));
const gameInfoFindInstalled = new RegExp("\\s+(\\S+)" + gameInfoFind.source + "\\s", "g");
const gameInfoFindReplace = /(SearchPaths(\s+){)/;
const gameInfoToReplace = "\r\n\t\t\tGame\t\t\t\t" + (NAME_MOD + "_TEST");

(async () => {

	const { DOTA_PATH } = await LoadConfig(join(rootPath, "config.json"));
	const config = await LoadConfig(join(MOD_SRC_COMPILER, "config.json"));
	const xmls = await LoadConfig(join(MOD_SRC_COMPILER, "xmls.json"));
	
	const DOTA_CONTENT_PATH = join(DOTA_PATH, "content", "dota_addons", NAME_MOD);
	const DOTA_GAME_PATH = join(DOTA_PATH, "game", "dota_addons", NAME_MOD);
	
	console.log("Killing processes");
	
	{ // kill processes
		
		let processes = ["dota2", "vconsole2"].map(async item => {
		
			let procs = await find("name", item)
			
			procs.forEach(proc => process.kill(proc.pid));
		});
		
		await Promise.all(processes);
	}
	
	console.log("Copying to workshop 'Content' folder for compiling");
	
	{
		await remove(DOTA_CONTENT_PATH);
		
		let contentFolder = join(MOD_SRC, "workshop", "content");
		
		await copy(contentFolder, DOTA_CONTENT_PATH, {
			filter: src => !src.includes("custom_ui_manifest")
		});
	}
	
	console.log("Dowloading layouts..");
	
	{
		await Promise.all(xmls.map(async layout => {

			let xml = await Request(URL_TO_TRACKING + layout.LAYOUT);

			xml = ReplaceXML(xml, layout.SCRIPTS);

			let pathToLayout = join(DOTA_CONTENT_PATH, "panorama", "layout", layout.LAYOUT);

			await ensureFile(pathToLayout);
			
			await writeFile(pathToLayout, xml);

			console.log(`\t- Downloaded: ${layout.LAYOUT}`);
		}));
	}
	
	if (config.OBFUSCATE) {
		
		console.log("Staring obfuscate..");

		await obfuscator(MOD_SRC, "-source", DOTA_CONTENT_PATH, "-output", DOTA_CONTENT_PATH);
	}

	console.log("Compiling..");
	
	{
		await remove(DOTA_GAME_PATH);
		
		let pathToCompiler = join(DOTA_PATH, "game", "bin", "win64", "resourcecompiler.exe");
		
		await execPromise(`"${pathToCompiler}" -vpkincr -i "${DOTA_CONTENT_PATH}/*" -f -r`);
		
		await remove(DOTA_CONTENT_PATH);
	}
	
	/* console.log("Copying to workshop 'Game' folder");
	
	{
		let gameFolder = join(MOD_SRC, "workshop", "game", "panorama");
		
		await copy(gameFolder, join(DOTA_GAME_PATH, "panorama"), {
			overwrite: false,
			filter: src => !src.includes("custom_ui_manifest")
		});
	} */
	
	console.log("Copying to 'Output' folder");
	
	let outputFolder = join(MOD_SRC_OUTPUT, "vpk");
	
	await remove(outputFolder);
	
	let outputSourceFolder = join(outputFolder, "source");
	
	{
		await move(DOTA_GAME_PATH, outputSourceFolder);
		
		await remove(DOTA_GAME_PATH);
	}
	
	if (!config.VPK_CREATE) {
		console.log("done");
		return;
	}
	
	console.log("Creating VPK..");
	
	let vpkFolder = join(outputFolder, "pak01_dir.vpk");
	
	{
		await remove(vpkFolder);
		
		let vpkExecPath = join(__dirname, "src", "vpk_executable");
		let vpkExecFolder = join(vpkExecPath, "pak01_dir");
		
		await remove(vpkExecFolder);
		
		await copy(outputSourceFolder, vpkExecFolder);
		
		await execPromise(`"${join(vpkExecPath, "vpk.exe")}" "${vpkExecFolder}"`);
		
		await remove(vpkExecFolder);
		
		await move(join(vpkExecPath, "pak01_dir.vpk"), vpkFolder);
	}
	
	if (!config.GAMEINFO_INCULDE) {
		console.log("done");
		return;
	}
	
	console.log("Creating test dir..");
	
	{
		let gameInfoPath = join(DOTA_PATH, "game", "dota", "gameinfo.gi");
		let vpkPath = join(DOTA_PATH, "game", (NAME_MOD + "_TEST"), "pak01_dir.vpk")
		
		await copy(vpkFolder, vpkPath);
		
		let gameInfoBuffer = await readFile(gameInfoPath);
		let gameInfo = gameInfoBuffer.toString();
		
		let findInstalled = gameInfoFindInstalled.exec(gameInfo)
		
		if (findInstalled === null) {
			
			if (!gameInfoFind.test(gameInfo)) {
				let findToReplace = gameInfoFindReplace.exec(gameInfo);

				if (findToReplace === null)
					throw new Error("Not found 'SearchPaths' in GameInfo")

				gameInfo = gameInfo.replace(gameInfoFindReplace, findToReplace[1] + gameInfoToReplace);
				
				await writeFile(gameInfoPath, gameInfo);
			}
		}
		else {
			
			let replace = findInstalled[0].match(findInstalled[1]);
			
			let index = findInstalled.index + replace.index;
			let endIndex = index + findInstalled[1].length;

			gameInfo = gameInfo.substring(0, index) + gameInfo.substring(endIndex);
			await writeFile(gameInfoPath, gameInfo);
		}
	}
	
	if (!config.START_DOTA) {
		console.log("done");
		return;
	}
	
	console.log("Starting Dota2");
	
	{
		exec(`"${join(DOTA_PATH, "game", "bin", "win" + config.LAUNCH_DOTA_ARCH, "dota2.exe")}" ${config.LAUNCH_DOTA_OPTIONS}`).unref();

		setTimeout(() => process.exit(), 1000);
	}
	
})();

async function LoadConfig(src) {

	try {
		
		if (!existsSync(src))
			throw Error();

		let file = await readFile(src);
			
		return JSON.parse(file.toString());
		
	} catch (err) {
		throw console.error(`Config '${src}' is not found! Try 'npm i' again`);
	}
}

function Request(url) {
	return new Promise(function (resolve, reject) {
		request(url, function (error, res, body) {
			if (!error && res.statusCode == 200) {
				resolve(body);
			} else {
				reject(error);
			}
		});
	});
}

function ReplaceXML(xml, text) {
	
	text = text.join("\n\t\t");
	
	let scriptsTag = xml.match("</scripts>");
	
	if (scriptsTag !== null) {
	
		let index = scriptsTag.index;
		
		xml = xml.slice(0, index) 
			+ text
			+ "\n\t" 
			+ xml.slice(index);
	} 
	else {
		
		let rootTag = xml.match("<root>");
		
		let index = rootTag.index + rootTag[0].length;
		
		xml = xml.slice(0, index)
			+ "\n\t<scripts>\n\t\t"
			+ text
			+ "\n\t</scripts>\n"
			+ xml.slice(index);
	}
	
	return xml;
}

function execPromise(cmd) {
	return new Promise((resolve, reject) => {
		exec(cmd, (error, stdout, stderr) => {
			if (error)
				return reject(error);
			if (stderr)
				return reject(stderr);
			resolve();
		});
	});
}