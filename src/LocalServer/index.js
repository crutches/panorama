// @ts-check
const express = require("express");
const { join } = require("path");
const app = express();
const chokidar = require('chokidar');

{
	let old_log = console.log;
	console.log = function () {
		old_log.apply(console, [`[ ${new Date().toLocaleString()} ]`, ...arguments]);
	}
}

process.on('uncaughtException', (err) => {
	if (err.message.includes("listen EADDRINUSE: address already in use"))
		return console.log('Server already running');

	throw err;
})

var args = process.argv.slice(2);
	
if (args[0] === undefined)
	throw Error("Need set mod src!")

const MOD_SRC = join(process.cwd(), "mods", args[0]);

let config;
try {
	config = require(join(MOD_SRC, "server", "config.json"));
} catch (e) {
	throw Error("can't execute config of mod! Pls, check correctly path to mod")
}

const NAME = config.name || args[0];
const PORT = config.port || 8085;
const CALLBACK_SRC = config.callback || "index.js";

const PATH_TO_CALLBACK = join(MOD_SRC, "server", CALLBACK_SRC);

let callback;

function RequireCallback() {
	try {
		callback = require(PATH_TO_CALLBACK);
	} catch (e) {
		console.log(Error("can't execute callback!" + e.stack));
	}
}
RequireCallback();

chokidar.watch(PATH_TO_CALLBACK).on("change", function () {
	RequireCallback();
	console.log("Server changed");
});


app.use(express.urlencoded({ extended: true }));
app.post("/", (...args) => callback(...args));
app.listen(PORT, (err) => {
	if (err)
		return console.log('Server was not running ', err)

	console.log('Host for: ' + NAME);
});
