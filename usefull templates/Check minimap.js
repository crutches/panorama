var dotaHud = Game.InGameDotaHUD.FindChildTraverse("Hud");
var miniMapContainer = Game.InGameDotaHUD.FindChildTraverse("minimap_container");

if (Panels.IsValid(dotaHud) && Panels.IsValid(miniMapContainer)) {
	var miniMapBlock = Game.InGameDotaHUD.FindChildTraverse("minimap_block");

	MiniMapSize.SetVector(
		miniMapBlock.actuallayoutwidth,
		miniMapBlock.actuallayoutheight);

	MiniMapRight = dotaHud.BHasClass("HUDFlipped");

	MiniMapScreenPosition.SetVector(
		MiniMapRight ? GameSize_Width - MiniMapSize.x : 0,
		GameSize_Height - MiniMapSize.y);

	MiniMapScale.SetVector(MiniMapSize.x / MAP_WIDTH, MiniMapSize.y / MAP_HEIGHT);

	// top
	if (panels.get(1) === undefined || !panels.get(1).IsValid()) {
		var panel = $.CreatePanel("Panel", Game.InGameHUD, "1");
		panel.BLoadLayoutFromString("<root>\
						<Panel style='z-index: 9999;background-color: red;height: 1px;width:" + (MiniMapSize.x / miniMapBlock.actualuiscale_x) + "px;'/>\
					</root>", false, false);
		panels.set(1, panel);
	}
	var pos = MiniMapScreenPosition.Clone();
	Drawing.VectorToScreen(pos);
	//$.Msg(pos);
	Drawing.VectorToPanelPercent(pos, panels.get(1));

	// bot
	if (panels.get(2) === undefined || !panels.get(2).IsValid()) {
		var panel = $.CreatePanel("Panel", Game.InGameHUD, "2");
		panel.BLoadLayoutFromString("<root>\
						<Panel style='z-index: 9999;background-color: red;height: 1px;width:" + (MiniMapSize.x / miniMapBlock.actualuiscale_x) + "px;'/>\
					</root>", false, false);
		panels.set(2, panel);
	}
	var pos = MiniMapScreenPosition.Clone().AddScalarY(MiniMapSize.y);
	Drawing.VectorToScreen(pos);
	//$.Msg(pos);
	Drawing.VectorToPanelPercent(pos, panels.get(2));

	// left
	if (panels.get(3) === undefined || !panels.get(3).IsValid()) {
		var panel = $.CreatePanel("Panel", Game.InGameHUD, "3");
		panel.BLoadLayoutFromString("<root>\
						<Panel style='z-index: 9999;background-color: red;height:" + (MiniMapSize.y / miniMapBlock.actualuiscale_y) + "px;width: 1px;'/>\
					</root>", false, false);
		panels.set(3, panel);
	}
	var pos = MiniMapScreenPosition.Clone();
	Drawing.VectorToScreen(pos);
	//$.Msg(pos);
	Drawing.VectorToPanelPercent(pos, panels.get(3));

	// right
	if (panels.get(4) === undefined || !panels.get(4).IsValid()) {
		var panel = $.CreatePanel("Panel", Game.InGameHUD, "4");
		panel.BLoadLayoutFromString("<root>\
						<Panel style='z-index: 9999;background-color: red;height:" + (MiniMapSize.y / miniMapBlock.actualuiscale_y) + "px;width: 1px;'/>\
					</root>", false, false);
		panels.set(4, panel);
	}
	var pos = MiniMapScreenPosition.Clone().AddScalarX(MiniMapSize.x);
	Drawing.VectorToScreen(pos);
	//$.Msg(pos);
	Drawing.VectorToPanelPercent(pos, panels.get(4));
}