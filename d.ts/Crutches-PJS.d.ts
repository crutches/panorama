/// <reference path="dota_enums.d.ts" />
/// <reference path="dota_panels.d.ts" />
/// <reference path="dota.d.ts" />

/* ================================== Dota Global ==================================  */

interface CScriptBindingPR_Game {
	readonly Language: Map<string, string>;

	/**
	 * Top panel in 'Dashboard'
	 */
	readonly DashboardDotaHUD: Panel;
	/**
	 * Crutches panel in 'Dashboard'
	 */
	readonly DashboardHUD: Panel;
	/**
	 * Top panel in 'InGame'
	 */
	readonly InGameDotaHUD: Panel;
	/**
	 * Crutches panel in 'InGame'
	 */
	readonly InGameHUD: Panel;

	readonly Events: Events;
	readonly MouseEvents: MouseEvents;

	AutoAcceptMatch: boolean;
	/**
	 * Delay before accept match
	 */
	AutoAcceptMatchTime: number;

	ExecuteCmd(conVar: string): void;
	/**
	 * Object with all commands
	 */
	readonly Commands: { [key: string]: any; };

	readonly IsInGame: boolean;
	CursorToWorld(): Vector;
	readonly NeutralSpots: [Vector[]];
}

interface GameConsole {
	/**
	 * Only for Supported OS
	 *
	 * @param soundname path to sound
	 * @param vol volume (1 - 100) (default: 0.5)
	 *
	 * @example
	 *  Chat.Sound("sounds\\ui\\scan.vsnd", 50)
	 */
	Sound(soundName: string, volume?: number): void;
	/**
	 * Say to all chat
	 */
	SayAll(msg: string): void;
	/**
	 * Say to all chat
	 */
	SayAll(...msg: string[]): void;
	/**
	 * Say to team chat
	 */
	SayTeam(msg: string): void;
	/**
	 * Say to team chat
	 */
	SayTeam(...msg: string[]): void;
	/**
	 * Chat wheel
	 *
	 * Only for Supported OS
	 */
	ChatWheel(id: number): void;
	/**
	 * Laugh to chat (ex: lol, rofl). Timeout 15 sec
	 *
	 * Only for Supported OS
	 */
	Laugh(): void;
	/**
	 * Only for Supported OS
	 */
	Taunt(): void;
}

declare var GameConsole: GameConsole;

/* ================================== GLOBAL ==================================  */

/**
 * DispatchEvent with custom popup
 */
declare function ShowMessagePopup(title: string, message: string): void;

/* ================================== Math ==================================  */

/**
 * Truncation + Rounding up
 */
declare function ROUND_PLUS(x: number, n: number): number;
/**
 * Rounding up integers
 */
declare function ROUND_COUNT(x: number, n: number): number;
/**
 * Truncation
 */
declare function TRUNCATED(x: number, n: number): number;
/**
 * Logarithm of the base
 */
declare function LOG_BASE(x: number, n: number): number;

/* ================================== Object ==================================  */

/**
 * The target object to copy to.
 * 
 * Copy the values of all of the enumerable own properties from one or more source objects to a target object. Returns the target object.
 */
declare function OBJECT_ASSIGN(target: object, ...sources: any[]): any;

/* ================================== String ==================================  */

/**
 * Returns true if the sequence of elements of searchString converted to a String is the same as the corresponding elements of this object (converted to a String) starting at position. Otherwise returns false.
 */
declare function STRING_STARTWITH(source: string, searchString: string, position?: number): boolean;
/**
 * Returns a new string which contains the specified number of copies of the string on which it was called, concatenated together.
 */
declare function STRING_REPEAT(source: string, count: number): string;

/* ================================== Number ==================================  */

declare function RANDOM_INTEGER(min: number, max: number): number;

/* ================================== Array ==================================  */

declare function ARGUMENTS_INLINE_SLICE(args: IArguments): any[];

/**
 * Like 'forEach' (not exits Dota2 API JS), but faster $.Each.
 * 
 * Uses back loop
 */
declare function ARRAY_FOREACH<T>(source: T[], callbackfn: (value?: T, index?: number, array?: T[]) => void): void;
/**
 * Like 'forEach' (not exits Dota2 API JS), but faster $.Each.
 * 
 * Uses forward loop
 */
declare function ARRAY_FOREACH_PLUS<T>(source: T[], callbackfn: (value?: T, index?: number, array?: T[]) => void): void;
/**
 * Like 'some', but faster.
 * 
 * Uses back loop
 */
declare function ARRAY_SOME<T>(source: T[], callbackfn: (value?: T, index?: number, array?: T[]) => boolean): boolean;
/**
 * Like 'some', but faster.
 *
 * Uses forward loop
 */
declare function ARRAY_SOME_PLUS<T>(source: T[], callbackfn: (value?: T, index?: number, array?: T[]) => boolean): boolean;
/**
 * Like 'find', but faster.
 *
 * Uses back loop
 */
declare function ARRAY_FIND<T>(source: T[], callbackfn: (value?: T, index?: number, array?: T[]) => boolean): T;
/**
 * Like 'find', but faster.
 *
 * Uses forward loop
 */
declare function ARRAY_FIND_PLUS<T>(source: T[], callbackfn: (value?: T, index?: number, array?: T[]) => boolean): T;
/**
 * Like 'filter', but faster.
 * 
 * Uses back loop
 */
declare function ARRAY_FILTER<T>(source: T[], callbackfn: (value?: T, index?: number, array?: T[]) => boolean): T[];
/**
 * Like 'filter', but faster.
 * 
 * Uses forward loop
 */
declare function ARRAY_FILTER_PLUS<T>(source: T[], callbackfn: (value?: T, index?: number, array?: T[]) => boolean): T[];
/**
 * Remove any value from array
 */
declare function ARRAY_REMOVE<T>(source: T[], obj: T): boolean;
/**
 * Remove any value from array
 * 
 * Uses operator 'delete' instead of 'splice'
 */
declare function ARRAY_REMOVE_AS_DELETE<T>(source: T[], obj: T): boolean;
/**
 * Remove all any value from array
 */
declare function ARRAY_REMOVE_ALL<T>(source: T[], obj: T): boolean;
/**
 * 
 * @param x the first index
 * @param y the second index
 * 
 * @returns array with swapped values in indexes
 */
declare function ARRAY_SWAP<T>(source: T[], x: number, y: number): T[];
/**
 * Like 'include' from ES6
 */
declare function ARRAY_INCLUDE<T>(source: T[], obj: T): boolean;

declare function ARRAY_TO_STRING<T>(source: T[]): string[];

declare function ARRAY_SORT<T>(source: T[], index: number | string, invert?: boolean): T[];
/**
 * Creates an array from an iterable object.
 * @param iterable — An iterable object to convert to an array.
 */
declare function ARRAY_FROM<T>(iterable: Iterable<T> | ArrayLike<T>): T[];

/**
 * The best way to full copy array
 */
declare function ARRAY_COPY<T>(source: T[]): T[];

/**
 * The best way to full clean array
 */
declare function ARRAY_CLEAN<T>(source: T[]): void;

declare function HAS_BIT(num: number, bit: number): boolean;
declare function HAS_MASK(num: number, mask: number): boolean;

declare function MASK_TO_ARRAY(num: number): boolean;

declare function HASH_PAIR(x: number, y: number): number;
declare function HASH_UNPAIR(z: number): [number, number];


declare function ClassLog(obj: Object, fields?: string | string[], recursiveCount?: number): void;

/* ================================== PARTICLES ==================================  */

declare const PARTICLES_RENDER_NORMAL: ControlText;
declare const PARTICLES_RENDER_ROPE: ControlText;
declare const PARTICLES_RENDER_ANIMATION: ControlText;

/* ==================================  Vector ==================================  */

declare class Vector {

	/* ================== Static ================== */

	static fromArray(array: [number?, number?, number?]): Vector;
	static FromAngle(angle: number): Vector;
	static FromAngleCoordinates(radial: number, angle: number): Vector;
	static GetCenterType<T>(array: T[], callback: (value: T) => Vector): Vector;
	static GetCenter(array: Vector[]): Vector;
	static CopyFrom(vec: Vector): Vector;

	/* ================== Fields ================== */

	x: number;
	y: number;
	z: number;

	/**
	 * Create new Vector with x, y, z
	 * @example
	 * var vector = new Vector(1, 2, 3);
	 *
	 * vector.Normalize();
	 */
	constructor(x?: number, y?: number, z?: number);

	/* ================== Getters ================== */

	/**
	 * Is this vector valid? (every value must not be infinity/NaN)
	 */
	readonly IsValid: boolean;
	/**
	 * Get the length of the vector squared. This operation is cheaper than Length().
	 */
	readonly LengthSqr: number;
	/**
	 * Get the length of the vector
	 */
	readonly Length: number;
	/**
	 * Angle of the Vector
	 */
	readonly Angle: number;
	/**
	 * Returns the polar for vector angle (in Degrees).
	 */
	readonly Polar: number;

	/* ================== Methods ================== */

	Equals(vec: Vector): boolean;
	/**
	 * Are all components of this vector are 0?
	 */
	IsZero(tolerance?: number): boolean;
	/**
	 * Are length of this vector are  greater than value?
	 */
	IsLengthGreaterThan(val: number): boolean;
	/**
	 * Invalidates this vector
	 */
	Invalidate(): this;
	/**
	 * Zeroes this vector
	 */
	toZero(): this;
	/**
	 * Negates this vector (equiv to x = -x, z = -z, y = -y)
	 */
	Negate(): this;
	/**
	 * Randomizes this vector within given values
	 */
	Random(minVal: number, maxVal: number): this;
	/**
	 * Returns a vector whose elements are the minimum of each of the pairs of elements in the two source vectors
	 * @param The another vector
	 */
	Min(vec: Vector): Vector;
	/**
	 * Returns a vector whose elements are the minimum of each of the pairs of elements in the two source vectors
	 * @param The another vector
	 */
	Max(vec: Vector): Vector;
	/**
	 * Returns a vector whose elements are the absolute values of each of the source vector's elements.
	 */
	Abs(): Vector;
	/**
	 * Returns a vector whose elements are the square root of each of the source vector's elements
	 */
	SquareRoot(): Vector;
	/**
	 * Copy this vector to another vector and return it
	 * @param vec The another vector
	 * @returns another vector
	 */
	CopyTo(vec: Vector): Vector;
	/**
	 * Copy from another vector to this vector and return it
	 * @param vec The another vector
	 * @returns this vector
	 */
	CopyFrom(vec: Vector): this;
	/**
	 * Copy from array to this vector and return it
	 * @param vec The Array
	 * @returns this vector
	 */
	CopyFromArray(array: [number?, number?, number?]): this;
	Clone(): Vector;
	/**
	 * Set vector by numbers
	 */
	SetVector(x?: number, y?: number, z?: number): this;
	/**
	 * Set X of vector by number
	 */
	SetX(num: number): this;
	/**
	 * Set Y of vector by number
	 */
	SetY(num: number): this;
	/**
	 * Set Z of vector by number
	 */
	SetZ(num: number): this;
	/**
	 * Normalize the vector
	 */
	Normalize(scalar?: number): this;
	/**
	 * The cross product of this and vec.
	 */
	Cross(vec: Vector): Vector;
	/**
	 * The dot product of this vector and another vector.
	 * @param vec The another vector
	 */
	Dot(vec: Vector): number;
	/**
	 * Scale the vector to length. ( Returns 0 vector if the length of this vector is 0 )
	 */
	ScaleTo(scalar: number): this;
	/**
	 * Divides both vector axis by the given scalar value
	 */
	DivideTo(scalar: number): this;
	/**
	 * Restricts a vector between a min and max value.
	 * @returns (new Vector)
	 */
	Clamp(min: Vector, max: Vector): Vector;

	/* ======== Add ======== */

	/**
	 * Adds two vectors together
	 * @param vec The another vector
	 * @returns	The summed vector (new Vector)
	 */
	Add(vec: Vector): Vector;
	/**
	 * Adds two vectors together
	 * @param vec The another vector
	 * @returns	The summed vector (this vector)
	 */
	AddForThis(vec: Vector): this;
	/**
	 * Add scalar to vector
	 * @returns (new Vector)
	 */
	AddScalar(scalar: number): Vector;
	/**
	 * Add scalar to vector
	 * @returns (this Vector)
	 */
	AddScalarForThis(scalar: number): this;
	/**
	 * Add scalar to X of vector
	 */
	AddScalarX(scalar: number): this;
	/**
	 * Add scalar to Y of vector
	 */
	AddScalarY(scalar: number): this;
	/**
	 * Add scalar to Z of vector
	 */
	AddScalarZ(scalar: number): this;

	/* ======== Subtract ======== */

	/**
	 * Subtracts the second vector from the first.
	 * @param vec The another vector
	 * @returns The difference vector (new Vector)
	 */
	Subtract(vec: Vector): Vector;
	/**
	 * Subtracts the second vector from the first.
	 * @param vec The another vector
	 * @returns The difference vector (this vector)
	 */
	SubtractForThis(vec: Vector): this;
	/**
	 * Subtract scalar from vector
	 * @returns (new Vector)
	 */
	SubtractScalar(scalar: number): Vector;
	/**
	 * Subtract scalar from vector
	 * @returns (this vector)
	 */
	SubtractScalarForThis(scalar: number): this;
	/**
	 * Subtract scalar from X of vector
	 */
	SubtractScalarX(scalar: number): this;
	/**
	 * Subtract scalar from Y of vector
	 */
	SubtractScalarY(scalar: number): this;
	/**
	 * Subtract scalar from Z of vector
	 */
	SubtractScalarZ(scalar: number): this;

	/* ======== Multiply ======== */

	/**
	 * Multiplies two vectors together.
	 * @param vec The another vector
	 * @return The product vector (new Vector)
	 */
	Multiply(vec: Vector): Vector;
	/**
	 * Multiplies two vectors together.
	 * @param vec The another vector
	 * @return The product vector (this vector)
	 */
	MultiplyForThis(vec: Vector): this;
	/**
	 * Multiply the vector by scalar
	 * @return (new Vector)
	 */
	MultiplyScalar(scalar: number): Vector;
	/**
	 * Multiply the vector by scalar
	 * @return (this vector)
	 */
	MultiplyScalarForThis(scalar: number): this;
	/**
	 * Multiply the X of vector by scalar
	 */
	MultiplyScalarX(scalar: number): this;
	/**
	 * Multiply the Y of vector by scalar
	 */
	MultiplyScalarY(scalar: number): this;
	/**
	 * Multiply the Z of vector by scalar
	 */
	MultiplyScalarZ(scalar: number): this;
	/* ======== Divide ======== */
	/**
	 * Divide this vector by another vector
	 * @param vec The another vector
	 * @return The vector resulting from the division (new Vector)
	 */
	Divide(vec: Vector): Vector;
	/**
	 * Divide this vector by another vector
	 * @param vec The another vector
	 * @return The vector resulting from the division (this vector)
	 */
	DivideForThis(vec: Vector): this;
	/**
	 * Divide the scalar by vector
	 * @returns (new Vector)
	 */
	DivideScalar(scalar: number): Vector;
	/**
	 * Divide the scalar by vector
	 * @returns (this vector)
	 */
	DivideScalarForThis(scalar: number): this;
	/**
	 * Divide the scalar by X of vector
	 */
	DivideScalarX(scalar: number): this;
	/**
	 * Divide the scalar by Y of vector
	 */
	DivideScalarY(scalar: number): this;
	/**
	 * Divide the scalar by Z of vector
	 */
	DivideScalarZ(scalar: number): this;

	/**
	 * Multiply, add, and assign to this vector
	 */
	MultiplyAdd(vec2: Vector, scalar: number): Vector;
	/**
	 * Multiply, add, and assign to this vector and return new vector
	 */
	MultiplyAddForThis(vec2: Vector, scalar: number): this;

	/* ======== Distance ======== */

	/**
	 * Returns the squared distance between the this and another vector
	 *
	 * @param vec The another vector
	 */
	DistanceSqr(vec: Vector): number;
	/**
	 * Returns the squared distance between the this and another vector
	 *
	 * @param vec The another vector
	 */
	DistanceSqr2D(vec: Vector): number;
	/**
	 * Returns the distance between the this and another vector
	 *
	 * @param vec The another vector
	 */
	Distance(vec: Vector): number;
	/**
	 * Returns the distance between the this and another vector in 2D
	 *
	 * @param vec The another vector
	 */
	Distance2D(vec: Vector): number;

	/* ================== Geometric ================== */

	/**
	 *
	 * @param {number} offset Axis Offset (0 = X, 1 = Y)
	 */
	Perpendicular(is_x?: boolean): Vector;
	/**
	 * Calculates the polar angle of the given vector. Returns degree values on default, radian if requested.
	 */
	PolarAngle(radian?: boolean): number;
	/**
	 * Rotates the Vector to a set angle.
	 */
	Rotated(angle: number): Vector;
	/**
	 * Extends vector in the rotation direction
	 * @param rotation for ex. Entity#Forward
	 * @param distance distance to be added
	 */
	Rotation(rotation: Vector, distance: number): Vector;
	/**
	 * Extends vector in the rotation direction
	 * @param rotation for ex. Entity#Forward
	 * @param distance distance to be added
	 * 
	 * @returns (this vector)
	 */
	RotationForThis(rotation: Vector, distance: number): Vector;
	/**
	 * Extends vector in the rotation direction by radian
	 * @param rotation for ex. Entity#Forward
	 * @param distance distance to be added
	 */
	RotationRad(rotation: Vector, distance: number): Vector;
	/**
	 * Extends vector in the rotation direction by angle
	 * @param angle for ex. Entity#NetworkRotationRad
	 * @param distance distance to be added
	 */
	InFrontFromAngle(angle: number, distance: number): Vector;
	/**
	 * 
	 * @param vec The another vector
	 * @param vecAngleRadian Angle of this vector
	 */
	FindRotationAngle(vec: Vector, vecAngleRadian: number): number;
	RotationTime(rot_speed: number): number;
	/**
	 * Angle between two vectors
	 * @param vec The another vector
	 */
	AngleBetweenVectors(vec: Vector): number;
	/**
	 * Angle between two fronts
	 * @param vec The another vector
	 */
	AngleBetweenFaces(front: Vector): number;
	/**
	* Extends this vector in the direction of 2nd vector for given distance
	* @param vec 2nd vector
	* @param distance distance to extend
	* @returns extended vector (new Vector)
	*/
	Extend(vec: Vector, distance: number): Vector;
	/**
	 * Returns if the distance to target is lower than range
	 */
	IsInRange(vec: Vector, range: number): boolean;
	/**
	 * Seeks for the closest Vector to the arrays Vectors.
	 */
	Closest(vecs: Vector[]): Vector;
	/**
	 * Returns true if the point is under the rectangle
	 */
	IsUnderRectangle(x: number, y: number, width: number, height: number): boolean;
	/**
	 * x * 180 / PI
	 */
	RadiansToDegrees(): Vector;
	/**
	 * x * PI / 180
	 */
	DegreesToRadians(): Vector;

	/* ================== To ================== */
	GetHashCode(): number;
	/**
	 * Vector to String Vector
	 * @return new Vector(x,y,z)
	 */
	toString(): string;
	/**
	 * @return [x?, y?, z?]
	 */
	toArray(count?: number): [number?, number?, number?];
}

/* ==================================  Color ==================================  */

declare class Color {
	public static CopyFrom(color: Color): Color;

	public static readonly Black: Color;
	public static readonly Red: Color;
	public static readonly Green: Color;
	public static readonly Blue: Color;
	public static readonly Yellow: Color;
	public static readonly Orange: Color;
	public static readonly Fuchsia: Color;
	public static readonly Aqua: Color;
	public static readonly White: Color;

	/* =================== Fields =================== */
	r: number;
	g: number;
	b: number;
	a: number;

	/* ================ Constructors ================ */
	/**
	 * Create new Color with r, g, b, a
	 *
	 * @example
	 * var color = new Color(1, 2, 3)
	 */
	constructor(r?: number, g?: number, b?: number, a?: number);

	/* ================== Getters ================== */

	/**
	 * Is this color valid? (every value must not be infinity/NaN)
	 */
	readonly IsValid: boolean;

	/* ================== Methods ================== */

	Equals(color: Color): boolean;
	/**
	 * Invalidates this color
	 */
	Invalidate(): this;
	/**
	 * Set Color by numbers
	 */
	SetColor(r?: number, g?: number, b?: number, a?: number): this;
	/**
	 * Set R of color by number
	 */
	SetR(r: number): this;
	/**
	 * Set G of color by number
	 */
	SetG(g: number): this;
	/**
	 * Set B of color by number
	 */
	SetB(b: number): this;
	/**
	 * Set A of color by number
	 */
	SetA(a: number): this;

	CopyTo(color: Color): this;
	CopyFrom(color: Color): this;
	Clone(): Color;
	/* ================== To ================== */
	GetHashCode(): number;
	/**
	 * Color to String Color
	 * @return Color(r,g,b,a)
	 */
	toString(): string;
	/**
	 * @return [r?, g?, b?, a?]
	 */
	toArray(count?: number): [number?, number?, number?, number?];
}

/* ================================== EventEmitter ==================================  */

type Listener = (...args: any) => any;
declare class EventEmitter {
	public on(name: string, listener: Listener): this;
	public removeListener(name: string, listener: Listener): this;
	public removeAllListeners(): this;
	public emit(name: string, cancellable?: boolean, ...args: any[]): boolean;
	public once(name: string, listener: Listener): this;
}

declare interface Events extends EventEmitter {
	/**
	 * Emmited before reload scripts
	 */
	on(name: "ReloadScripts", callback: () => void): this;
	on(name: "ReloadScriptsEnded", callback: () => void): this;

	on(name: "Language", callback: (language: Language) => void): this;

	on(name: "Update", callback: () => void): this;
	on(name: "Tick", callback: () => void): this;

	on(name: "ResolutionChanges", callback: (width: number, height: number) => void): this;
	on(name: "DashboardCreated", callback: () => void): this;

	on(name: "GameLoaded", callback: () => void): this;
	on(name: "GameStarted", callback: () => void): this;
	on(name: "GameEnded", callback: () => void): this;
}
declare var Events: Events;

declare interface NativeEvents extends EventEmitter {
	on(name: "AssignedHero", callback: (event: { playerid: number; }) => void): this;
	on(name: "EntitySpawned", callback: (event: { entindex: number; }) => void): this;
	on(name: "EntityKilled", callback: (event: { entindex_killed: number; }) => void): this;
	on(name: "SelectedUnitsUpdate", callback: () => void): this;
	on(name: "QueryUnitsUpdate", callback: () => void): this;
	on(name: "HeroSwap", callback: () => void): this;
}
declare var NativeEvents: NativeEvents;

/**
 * All events is cancellable
 */
interface MouseEvents extends EventEmitter {
	on(name: "pressed", callbackfn: (key: MouseButton) => void): this;
	on(name: "doublepressed", callbackfn: (key: MouseButton) => void): this;
	on(name: "released", callbackfn: (key: MouseButton) => void): this;
	on(name: "wheeled", callbackfn: (key: WheelScroll) => void): this;
}
declare var MouseEvents: MouseEvents;

declare interface InGameEvents extends EventEmitter {
	on(name: "Update", callback: () => void): this;
	on(name: "Tick", callback: () => void): this;

	/**
	 * 
	 */
	on(name: "PlayerLoaded", callback: (player: Player) => void): this;
	/**
	 *
	 */
	on(name: "AssignedHero", callback: (player: Player, unit: Entity) => void): this;

	/**
	 * Emmited when local player send prepare order by SCRIPTS!!
	 */
	on(name: "PrepareOrders", callback: (order: PrepareOrdersEvent) => boolean): this;
	/**
	 *
	 */
	on(name: "EntitySpawned", callback: (entity: Entity) => void): this;
	on(name: "EntityKilled", callback: (entity: Entity) => void): this;

	on(name: "BuffDestroyed", callback: (buff: Buff) => void): this;
	on(name: "BuffStackCountChanged", callback: (buff: Buff, stackCount: number) => void): this;
}
declare var InGameEvents: InGameEvents;

/* ================================== Panels ==================================  */

interface MovePanelEvents extends EventEmitter {
	on(name: "Move", callback: (panelPos: Vector) => void): this;
	on(name: "MoveStart", callback: (panelPos: Vector) => void): this;
	on(name: "MoveEnded", callback: (panelPos: Vector) => void): this;
}

declare enum MOVE_PANEL_TYPE {
	NONE = 0,
	ONLY_PRESSED = 1,
	CTRL_NEED = 2
}

type MiniMap = {
	readonly IsValid: boolean;
	Position: Vector;
	Remove(): void;
};

type Notify = {
	readonly Panel: Panel;
	readonly Children: Panel;
	Remove(): void;
};

type TextNearMouse = {
	CreateOrUpdate(text: string, color: Color): void;
	Delete(): void;
};

interface Panels {
	IsValid(panel: Panel): boolean;
	DeletePanels(panels: Panel | Map<any, Panel> | { [name: string]: Panel; }): void;
	MovePanel(panel: Panel, type?: MOVE_PANEL_TYPE): MovePanelEvents;
	MovePanel(panel: Panel, type: MOVE_PANEL_TYPE.ONLY_PRESSED, panelMove?: Panel): MovePanelEvents;
	WorldToScreen(pos: Vector, offsetVec?: Vector, panel?: Panel, changeVisible?: boolean): void;
	/**
	 * @param image images from panorama/image/ (w/o "_png.vtex")
	 */
	MiniMap(image: string, pos: Vector, size: number | Vector, delayRemove?: number): MiniMap;
	/**
	 * @param func iternal function of Dota
	 * @param parent default Global Contex
	 */
	RunFunc(func: string, parent?: Panel): void;
}
declare var Panels: Panels;

interface Drawing {
	readonly GameWidth: number;
	readonly GameHeight: number;
	readonly GameSize: Vector;

	VectorToScreen(vec: Vector): Vector;
	VectorToPanelPercent(vec: Vector, panel?: Panel, changeVisible?: boolean): void;
	VectorToPanelPx(vec: Vector, panel: Panel, changeVisible?: boolean): void;

	WorldToScreenXY(sourceVec: Vector, offsetVec: Vector): Vector;
	WorldToMiniMap(worldPos: Vector, selfVector?: boolean): Vector;
	/**
	 * Using:
	 * 	core/notify.xml 
	 * 	core/notify.css
	 * 
	 * @param panel xml w/o <root/>
	 * @param delayRemove 
	 */
	Notify(panel: string, delayRemove?: number): Notify;
	TextNearMouse: TextNearMouse;
}
declare var Drawing: Drawing;

/* ================================== Sleeper ==================================  */

/**
 * Sleeper by Date.now()
 */
declare class Sleeper {
	/**
	 * @param id unique name
	 * @param extend extending right time by name
	 * @returns time in ms
	 */
	Sleep(ms: number, id: string, extend?: boolean): number;
	/**
	 * @param id unique name
	 */
	Sleeping(id: string): boolean;
	/**
	 * Reset time by name
	 * @param id unique name
	 */
	Reset(id: string): this;
	/**
	 * full Reset Data Base
	 */
	FullReset(): this;
}

/**
 * Sleeper by Game.GetGameTime()
 */
declare type GameSleeper = Sleeper;

/* ================================== Thread ==================================  */

declare class Thread extends EventEmitter {

	/**
	 * Pseudo Thread. Used by $.Schedule(time)
	 * 
	 * @param delay in sec
	 * @param callback emmited on.Update
	 * 
	 * @example
	 * var thread = new Thread(10);
	 *
	 * thread.on("Update", function () {  });
	 *
	 * thread.Start();
	 */
	constructor(delay: number, callback?: () => void);
	/**
	 * in sec
	 */
	Delay: number;
	readonly IsStarted: boolean;
	/**
	 * Start thread
	 */
	Start(): this;
	/**
	 * Stop thread
	 */
	Stop(): this;
	/**
	 * Start / Stop thread by state
	 */
	UpdateState(state: boolean): this;
	/**
	 * Set delay
	 */
	SetDelay(value: number): this;

	on(name: "Update", callback: () => void): this;
	on(name: "Start", callback: () => void): this;
	on(name: "Stop", callback: () => void): this;
}

declare class ThreadCallback {
	/**
	 * Pseudo Thread. Used by $.Schedule(time)
	 * 
	 * @param delay in sec
	 * @param callback emmited on.Update. resolve number - new delay
	 * @param startAfterDelay if 'true' then 'Update' will be started after delay
	 * 
	 * @example
	 * var thread = new Thread(10, function() { return new Promise() ;});
	 *
	 * thread.Start();
	 */
	constructor(delay: number, callback: () => Promise<number>, startAfterDelay?: boolean);
	/**
	 * in sec
	 */
	Delay: number;
	readonly IsStarted: boolean;
	/**
	 * Start thread
	 */
	Start(): this;
	/**
	 * Stop thread
	 */
	Stop(): this;
	/**
	 * Start / Stop thread by state
	 */
	UpdateState(state: boolean): this;
	/**
	 * Set delay
	 */
	SetDelay(value: number): this;
}

/* ================================== WebRequest ==================================  */

declare enum REQUEST_TYPE {
	GET = 1,
	SAVE_CONFIG = 2,
	CHECK_VERSION = 3,
	UPDATE_ONLINE = 4,
	CHECK_LAUNCHER = 5,
	CHECK_SERVER = 6
}

declare enum RESPONSE_TYPE {
	OK = 1,
	ERROR = 2,
	BADVERSION = 3,
	CONFIG = 4,
	SERVER_UPDATING = 5,
	SERVER_OFFLINE = 6
}


type WebResponse = {
	type: RESPONSE_TYPE;
	/**
	 * by RESPONSE_TYPE.BADVERSION
	 */
	version?: number;
	/**
	 * by RESPONSE_TYPE.GET
	 */
	settings?: string;
	/**
	 * by RESPONSE_TYPE.GET
	 */
	os?: string;
	/**
	 * by RESPONSE_TYPE.GET
	 */
	arch?: string;
	/**
	 * by RESPONSE_TYPE.GET
	 */
	scripts?: { [name: string]: string; };
	json?: {
		items: Object,
		npc_abilities: Object,
		npc_heroes: Object,
		npc_units: Object;
	};
	/**
     * by RESPONSE_TYPE.ERROR
	 */
	message?: string;
};
/* ================================== DashboardPanel ==================================  */

declare function DashboardPanelUpdate(response: RESPONSE_TYPE, countTrying?: number): void;

/* ================================== InGameOpenButton ==================================  */

interface InGameOpenButton {
	ChangeVision(visibility?: boolean): void;
	Move(vector: Vector): void;
}

declare var InGameOpenButton: InGameOpenButton;

/* ================================== Crutches ==================================  */

type Language = "english" | "russian";

interface Crutches {
	readonly VersionCore: number;
	readonly Contacts: {
		VK: string,
		Site: string;
	};
	readonly OS: string,
	readonly Arch: string,
	readonly IsOSSuppoted: boolean,
	WebRequest(data: {
		type: REQUEST_TYPE,
		/**
		 * need for REQUEST_TYPE.CHECK_VERSION
		 */
		version?: number,
		/**
		 * need for REQUEST_TYPE.SAVE_CONFIG
		 */
		config?: string,
	}, callback?: (response: WebResponse) => void): void;
	ReloadScripts(): void;

	readonly SteamID: string;

	Settings: Object;
	JSON: {
		items: Object,
		npc_abilities: Object,
		npc_heroes: Object,
		npc_units: Object;
	};
	SaveConfig(): void;
	/**
	 * Default: by game language
	 */
	Language: Language;
}
declare var Crutches: Crutches;

interface CrutchesPanels {
	Dashboard: {
		OpenButton: Panel,
		Menu: Panel;
	},
	InGame: {
		OpenButton: Panel,
		Menu: Panel;
	},
}

declare var CrutchesPanels: CrutchesPanels;





/* ####################################################################################### */

type ControlTextEnter = string | {
	en: string,
	ru?: string;
};

type ControlText = {
	en: string,
	ru: string;
};

type XYTree = {
	tree: MenuTree;
	X: MenuSlider;
	Y: MenuSlider;
	readonly Vector: Vector;
};

type XYZTree = {
	tree: MenuTree;
	X: MenuSlider;
	Y: MenuSlider;
	Z: MenuSlider;
	readonly Vector: Vector;
};

type RGBTree = {
	tree: MenuTree;
	R: MenuSlider;
	G: MenuSlider;
	B: MenuSlider;
	readonly Color: Color;
};

type RGBATree = {
	tree: MenuTree;
	R: MenuSlider;
	G: MenuSlider;
	B: MenuSlider;
	A: MenuSlider;
	readonly Color: Color;
};

type ParticleTree = {
	tree: MenuTree;
	R: MenuSlider;
	G: MenuSlider;
	B: MenuSlider;
	A: MenuSlider;
	Width: MenuSlider;
	Style: MenuSlider;
	readonly Color: Color;
};

declare class MenuBase extends EventEmitter {

	parent: MenuTree;
	name: ControlText;
	tooltip: ControlText;
	/**
	 * path to image
	 */
	logo: string;
	LogoSize: Vector;
	value: any;
	readonly default: any;
	save: boolean;

	constructor(parent: MenuBase, name: ControlTextEnter, value?: any);

	Index: number;

	/** @returns Eng name */
	toString(): string;
	ChangeParentTo(parent: MenuTree): this;
	Remove(): this;
	SetToolTip(text: ControlTextEnter): this;
	Saving(saving: boolean): this;
	/**
	 * Supporting emoticons, skills/items/heroes, any src string
	 * @example
	 * tree.AddLogo("s2r://panorama/images/hud/reborn/icon_scan_on_psd.vtex");
	 * tree.AddLogo("item_blink");
	 * tree.AddLogo("invoker_sun_strike");
	 * tree.AddLogo("npc_dota_hero_invoker");
	 */
	AddLogo(path: string, size?: Vector | number): this;
	ChangeValue(value: any): this;
	ChangeToDefault(): this;

	//on(name: "LogoChanged", callbackfn: (self: this) => void): this;
	//on(name: "ToolTipChanged", callbackfn: (self: this) => void): this;
}

declare class MenuTree extends MenuBase {

	readonly entries: MenuBase[];

	constructor(parent: MenuTree, name: ControlTextEnter);
	constructor(parent: MenuTree, name: ControlTextEnter, isToggle?: boolean, value?: boolean);

	readonly IsTreeToogle: boolean;
	/**
	 * only if IsTreeToogle
	 */
	value: boolean;
	/**
	 * only if IsTreeToogle
	 */

	ChangeReverse(): this;

	on(name: "Value", callbackfn: (value: boolean, self: this) => void): this;
	on(name: "Activated", callbackfn: (self: this) => void): this;
	on(name: "Deactivated", callbackfn: (self: this) => void): this;

	AddControl(ctrl: MenuBase, index?: number): this;
	HasControl(ctrl: MenuBase): boolean;
	GetControl(name: ControlTextEnter): MenuBase;
	ChangeIndexControl(ctrl: MenuBase, index?: number): this;
	RemoveControl(ctrl: MenuBase): this;

	/**
	 * Create or get tree control by name
	 */
	AddTree(name: ControlTextEnter, isToggle?: boolean, value?: boolean | number): MenuTree;
	/**
	 * Create or get toggle control by name
	 */
	AddToggle(name: ControlTextEnter, value?: boolean | number): MenuToggle;
	AddSlider(name: ControlTextEnter, value?: number, minValue?: number, maxValue?: number, isFloat?: boolean): MenuSlider;
	AddSwitch(name: ControlTextEnter, values: ControlTextEnter[], value?: number): MenuSwitch;
	AddButton(name: ControlTextEnter): MenuButton;
	AddTitle(name: ControlTextEnter): MenuTitle;
	AddSpells(name: ControlTextEnter, values: string[], turnValues?: string[], dragAndDrop?: boolean): MenuSelectorSpells;
	AddItems(name: ControlTextEnter, values: string[], turnValues?: string[], dragAndDrop?: boolean): MenuSelectorItems;
	AddHeroes(name: ControlTextEnter, values: string[], turnValues?: string[], dragAndDrop?: boolean): MenuSelectorHeroes;
}

declare class MenuToggle extends MenuBase {

	value: boolean;
	readonly default: boolean;

	constructor(parent: MenuTree, name: ControlTextEnter);

	ChangeReverse(): this;

	on(name: "Value", callbackfn: (value: boolean, self: this) => void): this;
	on(name: "Activated", callbackfn: (self: this) => void): this;
	on(name: "Deactivated", callbackfn: (self: this) => void): this;
}

declare class MenuSlider extends MenuBase {

	readonly IsFloat: boolean;
	value: number;
	min: number;
	max: number;

	readonly default: number;

	constructor(parent: MenuTree, name: ControlTextEnter, value?: number, minValue?: number, maxValue?: number, isFloat?: boolean);

	on(name: "Value", callbackfn: (value: number, self: this) => void): this;
}

declare class MenuSwitch extends MenuBase {

	readonly values: ControlTextEnter[];
	value: number;

	readonly default: number;

	constructor(parent: MenuTree, name: ControlTextEnter, values: ControlTextEnter[], value?: number);

	ChangeToLeft(): void;
	ChangeToRight(): void;

	on(name: "Value", callbackfn: (value: number, self: this) => void): this;
}

declare class MenuButton extends MenuBase {

	readonly value: false;
	readonly default: false;

	on(name: "Pressed", callbackfn: (self: this) => void): this;
}

declare class MenuTitle {
	parent: MenuTree;
	name: ControlText;
	tooltip: ControlText;

	constructor(parent: MenuBase, name: ControlTextEnter);

	Index: number;

	ChangeParentTo(parent: MenuTree): this;
	Remove(): this;
	SetToolTip(text: ControlTextEnter): this;
}

/**
 * @abstract
 */
declare class MenuSelector extends MenuBase {

	values: string[];

	value: string[];

	constructor(parent: MenuTree, name: ControlTextEnter, values: string[], turnValues?: string[], dragAndDrop?: boolean);

	DragAndDrop: boolean;

	ChangeToAll(): void;
	ChangeToEmpty(): void;
	ChangeValues(values: string[]): this;

	on(name: "Value", callbackfn: (value: string[], self: this) => void): this;
}

declare class MenuSelectorSpells extends MenuSelector { }
declare class MenuSelectorItems extends MenuSelector { }
declare class MenuSelectorHeroes extends MenuSelector { }

declare class MenuCategory extends MenuTree {
	constructor(name: ControlTextEnter);

	Update(): void;
	Save(): void;
}

interface Menu {
	readonly ToolTipOSWarning: ControlText;
	/**
	 * @param trees name of MenuTree or names of Array of MenuTree
	 */
	FindControl(trees: string[] | string, controlName: string): MenuBase;
	/**
	 * @param trees name of MenuTree or names of Array of MenuTree
	 */
	FindSettings(trees: string[] | string | MenuBase): any;
	/**
	 * 
	 */
	Factory(categoryName: "Visual" | "Auto Using" | "Heroes" | "Utility" | "Settings"): MenuTree;
	Factory(categoryName: ControlTextEnter): MenuTree;
	CreateXY(parent: MenuTree, name: ControlTextEnter, vector: Vector, minVector: Vector, maxVector: Vector, isToggle?: boolean, value?: boolean, isFloat?: boolean): XYTree;
	CreateXYZ(parent: MenuTree, name: ControlTextEnter, vector: Vector, minVector: Vector, maxVector: Vector, isToggle?: boolean, value?: boolean, isFloat?: boolean): XYZTree;
	CreateRGB(parent: MenuTree, name: ControlTextEnter, color?: Color | number, isToggle?: boolean, value?: boolean): RGBTree;
	CreateRGBA(parent: MenuTree, name: ControlTextEnter, color?: Color | number, isToggle?: boolean, value?: boolean): RGBATree;
	CreateParticle(parent: MenuTree, name: ControlTextEnter, color?: Color | number, render?: ControlTextEnter[], renderValue?: number, isToggle?: boolean, value?: boolean): ParticleTree;
}

declare var Menu: Menu;


interface MenuPanel {
	SendToDashboard(visibility?: boolean): void;
	SendToInGame(visibility?: boolean): void;
}

declare var MenuPanel: MenuPanel;




/* ####################################################################################### */

declare const MAX_ITEMS = 16;
declare const MAX_SKILLS = 24;

type PrepareOrders = {
	OrderType: dotaunitorder_t,
	TargetIndex?: Entity | number,
	Position?: Vector,
	AbilityIndex?: Ability | number,
	OrderIssuer?: PlayerOrderIssuer_t,
	UnitIndex?: Entity | number,
	QueueBehavior?: OrderQueueBehavior_t,
	ShowEffects?: boolean;
};

type PrepareOrdersEvent = {
	OrderType: dotaunitorder_t,
	TargetIndex?: Entity,
	Position?: Vector,
	AbilityIndex?: Ability,
	OrderIssuer?: PlayerOrderIssuer_t,
	UnitIndex?: Entity,
	QueueBehavior?: OrderQueueBehavior_t,
	ShowEffects?: boolean;
};

declare class Player {

	readonly PlayerID: number;

	/* ============= GETTERS ============= */

	/* ====== DOTA ====== */
	readonly CanPlayerBuyback: boolean;
	readonly Assists: number;
	readonly ClaimedDenies: number;
	readonly ClaimedMisses: number;
	readonly Deaths: number;
	readonly Denies: number;
	readonly Gold: number;
	readonly GoldPerMin: number;
	readonly Kills: number;
	readonly LastBuybackTime: number;
	readonly LastHitMultikill: number;
	readonly LastHitStreak: number;
	readonly LastHits: number;
	readonly Level: number;
	readonly Misses: number;
	readonly NearbyCreepDeaths: number;
	readonly PerspectivePlayerEntityIndex: number;
	readonly PerspectivePlayerId: number;
	// test
	readonly PlayerColor: number;
	readonly HeroEntity: Entity;
	readonly PlayerName: string;
	readonly PlayerSelectedHero: string;
	readonly QueryUnit: Entity[];
	readonly ReliableGold: number;
	readonly RespawnSeconds: number;
	readonly SelectedEntities: Entity[];
	readonly Streak: number;
	readonly Team: DOTATeam_t;
	readonly TotalEarnedGold: number;
	readonly TotalEarnedXP: number;
	readonly UnreliableGold: number;
	readonly XPPerMin: number;
	readonly HasCustomGameTicketForPlayerID: boolean;
	readonly IsLocalPlayerInPerspectiveCamera: boolean;
	readonly IsSpectator: boolean;
	readonly IsValidPlayerID: boolean;

	/* ====== CUSTOM ====== */
	readonly IsPlayerMuted: boolean;

	/* ============= METHODS ============= */

	/* ====== DOTA ====== */
	BuffClicked(nBuffSerial: Buff | number, bAlert: boolean): void;
	PlayerPortraitClicked(bHoldingCtrl: boolean, bHoldingAlt: boolean): void;

	/* ====== CUSTOM ====== */
	/**
	 * @returns Player Name
	 */
	toString(): void;

	/* ================== Static ================== */

	static readonly PlayerPortraitUnit: Entity;

	/* ====== ORDERS ====== */

	static readonly IsOrdersBlocked: boolean;

	static PrepareOrders(order: PrepareOrders): boolean;
	/**
	 * Only for LocalPlayer
	 */
	static Buyback(queue?: boolean, showEffects?: boolean): boolean;
	/**
	 * Only for LocalPlayer
	 */
	static Glyph(queue?: boolean, showEffects?: boolean): boolean;
	/**
	 * Only for LocalPlayer
	 */
	static CastRiverPaint(position: Vector, queue?: boolean, showEffects?: boolean): boolean;
	/**
	 * Only for LocalPlayer
	 */
	static PreGameAdgustItemAssigment(ItemID: number, queue?: boolean, showEffects?: boolean): boolean;
	/**
	 * Only for LocalPlayer
	 */
	static Scan(position: Vector, queue?: boolean, showEffects?: boolean): boolean;
}

declare class Entity {

	readonly Index: number;

	/* ============= GETTERS ============= */

	/* ====== DOTA ====== */
	readonly CanBeDominated: boolean;
	readonly AbilityCount: number;
	readonly AbilityPoints: number;
	readonly Position: Vector;
	readonly AttackRange: number;
	readonly AttackSpeed: number;
	readonly AttacksPerSecond: number;
	readonly BaseAttackTime: number;
	readonly BaseMagicalResistanceValue: number;
	readonly BaseMoveSpeed: number;
	readonly BonusPhysicalArmor: number;
	readonly ChosenTarget: Entity;
	readonly Class: string;
	readonly CollisionPadding: number;
	readonly CombatClassAttack: number;
	readonly CombatClassDefend: number;
	readonly ContainedItem: Item;
	readonly CurrentVisionRange: number;
	readonly CurrentXP: number;
	readonly DamageBonus: number;
	readonly DamageMax: number;
	readonly DamageMin: number;
	readonly DayTimeVisionRange: number;
	readonly DisplayedUnitName: string;
	readonly EffectiveInvisibilityLevel: number;
	readonly Forward: Vector;
	readonly HasteFactor: number;
	readonly HP: number;
	readonly HealthBarOffset: number;
	readonly HPPercent: number;
	readonly HPRegen: number;
	readonly HullRadius: number;
	readonly IdealSpeed: number;
	readonly IncreasedAttackSpeed: number;
	readonly Level: number;
	readonly MagicalArmorValue: number;
	readonly Mana: number;
	readonly ManaRegen: number;
	readonly MaxHP: number;
	readonly MaxMana: number;
	readonly MoveSpeedModifier: number;
	readonly NeededXPToLevel: number;
	readonly NightTimeVisionRange: number;
	readonly NumBuffs: number;
	readonly NumItemsInInventory: number;
	readonly NumItemsInStash: number;
	readonly PaddedCollisionRadius: number;
	readonly PercentInvisible: number;
	readonly PhysicalArmorValue: number;
	readonly PlayerID: number;
	readonly ProjectileCollisionSize: number;
	readonly Right: Vector;
	readonly RingRadius: number;
	readonly SecondsPerAttack: number;
	readonly SelectionGroup: string;
	readonly SoundSet: string;
	readonly States: modifierstate[];
	readonly TeamNumber: DOTATeam_t;
	readonly TotalDamageTaken: number;
	readonly TotalPurchasedUpgradeGoldCost: number;
	readonly Label: string;
	readonly Name: string;
	readonly Up: Vector;
	readonly HasAttackCapability: boolean;
	readonly HasCastableAbilities: boolean;
	readonly HasFlyMovementCapability: boolean;
	readonly HasFlyingVision: boolean;
	readonly HasGroundMovementCapability: boolean;
	readonly HasMovementCapability: boolean;
	readonly HasScepter: boolean;
	readonly HasUpgradeableAbilities: boolean;
	readonly HasUpgradeableAbilitiesThatArentMaxed: boolean;
	readonly IsAlive: boolean;
	readonly IsAncient: boolean;
	readonly IsAttackImmune: boolean;
	readonly IsBarracks: boolean;
	readonly IsBlind: boolean;
	readonly IsBoss: boolean;
	readonly IsBuilding: boolean;
	readonly IsCommandRestricted: boolean;
	readonly IsConsideredHero: boolean;
	readonly IsControllableByAnyPlayer: boolean;
	readonly IsCourier: boolean;
	readonly IsCreature: boolean;
	readonly IsCreep: boolean;
	readonly IsCreepHero: boolean;
	readonly IsDeniable: boolean;
	readonly IsDisarmed: boolean;
	readonly IsDominated: boolean;
	readonly IsEnemy: boolean;
	readonly IsEvadeDisabled: boolean;
	readonly IsFort: boolean;
	readonly IsFrozen: boolean;
	readonly IsGeneratedByEconItem: boolean;
	readonly IsHallofFame: boolean;
	readonly IsHero: boolean;
	readonly IsHexed: boolean;
	readonly IsIllusion: boolean;
	readonly IsInRangeOfFountain: boolean;
	readonly IsInventoryEnabled: boolean;
	readonly IsInvisible: boolean;
	readonly IsInvulnerable: boolean;
	readonly IsItemPhysical: boolean;
	readonly IsLaneCreep: boolean;
	readonly IsLowAttackPriority: boolean;
	readonly IsMagicImmune: boolean;
	readonly IsMoving: boolean;
	readonly IsMuted: boolean;
	readonly IsNeutralUnitType: boolean;
	readonly IsNightmared: boolean;
	readonly IsOther: boolean;
	readonly IsOutOfGame: boolean;
	readonly IsOwnedByAnyPlayer: boolean;
	readonly IsPhantom: boolean;
	readonly IsRanged: boolean;
	readonly IsRealHero: boolean;
	readonly IsRooted: boolean;
	readonly IsRoshan: boolean;
	readonly IsSelectable: boolean;
	readonly IsShop: boolean;
	readonly IsSilenced: boolean;
	readonly IsSpeciallyDeniable: boolean;
	readonly IsStunned: boolean;
	readonly IsSummoned: boolean;
	readonly IsTower: boolean;
	readonly IsUnselectable: boolean;
	readonly IsValid: boolean;
	readonly IsWard: boolean;
	readonly IsZombie: boolean;
	readonly ManaFraction: number;
	readonly NoHealthBar: boolean;
	readonly NoTeamMoveTo: boolean;
	readonly NoTeamSelect: boolean;
	readonly NoUnitCollision: boolean;
	readonly NotOnMinimap: boolean;
	readonly NotOnMinimapForEnemies: boolean;
	readonly PassivesDisabled: boolean;
	readonly ProvidesVision: boolean;
	readonly UsesHeroAbilityNumbers: boolean;

	/* ====== CUSTOM ====== */
	readonly CreateTime: number;
	readonly LastVisibleTime: number;
	readonly ManaPercent: number;
	readonly NameJSON: string;
	readonly IsControllable: boolean;
	readonly IsIllusionHero: boolean;
	readonly IsVisible: boolean;
	readonly IsDormant: boolean;
	readonly IsChanneling: boolean;
	readonly IsInFadeTime: boolean;
	// test: is illusion meppo and etc. (Player === undefined ?)
	readonly Player: Player;
	readonly Spells: Ability[];
	readonly Items: Item[];
	readonly Buffs: Buff[];
	readonly CastRangeBonus: number;
	readonly SpellAmplification: number;
	readonly WorldToMiniMap: Vector;
	/* === Protected === */
	readonly IsLinkensProtected: boolean;
	readonly IsAeonProtected: boolean;
	readonly IsCounterspellProtected: boolean;
	readonly IsReflectingAbilities: boolean;
	readonly HasAeonDisk: boolean;
	readonly TPScroll: Item;

	/* ============= METHODS ============= */

	/* ====== DOTA ====== */
	CanAcceptTargetToAttack(target: Entity | number): boolean;
	Ability(slot: number): Ability;
	AbilityByName(name): Ability;
	ArmorForDamageType(type: DAMAGE_TYPES): number;
	ArmorReductionForDamageType(type: DAMAGE_TYPES): number;
	Buff(index: number): Buff;
	Item(slot: DOTAScriptInventorySlot_t): Item;
	RangeToUnit(target: Entity | number): number;
	HasItemInInventory(name: string): boolean;
	InState(state: modifierstate): boolean;
	IsControllableByPlayer(player: Player | number): boolean;
	IsEntityInRange(entity: Entity | number, range: number): boolean;
	IsInRangeOfShop(iShopType: number, bSpecific: boolean): boolean;

	/* ====== CUSTOM ====== */

	/* ==== Orders ==== */
	UseSmartAbility(ability: Ability, target?: Vector | Entity, checkToggled?: boolean, queue?: boolean, showEffects?: boolean): boolean;

	MoveTo(position: Vector, queue?: boolean, showEffects?: boolean): boolean;
	MoveToTarget(target: Entity | number, queue?: boolean, showEffects?: boolean): boolean;
	AttackMove(position: Vector, queue?: boolean, showEffects?: boolean): boolean;
	AttackTarget(target: Entity | number, queue?: boolean, showEffects?: boolean): boolean;
	CastPosition(ability: Ability, position: Vector, queue?: boolean, showEffects?: boolean): boolean;
	CastTarget(ability: Ability, target: Entity | number, queue?: boolean, showEffects?: boolean): boolean;
	CastTargetTree(ability: Ability, tree: Entity | number, queue?: boolean, showEffects?: boolean): boolean;
	CastNoTarget(ability: Ability, queue?: boolean, showEffects?: boolean): boolean;
	CastToggle(ability: Ability, queue?: boolean, showEffects?: boolean): boolean;
	HoldPosition(position: Vector, queue?: boolean, showEffects?: boolean): boolean;
	TrainAbility(ability: Ability): boolean;
	DropItem(item: Item, position: Vector, queue?: boolean, showEffects?: boolean): boolean;
	GiveItem(item: Item, target: Entity | number, queue?: boolean, showEffects?: boolean): boolean;
	PickupItem(physicalItem: Entity | number, queue?: boolean, showEffects?: boolean): boolean;
	PickupRune(rune: Entity | number, queue?: boolean, showEffects?: boolean): boolean;
	// check
	PurchaseItem(itemID: number, queue?: boolean, showEffects?: boolean): boolean;
	SellItem(item: Item): boolean;
	// check
	DisassembleItem(item: Item, queue?: boolean): boolean;
	MoveItem(item: Item, slot: DOTAScriptInventorySlot_t): boolean;
	CastToggleAuto(item: Item, queue?: boolean, showEffects?: boolean): boolean;
	OrderStop(queue?: boolean, showEffects?: boolean): boolean;
	UnitTaunt(queue?: boolean, showEffects?: boolean): boolean;
	ItemFromStash(item: Item): boolean;
	CastRune(runeItem: Item | number, queue?: boolean, showEffects?: boolean): boolean;
	PingAbility(ability: Ability): boolean;
	MoveToDirection(position: Vector, queue?: boolean, showEffects?: boolean): boolean;
	Patrol(position: Vector, queue?: boolean, showEffects?: boolean): boolean;
	VectorTargetPosition(ability: Ability, Direction: Vector, queue?: boolean, showEffects?: boolean): boolean;
	CastVectorTargetPosition(ability: Ability, position: Vector | Entity, Direction: Vector, queue?: boolean, showEffects?: boolean): void;
	ItemLock(item: Item, state?: boolean | number): boolean;
	OrderContinue(item: Item, queue?: boolean, showEffects?: boolean): boolean;
	VectorTargetCanceled(position: Vector, queue?: boolean, showEffects?: boolean): boolean;

	/* ==== Geometry ==== */
	/**
	 * 
	 */
	Distance(vec: Vector | Entity): number;
	/**
	 * 
	 */
	Distance2D(vec: Vector | Entity): number;
	/**
	 * 
	 */
	DistanceSquared(vec: Vector | Entity): number;
	/**
	 * 
	 */
	DistanceSquared2D(vec: Vector | Entity): number;
	AngleBetweenFaces(front: Vector): number;
	InFront(distance: number): Vector;
	InFrontFromAngle(angle: number, distance: number): Vector;
	FindRotationAngle(vec: Vector | Entity): number;
	/**
	 * faster (Distance <= range)
	 */
	IsInRange(ent: Vector | Entity, range: number): boolean;
	Closest(ents: Entity[]): Entity;

	/* ==== Items | Spells ==== */
	HasAbility(name: string): boolean;
	ItemByName(name: string, includeBackpack?: boolean): Item;
	HasItem(name: string, includeBackpack?: boolean): boolean;
	GetItems(start: number, end: number): Item[];
	GetFreeSlots(start: number, end: number): DOTAScriptInventorySlot_t[];
	HasFreeSlots(start: number, end: number, howMany?: number): boolean;
	CountItemByOtherPlayer(unit: Entity): number;
	BuffByName(name: string): Buff;
	HasBuff(name: string): boolean;
	/* ==== Other ==== */
	IsEnemyTo(entity: Entity): boolean;
	AttackRangeBonus(entity?: Entity): number;
	IsBlockingAbilities(checkReflecting?: boolean): boolean;
	CanAttack(target?: Entity): boolean;
	/**
	 * @returns Name
	 */
	toString(): string;
}

declare class LocalUnit extends Entity {
	/** 
	 * That field need for for comparison with another Entity
	 */
	readonly Entity: Entity;

	readonly ActiveAbility: Item;
	HasItemByDefinition(item: Item | number): boolean;
	readonly SelectionEntities: Entity[];
	SelectUnit(ent?: Entity | number, addGroup?: boolean): void;
	SelectUnits(ents: Entity[] | number[], addGroup?: boolean): void;

	//DisassembleItem(item: Item, queue?: boolean, showEffects?: boolean): boolean;
	DropItemFromStash(item: Item): boolean;
	ItemAlertAllies(item: Item, queue?: boolean, showEffects?: boolean): boolean;
	MoveItemToStash(item: Item): boolean;
	//SellItem(item: Item, queue?: boolean, showEffects?: boolean): void;
}
declare var Me: LocalUnit;

declare class Ability extends Entity {

	readonly AbilitySlot: number;

	/* ============= GETTERS ============= */

	/* ====== DOTA ====== */

	readonly AbilityReady: number;
	readonly CanAbilityBeUpgraded: AbilityLearnResult_t;
	readonly CanBeExecuted: boolean;
	readonly CanLearn: boolean;
	readonly Radius: number;
	readonly Damage: number;
	readonly DamageType: DAMAGE_TYPES;
	readonly Name: string;
	readonly TargetFlags: DOTA_UNIT_TARGET_FLAGS;
	readonly TargetTeam: DOTA_UNIT_TARGET_TEAM;
	readonly TargetType: DOTA_UNIT_TARGET_TYPE;
	readonly TextureName: string;
	readonly Type: number;
	readonly AssociatedPrimaryAbilities: Ability[];
	readonly AssociatedSecondaryAbilities: Ability[];
	readonly AutoCastState: boolean;
	readonly BackswingTime: number;
	readonly Behavior: number;
	readonly CastPoint: number;
	readonly BaseCastRange: number;
	readonly ChannelStartTime: number;
	readonly ChannelTime: number;
	readonly ChannelledManaCostPerSecond: number;
	readonly Cooldown: number;
	readonly CooldownLength: number;
	readonly CooldownTime: number;
	readonly CooldownTimeRemaining: number;
	readonly CurrentCharges: number;
	readonly Duration: number;
	readonly EffectiveLevel: number;
	readonly HeroLevelRequiredToUpgrade: number;
	readonly HotkeyOverride: string;
	readonly IntrinsicModifierName: string;
	readonly Keybind: string;
	readonly Level: number;
	readonly ManaCost: number;
	readonly MaxLevel: number;
	readonly SharedCooldownName: string;
	readonly ToggleState: boolean;
	readonly UpgradeBlend: number;
	readonly HasScepterUpgradeTooltip: boolean;
	readonly IsActivated: boolean;
	readonly IsActivatedChanging: boolean;
	readonly IsAttributeBonus: boolean;
	readonly IsAutocast: boolean;
	readonly IsCooldownReady: boolean;
	readonly IsCosmetic: boolean;
	readonly IsDisplayedAbility: boolean;
	readonly IsHidden: boolean;
	readonly IsHiddenWhenStolen: boolean;
	readonly IsInAbilityPhase: boolean;
	readonly IsItem: boolean;
	readonly IsMarkedAsDirty: boolean;
	readonly IsMuted: boolean;
	readonly IsOnCastbar: boolean;
	readonly IsOnLearnbar: boolean;
	readonly IsOwnersGoldEnough: boolean;
	readonly IsOwnersGoldEnoughForUpgrade: boolean;
	readonly IsOwnersManaEnough: boolean;
	readonly IsPassive: boolean;
	readonly IsRecipe: boolean;
	readonly IsSharedWithTeammates: boolean;
	readonly IsStealable: boolean;
	readonly IsStolen: boolean;
	readonly IsToggle: boolean;

	/* ====== CUSTOM ====== */
	readonly IsShow: boolean;
	readonly Owner: Entity;
	readonly IsChanneling: boolean;
	readonly CastRange: number;


	/* ============= METHODS ============= */

	/* ====== DOTA ====== */
	AttemptToUpgrade(): boolean;
	CreateDoubleTapCastOrder(): void;
	ExecuteAbility(quickCast: boolean): boolean;
	CustomValueFor(name: string): number;
	LevelSpecialValueFor(name: string, level: number): number;
	SpecialValueFor(name: string): number;
	PingAbility(): boolean;

	/* ====== CUSTOM ====== */
	UseAbility(target?: Vector | Entity, checkToggled?: boolean, queue?: boolean, showEffects?: boolean): void;
	UpgradeAbility(): void;

	HasBehavior(flag: DOTA_ABILITY_BEHAVIOR): boolean;
	HasTargetFlags(flag: DOTA_UNIT_TARGET_FLAGS): boolean;
	HasTargetTeam(flag: DOTA_UNIT_TARGET_TEAM): boolean;
	HasTargetType(flag: DOTA_UNIT_TARGET_TYPE): boolean;
	IsManaEnough(bonusMana?: number): boolean;
	CanBeCasted(bonusMana?: number): boolean;
}

declare class Item extends Ability {

	/* ============= GETTERS ============= */

	/* ====== DOTA ====== */

	readonly AlwaysDisplayCharges: boolean;
	readonly CanBeExecuted: boolean;
	readonly CanBeSoldByLocalPlayer: boolean;
	readonly CanDoubleTapCast: boolean;
	readonly ForceHideCharges: boolean;
	readonly AbilityTextureSF: string;
	readonly AssembledTime: number;
	readonly Cost: number;
	readonly CurrentCharges: number;
	readonly DisplayedCharges: number;
	readonly InitialCharges: number;
	readonly ItemColor: number;
	readonly Purchaser: Entity;
	readonly PurchaseTime: number;
	readonly SecondaryCharges: number;
	readonly Shareability: EShareAbility;
	readonly IsAlertableItem: boolean;
	readonly IsCastOnPickup: boolean;
	readonly IsDisassemblable: boolean;
	readonly IsDroppable: boolean;
	readonly IsInnatelyDisassemblable: boolean;
	readonly IsKillable: boolean;
	readonly IsMuted: boolean;
	readonly IsPermanent: boolean;
	readonly IsPurchasable: boolean;
	readonly IsRecipe: boolean;
	readonly IsRecipeGenerated: boolean;
	readonly IsSellable: boolean;
	readonly IsStackable: boolean;
	readonly ProRatesChargesWhenSelling: boolean;
	readonly RequiresCharges: boolean;
	readonly ShouldDisplayCharges: boolean;
	readonly ShowSecondaryCharges: boolean;

	/* ====== CUSTOM ====== */
	readonly IsBottle: boolean;
	readonly IsEmptyBottle: boolean;
	readonly IsEmptyBottleRune: boolean;
	readonly IsTango: boolean;
	readonly IsDagon: boolean;

	/* ============= METHODS ============= */

	/* ====== CUSTOM ====== */

	CanBeCasted(bonusMana: number): boolean;
}

declare class Buff {
	static GetTrueSightBuff(buffs: Entity): Buff;
	static HasTrueSightBuff(buffs: Entity): boolean;

	readonly Index: number;
	readonly Owner: Entity;

	readonly Ability: Ability;
	readonly Caster: Entity;
	readonly Class: string;
	readonly CreationTime: number;
	readonly DieTime: number;
	readonly Duration: number;
	readonly ElapsedTime: number;
	readonly Name: string;
	readonly Parent: Entity;
	readonly RemainingTime: number;
	readonly StackCount: number;
	readonly Texture: string;
	readonly IsDebuff: boolean;
	readonly IsHidden: boolean;

	toString(): string;
}

declare const TRUESIGHT_MODIFIERS: string[];


interface EntityManager {
	readonly AllEntities: Entity[];

	readonly LocalPlayer: Player;
	readonly LocalHero: Entity;

	Players(team?: DOTATeam_t): Player[];
	Heroes(team?: DOTATeam_t): Entity[];

	EntityByIndex(index: number): Entity;
	BuffByIndex(index: number): Buff;
	GetPlayerByID(playerID: number): Player;
	AllByClass(className: string | string[]): Entity[];
	AllByName(className: string | string[]): Entity[];
	readonly AllRunes: Entity[];
	readonly AllBuildings: Entity[];
}
declare var EntityManager: EntityManager;

type ControlPoints = Array<boolean | number | number[] | Vector | Color>;

declare class Particle {
	static ControlPointsHashCode(ControlPoints: ControlPoints): number;

	readonly Name: string;
	readonly Path: string;
	readonly Entity: Entity;
	readonly Attachment: ParticleAttachment_t;
	readonly ControlPoints: ControlPoints;

	constructor(
		name: string,
		path: string,
		unit: Entity,
		attachment: ParticleAttachment_t,
		...controlPoints: ControlPoints);

	readonly IsValid: boolean;

	Create(): this;
	SetControlPoints(...controlPoints: ControlPoints): this;
	GetControlPointsHashCode(): number;
	Refresh(): this;
	Destroy(): this;

	/**
	 * @returns name file of path
	 */
	toString(): string;
}

interface ParticleManager {
	AddOrUpdate(
		name: string,
		unit: Entity,
		path: string,
		attachment: ParticleAttachment_t,
		...controlPoints: ControlPoints): Particle;

	Destroy(name: string): void;

	/**
	 * Custom circle
	 * @param range Not updatable!
	 */
	DrawCircle(name: string, unit: Entity, range?: number, options?: {
		/**
		 * from 0 to 2
		 */
		Render?: number,
		Attachment?: ParticleAttachment_t,
		Position?: Entity | Vector,
		Color?: Color | [number, number, number],
		Width?: number,
		Alpha?: number;
	}): Particle;
	/**
	 * Dota circle
	 */
	DrawSelectedRing(name: string, unit: Entity, range?: number, position?: Entity | Vector, color?: Color): Particle;
	DrawLine(name: string, unit: Entity, range?: number, options?: {
		Attachment?: ParticleAttachment_t,
		Position?: Entity | Vector,
		Color?: Color | [number, number, number],
		Width?: number,
		Alpha?: number;
	}): Particle;
	DrawRangeLine(name: string, unit: Entity, endPosition: Entity | Vector, red?: boolean, startPosition?: Entity | Vector): Particle;
	DrawLineToTarget(name: string, unit: Entity, target: Entity | Vector, color?: Color): Particle;
	DrawBoundingArea(name: string, unit: Entity, endPosition?: Entity | Vector, startPos?: Entity | Vector, options?: {
		/**
		 * from 0 to 1
		 */
		Render?: number,
		Position?: Entity | Vector,
		Color?: Color | [number, number, number],
		Width?: number,
		Alpha?: number;
	}): Particle;
}
declare var ParticleManager: ParticleManager;