// @ts-check
const {
	existsSync,
	lstat,
	readdir,
	copyFile
} = require("fs-extra");
const path = require("path");

const ignore = [
	"workshop",
	".git",
	".vscode",
	"node_modules"
]

async function findConfig(oldPath) {
	let paths = await readdir(oldPath);

	let promises = paths.map(async url => {
		
		if (ignore.includes(url))
			return;
		
		let fullPath = path.join(oldPath, url);

		const stat = await lstat(fullPath);
		
		if (stat.isDirectory())
			return findConfig(fullPath);
		
		if (url !== "config.examples.json")
			return;
			
		await setConfig(oldPath, fullPath);
	});

	return await Promise.all(promises);
}

async function setConfig(rootPath, fullPath) {
	
	let configPath = path.join(rootPath, "config.json");
	
	if (existsSync(configPath))
		return;
		
	await copyFile(fullPath, configPath);
}

(async () => {
	await findConfig(process.cwd());
	
	console.log("all user configs setted");
})();