// @ts-check
const { promisify } = require("util");
const {
	existsSync,
	readFile,
	writeFile,
	readdir,
	unlink,
	symlink
} = require("fs-extra");
const { join } = require("path");
const prompts = require("prompts");
const winreg = require("winreg");
const vdf = require('simple-vdf');

let CONFIG = {};

const CWD = process.cwd();

const PATH_TO_CONTENT = join("content", "dota_addons");
const PATH_TO_GAME = join("game", "dota_addons");

function CheckDotaPath(checkPath) {
	return (existsSync(join(checkPath, "game", "bin", "win64", "dota2.exe")) 
		|| existsSync(join(checkPath, "game", "bin", "win32", "dota2.exe")))
		&& existsSync(join(checkPath, "game", "dota", "pak01_dir.vpk"));
}

function CheckInstallWorkshop() {
	return existsSync(join(CONFIG.DOTA_PATH, PATH_TO_CONTENT));
}

async function PromptPathDota() {
	const path = await prompts({
		type: 'text',
		name: 'value',
		message: `Enter the path to DotA folder (include 'dota 2 beta')`
	});
	
	if (!CheckDotaPath(path.value)) {
		console.warn("Path to DotA is not correctly!")
		return await PromptPathDota();
	}
	
	SavePathDota(path.value);
}

async function SavePathDota(path) {
	CONFIG.DOTA_PATH = path;

	await writeFile("./config.json", JSON.stringify(CONFIG, null, "\t"));
}

// https://stackoverflow.com/a/34091380
async function AutoFindPathDota() {
	
	let reg = new winreg({
		hive: winreg.HKCU,
		key: "\\Software\\Valve\\Steam"
	});
	
	let get = await promisify(reg.get).bind(reg)
	
	try {
		let item = await get("SteamPath");

		let defaultPath = join(item.value, "steamapps", "common", "dota 2 beta");
		
		if (CheckDotaPath(defaultPath)) {
			SavePathDota(defaultPath);
			return true;
		}
		
		let file = await readFile(join(item.value, "steamapps", "libraryfolders.vdf"));
		
		let root = vdf.parse(file.toString());
		
		var gameFolders = root.LibraryFolders
		
		if (gameFolders === undefined)
			return false;
		
		for (let name in gameFolders) {
			var path = gameFolders[name];
			if (path === "TimeNextStatsReport" || path === "ContentStatsID")
				continue;
			
			let fullPath = join(path, "steamapps", "common", "dota 2 beta");
			
			if (CheckDotaPath(fullPath)) {
				SavePathDota(fullPath);
				return true;
			}
		}
		
		return false;
	}
	catch (e){ 
		return false;
	}
}

(async () => {
	
	const install = await prompts({
		type: 'toggle',
		name: 'value',
		message: 'Install mods in DotA folder?',
		initial: true,
		active: 'yes',
		inactive: 'no'
	});
	
	if (!install.value)
		process.exit();

	try {
		CONFIG = require(CWD + "/config.json");
	}
	catch (e) {}
	
	if (CONFIG.DOTA_PATH === undefined || !CheckDotaPath(CONFIG.DOTA_PATH)) {
		
		if (process.platform === "win32") {
			
			if (await AutoFindPathDota())
				console.log(`Finded path to DOTA: ${CONFIG.DOTA_PATH}`);
			else 
				await PromptPathDota();
		}
		else await PromptPathDota();
	}
	
	if (!CheckInstallWorkshop()) {
		console.warn(`Not installed Workshop Tools`);
		return;
	}
	
	let mods = await readdir(join(CWD, "./mods"));

	let promises = mods.map(async (mod, index) => {
		
		let fullPathContent = join(CONFIG.DOTA_PATH, PATH_TO_CONTENT, mod);
		
		if (existsSync(fullPathContent))
			await unlink(fullPathContent);
		
		let fullPathGame = join(CONFIG.DOTA_PATH, PATH_TO_GAME, mod);

		if (existsSync(fullPathGame))
			await unlink(fullPathGame);

		await symlink(join(CWD, "mods", mod, "workshop", "content"), fullPathContent, 'junction');
		await symlink(join(CWD, "mods", mod, "workshop", "game"), fullPathGame, 'junction');
		
		console.log(`${++index}. ${mod} - installed successfully`);
	});
	
	await Promise.all(promises);
})();