# **Crutches: Panorama**

Crutches project for Panorama of Dota

## How to Workshop

1. Install mods to Workshop folders: `npm run install_mods`
	* Auto installing after: `npm install`
2. Start DotA with Workshop
	* need choose `panorama_js`
3. Open VConsole and write: `dota_launch_custom_game panorama_js dota`