var mineDestroy = Menu.Factory("Auto Using")
	.AddToggle("Mine Destroy");

mineDestroy.AddLogo("techies_land_mines");

mineDestroy.SetToolTip({
	en: "Auto attacking Proximity Mines",
	ru: "Автоматически атакует Proximity Mines"
});

/** @type {Entity[]} */
var mines = [];

function Update() {
	if (Player.IsOrdersBlocked || !Me.IsAlive || Me.IsInFadeTime || Me.IsChanneling)
		return;

	var mine = ARRAY_FIND(mines, function (mine) {
		return mine.IsAlive && mine.IsInRange(Me, Me.AttackRange) && Me.CanAttack(mine)
	})

	if (mine !== undefined)
		Me.AttackTarget(mine);
}

InGameEvents.on("EntitySpawned", function (ent) {
	if (ent.Name !== "npc_dota_techies_land_mine" || !ent.IsEnemy)
		return;

	mines.push(ent);
});

InGameEvents.on("EntityKilled", function (ent) {
	if (ent.Name !== "npc_dota_techies_land_mine")
		return;

	ARRAY_INCLUDE(mines, ent);
});

mineDestroy.on("Activated", function () {
	if (Me === undefined)
		return;

	InGameEvents.on("Tick", Update);
});
mineDestroy.on("Deactivated", function () {
	InGameEvents.removeListener("Tick", Update);
});
InGameEvents.on("AssignedHero", function () {
	if (!mineDestroy.value)
		return;

	InGameEvents.on("Tick", Update);
});