var MaxBlink = Menu.Factory("Utility")
	.AddToggle("Max Dagger")
	.SetToolTip({
		ru: "Использует Blink Dagger на максимальное расстояние (1200) без штрафа (1/5) <br/><br/>!НЕ РАБОТАЕТ С КВИКАСТАМИ!",
		en: "Use blink on max distance (1200). <br/><br/>!NOT WORKED WITH QICKCAST!"
	});

MaxBlink.AddLogo("item_blink");

function MouseCall(arg) {
	if (arg !== 0 || GameUI.GetClickBehaviors() !== CLICK_BEHAVIORS.DOTA_CLICK_BEHAVIOR_CAST)
		return;

	var ability = Me.ActiveAbility;

	if (ability === undefined || ability.Name !== "item_blink")
		return;

	var heroes = Me.Player.SelectedEntities;

	if (heroes.length === 0)
		return;

	var hero = heroes[0];
	var position = Game.CursorToWorld();
	var radius = ability.Radius;

	if (!hero.IsInRange(position, radius)) {
		hero.CastPosition(ability, hero.Position.Extend(position, radius - 1));
		return false;
	}
}

MaxBlink.on("Activated", function () {
	if (Me === undefined)
		return;

	MouseEvents.on("pressed", MouseCall);
});
MaxBlink.on("Deactivated", function () {
	MouseEvents.removeListener("pressed", MouseCall);
});
InGameEvents.on("AssignedHero", function () {
	if (!MaxBlink.value)
		return;

	MouseEvents.on("pressed", MouseCall);
});