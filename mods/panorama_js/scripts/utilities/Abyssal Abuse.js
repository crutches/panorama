
var abuse = Menu.Factory("Utility").AddToggle("Abyssal Abuse");

abuse.AddLogo("item_abyssal_blade");

/** @type {Buff} */
var buffBasher;
var buffDieTime = 0;

var itemsUnlocked = true;

function CheckUnlock() {
	if (!abuse.value || Me === undefined) {
		InGameEvents.removeListener("Tick", CheckUnlock);
		return;
	}

	if (!Me.IsAlive || (buffBasher === undefined && itemsUnlocked))
		return;

	var recipe = Me.ItemByName("item_recipe_abyssal_blade", true);
	var basher = Me.ItemByName("item_basher", true);
	var vanguard = Me.ItemByName("item_vanguard", true);

	if (recipe !== undefined)
		Me.ItemLock(recipe, false);

	if (basher !== undefined)
		Me.ItemLock(basher, false);

	if (vanguard !== undefined)
		Me.ItemLock(vanguard, false);

	itemsUnlocked = recipe === undefined && basher === undefined && vanguard === undefined;

	if (buffBasher !== undefined) {

		if (buffBasher.DieTime === buffDieTime && buffBasher.Owner.IsAlive
			&& buffBasher.Owner.Buffs.indexOf(buffBasher) !== -1)
			return;

		buffBasher = undefined;
	}
}

function Update() {
	if (!Me.IsAlive)
		return;

	if (!ARRAY_SOME(EntityManager.AllEntities, function (ent) {
		if (!ent.IsSelectable || ent.IsInvulnerable || !ent.IsAlive || ent.NoHealthBar)
			return false;

		var buff = ent.BuffByName("modifier_bashed");
		if (buff === undefined)
			return false;

		if (buff.Ability === undefined || buff.Ability.Owner !== Me.Entity || buffBasher === buff)
			return false;

		buffBasher = buff;
		buffDieTime = buff.DieTime;
		return true;
	})) return;

	var blade = Me.ItemByName("item_abyssal_blade");

	if (blade !== undefined && Me.HasFreeSlots(0, 8, 2)) {
		Me.DisassembleItem(blade);
		itemsUnlocked = false;
	}
}

abuse.on("Activated", function () {
	if (Me === undefined)
		return;

	InGameEvents.on("Update", Update);
	InGameEvents.on("Tick", CheckUnlock);
});
abuse.on("Deactivated", function () {
	InGameEvents.removeListener("Update", Update);
	InGameEvents.removeListener("Update", CheckUnlock);
})

InGameEvents.on("AssignedHero", function () {
	if (!abuse.value)
		return;

	InGameEvents.on("Update", Update);
	InGameEvents.on("Tick", CheckUnlock);
});