// @ts-check

var RuneSnatcher = Menu.Factory("Utility")
	.AddTree("Rune Snatcher", true)
	.SetToolTip({
		en: "TODO",
		ru: "Автоматическое поднятие рун"
	});

var pathToRune = "s2r://panorama/images/emoticons/";
var runes = [
	"bountyrune_png",
	"illusion_png",
	"doubledamage_png",
	"haste_png",
	"invisibility_png",
	"arcane_rune_png",
	"regeneration_png"
];
new Thread(5, function () {
	RuneSnatcher.AddLogo(pathToRune + runes[Math.floor(Math.random() * runes.length)] + ".vtex");
}).Start();

var takeRadius = RuneSnatcher.AddSlider({ en: "Pickup radius", ru: "Радиус поднятия" }, 150, 50, 350)
	.SetToolTip({
		en: "Default range is 150, that one don't require rotating unit to pickup rune",
		ru: "По умолчанию 150, при этой дистанции юнит не будет поворачиться для поднятия"
	});

takeRadius.on("Value", DestroyTakeParticle);

{ // Indicators

	var indicators = RuneSnatcher.AddTree({ en: "Indicators", ru: "Индикаторы" }, true)
		.on("Deactivated", DestroyParticles);

	var indicatorTake = Menu.CreateRGB(indicators, { en: "Take rune", ru: "Поднятие руны" }, undefined, true);

	var indicatorKill = indicators.AddToggle({ en: "Destroy rune", ru: "Уничтожение рун" })
		.SetToolTip({ en: "Always red color", ru: "Всегда красный цвет" });

	var Style = indicators.AddTree({ en: "Style", ru: "Стиль" });

	var styleAlpha = Style.AddSlider({ en: "Opacity (alpha)", ru: "Прозрачность" }, 255, 0, 255);

	var styleWidth = Style.AddSlider({ en: "Width", ru: "Ширина" }, 15, 0, 100);
	var styleRender = Style.AddSwitch({ en: "Style", ru: "Стиль" },
		[PARTICLES_RENDER_NORMAL, PARTICLES_RENDER_ROPE, PARTICLES_RENDER_ANIMATION]);

	indicators.on("Deactivated", DestroyParticles);
	indicatorTake.tree.on("Deactivated", DestroyTakeParticle);
	indicatorKill.on("Deactivated", DestroyKillParticle);
	styleRender.on("Value", DestroyParticles);
}

/** @type {Map<Entity, Particle>} */
var takeParticles = new Map();
/** @type {Map<Entity, Particle>} */
var killPartciles = new Map();

function DestroyParticles() {
	DestroyTakeParticle();
	DestroyKillParticle();
}

function DestroyTakeParticle() {
	takeParticles.forEach(function (particle) {
		particle.Destroy();
	})
	takeParticles.clear();
}

function DestroyKillParticle() {
	killPartciles.forEach(function (particle) {
		particle.Destroy();
	})
	killPartciles.clear();
}

function DestroyTakeParticleByEnt(rune) {
	var particle = takeParticles.get(rune);

	if (particle === undefined)
		return;

	particle.Destroy();
	takeParticles.delete(rune);
}

function DestroyKillParticleByEnt(rune) {
	var particle = killPartciles.get(rune);

	if (particle === undefined)
		return;

	particle.Destroy();
	takeParticles.delete(rune);
}

var pickUp = false;

function Update() {
	if (pickUp)
		return;

	ARRAY_SOME(EntityManager.AllRunes, function (rune) {

		if (!rune.IsValid)
			return;

		if (!Me.IsAlive || Me.IsStunned || !Me.IsSelectable || Me.IsChanneling)
			return false;

		var distance = Me.Distance2D(rune);

		var attackRange = Me.AttackRange;

		if (distance >= Math.max(500, attackRange) * 2) {
			DestroyTakeParticleByEnt(rune);
			DestroyKillParticleByEnt(rune);
			return false;
		}

		if (indicators.value) {

			if (indicatorTake.tree.value) {
				takeParticles.set(rune, ParticleManager.DrawCircle("take" + rune.Index, rune, takeRadius.value, {
					Color: indicatorTake.Color,
					Render: styleRender.value,
					Width: styleWidth.value,
					Alpha: styleAlpha.value
				}));
			}

			if (indicatorKill.value) {
				killPartciles.set(rune, ParticleManager.DrawCircle("kill" + rune.Index, rune, attackRange, {
					Color: [255, 0, 0],
					Render: styleRender.value,
					Width: styleWidth.value,
					Alpha: styleAlpha.value
				}));
			}
		}

		if (distance > takeRadius.value || (Me.IsInvulnerable && distance > 100))
			return false;

		Me.PickupRune(rune);
		pickUp = true;
		$.Schedule(1 / 30, function () {
			pickUp = false;
		})
		return true;
	});
}


RuneSnatcher.on("Activated", function () {
	if (Me === undefined)
		return;

	InGameEvents.on("Update", Update);
});
RuneSnatcher.on("Deactivated", function () {
	InGameEvents.removeListener("Update", Update);
	DestroyParticles();
});
InGameEvents.on("EntityKilled", function (entity) {
	DestroyTakeParticleByEnt(entity);
	DestroyKillParticleByEnt(entity);
});
InGameEvents.on("AssignedHero", function () {
	if (!RuneSnatcher.value)
		return;

	InGameEvents.on("Update", Update);
});
Events.on("GameEnded", function () {
	takeParticles.clear();
	killPartciles.clear();
})