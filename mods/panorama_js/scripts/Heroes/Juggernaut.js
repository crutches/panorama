var juggernaut = Menu.Factory("Heroes")
	.AddTree("Juggernaut");

juggernaut.AddLogo("npc_dota_hero_juggernaut");

var autoControlHW = juggernaut.AddToggle({ en: "Ward Control", ru: "Контроль Варда" });
autoControlHW.AddLogo("juggernaut_healing_ward");

autoControlHW.SetToolTip({
	en: "TODO",
	ru: "Автоматачески контролит Healing Ward.<br/><br/>\
		Также работает если вы имеете контроль над любым вардом Healing Ward"
});

/** @type {Entity} */
var ward;

function Update() {
	if (ward === undefined || !ward.IsAlive || !ward.IsControllable || Player.IsOrdersBlocked)
		return;

	ward.MoveTo(Me.InFront(30));
}

InGameEvents.on("EntitySpawned", function (ent) {
	if (ent.Name !== "npc_dota_juggernaut_healing_ward" || ent.IsEnemy)
		return;

	ward = ent;
});

InGameEvents.on("EntityKilled", function (ent) {
	if (ent === ward)
		ward = undefined;
});

autoControlHW.on("Activated", function () {
	if (Me === undefined)
		return;

	InGameEvents.on("Tick", Update);
});
autoControlHW.on("Deactivated", function () {
	InGameEvents.removeListener("Tick", Update);
});
InGameEvents.on("AssignedHero", function () {
	if (!autoControlHW.value)
		return;

	InGameEvents.on("Tick", Update);
});