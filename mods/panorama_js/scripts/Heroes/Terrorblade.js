var terrorblade = Menu.Factory("Heroes")
	.AddTree("Terrorblade");

terrorblade.AddLogo("npc_dota_hero_terrorblade");

var autoSwapSunder = terrorblade.AddTree("Auto Sunder", true);
autoSwapSunder.AddLogo("terrorblade_sunder");

autoSwapSunder.SetToolTip({
	en: "TODO",
	ru: "Автоматачески использует способность Sunder при достижении порога HP.<br/><br/>\
		Также работает на любом подконтрольном герое, который имеет способность Sunder"
});

var enemies = autoSwapSunder.AddTree({ en: "Enemy", ru: "Противники" }, true);
var enemiesMyHP = enemies.AddSlider({ en: "HP Percent", ru: "Процент HP" }, 30, 0, 99)
	.SetToolTip({
		en: "TODO",
		ru: "Процент HP героя при котором будет применена способность на противника"
	});

var allies = autoSwapSunder.AddTree({ en: "Allies", ru: "Союзники" }, true);
var alliesMyHP = allies.AddSlider({ en: "HP Percent", ru: "Процент HP" }, 50, 0, 99)
	.SetToolTip({
		en: "TODO",
		ru: "Процент HP героя при котором будет применена способность на союзника"
	});
var alliesAllieHP = allies.AddSlider({ en: "Ally HP Percent", ru: "Процент HP Союзника" }, 20, 0, 99)

/** @type {Ability} */
var Sunder;

var updateControllables = new Thread(1, function () {

	if (ARRAY_SOME(EntityManager.Heroes(), function (ent) {
		if (!ent.IsControllable)
			return false;

		var sunder = ent.AbilityByName("terrorblade_sunder");

		if (sunder === undefined)
			return false;

		Sunder = sunder;
		return true;
	})) return;

	Sunder = undefined;
});


function CheckValidToHeal(ent) {
	return ent !== Sunder.Owner && ent.IsAlive && ent.IsInRange(Sunder.Owner, Sunder.CastRange);
}

/** @param {Entity[]} heroes */
function FindEnemy(heroes) {

	var ents = ARRAY_FILTER(heroes, function (ent) {
		return CheckValidToHeal(ent) && ent.IsEnemyTo(Sunder.Owner);
	});

	ARRAY_FOREACH(Entities.GetAllHeroEntities(), function (entID) {
		var ent = EntityManager.EntityByIndex(entID);

		if (ARRAY_INCLUDE(ents, ent) && CheckValidToHeal(ent) && ent.IsEnemyTo(Sunder.Owner))
			ents.push(ent);
	})

	if (ents.length > 1)
		ARRAY_SORT(ents, "HPPercent", true);

	return ents[0];
}

/** @param {Entity[]} heroes */
function FindAlly(heroes) {
	var ents = ARRAY_FILTER(heroes, function (ent) {
		return CheckValidToHeal(ent) && !ent.IsEnemyTo(Sunder.Owner)
			&& ent.HPPercent <= alliesAllieHP.value;
	});

	if (ents.length > 1)
		ARRAY_SORT(ents, "HPPercent", true);

	return ents[0];
}

var SunderUp = false;

function Update() {
	if (Sunder === undefined || SunderUp || Player.IsOrdersBlocked)
		return;

	if (!enemies.value && !alliesMyHP.value)
		return;

	if (!Sunder.Owner.IsAlive || !Sunder.CanBeCasted())
		return;

	var heroes = EntityManager.Heroes();

	var ent;
	var HPPercent = Sunder.Owner.HPPercent;

	if (enemies.value && HPPercent <= enemiesMyHP.value)
		ent = FindEnemy(heroes);

	if (ent === undefined && allies.value && HPPercent <= alliesMyHP.value)
		ent = FindAlly(heroes);

	if (ent === undefined)
		return;

	Sunder.UseAbility(ent);
	$.Schedule(Sunder.CastPoint * 2, function () {
		SunderUp = false;
	})
	SunderUp = true;
}

autoSwapSunder.on("Activated", function () {
	if (Me === undefined)
		return;

	updateControllables.Start();
	InGameEvents.on("Tick", Update);
});
autoSwapSunder.on("Deactivated", function () {
	updateControllables.Stop();
	InGameEvents.removeListener("Tick", Update);
});
Events.on("GameEnded", function () {
	Sunder = undefined;
});
InGameEvents.on("AssignedHero", function () {
	if (!autoSwapSunder.value)
		return;

	updateControllables.Start();
	InGameEvents.on("Tick", Update);
});