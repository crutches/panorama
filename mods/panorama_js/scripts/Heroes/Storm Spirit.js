var stormSpirit = Menu.Factory("Heroes")
	.AddTree("Storm Spirit");

stormSpirit.AddLogo("npc_dota_hero_storm_spirit");

var stormSpiritDmgPanel = stormSpirit.AddTree({ en: "Damage Panel", ru: "Панель Урона" }, true);
stormSpiritDmgPanel.AddLogo("storm_spirit_ball_lightning");

stormSpiritDmgPanel.SetToolTip({
	en: "TODO",
	ru: "Панель для отображения затраты маны, дистанции, времени и урона(сырой, без усиления и сопротивления) Ball Lightning<br/><br/>\
		Также работает на любом подконтрольном герое, который имеет способность Ball Lightning"
});

var aroundMouse = stormSpiritDmgPanel.AddToggle({ en: "Under Cursor", ru: "Под Курсором" });

var panelSize = stormSpiritDmgPanel.AddSlider({ en: "Panel Size", ru: "Размер Панели"}, 8, 5, 20);

var panelPos = Menu.CreateXY(stormSpiritDmgPanel, { en: "Panel Position", ru: "Позиция Панели" }, 
	new Vector(18, 87), undefined, new Vector(93, 94), undefined, undefined, true);

/** @type {Ability} */
var lightBall;
/** @type {Panel} */
var panelDmg;

/** @type {LabelPanel} */
var distanceLabel;
/** @type {LabelPanel} */
var damageLabel;
/** @type {LabelPanel} */
var manaLabel;
/** @type {LabelPanel} */
var timeLabel;

var redColor = Color.Red;
var greenColor = Color.Green;

var panelInfo = {
	"mana": {
		english: "Mana",
		russian: "Мана"
	},
	"distance": {
		english: "Distance",
		russian: "Дистаниця"
	},
	"time": {
		english: "Time (sec)",
		russian: "Время (сек)"
	},
	"damage": {
		english: "Damage",
		russian: "Урон"
	}
}

function FindStormSelected() {
	if (!stormSpiritDmgPanel.value)
		return;

	var ent = Player.PlayerPortraitUnit;

	if (ent !== undefined && ent.IsControllable) {
		lightBall = ent.AbilityByName("storm_spirit_ball_lightning");

		if (lightBall !== undefined) {
			CreatePanel();
			return;
		}
	}
	
	if (Me === undefined)
		return;
	
	var lb = Me.AbilityByName("storm_spirit_ball_lightning");

	if (lb !== undefined) {
		lightBall = lb;
		CreatePanel();
		return;
	}
	
	DestroyPanel();
}

function CreatePanel() {
	if (panelDmg !== undefined)
		return;
	
	panelDmg = $.CreatePanel("Panel", Game.InGameHUD, "StormPanel");
	Drawing.VectorToPanelPercent(panelPos.Vector, panelDmg);
	
	panelDmg.BLoadLayoutFromString("\
		<root>\
			<Panel hittest='false' style='flow-children:down;'/>\
		</root>", false, false);
	
	for (var name in panelInfo) {
		
		panelDmg.BCreateChildren("\
			<Panel style='width:100%;margin: 2px 0;flow-children:right;'>\
				<Label id='" + name + "_name' style='color: white;'/>\
				<Label id='" + name + "' text='0' html='true' style='color: white; width:fill-parent-flow(1.0);text-align: right;text-overflow: shrink;'/>\
			</Panel>");
	}
	
	Language();
	ChangeSize();
	
	distanceLabel = panelDmg.FindChildTraverse("distance");
	damageLabel = panelDmg.FindChildTraverse("damage");
	manaLabel = panelDmg.FindChildTraverse("mana");
	timeLabel = panelDmg.FindChildTraverse("time");
}
function Language() {
	if (panelDmg === undefined)
		return;

	for (var name in panelInfo)
		panelDmg.FindChildTraverse(name + "_name").text = panelInfo[name][Crutches.Language] + ":";
}
function ChangeSize() {
	if (panelDmg === undefined)
		return;
		
	panelDmg.style.width = panelSize.value * 25 + "px";
	panelDmg.style.fontSize = panelSize.value * 2 + "px";
	
	ARRAY_FOREACH(panelDmg.Children(), function (child) {
		child.GetChild(0).style.width = panelSize.value * 25 / 1.875 + "px";
	});
}
function DestroyPanel() {
	Panels.DeletePanels(panelDmg);
	panelDmg = undefined;
}

function Update() {
	if (lightBall === undefined)
		return;
		
	var owner = lightBall.Owner;
	var level = lightBall.Level;
	
	if (level < 1)
		return;
	
	var distanceToCursor = owner.Position.Distance2D(Game.CursorToWorld());
	
	var manaTravel = lightBall.SpecialValueFor("ball_lightning_travel_cost_base")
		+ (owner.MaxMana * lightBall.SpecialValueFor("ball_lightning_travel_cost_percent") / 100);
		
	var manaCostDistance =lightBall.ManaCost + Math.floor((distanceToCursor / 100)) * manaTravel;
	
	var moveSpeed = lightBall.LevelSpecialValueFor("ball_lightning_move_speed", level);
	var timeDistance = distanceToCursor / moveSpeed;
	
	var manaRemaing = (owner.Mana - manaCostDistance) + (timeDistance * owner.ManaRegen);
	
	var manaText = TRUNCATED(manaCostDistance, 0) + " (" + TRUNCATED(Math.abs(manaRemaing), 0) + ")";
	manaLabel.text = manaText;
	
	if (aroundMouse.value)
		Drawing.TextNearMouse.CreateOrUpdate(manaText, manaRemaing < 0 ? redColor : greenColor);
	
	var radius = lightBall.SpecialValueFor("ball_lightning_aoe");
		
	if (manaRemaing < 0) {
		distanceLabel.style.color = manaLabel.style.color = "red";
		
		var mana = Math.max(owner.Mana - lightBall.ManaCost, 0);
		var castRange = mana / manaTravel * 100;

		distanceLabel.text = TRUNCATED(castRange, 2);
		damageLabel.text = TRUNCATED(lightBall.Damage * (Math.max(castRange - radius, 0) / 100), 2);
		timeLabel.text = TRUNCATED(castRange / moveSpeed + (mana > 0 ? lightBall.CastPoint : 0), 2);
	}
	else {
		damageLabel.text = TRUNCATED(lightBall.Damage * (Math.max(distanceToCursor - radius, 0) / 100), 2);
		distanceLabel.text = TRUNCATED(distanceToCursor, 2);
		timeLabel.text = TRUNCATED(timeDistance + lightBall.CastPoint, 2);

		distanceLabel.style.color = manaLabel.style.color = "white";
	}
}

panelPos.X.on("Value", function (value) {
	if (panelDmg === undefined)
		return;
	panelDmg.style.x = value + "%";
});
panelPos.Y.on("Value", function (value) {
	if (panelDmg === undefined)
		return;
	panelDmg.style.y = value + "%";
});
aroundMouse.on("Value", function (value) {
	if (!value)
		Drawing.TextNearMouse.Delete();
});

panelSize.on("Value", ChangeSize);
Events.on("Language", Language);

stormSpiritDmgPanel.on("Activated", function () {
	FindStormSelected();
	InGameEvents.on("Tick", Update);
});
stormSpiritDmgPanel.on("Deactivated", function () {
	InGameEvents.removeListener("Tick", Update);
	
	DestroyPanel();
	Drawing.TextNearMouse.Delete();
	lightBall = undefined;
});

Events.on("GameStarted", function () {
	if (!stormSpiritDmgPanel.value)
		return;
	
	FindStormSelected();
	InGameEvents.on("Tick", Update);
});
Events.on("GameEnded", function () {
	InGameEvents.removeListener("Tick", Update);
	
	DestroyPanel();
	Drawing.TextNearMouse.Delete();
	lightBall = undefined;
});

NativeEvents.on("SelectedUnitsUpdate", FindStormSelected);
NativeEvents.on("QueryUnitsUpdate", FindStormSelected);