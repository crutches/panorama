var treantProtector = Menu.Factory("Heroes")
	.AddTree("Treant Protector");

treantProtector.AddLogo("npc_dota_hero_treant");

var autoLivingArmor = treantProtector.AddTree("Auto Living Armor", true);
autoLivingArmor.AddLogo("treant_living_armor");

autoLivingArmor.SetToolTip({
	en: "TODO",
	ru: "Также работает на любом подконтрольном герое, который имеет способность Living Armor"
});

var allies = autoLivingArmor.AddTree({ en: "Allies", ru: "Союзники" }, true);
var alliesPercentHP = allies.AddSlider({ en: "Max HP (%)", ru: "Максимальное HP (%)" }, 50, 1, 99);
var alliesCheckEnemies = allies.AddToggle({ en: "Check Enemies", ru: "Проверка на врагов" })
	.SetToolTip({
		en: "TODO",
		ru: "Проверяет вражеских юнитов рядом c целью для лечения"
	});

var buildings = autoLivingArmor.AddTree({ en: "Buildings", ru: "Строения" }, true);
var buildingsPercentHP = buildings.AddSlider({ en: "Max HP (%)", ru: "Максимальное HP (%)" }, 50, 1, 99);
var buildingsCheckEnemies = buildings.AddToggle({ en: "Check Enemies", ru: "Проверка на врагов" })
	.SetToolTip({
		en: "TODO",
		ru: "Проверяет вражеских юнитов рядом c целью для лечения"
	});

var priority = autoLivingArmor.AddSwitch({ en: "Priority", ru: "Приоритет" }, [allies.name, buildings.name]);

function AddOrRemovePriority() {
	if (allies.value && buildings.value) {
		if (autoLivingArmor.HasControl(priority))
			return;

		autoLivingArmor.AddControl(priority);
	}
	else {
		if (!autoLivingArmor.HasControl(priority))
			return;

		autoLivingArmor.RemoveControl(priority);
	}
}
allies.on("Value", AddOrRemovePriority);
buildings.on("Value", AddOrRemovePriority)
AddOrRemovePriority();

/** @type {Entity[]} */
var allCreeps = [];

/** @type {Ability} */
var LivingArmor;

var updateControllables = new Thread(1, function () {
	if (!ARRAY_SOME(EntityManager.Heroes(), function (ent) {
		if (!ent.IsControllable)
			return false;

		var livingArmor = ent.AbilityByName("treant_living_armor");
		if (livingArmor === undefined)
			return false;

		LivingArmor = livingArmor;

		return true;
	})) return;

	LivingArmor = undefined;
});


function CheckValidToHeal(ent) {
	if (ent === LivingArmor.Owner || ent.IsEnemyTo(LivingArmor.Owner) || !ent.IsAlive)
		return false;

	var buff = ent.BuffByName("modifier_treant_living_armor");

	return buff === undefined || buff.StackCount <= 2 || buff.RemainingTime < (LivingArmor.CastPoint * 2);
}

function CheckEntNearEnt(ent, ent2) {
	return ent2.IsEnemyTo(ent) && ent2.IsAlive
		&& ent2.IsEntityInRange(ent, ent2.AttackRange)
}

function CheckNearUnits(heroes, ent) {
	return ARRAY_SOME(heroes, function (entity) {
		return CheckEntNearEnt(ent, entity);
	}) || ARRAY_SOME(allCreeps, function (creep) {
		return CheckEntNearEnt(ent, creep);
	})
}

function CheckAllies(heroes) {
	var ents = ARRAY_FILTER(heroes, function (ent) {
		if (!CheckValidToHeal(ent)
			|| ent.HPPercent > alliesPercentHP.value)
			return false;

		if (alliesCheckEnemies.value && CheckNearUnits(heroes, ent))
			return false;

		return true
	});

	if (ents.length > 1)
		ARRAY_SORT(ents, "HPPercent");

	return ents[0];
}

function CheckBuildings(heroes) {
	var ents = ARRAY_FIND(EntityManager.AllBuildings, function (ent) {
		if (!CheckValidToHeal(ent)
			|| ent.HPPercent > buildingsPercentHP.value)
			return false;

		if (buildingsCheckEnemies.value && CheckNearUnits(heroes, ent))
			return false;

		return true
	});

	if (ents.length > 1)
		ARRAY_SORT(ents, "HPPercent");

	return ents[0];
}

var livingArmorUp = false;

function Update() {
	if (LivingArmor === undefined || livingArmorUp || Player.IsOrdersBlocked)
		return;

	if (!LivingArmor.Owner.IsAlive || !LivingArmor.CanBeCasted())
		return;

	var heroes = EntityManager.Heroes();

	var ent;

	if (allies.value && buildings.value) {
		if (priority.value === 0) {

			ent = CheckAllies(heroes, LivingArmor);

			if (LivingArmor.Owner.HPPercent < alliesPercentHP.value)
				ent = LivingArmor.Owner;

			if (ent === undefined)
				ent = CheckBuildings(heroes, LivingArmor);
		}
		else {

			ent = CheckBuildings(heroes, LivingArmor);

			if (ent === undefined)
				ent = CheckAllies(heroes, LivingArmor);

			if (LivingArmor.Owner.HPPercent < alliesPercentHP.value)
				ent = LivingArmor.Owner;
		}
	}
	else {

		if (allies.value)
			ent = CheckAllies(heroes, LivingArmor);

		if (LivingArmor.Owner.HPPercent < alliesPercentHP.value)
			ent = LivingArmor.Owner;

		if (ent === undefined && buildings.value)
			ent = CheckBuildings(heroes, LivingArmor);
	}

	if (ent === undefined)
		return;

	LivingArmor.UseAbility(ent);
	$.Schedule(LivingArmor.CastPoint * 2, function () {
		livingArmorUp = false;
	})
	return livingArmorUp = true;
}

autoLivingArmor.on("Activated", function () {
	if (Me === undefined)
		return;

	updateControllables.Start();
	InGameEvents.on("Tick", Update);
});
autoLivingArmor.on("Deactivated", function () {
	updateControllables.Stop();
	InGameEvents.removeListener("Tick", Update);
});
Events.on("GameEnded", function () {
	LivingArmor = undefined;
});
InGameEvents.on("AssignedHero", function () {
	if (!autoLivingArmor.value)
		return;

	updateControllables.Start();
	InGameEvents.on("Tick", Update);
});
InGameEvents.on("EntitySpawned", function (entity) {
	if (entity.IsCreep) {
		allCreeps.push(entity);
		return;
	}
});
InGameEvents.on("EntityKilled", function (entity) {
	if (entity.IsCreep)
		ARRAY_REMOVE(allCreeps, entity);
});