var dazzle = Menu.Factory("Heroes")
	.AddTree("Dazzle");

dazzle.AddLogo("npc_dota_hero_dazzle");

var autoGrave = dazzle.AddTree("Auto Grave", true);
autoGrave.AddLogo("dazzle_shallow_grave");

autoGrave.SetToolTip({
	en: "TODO",
	ru: "Также работает на любом подконтрольном герое, который имеет способность Shallow Grave"
});

var percentHP = autoGrave.AddSlider({ en: "Max HP (%)", ru: "Максимальное HP (%)" }, 10, 1, 80);

/** @type {Ability} */
var Grave;

var updateControllables = new Thread(1, function () {
	if (!ARRAY_SOME(EntityManager.Heroes(), function (ent) {
		if (!ent.IsControllable)
			return false;

		var grave = ent.AbilityByName("dazzle_shallow_grave");
		if (grave === undefined)
			return false;

		Grave = grave;
		return true;
	})) return;

	Grave = undefined;
});

var graveUp = false;

function Update() {
	if (Grave === undefined || graveUp || Player.IsOrdersBlocked)
		return;

	if (!Grave.Owner.IsAlive || !Grave.CanBeCasted())
		return;

	var ents = ARRAY_FILTER(EntityManager.Heroes(), function (ent) {
		if (ent === Grave.Owner || ent.IsEnemy || !ent.IsAlive
			|| !ent.IsInRange(Grave.Owner, Grave.CastRange)
			|| ent.HPPercent > percentHP.value)
			return false;

		var buff = ent.BuffByName("modifier_dazzle_shallow_grave");

		return buff === undefined || buff.RemainingTime < (Grave.CastPoint * 2);
	});

	if (ents.length === 0)
		return;

	if (hpPriority.value && ents.length > 1)
		ARRAY_SORT(ents, "HPPercent");

	Grave.UseAbility(ents[0]);
	$.Schedule(Grave.CastPoint * 2, function () {
		graveUp = false;
	})
	return graveUp = true;
}

autoGrave.on("Activated", function () {
	if (Me === undefined)
		return;

	updateControllables.Start();
	InGameEvents.on("Tick", Update);
});
autoGrave.on("Deactivated", function () {
	updateControllables.Stop();
	InGameEvents.removeListener("Tick", Update);
});
Events.on("GameEnded", function () {
	Grave = undefined;
});
InGameEvents.on("AssignedHero", function () {
	if (!autoGrave.value)
		return;

	updateControllables.Start();
	InGameEvents.on("Tick", Update);
});