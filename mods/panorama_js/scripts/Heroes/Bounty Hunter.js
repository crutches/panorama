var bountyHunter = Menu.Factory("Heroes")
	.AddTree("Bounty Hunter");

bountyHunter.AddLogo("npc_dota_hero_bounty_hunter");

var autoTrack = bountyHunter.AddTree("Auto Track", true);
autoTrack.AddLogo("bounty_hunter_track");

autoTrack.SetToolTip({
	en: "TODO",
	ru: "Также работает на любом подконтрольном герое, который имеет способность Track"
})

var reflection = autoTrack.AddToggle({ en: "Check reflection", ru: "Проверка на отражение" })
	.SetToolTip({
		en: "Check for reflection items / skills",
		ru: "Проверять на отражающие предметы / способности"
	});

var hpPriority = autoTrack.AddToggle({ en: "HP Priority", ru: "Приоритет по HP" });

/** @type {Ability} */
var Track;

var updateControllables = new Thread(1, function () {

	if (!ARRAY_SOME(EntityManager.Heroes(), function (ent) {
		if (!ent.IsControllable)
			return false;

		var track = ent.AbilityByName("bounty_hunter_track");

		if (track === undefined)
			return false;

		Track = track;
		return true;
	})) return;

	Track = undefined;
});

var trackUp = false;

function Update() {
	if (Track === undefined || trackUp || Player.IsOrdersBlocked)
		return;

	if (!Track.Owner.IsAlive || !Track.CanBeCasted())
		return;

	var ents = ARRAY_FILTER(EntityManager.Heroes(), function (ent) {
		return (ent !== Track.Owner && ent.IsEnemy && ent.IsAlive
			&& !ent.IsMagicImmune && ent.IsInRange(Track.Owner, Track.CastRange)
			&& !ent.HasBuff("modifier_bounty_hunter_track")
			&& !ent.IsLinkensProtected
			&& ((reflection.value && !ent.IsReflectingAbilities) || !reflection.value))
	});

	if (ents.length === 0)
		return;

	if (hpPriority.value && ents.length > 1)
		ARRAY_SORT(ents, "HPPercent");

	Track.UseAbility(ents[0]);
	$.Schedule(Track.CastPoint * 2, function () {
		trackUp = false;
	})
	trackUp = true;
}

autoTrack.on("Activated", function () {
	if (Me === undefined)
		return;

	updateControllables.Start();
	InGameEvents.on("Tick", Update);
});
autoTrack.on("Deactivated", function () {
	updateControllables.Stop();
	InGameEvents.removeListener("Tick", Update);
});
Events.on("GameEnded", function () {
	Track = undefined;
});
InGameEvents.on("AssignedHero", function () {
	if (!autoTrack.value)
		return;

	updateControllables.Start();
	InGameEvents.on("Tick", Update);
});