// @ts-check
var RangeRadius = Menu.Factory("Visual")
	.AddTree("Range Radius", true)
	.SetToolTip({
		en: "TODO",
		ru: "Радиус способностей, строений, спавн боксов или собственные радиусы"
	});


{ // Towers

	var Buildings = RangeRadius.AddTree({ en: "Buildings", ru: "Строения" }, true)
		.on("Activated", BuildingsDraw)
		.on("Deactivated", BuildingsDestroy);

	var Shrine = Buildings.AddToggle({ en: "Shrines", ru: "Святыни (Shrines)" })
		.on("Value", BuildingsDestroyCreate);

	var BuildingsEnemy = Buildings.AddToggle({ en: "Only Enemy", ru: "Только вражеские" })
		.on("Value", BuildingsDestroyCreate);

	var BuildingsStyle = Menu.CreateParticle(Buildings, { en: "Style", ru: "Стиль" });

	MenuStyleUpdater(BuildingsStyle, BuildingsUpdate, BuildingsDestroyCreate);
}

{ // Spawn boxes

	var spawnBox = Menu.CreateParticle(RangeRadius, "Spawn box", undefined,
		[PARTICLES_RENDER_NORMAL, PARTICLES_RENDER_ROPE], undefined, true);

	spawnBox.tree.on("Activated", SpawnBoxDraw);
	spawnBox.tree.on("Deactivated", SpawnBoxDestroy);

	MenuStyleUpdater(spawnBox, SpawnBoxUpdate, SpawnBoxDestroyCreate);
}

{ // Custom

	var Custom = RangeRadius.AddTree({ en: "Custom", ru: "Кастомные" });

	var CustomCount = Custom.AddSlider({ en: "Amount", ru: "Количество" }, 3, 1, 5)
		.on("Value", ChangeCustomRadiuses);
}

/* ========== global ========== */

function MenuStyleUpdater(style, updateCallback, restartCallback) {
	style.R.on("Value", updateCallback);
	style.G.on("Value", updateCallback);
	style.B.on("Value", updateCallback);
	style.A.on("Value", updateCallback);
	style.Width.on("Value", updateCallback);
	style.Style.on("Value", restartCallback);
}

function DestroyParticles(array) {
	ARRAY_FOREACH(array, function (particle) {
		particle.Destroy();
	});

	ARRAY_CLEAN(array);
}

/* ========== Buildings ========== */

var radiusShrine = 0;

/** @type {Particle[]} */
var buildingsParticles = [];

function BuildingsDraw() {

	BuildingsDestroy();

	if (!RangeRadius.value || !Buildings.value)
		return;

	var towers = EntityManager.AllByClass("npc_dota_tower");

	if (Shrine.value) {
		var shrines = EntityManager.AllByClass("npc_dota_healer");

		if (radiusShrine === 0) {
			ARRAY_SOME(shrines, function (shrine) {
				var ability = shrine.AbilityByName("filler_ability");

				if (ability !== undefined)
					radiusShrine = ability.SpecialValueFor("radius");

				return radiusShrine !== 0;
			});
		}

		towers = towers.concat(shrines);
	}

	ARRAY_FOREACH(towers, function (tower) {
		BuildingsAddParticle(tower,
			tower.Class === "npc_dota_healer" ? radiusShrine : tower.AttackRangeBonus() + 25);
	});
}

function BuildingsAddParticle(unit, radius) {

	if (BuildingsEnemy.value && !unit.IsEnemy)
		return;

	var particle = ParticleManager.DrawCircle("RangeRadius_Buildings" + unit.Index,
		unit, radius,
		{
			Render: BuildingsStyle.Style.value,
			Color: BuildingsStyle.Color,
			Width: BuildingsStyle.Width.value,
			Alpha: BuildingsStyle.A.value
		});

	buildingsParticles.push(particle);
}

function BuildingsUpdate() {

	var widthParticle = BuildingsStyle.tree.GetControl("Width").value;

	ARRAY_FOREACH(buildingsParticles, function (particle) {
		particle.SetControlPoints(
			2, BuildingsStyle.Color,	// Color
			3, widthParticle,			// Width
			4, BuildingsStyle.A.value	// Alpha
		);
	});
}

function BuildingsDestroy(onlyArray) {

	if (onlyArray === true)
		return ARRAY_CLEAN(buildingsParticles);

	DestroyParticles(buildingsParticles);
}

function BuildingsDestroyCreate() {
	BuildingsDestroy();
	BuildingsDraw();
}

/* ========== Spawn Box ========== */

var spawnBoxParticles = [];

function SpawnBoxDraw() {
	SpawnBoxDestroy();

	if (!RangeRadius.value || !spawnBox.tree.value)
		return;

	if (Game.GetMapInfo().map_name.indexOf("dota") === -1)
		return;

	var styleRender = spawnBox.tree.GetControl("Style").value;
	var widthParticle = spawnBox.tree.GetControl("Width").value;

	ARRAY_FOREACH(Game.NeutralSpots, function (spot, index) {
		/*
		  	0			1
			v1----------B	-------
			|			|
			|    		|
			|			|	/ count
			|			|
			|			|
			v3----------v2	-------
			2			3
		
		*/

		var v1 = spot[0]						// 0 + 1
		var v2 = spot[1]						// 2 + 3

		var v3 = new Vector(spot[0].x, spot[1].y); 	// 0 + 3
		var v4 = new Vector(spot[1].x, spot[0].y); 	// 2 + 1 

		// top - left->right
		SpawnBoxAddParticle("top" + index, v1, v4, widthParticle, styleRender);
		// left - top->bot
		SpawnBoxAddParticle("left" + index, v1, v3, widthParticle, styleRender);
		// bot - right->left
		SpawnBoxAddParticle("bot" + index, v2, v3, widthParticle, styleRender);
		// right - bot->top
		SpawnBoxAddParticle("right" + index, v2, v4, widthParticle, styleRender);
	});
}

function SpawnBoxUpdate() {

	var widthParticle = spawnBox.tree.GetControl("Width").value;

	ARRAY_FOREACH(spawnBoxParticles, function (particle) {
		particle.SetControlPoints(
			2, spawnBox.Color,		// Color
			3, widthParticle,		// Width
			4, spawnBox.A.value		// Alpha
		);
	});
}

function SpawnBoxAddParticle(name, startPos, endPos, width, style) {

	var particle = ParticleManager.DrawBoundingArea("RangeRadiusSpawnBox" + name,
		EntityManager.EntityByIndex(1), endPos, startPos,
		{
			Render: style,
			Color: spawnBox.Color,
			Width: width,
			Alpha: spawnBox.A.value,
			Position: startPos
		});

	spawnBoxParticles.push(particle);
}

function SpawnBoxDestroy(onlyArray) {

	if (onlyArray === true)
		return ARRAY_CLEAN(spawnBoxParticles);

	DestroyParticles(spawnBoxParticles);
}

function SpawnBoxDestroyCreate() {
	SpawnBoxDestroy();
	SpawnBoxDraw();
}

/* ========== CUSTOM ========== */

/** @type {RGBATree[]} */
var customRadiusSliders = [];
/** @type {Map<MenuTree, Particle>} */
var customRadiusParticles = new Map();

function ChangeCustomRadiuses(value) {

	var nowCount = customRadiusSliders.length;

	if (nowCount < value) {
		for (var i = nowCount; i < value; i++)
			customRadiusSliders.push(AddCustomRadius(i));
	}
	else {
		for (var i = nowCount - 1; i > value - 1; i--) {
			RemoveCustomSlider(i);
			customRadiusSliders.splice(i, 1);
		}
	}
}

function AddCustomRadius(index) {

	var Style = Menu.CreateParticle(Custom,
		{ en: "Radius " + (index + 1), ru: "Радиус " + (index + 1) }, undefined, undefined, undefined, true);

	Style.tree.on("Value", function (value) {
		value ? CreateCustomRadius(Style) : RemoveCustomRadius(Style.tree);
	});

	MenuStyleUpdater(Style, function () {
		var particle = customRadiusParticles.get(Style.tree);
		if (particle === undefined)
			return;

		particle.SetControlPoints(
			2, Style.Color,			// Color
			3, Style.Width.value,	// Width
			4, Style.A.value		// Alpha
		);
	}, RefreshParticle);

	var range = Style.tree.AddSlider({ en: "Range", ru: "Радиус" }, 1200 + (index * 50), 50, 5000)
		.on("Value", RefreshParticle);

	range.Index = 0;

	function RefreshParticle() {
		RemoveCustomRadius(Style.tree);
		CreateCustomRadius(Style);
	}

	return Style;
}

function RemoveCustomSlider(index) {
	var tree = customRadiusSliders[index].tree;

	RemoveCustomRadius(tree);

	tree.Remove();
}

ChangeCustomRadiuses(CustomCount.value);

function CreateCustomRadius(tree) {
	if (Me === undefined)
		return;

	var menuTree = tree.tree;

	if (!menuTree.value)
		return;

	var particle = ParticleManager.DrawCircle(menuTree.name.en + Me.Index,
		Me, menuTree.GetControl("Range").value,
		{
			Render: tree.Style.value,
			Color: tree.Color,
			Width: tree.Width.value,
			Alpha: tree.A.value
		});

	customRadiusParticles.set(menuTree, particle);
}

function RemoveCustomRadius(tree) {
	var particle = customRadiusParticles.get(tree);
	if (particle !== undefined) {
		particle.Destroy();
		customRadiusParticles.delete(tree);
	}
}

function CustomRadiusDraw() {
	ARRAY_FOREACH(customRadiusSliders, function (rgbaTree) {
		CreateCustomRadius(rgbaTree);
	});
}

function CustomRadiusDestroy(onlyArray) {

	if (onlyArray === true)
		return customRadiusParticles.clear();

	customRadiusParticles.forEach(function (particle) {
		particle.Destroy();
	});
	customRadiusParticles.clear();
}


/* ========== MAIN ========== */

function Activated() {
	BuildingsDraw();
	SpawnBoxDraw();
	CustomRadiusDraw();
}

function Deactivated(onlyArray) {
	BuildingsDestroy(onlyArray);
	SpawnBoxDestroy(onlyArray);
	CustomRadiusDestroy(onlyArray);
}

RangeRadius.on("Activated", Activated)
RangeRadius.on("Deactivated", Deactivated);

Events.on("GameStarted", Activated);
InGameEvents.on("AssignedHero", function () {
	if (!RangeRadius.value)
		return;

	CustomRadiusDraw();
})
Events.on("GameEnded", function () {
	Deactivated(true);
});