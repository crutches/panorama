var AbilityAlert = Menu.Factory("Visual")
	.AddTree("Ability Alert", true)
	.SetToolTip({
		en: "Alert of using hidden Spells",
		ru: "Оповещение о использовании скрытых способностей"
	});

var FindThinkers = {
	"modifier_invoker_sun_strike": {
		Skill: "invoker_sun_strike",
		SkillNormal: "Invoker: Sun Strike",
		Sound: "invoker/sunstrike_charge",//"invoker_invo_ability_sunstrike_01",
		Particle: [
			{
				Name: "Default",
				Particle: "particles/units/heroes/hero_invoker/invoker_sun_strike_team.vpcf",
			},
			{
				Name: "Magus Apex",
				Particle: "particles/econ/items/invoker/invoker_apex/invoker_sun_strike_team_immortal1.vpcf",
			},
		]
	},
	"modifier_invoker_sun_strike_cataclysm": {
		Skill: "invoker_sun_strike"
	},
	"modifier_invoker_sun_strike_cataclysm_thinker": {
		Skill: "invoker_sun_strike"
	},
	"modifier_kunkka_torrent_thinker": {
		Skill: "kunkka_torrent",
		SkillNormal: "Kunkka: Torrent",
		Sound: "kunkka/ability_pre_geyser",// "Ability.pre.Torrent",
		Particle: [
			{
				Name: "Default",
				Particle: "particles/units/heroes/hero_kunkka/kunkka_spell_torrent_bubbles.vpcf",
			},
			{
				Name: "Divine Anchor",
				Particle: "particles/econ/items/kunkka/divine_anchor/hero_kunkka_dafx_skills/kunkka_spell_torrent_bubbles_fxset.vpcf",
			},
		]
	},
	"modifier_lina_light_strike_array": {
		Skill: "lina_light_strike_array",
		SkillNormal: "Lina: Light Strike Array",
		Sound: "lina/prelightstrikearray",//"Ability.PreLightStrikeArray",
		Particle: [
			{
				Name: "Default",
				Particle: "particles/units/heroes/hero_lina/lina_spell_light_strike_array_ray_team.vpcf",
			},
			{
				Name: "TI 7 Style",
				Particle: "particles/econ/items/lina/lina_ti7/light_strike_array_pre_ti7.vpcf",
			},
		]
	},
	"modifier_leshrac_split_earth_thinker": {
		Skill: "leshrac_split_earth",
		SkillNormal: "Leshrac: Split Earth",
		Sound: "leshrac/split_earth",
		Particle: [
			{
				Name: "Default",
				Particle: "particles/units/heroes/hero_leshrac/leshrac_split_projected.vpcf",
			},
			{
				Name: "Tormented Staff",
				Particle: "particles/econ/items/leshrac/leshrac_tormented_staff/leshrac_split_projected_tormented.vpcf",
			},
		]
	},
	"modifier_wisp_relocate_thinker": {
		Skill: "wisp_relocate",
		SkillNormal: "IO: Relocate",
		Sound: "wisp/relocate",
		Special: "cast_delay"
	},
	"modifier_dream_coil_thinker": {
		Skill: "puck_dream_coil",
		SkillNormal: "Puck: Dream Coil",
		Sound: "puck/dream_coil",
	},
	"modifier_monkey_king_spring_thinker": {
		Skill: "monkey_king_primal_spring",
		SkillNormal: "Monkey King: Primal Spring",
		Particle: [
			{
				Name: "Default",
				Particle: "particles/units/heroes/hero_monkey_king/monkey_king_spring_cast.vpcf",
			},
			{
				Name: "Arcana: Water",
				Particle: "particles/econ/items/monkey_king/arcana/water/monkey_king_spring_cast_arcana_water.vpcf",
			},
			{
				Name: "Arcana: Death",
				Particle: "particles/econ/items/monkey_king/arcana/death/monkey_king_spring_cast_arcana_death.vpcf",
			},
			{
				Name: "Arcana: Fire",
				Particle: "particles/econ/items/monkey_king/arcana/fire/monkey_king_spring_cast_arcana_fire.vpcf",
			}
		]
	},
	"modifier_enigma_black_hole_thinker": {
		Skill: "enigma_black_hole",
		SkillNormal: "Enigma: Black Hole",
	},
	"modifier_alchemist_acid_spray_thinker": {
		Skill: "alchemist_acid_spray",
		SkillNormal: "Alchemist: Acid Spray",
	},
	"modifier_arc_warden_spark_wraith_thinker": {
		Skill: "arc_warden_spark_wraith",
		SkillNormal: "Arc Warden: Spark Wraith",
	},
	"modifier_abyssal_underlord_firestorm_thinker": {
		Skill: "abyssal_underlord_firestorm",
		SkillNormal: "Underlord: Firestorm",
	},
	"modifier_abyssal_underlord_pit_of_malice_thinker": {
		Skill: "abyssal_underlord_pit_of_malice",
		SkillNormal: "Underlord: Pit of Malice",
	},
	"modifier_rattletrap_cog_thinker": {
		Skill: "rattletrap_power_cogs",
		SkillNormal: "Clockwerk: Power Cogs",
	}
}

var FindBuffs = {
	"modifier_spirit_breaker_charge_of_darkness_vision": {
		Skill: "spirit_breaker_charge_of_darkness",
		SkillNormal: "Spirit Breaker: Charge of Darkness",
		Sound: "spirit_breaker/charge_cast",
		Particle: "hero_spirit_breaker/spirit_breaker_charge_target_mark.vpcf"
	},
	"modifier_tusk_snowball_visible": {
		Skill: "tusk_snowball",
		SkillNormal: "Tusk: Snowball",
		Sound: "tusk/snowball_cast",
		Particle: "hero_tusk/tusk_snowball_target.vpcf"
	},
	"modifier_sniper_assassinate": {
		Skill: "sniper_assassinate",
		SkillNormal: "Sniper: Assasinate",
		Sound: "sniper/sniper_assassinate_load",
		Particle: "hero_sniper/sniper_crosshair.vpcf"
	},
	"modifier_bounty_hunter_track": {
		Skill: "bounty_hunter_track",
		SkillNormal: "Bounty Hunter: Track",
		Sound: "bounty_hunter/track",
		Particle: "hero_bounty_hunter/bounty_hunter_track_shield_mark.vpcf"
	},
	"modifier_slark_dark_pact": {
		Skill: "slark_dark_pact",
		SkillNormal: "Slark: Dark Pact",
		Sound: "slark/dark_pact_precast",
		Particle: "hero_slark/slark_dark_pact_start.vpcf"
	},
	"modifier_life_stealer_infest_effect": {
		Skill: "life_stealer_infest",
		SkillNormal: "Life Stealer: Infest",
		Sound: "lifestealer/infest",
		Particle: "hero_life_stealer/life_stealer_infested_unit.vpcf"
	},
	"modifier_life_stealer_assimilate_effect": {
		Skill: "life_stealer_infest",
		SkillNormal: "Life Stealer: Infest",
		Sound: "lifestealer/infest",
		Particle: "hero_life_stealer/life_stealer_infested_unit.vpcf"
	},
	"modifier_oracle_fortunes_end_channel_target": {
		Skill: "oracle_fortunes_end",
		SkillNormal: "Oracle: Fortune's End",
		Sound: "oracle/fortune_channel",
		Particle: "hero_oracle/oracle_fortune_channel.vpcf"//Particle: "oracle_fortune_cast.vpcf"
	}
}

var FindGround = {
	"npc_dota_templar_assassin_psionic_trap": {
		Skill: "templar_assassin_psionic_trap",
		SkillNormal: "Templar Assassin: Psionic Trap",
		Particle: "particles/units/heroes/hero_templar_assassin/templar_assassin_trap.vpcf"
	},
	"npc_dota_treant_eyes": {
		Skill: "treant_eyes_in_the_forest",
		SkillNormal: "Treant Protector: Eyes in the Forest",
		Particle: "particles/units/heroes/hero_treant/treant_eyesintheforest.vpcf"
	},
	"npc_dota_techies_remote_mine": {
		Skill: "techies_remote_mines",
		SkillNormal: "Techies: Remote Mines",
	},
	"npc_dota_techies_land_mine": {
		Skill: "techies_land_mines",
		SkillNormal: "Techies: Proximity Mines",
	},
	"npc_dota_techies_stasis_trap": {
		Skill: "techies_stasis_trap",
		SkillNormal: "Techies: Statis Trap",
	}
}

var spells = AbilityAlert.AddTree({ en: "Skills", ru: "Способности" });

/** @type {Map<string, MenuTree>} */
var ThinkersSkillToMenu = new Map();

/** @type {Map<string, ParticleTree>} */
var ThinkersToParticleMenu = new Map();

/** @type {Map<string, RGBATree>} */
var ThinkersToRGBATree = new Map();

for (var name in FindThinkers) {
	var buff = FindThinkers[name];

	if (ThinkersSkillToMenu.get(buff.Skill) !== undefined)
		continue;

	var nameOfSkill = buff.SkillNormal.split(": ")[1];

	var menuSkill = spells.AddTree(nameOfSkill, true, true)
		.SetToolTip(buff.SkillNormal)
		.AddLogo(buff.Skill);

	var particleMenu = CreateMenuParticles(menuSkill);

	if (buff.Particle !== undefined) {
		if (Array.isArray(buff.Particle)) {

			var origPrtcl = menuSkill.AddTree({ en: "Original Particle", ru: "Ориг. партикль" }, true, true);

			origPrtcl.AddSwitch({ en: "Style", ru: "Стиль" }, buff.Particle.map(function (prtcl) { return prtcl.Name; }));
		}
		else {
			var origPrtcl = menuSkill.AddToggle({ en: "Original Particle", ru: "Ориг. партикль" }, true)
		}

		origPrtcl.SetToolTip({
			en: "TODO",
			ru: "Отображение оригинального партикля"
		});
	}

	CreateMenuThinkers(menuSkill, buff.Sound !== undefined);

	var colorMenu = CreateMenuIconOnGround(menuSkill);

	ThinkersSkillToMenu.set(buff.Skill, menuSkill);
	ThinkersToParticleMenu.set(buff.Skill, particleMenu);
	ThinkersToRGBATree.set(buff.Skill, colorMenu);
}

/* ============================ */

var buffs = AbilityAlert.AddTree({ en: "Buffs", ru: "Модификаторы" });

/** @type {Map<string, MenuTree>} */
var BuffsSkillToMenu = new Map();
var allBuffsToFind = Object.keys(FindBuffs);

/** @type {MenuSwitch} */
var lifeStealerCheck;
/** @type {MenuToggle} */
var breakerChargeMiniMap;

for (var name in FindBuffs) {

	var buff = FindBuffs[name];

	if (BuffsSkillToMenu.get(buff.Skill) !== undefined)
		continue;

	var nameOfSkill = buff.SkillNormal.split(": ")[1];

	var menuSkill = buffs.AddTree(nameOfSkill, true, true)
		.SetToolTip(buff.SkillNormal)
		.AddLogo(buff.Skill);

	CreateMenuBuffs(menuSkill, buff);

	BuffsSkillToMenu.set(buff.Skill, menuSkill);
}

/* ============================ */

var ground = AbilityAlert.AddTree({ en: "Ground", ru: "На земле" });

/** @type {Map<string, MenuTree>} */
var GroundEntToMenu = new Map();

/** @type {Map<string, ParticleTree>} */
var GroundEntToParticleMenu = new Map();

/** @type {Map<string, RGBATree>} */
var GroundEntToRGBATree = new Map();

for (var name in FindGround) {
	var ent = FindGround[name];

	if (GroundEntToMenu.get(name) !== undefined)
		continue;

	var nameOfSkill = ent.SkillNormal.split(": ")[1];

	var menuSkill = ground.AddTree(nameOfSkill, true, true)
		.SetToolTip(ent.SkillNormal)
		.AddLogo(ent.Skill);

	if (ent.SkillNormal.indexOf("Techies") !== -1) {
		menuSkill.SetToolTip({
			en: ent.SkillNormal + "<br/><br/>TODO",
			ru: ent.SkillNormal + "<br/><br/>ВАЖНО!<br/><br/>Показывает только если мина была установлена не в тумане войны!"
		})
	}

	var particleMenu = CreateMenuParticles(menuSkill);

	if (ent.Particle !== undefined) {
		menuSkill.AddToggle({ en: "Original Particle", ru: "Ориг. партикль" }, true)
			.SetToolTip({
				en: "TODO",
				ru: "Отображение оригинального партикля"
			});
	}

	var colorMenu = CreateMenuIconOnGround(menuSkill);

	GroundEntToMenu.set(name, menuSkill);
	GroundEntToParticleMenu.set(name, particleMenu);
	GroundEntToRGBATree.set(name, colorMenu);
}

/* ============================ */

var Scan = AbilityAlert.AddTree({ en: "Enemy Scan", ru: "Вражеский Радар" }, true)
	.AddLogo("s2r://panorama/images/hud/reborn/icon_scan_on_psd.vtex", 25);

var scanParticleMenu = CreateMenuParticles(Scan);
CreateMenuThinkers(Scan);
var scanColorMenu = CreateMenuIconOnGround(Scan);

/* ============================ */

function CreateMenuBase(tree, sound) {

	var panel = tree.AddTree({ en: "Panel", ru: "Панель" }, true)
		.SetToolTip({ en: "TODO", ru: "Боковая панель для уведомлений" });

	panel.AddSlider({ en: "Remove delay", ru: "Время до удаления" }, 5, 0.5, 20, true);

	tree.AddToggle({ en: "Chat", ru: "Чат" })
		.SetToolTip({ en: "TODO", ru: "Оповещение в чат" });

	if (sound) {
		var soundMenu = tree.AddTree({ en: "Sounds", ru: "Звуки" }, true);

		if (!Crutches.IsOSSuppoted)
			soundMenu.SetToolTip(Menu.ToolTipOSWarning);

		soundMenu.AddSlider({ en: "Volume (%)", ru: "Громкость (%)" }, 0.5, 0.1, 100, true);
	}
}

/* ============================ */

function CreateMenuIconOnGround(tree) {
	var time = tree.AddTree({ en: "On Ground", ru: "На земле" }, true)
		.SetToolTip({
			en: "Draw spell and timings on ground",
			ru: "Отображение способности (и времени) на земле"
		});

	time.AddSwitch({ en: "Style", ru: "Стиль" }, [
		{ en: "Icon", ru: "Иконка" },
		{ en: "Text", ru: "Текст" }
	]);

	return Menu.CreateRGBA(time, { en: "Color", ru: "Цвет" }, Color.White);
}

function CreateMenuParticles(tree) {

	var particleMenu = Menu.CreateParticle(tree, { en: "Circle", ru: "Круг" });

	particleMenu.tree.SetToolTip({
		en: "TODO",
		ru: "Добавить круг для отображения"
	})

	return particleMenu;
}

function CreateMenuThinkers(tree, sound) {

	CreateMenuBase(tree, sound);

	var minimap = tree.AddTree({ en: "MiniMap", ru: "МиниКарта" }, true)
		.SetToolTip({ en: "TODO", ru: "Показ иконки на миникарте" });

	minimap.AddSlider({ en: "Size icons", ru: "Размер иконки" }, 18, 10, 35);
}

/* ============================ */

/** @param {MenuTree} tree */
function CreateMenuBuffs(tree, buff) {
	CreateMenuBase(tree, true);

	switch (buff.Skill) {
		case "life_stealer_infest":
			lifeStealerCheck = tree.AddSwitch({ en: "Check units", ru: "Проверка юнитов" }, [
				{ en: "Heroes", ru: "Герои" },
				{ en: "All units", ru: "Все юниты" }
			]).SetToolTip({
				en: "TODO",
				ru: "Включение параметра 'Все юниты' может повлиять на снижение FPS"
			})
			break;

		/* case "spirit_breaker_charge_of_darkness":
			breakerChargeMiniMap = tree.AddToggle({ en: "MiniMap", ru: "МиниКарта" })
				.SetToolTip({ 
					en: "TODO", 
					ru: "Показ иконки на миникарте во время " + buff.SkillNormal.split(": ")[1] 
				});
			break; */
	}
}

/* ============================ */

function RandomObject(obj) {
	var keys = Object.keys(obj);
	return obj[keys[keys.length * Math.random() << 0]];
}

new Thread(RANDOM_INTEGER(5, 30), function () {
	AbilityAlert.AddLogo(RandomObject(RANDOM_INTEGER(0, 1) ? FindThinkers : FindBuffs).Skill);
}).Start();

new Thread(RANDOM_INTEGER(5, 20), function () {
	spells.AddLogo(RandomObject(FindThinkers).Skill);
}).Start();
new Thread(RANDOM_INTEGER(5, 20), function () {
	buffs.AddLogo(RandomObject(FindBuffs).Skill);
}).Start();
new Thread(RANDOM_INTEGER(5, 20), function () {
	ground.AddLogo(RandomObject(FindGround).Skill);
}).Start();

/* ============================ */

/** @param {Buff} buff @param {Entity} ent */
function NotifyPanel(buff, ent, delay) {
	return Drawing.Notify('\
		<DOTAAbilityImage class="ability-image" abilityname="' + buff.Ability.Name + '" />\
		<Panel class="info-notify" style="margin: 0 15px;" />\
		<DOTAHeroImage class="hero-image" heroname="' + (ent || buff.Caster.Name) + '" />\
	', delay);
}

var SizeIconOnGround = 50;
var OffsetIconOnGround = new Vector(-(SizeIconOnGround / 2), (SizeIconOnGround / 2));
var LabelTextIcon = "<Label style='horizontal-align: left;wash-color: #0000006F;font-size: 20px;font-family: Radiance;' ";

/** @param {Buff} buff */
function PanelOnGroundThinker(buff, style, isRadar) {
	var panel = PanelOnGroundIcon(buff.Texture, buff.Owner.Position, style, isRadar);

	panel.BCreateChildren(LabelTextIcon + "/>");

	return panel;
}

/** @param {Buff} buff */
function PanelOnGroundIcon(skillName, pos, style, isRadar) {
	var timingsPanel = $.CreatePanel("Panel", Game.InGameHUD, skillName)

	Panels.WorldToScreen(pos, undefined, timingsPanel, true);

	timingsPanel.BLoadLayoutFromString("<root><Panel style='flow-children:down;' /></root>", false, false);

	var stylePanel = "";

	if (style === 0) {

		stylePanel = isRadar
			? "<Image src='s2r://panorama/images/hud/reborn/icon_scan_on_psd.vtex' style='border-radius:25px;width:"
			+ SizeIconOnGround + "px;height:" + SizeIconOnGround + "px;' />"
			: "<DOTAAbilityImage style='border-radius:25px;width:"
			+ SizeIconOnGround + "px;height:" + SizeIconOnGround + "px;' abilityname='" + skillName + "' />";
	}
	else stylePanel = (LabelTextIcon + " text='" + (isRadar ? "radar" : skillName) + "'/>");

	timingsPanel.BCreateChildren(stylePanel);

	return timingsPanel;
}

/* ============================ */
/** @type {CallableFunction[]} */
var allUpdate = [];
/** @type {CallableFunction[]} */
var allDestroy = [];
/** @type {Map<Entity|Buff, CallableFunction>} */
var allFullyDestroy = new Map();

var IsCataclysm = false;

/** @param {Buff} buff */
function CreateRadar(buff) {

	allUpdate = [];

	var buffOwner = buff.Owner;

	var render = scanParticleMenu.Style;
	var width = scanParticleMenu.Width;
	var alpha = scanParticleMenu.A;

	var particle;

	/** @type {MenuTree} */
	var panelControl = Scan.GetControl("Panel");
	var notify;

	var chatControl = Scan.GetControl("Chat");
	var chat;

	/** @type {MenuTree} */
	var soundControl = Scan.GetControl("Sounds");
	var sound;

	var minimap = Scan.GetControl("MiniMap");
	/** @type {MiniMap} */
	var panel;

	/** @type {MenuTree} */
	var timeGround = Scan.GetControl("Timings");
	var styleTime = timeGround.GetControl("Style");

	var colorGround_R = scanColorMenu.R;
	var colorGround_G = scanColorMenu.G;
	var colorGround_B = scanColorMenu.B;
	var colorGround_A = scanColorMenu.A;
	var lastStyle = styleTime.value;
	/** @type {Panel} */
	var panelGround;

	var hasHero = false;
	/** @type {Player} */
	var hero;

	function Update() {
		if (!AbilityAlert.value || !menu.value)
			return;

		if (panelControl.value && notify === undefined) {
			GameEvents.SendEventClientSide('dota_scan_used', {
				teamnumber: buffOwner.TeamNumber
			});

			notify = Drawing.Notify('\
			<Image class="ability-image" src="s2r://panorama/images/hud/reborn/icon_scan_on_psd.vtex" />\
			<Panel class="info-notify" style="margin: 0 15px;" />\
			<Image class="hero-image" src="s2r://panorama/images/team_icons/'
				+ (buffOwner.TeamNumber === DOTATeam_t.DOTA_TEAM_GOODGUYS ? "radiant" : "dire") + '_png.vtex" />\
			', panelControl.GetControl("Remove delay").value);
		}

		if (chat === undefined && chatControl.value) {
			var chatSay = "";
			if (Crutches.Language === "russian") {
				chatSay = "Силы "
					+ (buffOwner.TeamNumber === DOTATeam_t.DOTA_TEAM_BADGUYS ? "Тьмы" : "Света")
					+ " сканируют территорию";
			}
			else {
				chatSay = "The "
					+ (buffOwner.TeamNumber === DOTATeam_t.DOTA_TEAM_BADGUYS ? "Dire" : "Radiant")
					+ " using Scan";
			}
			GameConsole.SayTeam("> " + chatSay);
			chat = true;
		}

		if (sound === undefined && soundControl !== undefined && soundControl.value) {
			GameConsole.Sound("sounds/ui/scan", soundControl.GetControl("Volume (%)").value);
			sound = true;
		}

		if (minimap.value && (panel === undefined || !panel.IsValid))
			panel = Panels.MiniMap("hud/reborn/icon_scan_on_psd", buffOwner.Position, minimap.GetControl("Size icons").value);

		if (timeGround.value) {

			if (lastStyle !== styleTime.value) {
				if (panelGround !== undefined) {
					Panels.DeletePanels(panelGround);
					panelGround = undefined;
				}
				lastStyle = styleTime.value;
			}

			if (panelGround === undefined)
				panelGround = PanelOnGroundThinker(buff, lastStyle, true);

			Panels.WorldToScreen(buff.Owner.Position,
				lastStyle === 0 ? OffsetIconOnGround : undefined, panelGround, true);
			var color = "rgb("
				+ colorGround_R.value + "," + colorGround_G.value + "," + colorGround_B.value + ")";

			var timePanel = panelGround.GetChild(1);

			timePanel.text = Math.abs(ROUND_PLUS(buff.RemainingTime, 2));

			panelGround.style.opacity = colorGround_A.value / 100;

			timePanel.style.color = color;
			panelGround.GetChild(0).style.color = color;
		}

		var findHero = ARRAY_FIND(EntityManager.Heroes(buffOwner.TeamNumber), function (hero) {
			return hero !== undefined && hero.IsAlive && hero.IsInRange(buffOwner, 900);
		});



		if (findHero === undefined) {

			if (hasHero) {
				if (hero !== undefined && chatControl.value) {
					GameConsole.SayTeam("> " + hero.NameJSON + " "
						+ (Crutches.Language === "russian"
							? "вышел из вражеской зоны сканирования"
							: "left the enemy Scan area"));
					hero = undefined;
				}

				hasHero = false;
			}

			particle = ParticleManager.DrawCircle(buffOwner, buffOwner, 900, {
				Render: render.value,
				Color: scanParticleMenu.Color,
				Width: width.value,
				Alpha: alpha.value
			});
			allParticles.set(buffOwner, particle);
			return;
		}

		if (hasHero)
			return;

		hasHero = true;

		if (hero === undefined && chatControl.value) {
			GameConsole.SayTeam("> " + findHero.NameJSON + " "
				+ (Crutches.Language === "russian" ? "в зоне вражеского сканирования" : "is in enemy Scan area"));
			hero = findHero;
		}

		if (soundControl !== undefined && soundControl.value)
			GameConsole.Sound("sounds/ui/scan_enemy", soundControl.GetControl("Volume (%)").value);

		particle = ParticleManager.DrawCircle(buffOwner, buffOwner, 900, {
			Render: 2,
			Color: Color.Red,
			Width: width.value,
			Alpha: 255
		});

		allParticles.set(buffOwner, particle);

		$.Schedule(1, function () {
			hasHero = false;
		})
	}

	function Destroy() {
		DestroyParticle(buffOwner);

		if (panel !== undefined)
			panel.Remove();

		Panels.DeletePanels(panelGround);
	}

	function FullDestroy() {
		ARRAY_REMOVE(allUpdate, Update);
		ARRAY_REMOVE(allDestroy, buffOwner);
		allFullyDestroy.delete(buffOwner);

		Destroy();

		allParticles.delete(buffOwner);
	}

	allUpdate.push(Update);

	allDestroy.push(Destroy);
	allFullyDestroy.set(buff, FullDestroy);

	Update();
}
function FindDuration(buff) {
	var duration = buff.Duration;

	if (duration !== -1)
		return duration;

	duration = buff.Ability.ChannelTime

	if (duration !== 0)
		return duration;

	duration = buff.Ability.SpecialValueFor(FindThinkers[buff.Name].Special || "delay");

	return duration;
}

/** @param {MenuTree} menu @param {Buff} buff */
function CreateThinker(menu, buff) {
	var buffOwner = buff.Owner;

	var duration = FindDuration(buff);
	var time = Game.GetGameTime();

	var ability = buff.Ability;
	var radiusAbility = ability.Name === "puck_dream_coil"
		? ability.SpecialValueFor("coil_break_radius")
		: ability.Radius;

	var originalPrtcl = menu.GetControl("Original Particle");
	if (originalPrtcl !== undefined && originalPrtcl instanceof MenuTree)
		var styleOrigPrcl = originalPrtcl.GetControl("Style");

	var particleStyle = ThinkersToParticleMenu.get(buff.Texture);

	var render = particleStyle.Style;
	var width = particleStyle.Width;
	var alpha = particleStyle.A;

	/** @type {MenuTree} */
	var panelControl = menu.GetControl("Panel");
	var notify;

	var chatControl = menu.GetControl("Chat");
	var chat;

	/** @type {MenuTree} */
	var soundControl = menu.GetControl("Sounds");
	var sound;

	var minimap = menu.GetControl("MiniMap");
	/** @type {MiniMap} */
	var panel;

	/** @type {MenuTree} */
	var timeGround = menu.GetControl("On Ground");
	var styleTime = timeGround.GetControl("Style");
	var colorGround = ThinkersToRGBATree.get(buff.Texture);
	var colorGround_R = colorGround.R;
	var colorGround_G = colorGround.G;
	var colorGround_B = colorGround.B;
	var colorGround_A = colorGround.A;
	var lastStyle = styleTime.value;
	/** @type {Panel} */
	var panelGround;

	function Update() {
		if (!AbilityAlert.value || !menu.value)
			return;

		if (!IsCataclysm) {

			if ((buff.Name === "modifier_invoker_sun_strike_cataclysm"
				|| buff.Name === "modifier_invoker_sun_strike_cataclysm_thinker"))
				IsCataclysm = true;

			if (notify === undefined && panelControl.value)
				notify = NotifyPanel(buff, undefined, panelControl.GetControl("Remove delay").value);

			if (chat === undefined && chatControl.value) {
				ability.PingAbility();
				chat = true;
			}

			if (sound === undefined && soundControl !== undefined && soundControl.value) {
				GameConsole.Sound("sounds/weapons/hero/" + FindThinkers[buff.Name].Sound, soundControl.GetControl("Volume (%)").value);
				sound = true;
			}
		}

		if ((panel === undefined || !panel.IsValid) && minimap.value)
			panel = Panels.MiniMap("spellicons/" + ability.Name + "_png", buffOwner.Position, minimap.GetControl("Size icons").value);

		if (timeGround.value) {

			if (lastStyle !== styleTime.value) {
				if (panelGround !== undefined) {
					Panels.DeletePanels(panelGround);
					panelGround = undefined;
				}
				lastStyle = styleTime.value;
			}

			if (panelGround === undefined)
				panelGround = PanelOnGroundThinker(buff, lastStyle);

			Panels.WorldToScreen(buff.Owner.Position,
				lastStyle === 0 ? OffsetIconOnGround : undefined, panelGround, true);
			var color = "rgb("
				+ colorGround_R.value + "," + colorGround_G.value + "," + colorGround_B.value + ")";

			var timePanel = panelGround.GetChild(1);

			timePanel.text = Math.abs(ROUND_PLUS(time - Game.GetGameTime() + duration, 2));

			panelGround.style.opacity = colorGround_A.value / 100;

			timePanel.style.color = color;
			panelGround.GetChild(0).style.color = color;
		}

		var particle = ParticleManager.DrawCircle(buffOwner, buffOwner, radiusAbility, {
			Render: render.value,
			Color: particleStyle.Color,
			Width: width.value,
			Alpha: alpha.value
		});

		if (originalPrtcl !== undefined && originalPrtcl.value) {
			if (allParticles.has(buffOwner))
				return;

			var particles = [particle];

			particles.push(ParticleManager.AddOrUpdate("aa_ground" + buffOwner.Index, buffOwner,
				styleOrigPrcl !== undefined ? FindThinkers[buff.Name].Particle[styleOrigPrcl.value].Particle : FindThinkers[buff.Name].Particle,
				ParticleAttachment_t.PATTACH_ABSORIGIN));

			allParticles.set(buffOwner, particles);

			return;
		}

		allParticles.set(buffOwner, particle);
	}

	function Destroy() {
		if ((buff.Name === "modifier_invoker_sun_strike_cataclysm"
			|| buff.Name === "modifier_invoker_sun_strike_cataclysm_thinker"))
			IsCataclysm = false;

		DestroyParticle(buffOwner);

		if (panel !== undefined) {
			panel.Remove();
			panel = undefined;
		}

		Panels.DeletePanels(panelGround);
		panelGround = undefined;
	}

	function FullDestroy() {
		ARRAY_REMOVE(allUpdate, Update);
		ARRAY_REMOVE(allDestroy, buffOwner);
		allFullyDestroy.delete(buffOwner);

		Destroy();

		allParticles.delete(buffOwner);
	}

	allUpdate.push(Update);

	allDestroy.push(Destroy);
	allFullyDestroy.set(buff, FullDestroy);

	Update();
}

/* ============================ */

/** @param {Buff} buff */
function CreateBuff(buff, menu, valuesBuff) {

	/** @type {MenuTree} */
	var panelControl = menu.GetControl("Panel");

	/** @type {Particle} */
	var particle;
	var notify;

	/** @type {MenuTree} */
	var soundControl = menu.GetControl("Sounds");
	var sound;

	var chatControl = menu.GetControl("Chat");
	var chat;

	function Update() {
		if (sound === undefined && soundControl !== undefined && soundControl.value) {
			GameConsole.Sound("sounds/weapons/hero/" + valuesBuff.Sound, soundControl.GetControl("Volume (%)").value);
			sound = true;
		}

		if (chat === undefined && chatControl.value) {
			buff.Ability.PingAbility();
			chat = true;
		}

		if (particle === undefined) {
			particle = ParticleManager.AddOrUpdate("buff" + buff.Index + buff.Owner.Index, buff.Owner,
				"particles/units/heroes/" + valuesBuff.Particle, ParticleAttachment_t.PATTACH_ABSORIGIN_FOLLOW);

			allParticles.set(buff, particle);
		}

		if (buff.Texture === "slark_dark_pact")
			particle.SetControlPoints(1, buff.Owner.Position);

		if (notify === undefined && panelControl.value)
			notify = NotifyPanel(buff, buff.Owner, panelControl.GetControl("Remove delay").value);

		// TODO
		/* if (buff.Name === "spirit_breaker_charge_of_darkness" && breakerChargeMiniMap.value) {
			
		} */
	}

	function Destroy() {
		ARRAY_REMOVE(allUpdate, Update);
		allDestroy.delete(buff);

		if (particle !== undefined)
			particle.Destroy();

		allParticles.delete(buff);
	}

	allUpdate.push(Update);
	allDestroy.set(buff, Destroy);
	Update();
}

function CheckHasBuff(buff, menu, valuesBuff) {
	if (buff !== undefined && allParticles.get(buff) === undefined) {
		CreateBuff(buff, menu, valuesBuff);
		return true;
	}
	return false;
}

/* ============================ */

function FindTechiesRadius(entName, skill) {
	if (FindGround[entName].SkillRadius === undefined) {

		var skillFind = ARRAY_FIND(EntityManager.AllEntities, function (ent) {
			return ent.Name === skill;
		});

		if (skillFind !== undefined)
			FindGround[entName].SkillRadius = skillFind.SpecialValueFor(skill === "techies_stasis_trap" ? "activation_radius" : "radius");
	}

	return FindGround[entName].SkillRadius || 0;
}

function CreateOnGround(menu, entity) {

	var skill = FindGround[entity.Name].Skill;

	var radius = 0;

	if (entity.Name.indexOf("npc_dota_techies") !== -1)
		radius = FindTechiesRadius(entity.Name, skill);

	else radius = entity.CurrentVisionRange;

	var originalPrtcl = menu.GetControl("Original Particle");

	var particleStyle = GroundEntToParticleMenu.get(entity.Name);

	var render = particleStyle.Style;
	var width = particleStyle.Width;
	var alpha = particleStyle.A;

	/** @type {MenuTree} */
	var timeGround = menu.GetControl("On Ground");
	var styleTime = timeGround.GetControl("Style");
	var colorGround = GroundEntToRGBATree.get(entity.Name);
	var colorGround_R = colorGround.R;
	var colorGround_G = colorGround.G;
	var colorGround_B = colorGround.B;
	var colorGround_A = colorGround.A;
	var lastStyle = styleTime.value;
	/** @type {Panel} */
	var panelGround;

	function Update() {
		if (!AbilityAlert.value || !menu.value)
			return;

		if (timeGround.value) {

			if (lastStyle !== styleTime.value) {
				if (panelGround !== undefined) {
					Panels.DeletePanels(panelGround);
					panelGround = undefined;
				}
				lastStyle = styleTime.value;
			}

			if (panelGround === undefined || !panelGround.IsValid())
				panelGround = PanelOnGroundIcon(skill, entity.Position, lastStyle);

			Panels.WorldToScreen(entity.Position,
				lastStyle === 0 ? OffsetIconOnGround : undefined, panelGround, true);
			var color = "rgb("
				+ colorGround_R.value + "," + colorGround_G.value + "," + colorGround_B.value + ")";

			panelGround.style.opacity = colorGround_A.value / 100;

			panelGround.GetChild(0).style.color = color;
		}
		else Panels.DeletePanels(panelGround);

		if (radius === 0)
			radius = FindTechiesRadius(entity.Name, skill)

		var particle = ParticleManager.DrawCircle("aa_ground" + entity.Index, entity, radius, {
			Attachment: ParticleAttachment_t.PATTACH_WORLDORIGIN,
			Render: render.value,
			Color: particleStyle.Color,
			Width: width.value,
			Alpha: alpha.value
		});

		if (originalPrtcl !== undefined) {

			var particles = allParticles.get(entity);

			if (originalPrtcl.value) {

				if (Array.isArray(particles))
					return;

				particles = [particle];

				particles.push(ParticleManager.AddOrUpdate("aa_ground2" + entity.Index, entity,
					FindGround[entity.Name].Particle, ParticleAttachment_t.PATTACH_WORLDORIGIN,
					0, entity.Position, 1, new Vector(radius, radius, radius)));

				allParticles.set(entity, particles);

				return;
			}

			if (Array.isArray(particles))
				DestroyParticle(entity);
		}

		allParticles.set(entity, particle);
	}

	function Destroy() {
		DestroyParticle(entity);

		Panels.DeletePanels(panelGround);
	}

	function FullDestroy() {
		ARRAY_REMOVE(allUpdate, Update);
		ARRAY_REMOVE(allDestroy, buffOwner);
		allFullyDestroy.delete(buffOwner);

		Destroy();

		allParticles.delete(entity);
	}

	allUpdate.push(Update);

	allDestroy.push(Destroy);
	allFullyDestroy.set(buff, FullDestroy);

	Update();
}

function DestroyParticle(entity) {
	var particle = allParticles.get(entity);
	if (particle !== undefined) {

		if (Array.isArray(particle)) {
			ARRAY_FOREACH(particle, function (prtcl) {
				prtcl.Destroy();
			});
		}
		else particle.Destroy();

		allParticles.delete(entity);
	}
}

/* ============================ */

/** @type {Map<Entity | Buff, Particle|Particle[]>} */
var allParticles = new Map();

function Update() {
	if (Game.IsGamePaused())
		return;

	ARRAY_FOREACH(allUpdate, function (callback) {
		callback();
	});

	ARRAY_FOREACH(allBuffsToFind, function (buff) {
		var valuesBuff = FindBuffs[buff];
		var skill = valuesBuff.Skill;

		var menu = BuffsSkillToMenu.get(skill);

		if (!menu.value)
			return;

		var heroes = EntityManager.Heroes();

		if (skill === "life_stealer_infest") {

			if (ARRAY_SOME(heroes, function (hero) {
				return hero.IsEnemy && CheckHasBuff(hero.BuffByName(buff), menu, valuesBuff);
			})) return;

			if (lifeStealerCheck.value === 1) {
				ARRAY_SOME(EntityManager.AllEntities, function (creep) {
					return CheckHasBuff(creep.BuffByName(buff), menu, valuesBuff);
				});
			}

			return;
		}

		ARRAY_FOREACH(heroes, function (hero) {
			if (hero.IsEnemy && skill !== "slark_dark_pact")
				return;

			CheckHasBuff(hero.BuffByName(buff), menu, valuesBuff);
		})
	});
}


AbilityAlert.on("Activated", function () {
	InGameEvents.on("Update", Update);
});
AbilityAlert.on("Deactivated", function () {
	InGameEvents.removeListener("Update", Update);

	ARRAY_FOREACH(allDestroy, function (callback) {
		callback();
	});
});

Events.on("GameStarted", function () {
	if (!AbilityAlert.value)
		return;

	InGameEvents.on("Update", Update);
});

InGameEvents.on("BuffDestroyed", function (buff) {
	var destroy = allFullyDestroy.get(buff);
	if (destroy !== undefined)
		destroy();
});

InGameEvents.on("EntitySpawned", function (ent) {
	if (!ent.IsEnemy || !ent.IsAlive)
		return;

	if (ent.Name === "npc_dota_thinker") {

		var buff = ent.Buff(0);
		if (buff === undefined)
			return;

		if (buff.Name === "modifier_radar_thinker") {

			if (Scan.value)
				CreateRadar(buff);
			return;
		}

		var menu = ThinkersSkillToMenu.get(buff.Texture);

		if (menu === undefined || !menu.value)
			return;

		CreateThinker(menu, buff);
		return;
	}

	var menu = GroundEntToMenu.get(ent.Name);

	if (menu === undefined)
		return;

	CreateOnGround(menu, ent);
});
InGameEvents.on("EntityKilled", function (ent) {
	var destroy = allFullyDestroy.get(ent);
	if (destroy !== undefined)
		destroy();
});