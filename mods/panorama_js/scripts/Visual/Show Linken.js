var linkenSphere = Menu.Factory("Visual").AddToggle("Show Linken")
	.SetToolTip({
		en: "TODO",
		ru: "Показывает эффект на герое/юните если у героя есть Linken Sphere",
	});

linkenSphere.AddLogo("item_sphere");

var particle = "particles/items_fx/immunity_sphere_buff.vpcf";

/** @type {Map<Entity, Particle>} */
var allParticles = new Map();

function DestroyAllParticles() {
	allParticles.forEach(function (particle) {
		particle.Destroy();
	});

	allParticles.clear();
}

function DestroyParticle(entity) {
	var particle = allParticles.get(entity);

	if (particle === undefined)
		return;

	particle.Destroy();
	allParticles.delete(entity);
}

function AddParticle(entity) {
	allParticles.set(entity, ParticleManager.AddOrUpdate("linkenSphere" + entity.Index,
		entity, particle, ParticleAttachment_t.PATTACH_CENTER_FOLLOW));
}

function Update() {

	ARRAY_FOREACH(EntityManager.Heroes(), function (hero) {
		if (!hero.IsEnemy)
			return;
		
		var hasParticle = allParticles.has(hero);

		if (hero.IsAlive && (hero.IsLinkensProtected || hero.IsCounterspellProtected)) {
			if (!hasParticle)
				AddParticle(hero);

		} else if (hasParticle)
			DestroyParticle(hero);
	});
}


linkenSphere.on("Activated", function () {
	InGameEvents.on("Tick", Update);
});
linkenSphere.on("Deactivated", function () {
	InGameEvents.removeListener("Tick", Update);
});
InGameEvents.on("EntityKilled", function (entity) {
	DestroyParticle(entity);
});
Events.on("GameStarted", function () {
	if (!linkenSphere.value)
		return;

	InGameEvents.on("Tick", Update);
});
Events.on("GameEnded", function () {
	allParticles.clear();
});