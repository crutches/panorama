var showIllusion = Menu.Factory("Visual").AddTree("Show Illusion", true)
	.SetToolTip({
		en: "TODO",
		ru: "Показывает эффект на герое если он является юллюзией",
	});

showIllusion.AddLogo("s2r://panorama/images/emoticons/illusion_png.vtex");

var particlesNames = [
	"Illusions Mod",
	"Illusions Mod v2",
	"Illusions Ballons",
	"Shadow Amulet",
	"Smoke of Deceit"
];

var particles = [
	"panorama/crutches/particles/illusions/illusions_mod.vpcf",
	"panorama/crutches/particles/illusions/illusions_mod_v2.vpcf",
	"panorama/crutches/particles/illusions/illusions_mod_balloons.vpcf",
	"particles/items2_fx/shadow_amulet_active_ground_proj.vpcf",
	"particles/items2_fx/smoke_of_deceit_buff.vpcf"
];

var styleParticle = showIllusion.AddSwitch({ en: "Style", ru: "Стиль" }, particlesNames, 1)
	.on("Value", DestroyAllParticles);

var sizeParticle = showIllusion.AddSlider({ en: "Size", ru: "Размер" }, 80, 1, 250)
	.on("Value", DestroyAllParticles);

var colorParticle = Menu.CreateRGBA(showIllusion, { en: "Color", ru: "Цвет" }, Color.White);

colorParticle.R.on("Value", DestroyAllParticles);
colorParticle.G.on("Value", DestroyAllParticles);
colorParticle.B.on("Value", DestroyAllParticles);
colorParticle.A.on("Value", DestroyAllParticles);

function ChangeStyle() {
	if (styleParticle.value < 3) {

		if (showIllusion.HasControl(sizeParticle))
			return;

		showIllusion.AddControl(sizeParticle);
		showIllusion.AddControl(colorParticle.tree);
	}
	else {
		if (!showIllusion.HasControl(sizeParticle))
			return;

		showIllusion.RemoveControl(sizeParticle);
		showIllusion.RemoveControl(colorParticle.tree);
	}
}
ChangeStyle();
styleParticle.on("Value", ChangeStyle);

/** @type {Map<Entity, Particle>} */
var allParticles = new Map();

var ControlParticleSize = new Vector();
var ControlParticleColor = new Vector();

function ChangeControlParticleSize() {
	ControlParticleSize.SetVector(sizeParticle.value, colorParticle.A.value, 0);
}
function ChangeControlParticleColor() {
	ControlParticleColor = colorParticle.Color;
}
ChangeControlParticleSize();
ChangeControlParticleColor();
sizeParticle.on("Value", ChangeControlParticleSize)
colorParticle.A.on("Value", ChangeControlParticleSize);
colorParticle.R.on("Value", ChangeControlParticleColor);
colorParticle.G.on("Value", ChangeControlParticleColor);
colorParticle.B.on("Value", ChangeControlParticleColor);

function DestroyAllParticles() {
	allParticles.forEach(function (particle) {
		particle.Destroy();
	});

	allParticles.clear();
}

function DestroyParticle(entity) {
	var particle = allParticles.get(entity);

	if (particle === undefined)
		return;

	particle.Destroy();
	allParticles.delete(entity);
}

function UpdateParticle(entity) {
	if (allParticles.has(entity))
		return;

	var particle

	var valueStyle = styleParticle.value;

	if (valueStyle < 3) {
		var particle = ParticleManager.AddOrUpdate("linkenSphere" + entity.Index,
			entity, particles[valueStyle], ParticleAttachment_t.PATTACH_CENTER_FOLLOW, 1, ControlParticleSize, 2, ControlParticleColor);
	}
	else {
		particle = ParticleManager.AddOrUpdate("linkenSphere" + entity.Index,
			entity, particles[valueStyle], ParticleAttachment_t.PATTACH_CENTER_FOLLOW);
	}

	allParticles.set(entity, particle);
}

function Update() {
	ARRAY_FOREACH(EntityManager.AllEntities, function (hero) {

		if (hero.IsDormant || !hero.IsEnemy || !hero.IsHero || !hero.IsIllusionHero)
			return;

		hero.IsAlive ? UpdateParticle(hero) : DestroyParticle(hero);
	});
}


showIllusion.on("Activated", function () {
	InGameEvents.on("Tick", Update);
});
showIllusion.on("Deactivated", function () {
	DestroyAllParticles();

	InGameEvents.removeListener("Tick", Update);
});
InGameEvents.on("EntityKilled", function (entity) {
	DestroyParticle(entity);
});
Events.on("GameStarted", function () {
	if (!showIllusion.value)
		return;

	InGameEvents.on("Tick", Update);
});
Events.on("GameEnded", function () {
	allParticles.clear();
});