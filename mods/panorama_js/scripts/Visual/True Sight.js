var trueSight = Menu.Factory("Visual").AddTree("True Sight", true)
	.SetToolTip({
		en: "TODO",
		ru: "Показывает эффект на герое/юните если на нем есть эффект True Sight",
	});

trueSight.AddLogo("s2r://panorama/images/emoticons/gem_png.vtex", 23);

var allies = trueSight.AddToggle({ en: "Allies", ru: "Союзники" }).on("Value", DestroyAllParticles);
var creeps = trueSight.AddToggle({ en: "Creeps", ru: "Крипы" }).on("Value", DestroyAllParticles);
var mines = trueSight.AddToggle({ en: "Mines", ru: "Мины" }).on("Value", DestroyAllParticles);
var spiders = trueSight.AddToggle({ en: "Spiders", ru: "Паучки" }).on("Value", DestroyAllParticles);
var wards = trueSight.AddToggle({ en: "Wards", ru: "Варды" }).on("Value", DestroyAllParticles);
var other = trueSight.AddToggle({ en: "Other units", ru: "Остальные юниты" }).on("Value", DestroyAllParticles);

var style = trueSight.AddSwitch({ en: "Style", ru: "Стиль" }, ["Sentry", "Shiva`s Guard"])
	.on("Value", DestroyAllParticles);

var stylePath = [
	"particles/econ/wards/portal/ward_portal_core/ward_portal_eye_sentry.vpcf",
	"particles/items_fx/aura_shivas.vpcf"
];

/** @type {Map<Entity, Particle>} */
var allParticles = new Map();

/** @type {Entity[]} */
var allCreeps = [];
/** @type {Entity[]} */
var allSpiders = [];
/** @type {Entity[]} */
var allMines = [];
/** @type {Entity[]} */
var allWards = [];
/** @type {Entity[]} */
var allOther = [];

function DestroyAllParticles() {
	allParticles.forEach(function (particle) {
		particle.Destroy();
	});

	allParticles.clear();
}

function DestroyParticle(entity) {
	var particle = allParticles.get(entity);

	if (particle === undefined)
		return;

	particle.Destroy();
	allParticles.delete(entity);
}

function AddParticle(entity) {
	allParticles.set(entity, ParticleManager.AddOrUpdate("trueSight" + entity.Index,
		entity, stylePath[style.value], ParticleAttachment_t.PATTACH_ABSORIGIN_FOLLOW));
}

/** @param {Entity} entity */
function UpdateParticle(entity) {
	
	var buff = Buff.GetTrueSightBuff(entity);
	var particle = allParticles.has(entity);

	if (buff !== undefined && buff.Caster !== undefined && !buff.Caster.IsEnemy)
		return;

	if (buff !== undefined && entity.IsAlive) {
		if (!particle)
			AddParticle(entity);

	} else if (particle)
		DestroyParticle(entity);
}

function Update() {
	if (Game.IsGamePaused())
		return;

	var hasMe = Me !== undefined;

	if (hasMe)
		UpdateParticle(Me);

	if (allies.value) {
		ARRAY_FOREACH(EntityManager.Heroes(hasMe ? Me.TeamNumber : undefined), function (ent) {
			if (hasMe && Me.Entity === ent)
				return;

			UpdateParticle(ent);
		})
	}

	if (creeps.value)
		ARRAY_FOREACH(allCreeps, UpdateParticle);

	if (mines.value)
		ARRAY_FOREACH(allMines, UpdateParticle);
		
	if (spiders.value)
		ARRAY_FOREACH(allSpiders, UpdateParticle);
		
	if (wards.value)
		ARRAY_FOREACH(allWards, UpdateParticle);
		
	if (other.value)
		ARRAY_FOREACH(allOther, UpdateParticle);
}

trueSight.on("Activated", function () {
	InGameEvents.on("Tick", Update);
});
trueSight.on("Deactivated", function () {
	InGameEvents.removeListener("Tick", Update);
});
InGameEvents.on("EntitySpawned", function (entity) {
	if (entity.IsEnemy && !entity.IsNeutralUnitType)
		return;
		
	if (entity.IsCreep) {
		
		allCreeps.push(entity);
	}
	else if (entity.IsWard) {
		
		allWards.push(entity);
	}
	else if (entity.Class === "npc_dota_techies_mines") {
		
		allMines.push(entity);
	}
	else if (entity.Class === "npc_dota_broodmother_spiderling") {
		
		allSpiders.push(entity);
	}
	else {
		allOther.push(entity);
	}
});
InGameEvents.on("EntityKilled", function (entity) {
	DestroyParticle(entity);
	
	if (entity.IsCreep) {

		ARRAY_REMOVE(allCreeps, entity);
	}
	else if (entity.IsWard) {

		ARRAY_REMOVE(allWards, entity);
	}
	else if (entity.Class === "npc_dota_techies_mines") {

		ARRAY_REMOVE(allMines, entity);
	}
	if (entity.Class === "npc_dota_broodmother_spiderling") {

		ARRAY_REMOVE(allSpiders, entity);
	}
	else {
		ARRAY_REMOVE(allOther, entity);
	}
});
Events.on("GameStarted", function () {
	if (!trueSight.value)
		return;

	InGameEvents.on("Tick", Update);
});
Events.on("GameEnded", function () {
	allParticles.clear();
	ARRAY_CLEAN(allCreeps);
	ARRAY_CLEAN(allMines);
	ARRAY_CLEAN(allSpiders);
	ARRAY_CLEAN(allWards);
	ARRAY_CLEAN(allOther);
});