var enemyPanels = Menu.Factory("Visual").AddTree("Enemy Panels", true)
	.SetToolTip({
		en: "HP Bar <br/>Mana Bar <br/>Ability Bar <br/>Item Bar", //  <br/>Heros Panel <br/>Top Overlay
		ru: "Бар ХП <br/>Бар Маны <br/>Бар Способностей <br/>Бар Предметов" //  <br/>Панель Героев <br/>Верхний Оверлей
	});

/* HP Bars */
var hpBars = enemyPanels.AddTree({ en: "HP Bars", ru: "ХП Бары" }, true)
	.SetToolTip({ en: "Show hp numbers on enemy", ru: "Показывать кол-во ХП рядом с противником" });
var hpBarsStyle = hpBars.AddSwitch({ en: "Show HP", ru: "Показ ХП" }, [
	{ en: "Now + Max", ru: "Сейчас + Макс" },
	{ en: "Now", ru: "Сейчас" },
]);
var hpBarsPercent = hpBars.AddToggle({ en: "Show percent", ru: "Показывать проценты" });
var hpBarsColor = Menu.CreateRGBA(hpBars, { en: "Color ", ru: "Цвет" }, Color.White);
var hpBarsAllies = hpBars.AddToggle({ en: "Show on allies", ru: "На союзниках" });

/* Mana Bars */
var manaBars = enemyPanels.AddTree({ en: "Mana Bars", ru: "Мана Бары" }, true)
	.SetToolTip({ en: "Show mana on enemy", ru: "Показывать ману рядом с противником" });
var manaBarsNumbers = manaBars.AddTree({ en: "Show numbers", ru: "Показывать цифры" }, true);
var manaBarsNumbersStyle = manaBarsNumbers.AddSwitch({ en: "Show Mana", ru: "Показ Маны" }, [
	{ en: "Now + Max", ru: "Сейчас + Макс" },
	{ en: "Now", ru: "Сейчас" },
]);
var manaBarsPercent = manaBars.AddToggle({ en: "Show percent", ru: "Показывать проценты" });
var manaBarsColor = Menu.CreateRGBA(manaBars, { en: "Color ", ru: "Цвет" }, Color.White);
var manaBarsAllies = manaBars.AddToggle({ en: "Show on allies", ru: "На союзниках" });

/* Abilities Bars */
var abilsBars = enemyPanels.AddTree({ en: "Ability Bars", ru: "Бар Способностей" }, true);
var abilsBarsImage = abilsBars.AddToggle({ en: "Show Icons", ru: "Показывать Иконки" }, true);
var abilsBarsSize = abilsBars.AddSlider({ en: "Size", ru: "Размер" }, 25, 15, 50);
var abilsBarsPosition = abilsBars.AddSlider({ en: "Position (Y)", ru: "Позиция (Y)" }, 230, 0, 600);
var abilsBarsOpacity = abilsBars.AddSlider({ en: "Opacity", ru: "Прозрачность" }, 100, 0, 100);
var abilsBarsLvlColor = Menu.CreateRGBA(abilsBars, { en: "Levels Style", ru: "Стиль Уровней" }, Color.Aqua);
var abilsBarsLvlStyle = abilsBarsLvlColor.tree.AddSwitch({ en: "Style", ru: "Стиль" }, [
	{ en: "Dots", ru: "Точки" },
	{ en: "Numbers", ru: "Цифры" }
]);
abilsBarsLvlStyle.Index = 0;
var abilsBarsAllies = abilsBars.AddToggle({ en: "Show on allies", ru: "На союзниках" });

/* Items Bars */
var itemsBars = enemyPanels.AddTree({ en: "Items Bars", ru: "Бар Предметов" }, true);
var itemsBarsSize = itemsBars.AddSlider({ en: "Size", ru: "Размер" }, 20, 15, 50);
var itemsBarsPosition = itemsBars.AddSlider({ en: "Position (Y)", ru: "Позиция (Y)" }, 100, 0, 600);
var itemsBarsOpacity = itemsBars.AddSlider({ en: "Opacity", ru: "Прозрачность" }, 50, 0, 100);
var itemsBarsLvlColor = Menu.CreateRGBA(abilsBars, { en: "Charges/Levels Style", ru: "Стиль Зарядов/Уровней" }, Color.Aqua);
var itemsBarsAllies = itemsBars.AddToggle({ en: "Show on allies", ru: "На союзниках" });

/* Heroes Panel */
var heroesPanel = enemyPanels.AddTree({ en: "Heroes Panel", ru: "Панель Героев" }, true)
	.SetToolTip({ en: "TODO", ru: "Перемещаемая панель для отображения предметов у героев и их уровня" })
var heroesPanelStyle = heroesPanel.AddTree({ en: "Style Panel", ru: "Стиль Панели" });

var heroesPanelStyleFlow = heroesPanelStyle.AddSwitch({ en: "Style", ru: "Стиль" }, [
	{ en: "Horizontal", ru: "Горизонтальная" },
	{ en: "Vertical", ru: "Вертикальная" }
]);
var heroesPanelSize = heroesPanelStyle.AddSlider({ en: "Size", ru: "Размер" }, 30, 20, 50);
var heroesPanelOpacity = heroesPanelStyle.AddSlider({ en: "Opacity", ru: "Прозрачность" }, 75, 5, 100);
var heroesPanelPosition = Menu.CreateXY(heroesPanelStyle, { en: "Position", ru: "Позиция" }, new Vector(0, 60),
	undefined, undefined, undefined, undefined, true);
heroesPanelPosition.tree.SetToolTip({
	en: "TODO",
	ru: "Вы можете перемещать панель с помощью мыши за верхний/боковой край"
});

var heroesPanelLevel = heroesPanel.AddToggle({ en: "Level Heroes", ru: "Левел Героев" });
var heroesPanelTP = heroesPanel.AddToggle({ en: "Show Teleport", ru: "Показывать Телепорт" });
var heroesPanelItems = heroesPanel.AddTree({ en: "Items", ru: "Предметы" }, true);

var heroesPanelItemsCD = heroesPanelItems.AddToggle({ en: "Cooldown", ru: "Перезарядка" });
var heroesPanelItemsBackpack = heroesPanelItems.AddToggle({ en: "Backpack", ru: "Рюкзак" })
	.SetToolTip({ en: "Displayed as a colorless icon (like original in backpack)", ru: "Отображается как бесцветная иконка" });

var heroesPanelAllies = heroesPanel.AddToggle({ en: "Show Allies", ru: "Cоюзники" });

/* Top Overlay */
/* var topBars = enemyPanels.AddTree({ en: "Top Overlay", ru: "Верхний Оверлей" }, true)
	.SetToolTip({ en: "Show info under icon of enemy top bar", ru: "Показывать информацию под иконкой противника сверху" });
var topBarsAlt = topBars.AddToggle({ en: "Only 'ALT' is down", ru: "Если нажат 'ALT'" });
var topBarsHP = topBars.AddToggle({ en: "HP Bars", ru: "ХП Бары" })
var topBarsMana = topBars.AddToggle({ en: "Mana Bars", ru: "Мана Бары" });
var topBarsULT = topBars.AddToggle({ en: "Ultimate Cooldown", ru: "Перезарядка Ульты" }); */

/* Bottom Hud */
// TODO lower hud on event selected units

/** @type {Entity[]} */
var allPlayers = [];
/** @type {Map<Player, Entity>} */
var lastPlayerHero = new Map();

var HpBars = (function () {

	var PanelString = "\
		<root>\
			<Panel hittest='false' style='width: 100px;height: 12px;margin-top: -32px;margin-left: -52px;font-weight: bold;font-size: 11px;text-overflow: shrink;flow-children: right;'>\
				<Label style='color: white;vertical-align:center;padding-left: 3px;'/>\
				<Label style='color: white;vertical-align:center;padding-right: 3px;text-align:right;'/>\
			</Panel>\
		</root>";

	/** @type {Map<Entity, Panel>} */
	var allBars = new Map();

	/** @param {Panel} panel */
	function ChangePercent(panel) {
		var style = panel.GetChild(0).style;

		if (hpBarsPercent.value) {
			style.textAlign = "left";
			style.width = "fill-parent-flow(1.0)";
		}
		else {
			style.textAlign = "center";
			style.width = "100%";
		}
	}

	/** @param {Panel} panel */
	function ChangeColor(panel) {
		var color = "rgb("
			+ hpBarsColor.R.value + "," + hpBarsColor.G.value + "," + hpBarsColor.B.value + ")";

		panel.GetChild(0).style.color = color;
		panel.GetChild(1).style.color = color;

		panel.style.opacity = hpBarsColor.A.value / 100;
	}

	function ControlChangeColor() {
		if (!hpBars.value)
			return;

		allBars.forEach(ChangeColor);
	}

	hpBars.on("Deactivated", function () {
		RemoveAndDestroyPanels(allBars);
	});

	hpBarsAllies.on("Deactivated", function () {
		RemoveAndDestroyPanels(allBars);
	});

	hpBarsPercent.on("Value", function () {
		if (!hpBars.value)
			return;

		allBars.forEach(ChangePercent);
	});

	hpBarsColor.R.on("Value", ControlChangeColor);
	hpBarsColor.G.on("Value", ControlChangeColor);
	hpBarsColor.B.on("Value", ControlChangeColor);
	hpBarsColor.A.on("Value", ControlChangeColor);

	return {
		Destroy: function (unit) {
			RemoveAndDestroyPanels(allBars, unit);
		},
		/** @param {Entity} unit */
		Update: function (unit, position) {
			if (!hpBars.value)
				return;

			var panel = allBars.get(unit);

			if (panel === undefined)
				return;

			Drawing.VectorToPanelPercent(position, panel, true);
		},
		/** @param {Entity} unit */
		Tick: function (unit, visible) {
			if (!hpBars.value || (!hpBarsAllies.value && !unit.IsEnemy))
				return;

			var panel = allBars.get(unit);

			if (panel === undefined) {

				if (!visible)
					return;

				panel = $.CreatePanel("Panel", Game.InGameHUD, "hp" + unit.Index)
				panel.BLoadLayoutFromString(PanelString, false, false);
				ChangeColor(panel);
				ChangePercent(panel);

				allBars.set(unit, panel);
			}

			if (!visible) {
				RemoveAndDestroyPanels(allBars, unit);
				return;
			}

			if (hpBarsStyle.value === 0) {
				panel.GetChild(0).text = unit.HP + " / " + unit.MaxHP;
			}
			else {
				panel.GetChild(0).text = unit.HP;
			}

			if (hpBarsPercent.value)
				panel.GetChild(1).text = unit.HPPercent + "%";
		}
	}
})();

var ManaBars = (function () {

	var PanelString = "\
		<root>\
			<Panel hittest='false' style='width: 100px;height: 7px;margin-top: -20px;margin-left: -52px;background-color: black;font-weight: bold;font-size: 11px;text-overflow: shrink;'>\
				<Panel style='height: 100%;' />\
				<Label style='color: white;vertical-align:center;padding-left: 3px;'/>\
				<Label style='color: white;vertical-align:center;padding-right: 3px;text-align:right;horizontal-align:right;'/>\
			</Panel>\
		</root>";

	/** @type {Map<Entity, Panel>} */
	var allBars = new Map();

	/** @param {Panel} panel */
	function ChangePercent(panel) {

		var mana = panel.GetChild(1);
		var manaPercent = panel.GetChild(2);

		mana.text = "";
		manaPercent.text = "";

		if (!manaBarsNumbers.value && !manaBarsPercent.value) {
			panel.style.height = 5 + "px";
			panel.GetChild(0).style.backgroundColor = "#4FAAFF";
			return;
		}

		panel.style.height = 11 + "px";
		panel.GetChild(0).style.backgroundColor = "#3551A5FF";

		if (manaBarsPercent.value) {

			if (manaBarsNumbers.value) {
				mana.style.textAlign = "left";
				mana.style.horizontalAlign = "left";

				manaPercent.style.textAlign = "right";
				manaPercent.style.horizontalAlign = "right";
				return;
			}

			manaPercent.style.textAlign = "center";
			manaPercent.style.horizontalAlign = "center";
		}
		else {
			mana.style.textAlign = "center";
			mana.style.horizontalAlign = "center";
		}
	}

	/** @param {Panel} panel */
	function ChangeColor(panel) {
		var color = "rgb("
			+ manaBarsColor.R.value + "," + manaBarsColor.G.value + "," + manaBarsColor.B.value + ")";

		panel.GetChild(1).style.color = color;
		panel.GetChild(2).style.color = color;

		panel.style.opacity = manaBarsColor.A.value / 100;
	}

	function ControlChangeColor() {
		if (!manaBars.value)
			return;

		allBars.forEach(ChangeColor);
	}

	function ControlChangePercent() {
		if (!manaBars.value)
			return;

		allBars.forEach(ChangePercent);
	}

	manaBars.on("Deactivated", function () {
		RemoveAndDestroyPanels(allBars);
	});

	manaBarsAllies.on("Deactivated", function () {
		RemoveAndDestroyPanels(allBars);
	});

	manaBarsNumbers.on("Value", ControlChangePercent);
	manaBarsPercent.on("Value", ControlChangePercent);

	manaBarsColor.R.on("Value", ControlChangeColor);
	manaBarsColor.G.on("Value", ControlChangeColor);
	manaBarsColor.B.on("Value", ControlChangeColor);
	manaBarsColor.A.on("Value", ControlChangeColor);

	return {
		Destroy: function (unit) {
			RemoveAndDestroyPanels(allBars, unit);
		},
		/** @param {Entity} unit */
		Update: function (unit, position) {
			if (!manaBars.value)
				return;

			var panel = allBars.get(unit);

			if (panel === undefined)
				return;

			Drawing.VectorToPanelPercent(position, panel, true);
		},
		/** @param {Entity} unit */
		Tick: function (unit, visible) {
			if (!manaBars.value || (!manaBarsAllies.value && !unit.IsEnemy))
				return;

			var panel = allBars.get(unit);

			if (panel === undefined) {

				if (!visible)
					return;

				panel = $.CreatePanel("Panel", Game.InGameHUD, "mana" + unit.Index)
				panel.BLoadLayoutFromString(PanelString, false, false);
				ChangeColor(panel);
				ChangePercent(panel);

				allBars.set(unit, panel);
			}

			if (!visible) {
				RemoveAndDestroyPanels(allBars, unit);
				return;
			}

			var manaPercent = unit.ManaPercent + "%";

			panel.GetChild(0).style.width = manaPercent;

			if (manaBarsNumbers.value) {
				if (manaBarsNumbersStyle.value === 0) {
					panel.GetChild(1).text = unit.Mana + " / " + unit.MaxMana;
				}
				else {
					panel.GetChild(1).text = unit.Mana;
				}

			}

			if (manaBarsPercent.value)
				panel.GetChild(2).text = manaPercent;
		}
	}
})();

var AbilityBar = (function () {

	var PanelString = "\
		<root>\
			<Panel hittest='false' style='flow-children: right;font-weight: bold;text-overflow: shrink;font-size:30px;'/>\
		</root>";

	var PanelStringChild = "\
		<root>\
			<styles>\
				<include src='s2r://panorama/styles/main.vcss_c' />\
			</styles>\
			<Panel>\
				<Image id='image' style='background-size: 100%;background-position: 50% 50%;background-repeat:no-repeat;' />\
				<Label id='cooldown' style='text-overflow: shrink;width: 100%;margin: 1px;color: white;text-align: center;horizontal-align:center;vertical-align: center;' />\
				<Label id='lvl_text' style='text-overflow: shrink;color: white;vertical-align: bottom;horizontal-align:left;width: 40%;height: 40%;'/>\
				<Panel id='lvl' style='flow-children: right;vertical-align: bottom;width: 100%;height: 4px;' />\
			</Panel>\
		</root>";

	var PanelStringChildLvl = "<Label style='max-width: 4px;max-height: 4px;vertical-align:center;background-color: limegreen;margin-left: 1.5px;'/>";

	/** @type {Map<Entity, Panel>} */
	var allBars = new Map();
	/** @type {Map<Entity, Ability[]>} */
	var abilsHero = new Map();

	/** @param {Panel} panelAbil @param {Ability} abil */
	function UpdateChild(panelAbil, abil) {

		var image = panelAbil.FindChildTraverse('image');
		var panelAbilStyle = panelAbil.style;

		if (abil.Level <= 0) {
			image.AddClass("brightness");
			panelAbilStyle.border = "1px solid gray";
			return;
		}

		var cdLabel = panelAbil.FindChildTraverse("cooldown");

		if (abilsBarsLvlStyle.value === 0) {
			var lvlPanel = panelAbil.FindChildTraverse("lvl");

			var counLvlPanels = lvlPanel.GetChildCount();

			if (counLvlPanels !== abil.Level) {
				for (var i = abil.Level - counLvlPanels; i > 0; i--)
					lvlPanel.BCreateChildren(PanelStringChildLvl);

				ARRAY_FOREACH(lvlPanel.Children(), function (child) {
					var width = ((2 / abil.MaxLevel) * 10) + "px";
					child.style.width = width;
					child.style.height = width;
					ChangeLvlColorPanels(child);
				});
			}
		}
		else {
			panelAbil.FindChildTraverse("lvl_text").text = abil.Level;
		}


		if (abil.ManaCost > abil.Owner.Mana) {
			cdLabel.text = CoolDownRound(abil.ManaCost - abil.Owner.Mana) || "";

			image.AddClass("brightness");
			panelAbilStyle.border = "1px solid blue";
			return;
		}

		if (abil.IsInAbilityPhase) {

			image.AddClass("brightness");
			panelAbilStyle.border = "1px solid cornflowerblue";
			cdLabel.text = "";
			return;
		}

		var cooldown = CoolDownRound(abil.CooldownTimeRemaining);

		if (cooldown > 0) {
			cdLabel.text = cooldown;

			image.AddClass("brightness");
			panelAbilStyle.border = "1px solid red";
			return;
		}

		image.RemoveClass("brightness");
		panelAbilStyle.border = "1px solid limegreen";
		cdLabel.text = "";
	}

	function ChangePosition(panel) {
		panel.style.marginTop = ((abilsBarsPosition.value / 2) - 125) + "px";
	}
	function ChangeOpacity(panel) {
		panel.style.opacity = abilsBarsOpacity.value / 100;
	}
	function ChangeLvlColorText(panel) {
		panel.style.color = "rgb("
			+ abilsBarsLvlColor.R.value + "," + abilsBarsLvlColor.G.value
			+ "," + abilsBarsLvlColor.B.value + ")";

		panel.style.opacity = abilsBarsLvlColor.A.value / 100;
	}
	function ChangeLvlColorPanels(panel) {
		panel.style.backgroundColor = "rgb("
			+ abilsBarsLvlColor.R.value + "," + abilsBarsLvlColor.G.value
			+ "," + abilsBarsLvlColor.B.value + ")";

		panel.style.opacity = abilsBarsLvlColor.A.value / 100;
	}
	function ChangeLvlColor() {
		if (!abilsBars.value)
			return;

		allBars.forEach(function (panel) {
			ARRAY_FOREACH(panel.Children(), function (child) {
				ChangeLvlColorText(child.FindChildTraverse("lvl_text"));
				ARRAY_FOREACH(child.FindChildTraverse("lvl").Children(), ChangeLvlColorPanels);
			});
		})
	}

	abilsBars.on("Deactivated", function () {
		RemoveAndDestroyPanels(allBars, undefined, abilsHero);
	});
	abilsBarsAllies.on("Deactivated", function () {
		RemoveAndDestroyPanels(allBars, undefined, abilsHero);
	});
	abilsBarsImage.on("Value", function () {
		if (!abilsBars.value)
			return;

		allBars.forEach(function (panel) {
			ARRAY_FOREACH(panel.Children(), function (child) {
				child.FindChildTraverse("image").SetImage(abilsBarsImage.value
					? "s2r://panorama/images/spellicons/" + child.id + "_png.vtex"
					: "");
			});
		})
	});
	abilsBarsPosition.on("Value", function () {
		if (!abilsBars.value)
			return;

		allBars.forEach(ChangePosition);
	});
	abilsBarsSize.on("Value", function () {
		if (!abilsBars.value)
			return;

		allBars.forEach(function (panel) {
			var childs = panel.Children();
			ARRAY_FOREACH(childs, function (child) {
				var image = child.FindChildTraverse("image").style;
				image.width = abilsBarsSize.value + "px";
				image.height = abilsBarsSize.value + "px";

				child.FindChildTraverse("cooldown").style.fontSize = (abilsBarsSize.value / 1.6) + "px";
			});
			panel.style.marginLeft = "-" + ((abilsBarsSize.value * childs.length) / 2) + "px";
		});
	});
	abilsBarsOpacity.on("Value", function () {
		if (!abilsBars.value)
			return;

		allBars.forEach(ChangeOpacity);
	});

	abilsBarsLvlColor.R.on("Value", ChangeLvlColor);
	abilsBarsLvlColor.G.on("Value", ChangeLvlColor);
	abilsBarsLvlColor.B.on("Value", ChangeLvlColor);
	abilsBarsLvlColor.A.on("Value", ChangeLvlColor);

	abilsBarsLvlStyle.on("Value", function () {
		if (!abilsBars.value)
			return;

		allBars.forEach(function (panel) {
			ARRAY_FOREACH(panel.Children(), function (child) {
				child.FindChildTraverse("lvl_text").text = "";
				child.FindChildTraverse("lvl").RemoveAndDeleteChildren();
			});
		});
	});

	return {
		Destroy: function (unit) {
			RemoveAndDestroyPanels(allBars, unit, abilsHero);
		},
		/** @param {Entity} unit */
		Update: function (unit, position) {
			if (!abilsBars.value)
				return;

			var panel = allBars.get(unit);

			if (panel === undefined)
				return;

			Drawing.VectorToPanelPercent(position, panel, true);
		},
		/** @param {Entity} unit */
		Tick: function (unit, visible) {
			if (!abilsBars.value || (!abilsBarsAllies.value && !unit.IsEnemy))
				return;

			var abilities = ARRAY_FILTER_PLUS(unit.Spells, function (skill) {
				return skill.IsShow;
			});

			if (abilities.length === 0) {
				RemoveAndDestroyPanels(allBars, unit, abilsHero);
				return;
			}

			var panel = allBars.get(unit);

			if (panel === undefined) {

				if (!visible)
					return;

				panel = $.CreatePanel("Panel", Game.InGameHUD, "abilsbar" + unit.Index)
				ChangePosition(panel);
				ChangeOpacity(panel);

				panel.BLoadLayoutFromString(PanelString, false, false);

				allBars.set(unit, panel);
			}

			if (!visible) {
				RemoveAndDestroyPanels(allBars, unit, abilsHero);
				return;
			}

			var lastAbils = abilsHero.get(unit);

			if (lastAbils !== undefined) {

				if (abilities.length === lastAbils.length) {

					if (!ARRAY_SOME_PLUS(abilities, function (abil, index) {

						if (lastAbils[index] !== abil)
							return true;

						UpdateChild(panel.GetChild(index), abil);
						return false;
					})) return;
				}
				// we can write more logic (such as last.leng > now.leng), but is it necessary?
				// I taaak soidet ;)
				panel.RemoveAndDeleteChildren();
			}

			ARRAY_FOREACH_PLUS(abilities, function (abil) {

				var abilPanel = $.CreatePanel("Panel", panel, abil.Name);
				abilPanel.BLoadLayoutFromString(PanelStringChild, false, false);

				var image = abilPanel.FindChildTraverse('image');
				image.SetImage(abilsBarsImage.value
					? "s2r://panorama/images/spellicons/" + abil.Name + "_png.vtex"
					: "")
				image.style.width = abilsBarsSize.value + "px";
				image.style.height = abilsBarsSize.value + "px";

				abilPanel.FindChildTraverse("cooldown").style.fontSize = (abilsBarsSize.value / 1.6) + "px";

				ChangeLvlColorText(abilPanel.FindChildTraverse("lvl_text"));

				UpdateChild(abilPanel, abil);
			});
			panel.style.marginLeft = "-" + ((abilsBarsSize.value * abilities.length) / 2) + "px";

			abilsHero.set(unit, abilities);
		}
	}
})();

var ItemsBar = (function () {

	var PanelString = "\
		<root>\
			<Panel hittest='false' style='flow-children: right;font-weight: bold;text-overflow: shrink;font-size:30px;'/>\
		</root>";

	var PanelStringChild = "\
		<root>\
			<styles>\
				<include src='s2r://panorama/styles/dotastyles.vcss_c' />\
				<include src='s2r://panorama/styles/main.vcss_c' />\
			</styles>\
			<Panel>\
				<DOTAItemImage id='image' style='background-size: 100%;background-position: 50% 50%;background-repeat:no-repeat;' onmouseover='UIHideTextTooltip()' />\
				<Label id='cooldown' style='text-overflow: shrink;width: 100%;margin: 1px;color: white;text-align: center;horizontal-align:center;vertical-align: center;' />\
				<Label id='lvl' style='text-overflow: shrink;vertical-align: bottom;horizontal-align:right;width: 30%;height: 50%;'/>\
			</Panel>\
		</root>";

	/** @type {Map<Entity, Panel>} */
	var allBars = new Map();
	/** @type {Map<Entity, Item[]>} */
	var itemsHero = new Map();

	/** @param {Panel} panelItem @param {Item} item */
	function UpdateChild(panelItem, item) {

		var image = panelItem.FindChildTraverse('image');
		var panelItemStyle = panelItem.style;

		var cdLabel = panelItem.FindChildTraverse("cooldown");

		panelItem.FindChildTraverse("lvl").text = item.ShouldDisplayCharges
			? (item.CurrentCharges)
			: (item.Level > 1 ? item.Level : "");

		if (item.ManaCost > item.Owner.Mana) {
			cdLabel.text = CoolDownRound(item.ManaCost - item.Owner.Mana) || "";

			image.AddClass("brightness");
			panelItemStyle.border = "1px solid blue";
			return;
		}

		if (item.IsInAbilityPhase) {

			image.AddClass("brightness");
			panelItemStyle.border = "1px solid cornflowerblue";
			cdLabel.text = "";
			return;
		}

		var cooldown = CoolDownRound(item.CooldownTimeRemaining);

		if (cooldown > 0) {
			cdLabel.text = cooldown;

			image.AddClass("brightness");
			panelItemStyle.border = "1px solid red";
			return;
		}

		if (!item.IsPassive && item.CanBeExecuted > -1) {
			image.AddClass("brightness");
			panelItemStyle.border = "1px solid red";
			cdLabel.text = "";
			return;
		}

		image.RemoveClass("brightness");
		panelItemStyle.border = "1px solid limegreen";
		cdLabel.text = "";
	}

	itemsBars.on("Deactivated", function () {
		RemoveAndDestroyPanels(allBars, undefined, itemsHero);
	});

	function ChangePosition(panel) {
		panel.style.marginTop = ((itemsBarsPosition.value / 2) - 125) + "px";
	}
	function ChangeOpacity(panel) {
		panel.style.opacity = itemsBarsOpacity.value / 100;
	}
	function ChangeLvlColorText(panel) {
		panel.style.color = "rgb("
			+ itemsBarsLvlColor.R.value + "," + itemsBarsLvlColor.G.value
			+ "," + itemsBarsLvlColor.B.value + ")";

		panel.style.opacity = itemsBarsLvlColor.A.value / 100;
	}
	function ChangeLvlColor() {
		if (!itemsBars.value)
			return;

		allBars.forEach(function (panel) {
			ARRAY_FOREACH(panel.Children(), function (child) {
				ChangeLvlColorText(child.FindChildTraverse("lvl"));
			});
		})
	}

	itemsBarsSize.on("Value", function () {
		if (!itemsBars.value)
			return;

		allBars.forEach(function (panel) {
			var childs = panel.Children();
			ARRAY_FOREACH(childs, function (child) {
				var image = child.FindChildTraverse("image").style;
				image.width = (itemsBarsSize.value * 1.31) + "px";
				image.height = itemsBarsSize.value + "px";

				child.FindChildTraverse("cooldown").style.fontSize = (itemsBarsSize.value / 1.6) + "px";
			});
			panel.style.marginLeft = "-" + (((itemsBarsSize.value * 1.31) * childs.length) / 2) + "px"; // 1.31 - scale by item icon
		});
	});
	itemsBarsPosition.on("Value", function () {
		if (!itemsBars.value)
			return;

		allBars.forEach(ChangePosition);
	});
	itemsBarsOpacity.on("Value", function () {
		if (!itemsBars.value)
			return;

		allBars.forEach(ChangeOpacity);
	});

	itemsBarsLvlColor.R.on("Value", ChangeLvlColor);
	itemsBarsLvlColor.G.on("Value", ChangeLvlColor);
	itemsBarsLvlColor.B.on("Value", ChangeLvlColor);
	itemsBarsLvlColor.A.on("Value", ChangeLvlColor);

	return {
		Destroy: function (unit) {
			RemoveAndDestroyPanels(allBars, unit, itemsHero);
		},
		/** @param {Entity} unit */
		Update: function (unit, position) {
			if (!itemsBars.value)
				return;

			var panel = allBars.get(unit);

			if (panel === undefined)
				return;

			Drawing.VectorToPanelPercent(position, panel, true);
		},
		/** @param {Entity} unit */
		Tick: function (unit, visible) {
			if (!itemsBars.value || (!itemsBarsAllies.value && !unit.IsEnemy))
				return;

			var items = unit.Items;

			if (items.length === 0) {
				RemoveAndDestroyPanels(allBars, unit, itemsHero);
				return;
			}

			var panel = allBars.get(unit);

			if (panel === undefined) {

				if (!visible)
					return;

				panel = $.CreatePanel("Panel", Game.InGameHUD, "itemsbar" + unit.Index)
				ChangePosition(panel);
				ChangeOpacity(panel);

				panel.BLoadLayoutFromString(PanelString, false, false);

				allBars.set(unit, panel);
			}

			if (!visible) {
				RemoveAndDestroyPanels(allBars, unit, itemsHero);
				return;
			}

			var lastItems = itemsHero.get(unit);

			if (lastItems !== undefined) {

				if (items.length === lastItems.length) {

					if (!ARRAY_SOME_PLUS(items, function (item, index) {

						if (lastItems[index] !== item)
							return true;

						UpdateChild(panel.GetChild(index), item);
						return false;
					})) return;
				}

				// we can write more logic (such as last.leng > now.leng), but is it necessary?
				// I taaak soidet ;)
				panel.RemoveAndDeleteChildren();
			}

			ARRAY_FOREACH_PLUS(items, function (item) {

				var itemPanel = $.CreatePanel("Panel", panel, item.Name);
				itemPanel.BLoadLayoutFromString(PanelStringChild, false, false);

				var image = itemPanel.FindChildTraverse('image');

				image.itemname = item.Name;
				image.style.width = (itemsBarsSize.value * 1.31) + "px";
				image.style.height = itemsBarsSize.value + "px";

				itemPanel.FindChildTraverse("cooldown").style.fontSize = (itemsBarsSize.value / 1.6) + "px";

				ChangeLvlColorText(itemPanel.FindChildTraverse("lvl"));

				UpdateChild(itemPanel, item);
			});

			panel.style.marginLeft = "-" + (((itemsBarsSize.value * 1.31) * items.length) / 2) + "px"; // 1.31 - scale by item icon

			itemsHero.set(unit, items);
		}
	}
})();

var HeroesPanel = (function () {

	var PanelString = "\
		<root>\
			<Panel style=''>\
				<Panel id='move_panel' hittest='true' style='background-color: black;'/>\
				<Panel id='hero_list' hittest='false' style='min-height: 40px;min-width: 40px;border: 1px solid dimgrey;font-weight: bold;text-overflow: shrink;font-size:30px;' />\
			</Panel>\
		</root>";

	var PanelStringHero = "\
		<root>\
			<styles>\
				<include src='s2r://panorama/styles/dotastyles.vcss_c' />\
				<include src='s2r://panorama/styles/main.vcss_c' />\
			</styles>\
			<Panel>\
				<Panel id='hero_panel' style='vertical-align: center;'>\
					<DOTAHeroImage id='hero' style='margin: 1px;' />\
					<Label id='lvl_hero' style='text-overflow: shrink;color: white;vertical-align: bottom;horizontal-align: left;text-align: center;width: 40%;'/>\
				</Panel>\
				<Panel id='tp' style='border-radius: 50%;border: 1px solid dimgrey;horizontal-align: right;vertical-align: bottom;'>\
					<DOTAItemImage id='image' style='background-size: 100%;background-position: 50% 50%;background-repeat:no-repeat;' onmouseover='UIHideTextTooltip()' />\
					<Label id='cooldown' style='text-overflow: shrink;width: 100%;margin: 1px;color: white;text-align: center;horizontal-align:center;vertical-align: center;' />\
					<Label id='lvl' style='text-overflow: shrink;color: white;vertical-align: bottom;horizontal-align:right;text-align: center;width: 40%;height: 40%;'/>\
				</Panel>\
				<Panel id='item_panel' style='vertical-align: center;horizontal-align:center;'/>\
			</Panel>\
		</root>";

	var PanelStringItem = "\
		<root>\
			<styles>\
				<include src='s2r://panorama/styles/dotastyles.vcss_c' />\
				<include src='s2r://panorama/styles/main.vcss_c' />\
			</styles>\
			<Panel style='border: 1px solid dimgrey;horizontal-align:center;vertical-align: center;margin-left: 1px;'>\
				<DOTAItemImage id='image' style='background-size: 100%;background-position: 50% 50%;background-repeat:no-repeat;' onmouseover='UIHideTextTooltip()' />\
				<Label id='cooldown' style='text-overflow: shrink;height: 100%;color: white;text-align: center;horizontal-align:center;vertical-align: center;' />\
				<Label id='lvl' style='text-overflow: shrink;color: white;vertical-align: bottom;horizontal-align:right;text-align: center;width: 30%;height: 50%;'/>\
			</Panel>\
		</root>";

	/** @type {Panel} */
	var mainPanel;

	/** @type {Map<Entity, Panel>} */
	var heroesPanels = new Map();

	/** @type {Map<Entity, Panel>} */
	var scrollPanel = new Map();

	/** @type {Map<Entity, Item[]>} */
	var itemsHero = new Map();

	function CreatePanel() {
		if (!heroesPanel.value)
			return;

		mainPanel = $.CreatePanel("Panel", Game.InGameHUD, "HeroesPanel");
		ChangePosition();
		ChangeOpacity();
		mainPanel.BLoadLayoutFromString(PanelString, false, false);
		ChangeStyleFlow();

		Panels.MovePanel(mainPanel, MOVE_PANEL_TYPE.ONLY_PRESSED, mainPanel.FindChildTraverse("move_panel"))
			.on("Move", function (vec) {
				heroesPanelPosition.X.value = vec.x;
				heroesPanelPosition.Y.value = vec.y;
			});
	}
	/** @param {Panel} panelItem @param {Item} item */
	function UpdateItem(panelItem, item, visible) {

		var image = panelItem.FindChildTraverse('image');
		var panelItemStyle = panelItem.style;

		var cdLabel = panelItem.FindChildTraverse("cooldown");

		panelItem.FindChildTraverse("lvl").text = item.ShouldDisplayCharges
			? (item.CurrentCharges)
			: (item.Level > 1 ? item.Level : "");

		if (item.ManaCost > item.Owner.Mana) {
			cdLabel.text = CoolDownRound(item.ManaCost - item.Owner.Mana) || "";

			image.AddClass("brightness");
			panelItemStyle.border = "1px solid blue";
			return;
		}

		if (item.IsInAbilityPhase) {

			image.AddClass("brightness");
			cdLabel.text = "";
			return;
		}

		var cooldown = item.CooldownTimeRemaining;

		if (cooldown > 0) {

			if (!visible)
				cooldown -= Game.GetGameTime() - item.Owner.LastVisibleTime;

			cdLabel.text = CoolDownRound(cooldown);

			image.AddClass("brightness");
			return;
		}

		if (!item.IsPassive && item.CanBeExecuted > -1) {
			image.AddClass("brightness");
			cdLabel.text = "";
			return;
		}

		image.RemoveClass("brightness");
		cdLabel.text = "";
	}
	function Destroy() {
		Panels.DeletePanels(mainPanel);
		mainPanel = undefined;

		heroesPanels.clear();
		itemsHero.clear();
		scrollPanel.clear();
	}
	function ChangeStyleFlow() {
		if (!heroesPanel.value)
			return;

		var horizontal = heroesPanelStyleFlow.value === 0;

		mainPanel.style.flowChildren = horizontal ? "right" : "down";

		var movePanel = mainPanel.FindChildTraverse("move_panel");

		if (horizontal) {

			movePanel.style.width = "10px";
			movePanel.style.height = "100%";
		}
		else {
			movePanel.style.width = "100%";
			movePanel.style.height = "10px";
		}

		var heroes = mainPanel.FindChildTraverse("hero_list");
		heroes.style.flowChildren = horizontal ? "down" : "right";

		ARRAY_FOREACH(heroes.Children(), function (hero) {
			hero.style.flowChildren = horizontal ? "right" : "down";

			ChangeSize(hero);

			hero.FindChildTraverse("item_panel").style.flowChildren = horizontal ? "right" : "down";
		});
	}
	/** @param {Panel} panel */
	function ChangeSize(panel) {
		var horizontal = heroesPanelStyleFlow.value === 0;

		var heroIcon = panel.FindChildTraverse("hero");

		heroIcon.heroimagestyle = horizontal ? "landscape" : "icon";

		var heroStyle = heroIcon.style;

		if (horizontal) {
			heroStyle.height = heroesPanelSize.value + "px";
			heroStyle.width = (heroesPanelSize.value * 1.83) + "px";
		}
		else {
			heroStyle.height = (heroesPanelSize.value * 1.3) + "px";
			heroStyle.width = (heroesPanelSize.value * 1.3) + "px";
		}

		var sizeItem = heroesPanelSize.value - 5;

		var tpPanel = panel.FindChildTraverse("tp");

		if (horizontal)
			tpPanel.style.marginLeft = "-" + (sizeItem / 2) + "px";
		else
			tpPanel.style.marginTop = "-" + (sizeItem / 2) + "px";

		var imageTP = tpPanel.FindChildTraverse("image").style;
		imageTP.height = sizeItem + "px";
		imageTP.width = sizeItem + "px";

		tpPanel.FindChildTraverse("cooldown").style.fontSize = (heroesPanelSize.value / 1.6) + "px";

		ARRAY_FOREACH(panel.FindChildTraverse("item_panel").Children(), function (child) {
			child.FindChildTraverse("image").style.height = sizeItem + "px";
			child.FindChildTraverse("cooldown").style.fontSize = (heroesPanelSize.value / 1.6) + "px";
		});
	}
	function ChangePosition() {
		if (!heroesPanel.value)
			return;

		Drawing.VectorToPanelPercent(heroesPanelPosition.Vector, mainPanel);
	}
	function ChangeOpacity() {
		mainPanel.style.opacity = heroesPanelOpacity.value / 100;
	}
	function ChangeTPVisible(panel) {
		panel.visible = heroesPanelTP.value;
		panel.FindChildTraverse("cooldown").text = "";
		panel.FindChildTraverse("lvl").text = "";
	}

	NativeEvents.on("HeroSwap", function () {
		Destroy();
		CreatePanel();
	});
	heroesPanel.on("Value", function (value) {
		if (!enemyPanels.value)
			return;

		value ? CreatePanel() : Destroy();
	});
	heroesPanelStyleFlow.on("Value", ChangeStyleFlow);
	heroesPanelSize.on("Value", function () {
		if (!heroesPanel.value)
			return;

		heroesPanels.forEach(ChangeSize);
	});
	heroesPanelOpacity.on("Value", function () {
		if (!heroesPanel.value)
			return;

		ChangeOpacity();
	});
	heroesPanelPosition.X.on("Value", ChangePosition);
	heroesPanelPosition.Y.on("Value", ChangePosition);

	heroesPanelLevel.on("Value", function () {
		if (!heroesPanel.value)
			return;

		heroesPanels.forEach(function (panel) {
			panel.FindChildTraverse("lvl_hero").text = "";
		});
	});
	heroesPanelTP.on("Value", function () {
		if (!heroesPanel.value)
			return;

		scrollPanel.forEach(ChangeTPVisible);
	});
	heroesPanelItems.on("Value", function () {
		if (!heroesPanel.value)
			return;

		heroesPanels.forEach(function (panel) {
			panel.FindChildTraverse("item_panel").RemoveAndDeleteChildren();
		});
		itemsHero.clear();
	});
	heroesPanelItemsCD.on("Value", function () {
		if (!heroesPanel.value)
			return;

		heroesPanels.forEach(function (panel) {
			var tpPanel = panel.FindChildTraverse("tp");
			tpPanel.FindChildTraverse("cooldown").text = "";
			tpPanel.FindChildTraverse("lvl").text = "";

			ARRAY_FOREACH(panel.FindChildTraverse("item_panel").Children(), function (child) {
				child.FindChildTraverse("cooldown").text = "";
				child.FindChildTraverse("lvl").text = "";
			})
		});
	});
	heroesPanelAllies.on("Value", function () {
		Destroy();
		CreatePanel();
	});

	return {
		Create: CreatePanel,
		Destroy: Destroy,
		/** @param {Entity} unit */
		Tick: function (unit, visible) {
			if (!heroesPanel.value || (!heroesPanelAllies.value && !unit.IsEnemy))
				return;

			var panel = heroesPanels.get(unit);

			if (panel === undefined) {

				panel = $.CreatePanel("Panel", mainPanel.FindChildTraverse("hero_list"), unit.Name);

				panel.style.flowChildren = heroesPanelStyleFlow.value === 0 ? "right" : "down";

				panel.BLoadLayoutFromString(PanelStringHero, false, false);
				ChangeSize(panel);

				heroesPanels.set(unit, panel);

				var heroPanel = panel.FindChildTraverse("hero_panel");
				heroPanel.FindChildTraverse("hero").heroname = unit.Name;

				var tpPanel = panel.FindChildTraverse("tp");
				ChangeTPVisible(tpPanel);
				scrollPanel.set(unit, tpPanel);

				panel.FindChildTraverse("item_panel").style.flowChildren = heroesPanelStyleFlow.value === 0 ? "right" : "down";
			}

			if (heroesPanelLevel.value)
				panel.FindChildTraverse("lvl_hero").text = unit.Level;

			if (heroesPanelTP.value) {

				var tpPanel = scrollPanel.get(unit);
				var scroll = unit.TPScroll;

				tpPanel.FindChildTraverse("image").itemname = scroll !== undefined ? scroll.Name : "";

				if (scroll !== undefined && heroesPanelItemsCD.value)
					UpdateItem(tpPanel, scroll, visible);
			}

			if (!heroesPanelItems.value)
				return;

			var itemsPanel = panel.FindChildTraverse("item_panel");

			var items = unit.GetItems(0, heroesPanelItemsBackpack.value ? 9 : 6);

			if (items.length === 0) {
				itemsPanel.RemoveAndDeleteChildren();
				itemsHero.delete(unit);
				return;
			}

			var lastItems = itemsHero.get(unit);

			if (lastItems !== undefined) {

				if (items.length === lastItems.length) {

					if (!ARRAY_SOME_PLUS(items, function (item, index) {

						if (lastItems[index] !== item)
							return true;

						if (heroesPanelItemsCD.value)
							UpdateItem(itemsPanel.GetChild(index), item, visible);

						return false;
					})) return;
				}
			}

			itemsPanel.RemoveAndDeleteChildren();

			ARRAY_FOREACH_PLUS(items, function (item) {

				var itemPanel = $.CreatePanel("Panel", itemsPanel, item.Name);
				itemPanel.BLoadLayoutFromString(PanelStringItem, false, false);

				var image = itemPanel.FindChildTraverse('image');

				image.itemname = item.Name;

				if (item.AbilitySlot >= 6)
					itemPanel.AddClass("inactive_item");

				image.style.height = (heroesPanelSize.value - 5) + "px";

				itemPanel.FindChildTraverse("cooldown").style.fontSize = (heroesPanelSize.value / 1.6) + "px";

				if (heroesPanelItemsCD.value)
					UpdateItem(itemPanel, item, visible);
			});

			itemsHero.set(unit, items);
		}
	}
})();

/**
 * 
 * @param {Map<Entity, Panel>} map 
 * @param {Entity} unit 
 * @param {Map<Entity, any>} otherMap
 */
function RemoveAndDestroyPanels(map, unit, otherMap) {

	if (unit === undefined || !(unit instanceof Entity)) {
		Panels.DeletePanels(map);
		map.clear();

		if (otherMap !== undefined)
			otherMap.clear();

		return;
	}

	var panel = map.get(unit);
	if (panel === undefined)
		return;

	panel.DeleteAsync(0);
	map.delete(unit);

	if (otherMap !== undefined)
		otherMap.delete(unit);
}

function CoolDownRound(time) {
	if (time > 1)
		return Math.floor(time);

	if (time > 0)
		return ROUND_PLUS(time, 1);

	return 0;
}

function Update() {
	ARRAY_FOREACH(allPlayers, function (player) {
		var hero = player.HeroEntity;

		var position = Drawing.WorldToScreenXY(hero.Position.AddScalarZ(hero.HealthBarOffset))

		HpBars.Update(hero, position);
		ManaBars.Update(hero, position);
		AbilityBar.Update(hero, position);
		ItemsBar.Update(hero, position);
	});
}

function Tick() {
	allPlayers = ARRAY_FILTER(EntityManager.Players(), function (player) {
		var hero = player.HeroEntity;
		var lastHero = lastPlayerHero.get(player);

		if (hero === undefined || (lastHero !== undefined && hero !== lastHero)) {

			if (lastHero === undefined)
				return false;

			HpBars.Destroy(lastHero);
			ManaBars.Destroy(lastHero);
			AbilityBar.Destroy(lastHero);
			ItemsBar.Destroy(lastHero);

			lastPlayerHero.delete(player)
			return;
		}

		if (Me !== undefined && hero === Me.Entity)
			return false;

		if (lastHero === undefined)
			lastPlayerHero.set(player, hero);

		var visible = hero.IsAlive && hero.IsVisible;

		HpBars.Tick(hero, visible);
		ManaBars.Tick(hero, visible);
		AbilityBar.Tick(hero, visible);
		ItemsBar.Tick(hero, visible);
		HeroesPanel.Tick(hero, visible);

		return true;
	});
}

enemyPanels.on("Activated", function () {
	HeroesPanel.Create();

	InGameEvents.on("Update", Update);
	InGameEvents.on("Tick", Tick);
});
enemyPanels.on("Deactivated", function () {
	InGameEvents.removeListener("Update", Update);
	InGameEvents.removeListener("Tick", Tick);

	ARRAY_CLEAN(allPlayers);
	HpBars.Destroy();
	ManaBars.Destroy();
	AbilityBar.Destroy();
	ItemsBar.Destroy();
	HeroesPanel.Destroy();
});
Events.on("GameStarted", function () {
	if (!enemyPanels.value)
		return;

	HeroesPanel.Create();

	InGameEvents.on("Update", Update);
	InGameEvents.on("Tick", Tick);
});
Events.on("GameEnded", function () {
	ARRAY_CLEAN(allPlayers);
	HpBars.Destroy();
	ManaBars.Destroy();
	AbilityBar.Destroy();
	ItemsBar.Destroy();
	HeroesPanel.Destroy();
})