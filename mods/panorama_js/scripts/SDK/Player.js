// REQUIRED: Events

function Player(playerID) {
	this.PlayerID = playerID;
}

Player.prototype = {
	PlayerID: -1,

	/* ============= GETTERS ============= */

	/* ====== DOTA ====== */
	get CanPlayerBuyback() {
		return Players.CanPlayerBuyback(this.PlayerID);
	},
	get Assists() {
		return Players.GetAssists(this.PlayerID);
	},
	get ClaimedDenies() {
		return Players.GetClaimedDenies(this.PlayerID);
	},
	get ClaimedMisses() {
		return Players.GetClaimedMisses(this.PlayerID);
	},
	get Deaths() {
		return Players.GetDeaths(this.PlayerID);
	},
	get Denies() {
		return Players.GetDenies(this.PlayerID);
	},
	get Gold() {
		return Players.GetGold(this.PlayerID);
	},
	get GoldPerMin() {
		return Players.GetGoldPerMin(this.PlayerID);
	},
	get Kills() {
		return Players.GetKills(this.PlayerID);
	},
	get LastBuybackTime() {
		return Players.GetLastBuybackTime(this.PlayerID);
	},
	get LastHitMultikill() {
		return Players.GetLastHitMultikill(this.PlayerID);
	},
	get LastHitStreak() {
		return Players.GetLastHitStreak(this.PlayerID);
	},
	get LastHits() {
		return Players.GetLastHits(this.PlayerID);
	},
	get Level() {
		return Players.GetLevel(this.PlayerID);
	},
	get Misses() {
		return Players.GetMisses(this.PlayerID);
	},
	get NearbyCreepDeaths() {
		return Players.GetNearbyCreepDeaths(this.PlayerID);
	},
	get PerspectivePlayerEntityIndex() {
		return Players.GetPerspectivePlayerEntityIndex(this.PlayerID);
	},
	get PerspectivePlayerId() {
		return Players.GetPerspectivePlayerId(this.PlayerID);
	},
	get PlayerColor() {
		return Players.GetPlayerColor(this.PlayerID);
	},
	get HeroEntity() {
		return EntityManager.EntityByIndex(Players.GetPlayerHeroEntityIndex(this.PlayerID), Entity);
	},
	get PlayerName() {
		return Players.GetPlayerName(this.PlayerID);
	},
	get PlayerSelectedHero() {
		return Players.GetPlayerSelectedHero(this.PlayerID);
	},
	// test
	get QueryUnit() {
		var query = [];

		ARRAY_FOREACH_PLUS(Players.GetQueryUnit(this.PlayerID), function (entIndex) {

			var entity = EntityManager.EntityByIndex(entIndex);

			if (entity !== undefined)
				query.push(entity);
		});

		return query;
	},
	get ReliableGold() {
		return Players.GetReliableGold(this.PlayerID);
	},
	get RespawnSeconds() {
		return Players.GetRespawnSeconds(this.PlayerID);
	},
	get SelectedEntities() {
		var selected = [];

		ARRAY_FOREACH_PLUS(Players.GetSelectedEntities(this.PlayerID), function (entIndex) {

			var entity = EntityManager.EntityByIndex(entIndex);

			if (entity !== undefined)
				selected.push(entity);
		});

		return selected;
	},
	get Streak() {
		return Players.GetStreak(this.PlayerID);
	},
	get Team() {
		return Players.GetTeam(this.PlayerID);
	},
	get TotalEarnedGold() {
		return Players.GetTotalEarnedGold(this.PlayerID);
	},
	get TotalEarnedXP() {
		return Players.GetTotalEarnedXP(this.PlayerID);
	},
	get UnreliableGold() {
		return Players.GetUnreliableGold(this.PlayerID);
	},
	get XPPerMin() {
		return Players.GetXPPerMin(this.PlayerID);
	},
	get HasCustomGameTicketForPlayerID() {
		return Players.HasCustomGameTicketForPlayerID(this.PlayerID);
	},
	get IsLocalPlayerInPerspectiveCamera() {
		return Players.IsLocalPlayerInPerspectiveCamera(this.PlayerID);
	},
	get IsSpectator() {
		return Players.IsSpectator(this.PlayerID);
	},
	get IsValidPlayerID() {
		return Players.IsValidPlayerID(this.PlayerID);
	},

	/* ====== CUSTOM ====== */
	get IsPlayerMuted() {
		return Game.IsPlayerMuted(this.PlayerID);
	},

	/* ============= METHODS ============= */

	/* ====== DOTA ====== */
	BuffClicked: function (nBuffSerial, bAlert) {
		return Players.BuffClicked(this.PlayerID, nBuffSerial, bAlert);
	},
	PlayerPortraitClicked: function (bHoldingCtrl, bHoldingAlt) {
		return Players.PlayerPortraitClicked(this.PlayerID, bHoldingCtrl, bHoldingAlt);
	},

	/* ====== CUSTOM ====== */
	toString: function () {
		return this.PlayerName;
	}
};

/* ================== Static ================== */

/* ====== ORDERS ====== */

(function () {

	var IsOrdersBlocked = false;

	Object.defineProperty(Player, "IsOrdersBlocked", {
		get: function () {
			return IsOrdersBlocked;
		},
		set: function () { },
		configurable: false,
		enumerable: true
	})

	Player.PrepareOrders = function (order) {
		if (order === undefined)
			throw Error("Orders in not defined");

		if (IsOrdersBlocked)
			return false;

		if (order.OrderType === undefined)
			order.OrderType = dotaunitorder_t.DOTA_UNIT_ORDER_NONE;

		if (order.OrderIssuer === undefined)
			order.OrderIssuer = PlayerOrderIssuer_t.DOTA_ORDER_ISSUER_PASSED_UNIT_ONLY;

		if (InGameEvents.emit("PrepareOrders", true, order) === false)
			return false;

		if (order.TargetIndex instanceof Entity)
			order.TargetIndex = order.TargetIndex.Index;

		if (order.AbilityIndex instanceof Ability)
			order.AbilityIndex = order.AbilityIndex.Index;

		if (order.UnitIndex instanceof Entity)
			order.UnitIndex = order.UnitIndex.Index;

		order.ShowEffects = false;

		Game.PrepareUnitOrders(order);

		IsOrdersBlocked = true;
		$.Schedule(1 / 30, function () {
			IsOrdersBlocked = false;
		})
		return true;
	}

})();

Player.Buyback = function (queue, showEffects) {
	Player.PrepareOrders({
		OrderType: dotaunitorder_t.DOTA_UNIT_ORDER_BUYBACK,
		QueueBehavior: queue,
		ShowEffects: showEffects
	});
}
Player.Glyph = function (queue, showEffects) {
	Player.PrepareOrders({
		OrderType: dotaunitorder_t.DOTA_UNIT_ORDER_GLYPH,
		QueueBehavior: queue,
		ShowEffects: showEffects
	});
}
Player.CastRiverPaint = function (position, queue, showEffects) {
	Player.PrepareOrders({
		OrderType: dotaunitorder_t.DOTA_UNIT_ORDER_CAST_RIVER_PAINT,
		Position: position,
		QueueBehavior: queue,
		ShowEffects: showEffects
	});
}
Player.PreGameAdgustItemAssigment = function (ItemID, queue, showEffects) {
	Player.PrepareOrders({
		OrderType: dotaunitorder_t.DOTA_UNIT_ORDER_PREGAME_ADJUST_ITEM_ASSIGNMENT,
		TargetIndex: ItemID,
		QueueBehavior: queue,
		ShowEffects: showEffects
	});
}
Player.Scan = function (position, queue, showEffects) {
	Player.PrepareOrders({
		OrderType: dotaunitorder_t.DOTA_UNIT_ORDER_RADAR,
		Position: position,
		QueueBehavior: queue,
		ShowEffects: showEffects
	});
};

(function () {

	var playerPortraitUnit;

	Object.defineProperty(Player, 'PlayerPortraitUnit', {
		get: function () {
			return playerPortraitUnit
				|| (playerPortraitUnit = EntityManager.EntityByIndex(Players.GetLocalPlayerPortraitUnit()));
		},
		set: function () { },
		configurable: false
	});

	NativeEvents.on("QueryUnitsUpdate", function () {
		playerPortraitUnit = EntityManager.EntityByIndex(Players.GetLocalPlayerPortraitUnit());
	});
	NativeEvents.on("SelectedUnitsUpdate", function () {
		playerPortraitUnit = EntityManager.EntityByIndex(Players.GetLocalPlayerPortraitUnit());
	});
})();