
{ // Game Events

	var InGameEvents = new EventEmitter();
}

(function () {

	function InGameUpdate() {
		InGameEvents.emit("Update");
	}
	function InGameTick() {
		InGameEvents.emit("Tick");
	}

	Events.on("GameStarted", function () {
		Events.on("Update", InGameUpdate);
		Events.on("Tick", InGameTick);
	});

	Events.on("GameEnded", function () {
		Events.removeListener("Tick", InGameTick);
		Events.removeListener("Update", InGameUpdate);
	});



})();

(function () {

	/* Events.on("GameLoaded", function () {
		Game.ExecuteCmd("jointeam 2");
	}, true);
	
	InGameEvents.on("PlayerLoaded", function (player) {
		Game.ExecuteCmd("dota_select_hero npc_dota_hero_invoker");
	}, true);
	
	InGameEvents.on("AssignedHero", function (player, unit) {
		//Game.ExecuteCmd("dota_create_item item_blink");
		Game.ExecuteCmd("dota_ability_debug_enable");
	}, true); */
})();