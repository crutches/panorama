function Particle(name, unit, path, attachment) {
	this.Name = name;
	this.Entity = unit;
	this.Path = path;

	this.Attachment = attachment;

	this.ControlPoints = Array.prototype.slice.call(ARGUMENTS_INLINE_SLICE(arguments), Particle.length);

	this.Index = undefined;

	this.Create();
}

Particle.prototype = {
	get IsValid() {
		return this.Index !== undefined;
	},

	Create: function () {
		if (this.IsValid)
			return;

		this.Index = Particles.CreateParticle(this.Path, this.Attachment, this.Entity.Index);
		this.SetControlPoints();

		return this;
	},

	SetControlPoints: function () {
		if (!this.IsValid)
			return this;

		if (arguments.length > 0)
			this.ControlPoints = ARGUMENTS_INLINE_SLICE(arguments);

		for (var i = 0; i < this.ControlPoints.length; i += 2) {

			var index = this.ControlPoints[i];
			var controlPoint = this.ControlPoints[i + 1];

			if (controlPoint instanceof Entity)
				controlPoint = controlPoint.Position;

			if (controlPoint instanceof Vector || controlPoint instanceof Color)
				Particles.SetParticleControl(this.Index, index, controlPoint.toArray(3));
			else if (Array.isArray(controlPoint))
				Particles.SetParticleControl(this.Index, index, controlPoint);
			else
				Particles.SetParticleControl(this.Index, index, [controlPoint + 0, 0, 0]);
		}

		this._ControlPointsHashCode = undefined;
		this.GetControlPointsHashCode();

		return this;
	},
	GetControlPointsHashCode: function () {
		if (this._ControlPointsHashCode !== undefined)
			return this._ControlPointsHashCode;

		Object.defineProperty(this, "_ControlPointsHashCode", {
			value: Particle.ControlPointsHashCode(this.ControlPoints),
			enumerable: false
		});

		return this._ControlPointsHashCode;
	},
	Refresh: function () {
		if (!this.IsValid)
			return this;

		Particles.DestroyParticleEffect(this.Index, true);
		this.Index = undefined;

		this.Create();
		return this;
	},
	Destroy: function () {
		if (!this.IsValid)
			return this;

		if (Game.IsInGame)
			Particles.DestroyParticleEffect(this.Index, true);
		this.Index = undefined;

		ParticleManager.Destroy(this.Name);

		return this;
	},
	toString: function () {
		return this.Path.replace(/^.*(\\|\/|\:)/, "");
	}
}

Particle.ControlPointsHashCode = function (controlPoints) {
	var hash = 0;

	ARRAY_FOREACH(controlPoints, function (controlPoint) {

		if (controlPoint instanceof Entity || controlPoint instanceof Buff)
			hash += controlPoint.Index;

		else if (controlPoint instanceof Vector || controlPoint instanceof Color)
			hash += controlPoint.GetHashCode();

		else if (Array.isArray(controlPoint)) {
			var hashArray = 0;
			for (var length = controlPoint.length, i = (length >= 8 ? length - 8 : 0); i < length; i++)
				hashArray = ((hashArray << 5) + hashArray) ^ (controlPoint[i] || 0);

			hash += hashArray;
		}
		else
			hash += controlPoint
	});

	return hash;
}