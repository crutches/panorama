function Buff(index, owner) {
	this.Index = index;

	this.Owner = owner;

	var ownerIndex = this.Owner.Index;

	this.Ability = EntityManager.EntityByIndex(Buffs.GetAbility(ownerIndex, this.Index), Ability);
	this.Caster = EntityManager.EntityByIndex(Buffs.GetCaster(ownerIndex, this.Index));
	this.Class = Buffs.GetClass(ownerIndex, this.Index);
	this.CreationTime = Buffs.GetCreationTime(ownerIndex, this.Index);
	this.Parent = EntityManager.EntityByIndex(Buffs.GetParent(ownerIndex, this.Index));
	this.StackCount = Buffs.GetStackCount(ownerIndex, this.Index);
	this.Texture = Buffs.GetTexture(ownerIndex, this.Index);
	this.IsDebuff = Buffs.IsDebuff(ownerIndex, this.Index);
	this.IsHidden = Buffs.IsHidden(ownerIndex, this.Index);
	this.IsDebuff = Buffs.IsDebuff(ownerIndex, this.Index);
	this.Name = Buffs.GetName(ownerIndex, this.Index);

	EventEmitter.call(this);
}

Buff.prototype = {
	/* ============= GETTERS ============= */

	/* ====== DOTA ====== */
	get DieTime() {
		return Buffs.GetDieTime(this.Owner.Index, this.Index);
	},
	get Duration() {
		return Buffs.GetDuration(this.Owner.Index, this.Index);
	},
	get ElapsedTime() {
		return Buffs.GetElapsedTime(this.Owner.Index, this.Index);
	},
	get RemainingTime() {
		return Buffs.GetRemainingTime(this.Owner.Index, this.Index);
	},

	/* ====== CUSTOM ====== */

	/* ============= METHODS ============= */

	/* ====== CUSTOM ====== */

	toString: function () {
		return this.Name;
	}
};


const TRUESIGHT_MODIFIERS = [
	"modifier_truesight",
	"modifier_item_dustofappearance",
	"modifier_bloodseeker_thirst_vision",
	"modifier_bounty_hunter_track"
];

Buff.GetTrueSightBuff = function (entity) {
	if (!(entity instanceof Entity))
		return false;

	return ARRAY_FIND(entity.Buffs, function (buff) {
		return ARRAY_SOME(TRUESIGHT_MODIFIERS, function (sightBuff) {
			return buff.Name === sightBuff;
		})
	});
}
Buff.HasTrueSightBuff = function (entity) {
	if (!(entity instanceof Entity))
		return false;

	return ARRAY_SOME(entity.Buffs, function (buff) {
		return ARRAY_SOME(TRUESIGHT_MODIFIERS, function (sightBuff) {
			return buff.Name === sightBuff;
		})
	});
}