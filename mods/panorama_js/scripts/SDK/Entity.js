var Entity = (function () {

	function setJSON(entity) {
		if (entity._JSON === undefined) {

			if (Crutches.JSON === undefined
				|| Crutches.JSON.npc_units === undefined
				|| Crutches.JSON.npc_heroes === undefined)
				return false;

			Object.defineProperty(entity, "_JSON", {
				value: Crutches.JSON[entity.IsHero ? "npc_heroes" : "npc_units"][entity.Name] || false,
				enumerable: false,
				configurable: false,
				writable: false
			});
		}
		return entity._JSON;
	}

	function Entity(index) {
		if (index <= -1)
			throw Error("Invalid ID").stack;

		this.Index = index;

		this.Label = Entities.GetUnitLabel(index);
		this.Name = Entities.GetUnitName(index);
		this.Class = Entities.GetClassname(index);

		this.IsBuilding = Entities.IsBuilding(this.Index);
		this.IsCreature = Entities.IsCreature(this.Index);
		this.IsCreep = Entities.IsCreep(this.Index);
		this.IsCourier = Entities.IsCourier(this.Index);
		this.IsHero = Entities.IsHero(this.Index);
		this.IsTower = Entities.IsTower(this.Index);
		this.IsItemPhysical = Entities.IsItemPhysical(this.Index);
		this.IsLaneCreep = Entities.IsLaneCreep(this.Index);
		this.HealthBarOffset = Entities.GetHealthBarOffset(this.Index);

		this.CreateTime = this.LastVisibleTime = Game.GetGameTime();

		this.IsVisible = false;
		this.IsDormant = true;
	}

	Entity.prototype = {
		Index: -1,

		/* ============= GETTERS ============= */

		/* ====== DOTA ====== */
		get CanBeDominated() {
			return Entities.CanBeDominated(this.Index);
		},
		get AbilityCount() {
			return Entities.GetAbilityCount(this.Index);
		},
		get AbilityPoints() {
			return Entities.GetAbilityPoints(this.Index);
		},
		get Position() {
			return Vector.fromArray(Entities.GetAbsOrigin(this.Index) || []);
		},
		get AttackRange() {
			return Entities.GetAttackRange(this.Index);
		},
		get AttackSpeed() {
			return Entities.GetAttackSpeed(this.Index);
		},
		get AttacksPerSecond() {
			return Entities.GetAttacksPerSecond(this.Index);
		},
		get BaseAttackTime() {
			return Entities.GetBaseAttackTime(this.Index);
		},
		get BaseMagicalResistanceValue() {
			return Entities.GetBaseMagicalResistanceValue(this.Index);
		},
		get BaseMoveSpeed() {
			return Entities.GetBaseMoveSpeed(this.Index);
		},
		get BonusPhysicalArmor() {
			return Entities.GetBonusPhysicalArmor(this.Index);
		},
		get ChosenTarget() {
			return EntityManager.EntityByIndex(Entities.GetChosenTarget(this.Index));
		},
		get CollisionPadding() {
			return Entities.GetCollisionPadding(this.Index);
		},
		get CombatClassAttack() {
			return Entities.GetCombatClassAttack(this.Index);
		},
		get CombatClassDefend() {
			return Entities.GetCombatClassDefend(this.Index);
		},
		get ContainedItem() {
			return EntityManager.EntityByIndex(Entities.GetContainedItem(this.Index), Item);
		},
		get CurrentVisionRange() {
			return Entities.GetCurrentVisionRange(this.Index);
		},
		get CurrentXP() {
			return Entities.GetCurrentXP(this.Index);
		},
		get DamageBonus() {
			return Entities.GetDamageBonus(this.Index);
		},
		get DamageMax() {
			return Entities.GetDamageMax(this.Index);
		},
		get DamageMin() {
			return Entities.GetDamageMin(this.Index);
		},
		get DayTimeVisionRange() {
			return Entities.GetDayTimeVisionRange(this.Index);
		},
		get DisplayedUnitName() {
			return Entities.GetDisplayedUnitName(this.Index);
		},
		get EffectiveInvisibilityLevel() {
			return Entities.GetEffectiveInvisibilityLevel(this.Index);
		},
		get Forward() {
			return Vector.fromArray(Entities.GetForward(this.Index) || []);
		},
		// test (old api 'speed'?)
		get HasteFactor() {
			return Entities.GetHasteFactor(this.Index);
		},
		get HP() {
			return Entities.GetHealth(this.Index);
		},
		get HPPercent() {
			return Entities.GetHealthPercent(this.Index);
		},
		get HPRegen() {
			return Entities.GetHealthThinkRegen(this.Index);
		},
		get HullRadius() {
			return Entities.GetHullRadius(this.Index);
		},
		// test (IdealSpeed vs old api 'speed')
		get IdealSpeed() {
			return Entities.GetIdealSpeed(this.Index);
		},
		get IncreasedAttackSpeed() {
			return Entities.GetIncreasedAttackSpeed(this.Index);
		},
		get Level() {
			return Entities.GetLevel(this.Index);
		},
		get MagicalArmorValue() {
			return Entities.GetMagicalArmorValue(this.Index);
		},
		get Mana() {
			return Entities.GetMana(this.Index);
		},
		get ManaRegen() {
			return Entities.GetManaThinkRegen(this.Index);
		},
		get MaxHP() {
			return Entities.GetMaxHealth(this.Index);
		},
		get MaxMana() {
			return Entities.GetMaxMana(this.Index);
		},
		get MoveSpeedModifier() {
			return Entities.GetMoveSpeedModifier(this.Index, this.BaseMoveSpeed);
		},
		get NeededXPToLevel() {
			return Entities.GetNeededXPToLevel(this.Index);
		},
		get NightTimeVisionRange() {
			return Entities.GetNightTimeVisionRange(this.Index);
		},
		get NumBuffs() {
			return Entities.GetNumBuffs(this.Index);
		},
		get NumItemsInInventory() {
			return Entities.GetNumItemsInInventory(this.Index);
		},
		get NumItemsInStash() {
			return Entities.GetNumItemsInStash(this.Index);
		},
		get PaddedCollisionRadius() {
			return Entities.GetPaddedCollisionRadius(this.Index);
		},
		get PercentInvisible() {
			return Entities.GetPercentInvisible(this.Index);
		},
		get PhysicalArmorValue() {
			return Entities.GetPhysicalArmorValue(this.Index);
		},
		get PlayerID() {

			if (this._PlayerID !== undefined)
				return this._PlayerID;

			Object.defineProperty(this, "_PlayerID", {
				value: Entities.GetPlayerOwnerID(this.Index),
				configurable: false,
				writable: false,
				enumerable: false
			});

			return this._PlayerID;
		},
		get ProjectileCollisionSize() {
			return Entities.GetProjectileCollisionSize(this.Index);
		},
		get Right() {
			return Vector.fromArray(Entities.GetRight(this.Index) || []);
		},
		get RingRadius() {
			return Entities.GetRingRadius(this.Index);
		},
		get SecondsPerAttack() {
			return Entities.GetSecondsPerAttack(this.Index);
		},
		// test
		get SelectionGroup() {
			return Entities.GetSelectionGroup(this.Index);
		},
		get SoundSet() {
			return Entities.GetSoundSet(this.Index);
		},
		// test (MaskToArrayNumber ?)
		get States() {
			return Entities.GetStates(this.Index);
		},
		get TeamNumber() {
			return Entities.GetTeamNumber(this.Index);
		},
		get TotalDamageTaken() {
			return Entities.GetTotalDamageTaken(this.Index);
		},
		// test
		get TotalPurchasedUpgradeGoldCost() {
			return Entities.GetTotalPurchasedUpgradeGoldCost(this.Index);
		},
		get Up() {
			return Vector.fromArray(Entities.GetUp(this.Index) || []);
		},
		get HasAttackCapability() {
			return Entities.HasAttackCapability(this.Index);
		},
		get HasCastableAbilities() {
			return Entities.HasCastableAbilities(this.Index);
		},
		get HasFlyMovementCapability() {
			return Entities.HasFlyMovementCapability(this.Index);
		},
		get HasFlyingVision() {
			return Entities.HasFlyingVision(this.Index);
		},
		get HasGroundMovementCapability() {
			return Entities.HasGroundMovementCapability(this.Index);
		},
		get HasMovementCapability() {
			return Entities.HasMovementCapability(this.Index);
		},
		get HasScepter() {
			return Entities.HasScepter(this.Index);
		},
		get HasUpgradeableAbilities() {
			return Entities.HasUpgradeableAbilities(this.Index);
		},
		get HasUpgradeableAbilitiesThatArentMaxed() {
			return Entities.HasUpgradeableAbilitiesThatArentMaxed(this.Index);
		},
		get IsAlive() {
			return Entities.IsAlive(this.Index);
		},
		get IsAncient() {
			return Entities.IsAncient(this.Index);
		},
		get IsAttackImmune() {
			return Entities.IsAttackImmune(this.Index);
		},
		get IsBarracks() {
			return Entities.IsBarracks(this.Index);
		},
		get IsBlind() {
			return Entities.IsBlind(this.Index);
		},
		get IsBoss() {
			return Entities.IsBoss(this.Index)
		},
		get IsCommandRestricted() {
			return Entities.IsCommandRestricted(this.Index);
		},
		get IsConsideredHero() {
			return Entities.IsConsideredHero(this.Index);
		},
		get IsControllableByAnyPlayer() {
			return Entities.IsControllableByAnyPlayer(this.Index)
		},
		get IsCreepHero() {
			return Entities.IsCreepHero(this.Index);
		},
		get IsDeniable() {
			return Entities.IsDeniable(this.Index);
		},
		get IsDisarmed() {
			return Entities.IsDisarmed(this.Index);
		},
		get IsDominated() {
			return Entities.IsDominated(this.Index)
		},
		get IsEnemy() {
			return Entities.IsEnemy(this.Index);
		},
		get IsEvadeDisabled() {
			return Entities.IsEvadeDisabled(this.Index);
		},
		get IsFort() {
			return Entities.IsFort(this.Index);
		},
		get IsFrozen() {
			return Entities.IsFrozen(this.Index);
		},
		get IsGeneratedByEconItem() {
			return Entities.IsGeneratedByEconItem(this.Index)
		},
		get IsHallofFame() {
			return Entities.IsHallofFame(this.Index)
		},
		get IsHexed() {
			return Entities.IsHexed(this.Index);
		},
		get IsIllusion() {
			return Entities.IsIllusion(this.Index);
		},
		get IsInRangeOfFountain() {
			return Entities.IsInRangeOfFountain(this.Index);
		},
		get IsInventoryEnabled() {
			return Entities.IsInventoryEnabled(this.Index);
		},
		get IsInvisible() {
			return Entities.IsInvisible(this.Index);
		},
		get IsInvulnerable() {
			return Entities.IsInvulnerable(this.Index);
		},
		get IsLowAttackPriority() {
			return Entities.IsLowAttackPriority(this.Index);
		},
		get IsMagicImmune() {
			return Entities.IsMagicImmune(this.Index);
		},
		get IsMoving() {
			return Entities.IsMoving(this.Index);
		},
		get IsMuted() {
			return Entities.IsMuted(this.Index);
		},
		get IsNeutralUnitType() {
			return Entities.IsNeutralUnitType(this.Index);
		},
		get IsNightmared() {
			return Entities.IsNightmared(this.Index);
		},
		get IsOther() {
			return Entities.IsOther(this.Index);
		},
		get IsOutOfGame() {
			return Entities.IsOutOfGame(this.Index)
		},
		// test (is illusion)
		get IsOwnedByAnyPlayer() {
			return Entities.IsOwnedByAnyPlayer(this.Index)
		},
		get IsPhantom() {
			return Entities.IsPhantom(this.Index);
		},
		get IsRanged() {
			return Entities.IsRangedAttacker(this.Index);
		},
		get IsRealHero() {
			return Entities.IsRealHero(this.Index);
		},
		get IsRooted() {
			return Entities.IsRooted(this.Index);
		},
		get IsRoshan() {
			return Entities.IsRoshan(this.Index)
		},
		get IsSelectable() {
			return Entities.IsSelectable(this.Index);
		},
		get IsShop() {
			return Entities.IsShop(this.Index);
		},
		get IsSilenced() {
			return Entities.IsSilenced(this.Index);
		},
		get IsSpeciallyDeniable() {
			return Entities.IsSpeciallyDeniable(this.Index);
		},
		get IsStunned() {
			return Entities.IsStunned(this.Index);
		},
		get IsSummoned() {
			return Entities.IsSummoned(this.Index);
		},
		get IsUnselectable() {
			return Entities.IsUnselectable(this.Index);
		},
		get IsValid() {
			return Entities.IsValidEntity(this.Index);
		},
		get IsWard() {
			return Entities.IsWard(this.Index);
		},
		get IsZombie() {
			return Entities.IsZombie(this.Index);
		},
		// test
		get ManaFraction() {
			return Entities.ManaFraction(this.Index);
		},
		get NoHealthBar() {
			return Entities.NoHealthBar(this.Index);
		},
		get NoTeamMoveTo() {
			return Entities.NoTeamMoveTo(this.Index);
		},
		get NoTeamSelect() {
			return Entities.NoTeamSelect(this.Index);
		},
		get NoUnitCollision() {
			return Entities.NoUnitCollision(this.Index);
		},
		get NotOnMinimap() {
			return Entities.NotOnMinimap(this.Index);
		},
		get NotOnMinimapForEnemies() {
			return Entities.NotOnMinimapForEnemies(this.Index);
		},
		get PassivesDisabled() {
			return Entities.PassivesDisabled(this.Index);
		},
		get ProvidesVision() {
			return Entities.ProvidesVision(this.Index);
		},
		get UsesHeroAbilityNumbers() {
			return Entities.UsesHeroAbilityNumbers(this.Index);
		},

		/* ====== CUSTOM ====== */
		get ManaPercent() {
			return Math.floor(this.Mana / this.MaxMana * 100);
		},
		get NameJSON() {
			if (!setJSON(this))
				return this.Name;

			return this._JSON.workshop_guide_name || this.Name;
		},
		get IsControllable() {
			return Me !== undefined && this.IsControllableByPlayer(Me.PlayerID);
		},
		get IsIllusionHero() {
			if (!this.IsEnemy)
				return this.IsIllusion;

			var _this = this;
			return !ARRAY_SOME(EntityManager.Players(), function (player) {
				return player.HeroEntity === _this;
			});
		},
		get IsChanneling() {
			if (this.IsInventoryEnabled && ARRAY_SOME(this.Items, function (item) { return item.IsChanneling; }))
				return true;

			return ARRAY_SOME(this.Spells, function (spell) { return spell.IsChanneling; })
		},
		get IsInFadeTime() {
			return this.EffectiveInvisibilityLevel > 0;
		},
		// test: is illusion meppo and etc. (Player === undefined ?)
		get Player() {

			if (this._Player !== undefined)
				return this._Player;

			Object.defineProperty(this, "_Player", {
				value: EntityManager.GetPlayerByID(this.PlayerID),
				configurable: false,
				writable: false,
				enumerable: false
			});

			return this._Player;
		},
		get Spells() {
			var spells = [];
			for (var i = 0, len = /* this.AbilityCount */MAX_SKILLS; i < len; i++) {

				/** @type {Ability} */
				var abil = this.Ability(i);

				if (abil !== undefined && !abil.HasBehavior(DOTA_ABILITY_BEHAVIOR.DOTA_ABILITY_BEHAVIOR_ITEM))
					spells.push(abil);
			}
			return spells;
		},
		get Items() {
			var items = [];
			for (var i = 0; i < 6; i++) {

				/** @type {Item} */
				var item = this.Item(i);

				if (item !== undefined)
					items.push(item);
			}
			return items;
		},
		get Buffs() {
			var buffs = [];

			for (var i = 0, len = this.NumBuffs; i < len; i++)
				buffs.push(this.Buff(i));

			return buffs;
		},
		get CastRangeBonus() {
			var castRange = 0;

			var lens = this.ItemByName("item_aether_lens");
			if (lens !== undefined)
				castRange += lens.SpecialValueFor("cast_range_bonus");

			ARRAY_FOREACH(this.Spells, function (spell) {
				if (spell.Level > 0 && spell.Name.indexOf("special_bonus_cast_range") === 0)
					castRange += spell.SpecialValueFor("value");
			});
			return castRange
		},
		// TODO fix
		/* get SpellAmplification() {
			var spellAmp = 0;

			if (this.IsHero)
				spellAmp += this.GetIntelligence * 0.07 / 100; // https://dota2.gamepedia.com/Intelligence

			ARRAY_FOREACH(this.Items, function (item) {
				spellAmp += item.SpecialValueFor("spell_amp") / 100;
			});

			ARRAY_FOREACH(this.Spells, function (spell) {
				if (spell.Level > 0 && spell.Name.indexOf("special_bonus_spell_amplify") === 0)
					spellAmp += spell.SpecialValueFor("value") / 100;
			});

			return spellAmp;
		}, */
		get WorldToMiniMap() {
			return Drawing.WorldToMiniMap(this.Position);
		},
		/* === Protected === */
		get IsLinkensProtected() {
			/** @type {Item} */
			var linken = this.ItemByName("item_sphere");
			return (linken && linken.IsCooldownReady && !linken.IsMuted) || this.HasBuff("modifier_item_sphere_target");
		},
		get IsAeonProtected() {
			return this.HasBuff("modifier_item_aeon_disk_buff");
		},
		get IsCounterspellProtected() {
			return this.HasBuff("modifier_special_bonus_spell_block") ||
				this.HasBuff("modifier_antimage_counterspell");
		},
		get IsReflectingAbilities() {
			return this.HasBuff("modifier_item_lotus_orb_active")
				|| this.IsCounterspellProtected;
		},
		get HasAeonDisk() {
			/** @type {Item} */
			var item = this.ItemByName("item_aeon_disk");

			return item !== undefined && item.IsCooldownReady;
		},
		get TPScroll() {
			for (var i = 0; i < MAX_ITEMS; i++) {
				/** @type {Item} */
				var item = this.Item(i);

				if (item !== undefined && item.Name === "item_tpscroll")
					return item;
			}
		},
		/* ============= METHODS ============= */

		/* ====== DOTA ====== */
		CanAcceptTargetToAttack: function (target) {
			return Entities.CanAcceptTargetToAttack(
				target instanceof Entity ? target.Index : target, this.Index);
		},
		Ability: function (slot) {
			if (slot > MAX_SKILLS)
				return;

			var abil = EntityManager.EntityByIndex(Entities.GetAbility(this.Index, slot), Ability);

			if (abil !== undefined)
				abil.AbilitySlot = slot;

			return abil;
		},
		AbilityByName: function (name) {
			return EntityManager.EntityByIndex(Entities.GetAbilityByName(this.Index, name), Ability);
		},
		ArmorForDamageType: function (type) {
			return Entities.GetArmorForDamageType(this.Index, type);
		},
		ArmorReductionForDamageType: function (type) {
			return Entities.GetArmorReductionForDamageType(this.Index, type);
		},
		Buff: function (index) {
			return EntityManager.BuffByIndex(Entities.GetBuff(this.Index, index), this);
		},
		Item: function (slot) {
			if (slot > MAX_ITEMS)
				return;

			var item = EntityManager.EntityByIndex(Entities.GetItemInSlot(this.Index, slot), Item);

			if (item !== undefined)
				item.AbilitySlot = slot;

			return item;
		},
		RangeToUnit: function (target) {
			return Entities.GetRangeToUnit(this.Index,
				target instanceof Entity ? target.Index : target);
		},
		HasItemInInventory: function (name) {
			return Entities.HasItemInInventory(this.Index, name);
		},
		InState: function (state) {
			return Entities.InState(this.Index, state);
		},
		IsControllableByPlayer: function (player) {
			return Entities.IsControllableByPlayer(this.Index,
				player instanceof Player ? player.PlayerID : (player || Me.PlayerID));
		},
		IsEntityInRange: function (entity, range) {
			return Entities.IsEntityInRange(this.Index,
				entity instanceof Entity ? entity.Index : entity, range);
		},
		IsInRangeOfShop: function (iShopType, bSpecific) {
			return Entities.IsInRangeOfShop(this.Index, iShopType, bSpecific);
		},

		/* ====== CUSTOM ====== */

		/* ==== Orders ==== */

		UseSmartAbility: function (ability, target, checkToggled, queue, showEffects) {

			if (checkToggled && ability.HasBehavior(DOTA_ABILITY_BEHAVIOR.DOTA_ABILITY_BEHAVIOR_TOGGLE) && !ability.ToggleState)
				return this.CastToggle(ability, queue, showEffects);

			if (ability.HasBehavior(DOTA_ABILITY_BEHAVIOR.DOTA_ABILITY_BEHAVIOR_NO_TARGET))
				return this.CastNoTarget(ability, queue, showEffects);

			if (ability.HasBehavior(DOTA_ABILITY_BEHAVIOR.DOTA_ABILITY_BEHAVIOR_POINT)) {
				if (target instanceof Entity)
					target = target.Position;

				return this.CastPosition(ability, target, queue, showEffects);
			}

			if (ability.HasBehavior(DOTA_ABILITY_BEHAVIOR.DOTA_ABILITY_BEHAVIOR_UNIT_TARGET))
				return this.CastTarget(ability, target, showEffects);

			return false
		},
		MoveTo: function (position, queue, showEffects) {
			return Player.PrepareOrders({
				OrderType: dotaunitorder_t.DOTA_UNIT_ORDER_MOVE_TO_POSITION,
				UnitIndex: this,
				Position: position,
				QueueBehavior: queue,
				ShowEffects: showEffects
			});
		},
		MoveToTarget: function (target, queue, showEffects) {
			return Player.PrepareOrders({
				OrderType: dotaunitorder_t.DOTA_UNIT_ORDER_MOVE_TO_TARGET,
				UnitIndex: this,
				TargetIndex: target,
				QueueBehavior: queue,
				ShowEffects: showEffects
			});
		},
		AttackMove: function (position, queue, showEffects) {
			return Player.PrepareOrders({
				OrderType: dotaunitorder_t.DOTA_UNIT_ORDER_ATTACK_MOVE,
				UnitIndex: this,
				Position: position,
				QueueBehavior: queue,
				ShowEffects: showEffects
			});
		},
		AttackTarget: function (target, queue, showEffects) {
			return Player.PrepareOrders({
				OrderType: dotaunitorder_t.DOTA_UNIT_ORDER_ATTACK_TARGET,
				UnitIndex: this,
				TargetIndex: target,
				QueueBehavior: queue,
				ShowEffects: showEffects
			});
		},
		CastPosition: function (ability, position, queue, showEffects) {
			return Player.PrepareOrders({
				OrderType: dotaunitorder_t.DOTA_UNIT_ORDER_CAST_POSITION,
				UnitIndex: this,
				AbilityIndex: ability,
				Position: position,
				QueueBehavior: queue,
				ShowEffects: showEffects
			});
		},
		CastTarget: function (ability, target, queue, showEffects) {
			return Player.PrepareOrders({
				OrderType: dotaunitorder_t.DOTA_UNIT_ORDER_CAST_TARGET,
				UnitIndex: this,
				AbilityIndex: ability,
				TargetIndex: target,
				QueueBehavior: queue,
				ShowEffects: showEffects
			});
		},
		CastTargetTree: function (ability, tree, queue, showEffects) {
			return Player.PrepareOrders({
				OrderType: dotaunitorder_t.DOTA_UNIT_ORDER_CAST_TARGET_TREE,
				UnitIndex: this,
				AbilityIndex: ability,
				TargetIndex: tree,
				QueueBehavior: queue,
				ShowEffects: showEffects
			});
		},
		CastNoTarget: function (ability, queue, showEffects) {
			return Player.PrepareOrders({
				OrderType: dotaunitorder_t.DOTA_UNIT_ORDER_CAST_NO_TARGET,
				UnitIndex: this,
				AbilityIndex: ability,
				QueueBehavior: queue,
				ShowEffects: showEffects
			});
		},
		CastToggle: function (ability, queue, showEffects) {
			return Player.PrepareOrders({
				OrderType: dotaunitorder_t.DOTA_UNIT_ORDER_CAST_TOGGLE,
				UnitIndex: this,
				AbilityIndex: ability,
				QueueBehavior: queue,
				ShowEffects: showEffects
			});
		},
		HoldPosition: function (position, queue, showEffects) {
			return Player.PrepareOrders({
				OrderType: dotaunitorder_t.DOTA_UNIT_ORDER_HOLD_POSITION,
				UnitIndex: this,
				Position: position,
				QueueBehavior: queue,
				ShowEffects: showEffects
			});
		},
		TrainAbility: function (ability, queue, showEffects) {
			return Player.PrepareOrders({
				OrderType: dotaunitorder_t.DOTA_UNIT_ORDER_TRAIN_ABILITY,
				UnitIndex: this,
				AbilityIndex: ability,
				QueueBehavior: queue,
				ShowEffects: showEffects
			});
		},
		DropItem: function (item, position, queue, showEffects) {
			return Player.PrepareOrders({
				OrderType: dotaunitorder_t.DOTA_UNIT_ORDER_DROP_ITEM,
				UnitIndex: this,
				AbilityIndex: item,
				Position: position,
				QueueBehavior: queue,
				ShowEffects: showEffects
			});
		},
		GiveItem: function (item, target, queue, showEffects) {
			return Player.PrepareOrders({
				OrderType: dotaunitorder_t.DOTA_UNIT_ORDER_GIVE_ITEM,
				UnitIndex: this,
				AbilityIndex: item,
				TargetIndex: target,
				QueueBehavior: queue,
				ShowEffects: showEffects
			});
		},
		PickupItem: function (item, queue, showEffects) {
			return Player.PrepareOrders({
				OrderType: dotaunitorder_t.DOTA_UNIT_ORDER_PICKUP_ITEM,
				UnitIndex: this,
				TargetIndex: item,
				QueueBehavior: queue,
				ShowEffects: showEffects
			});
		},
		PickupRune: function (rune, queue, showEffects) {
			return Player.PrepareOrders({
				OrderType: dotaunitorder_t.DOTA_UNIT_ORDER_PICKUP_RUNE,
				UnitIndex: this,
				TargetIndex: rune,
				QueueBehavior: queue,
				ShowEffects: showEffects
			});
		},
		PurchaseItem: function (itemID, queue, showEffects) {
			return Player.PrepareOrders({
				OrderType: dotaunitorder_t.DOTA_UNIT_ORDER_PURCHASE_ITEM,
				UnitIndex: this,
				AbilityIndex: itemID,
				QueueBehavior: queue,
				ShowEffects: showEffects
			});
		},
		SellItem: function (item, queue, showEffects) {
			return Player.PrepareOrders({
				OrderType: dotaunitorder_t.DOTA_UNIT_ORDER_SELL_ITEM,
				UnitIndex: this,
				AbilityIndex: item,
				QueueBehavior: queue,
				ShowEffects: showEffects
			});
		},
		DisassembleItem: function (item, queue, showEffects) {
			return Player.PrepareOrders({
				OrderType: dotaunitorder_t.DOTA_UNIT_ORDER_DISASSEMBLE_ITEM,
				UnitIndex: this,
				AbilityIndex: item,
				QueueBehavior: queue,
				ShowEffects: showEffects
			});
		},
		MoveItem: function (item, slot, queue, showEffects) {
			return Player.PrepareOrders({
				OrderType: dotaunitorder_t.DOTA_UNIT_ORDER_CAST_TOGGLE_AUTO,
				UnitIndex: this,
				TargetIndex: item,
				AbilityIndex: slot,
				QueueBehavior: queue,
				ShowEffects: showEffects
			});
		},
		CastToggleAuto: function (item, queue, showEffects) {
			return Player.PrepareOrders({
				OrderType: dotaunitorder_t.DOTA_UNIT_ORDER_CAST_TOGGLE_AUTO,
				UnitIndex: this,
				AbilityIndex: item,
				QueueBehavior: queue,
				ShowEffects: showEffects
			});
		},
		OrderStop: function (queue, showEffects) {
			return Player.PrepareOrders({
				OrderType: dotaunitorder_t.DOTA_UNIT_ORDER_STOP,
				UnitIndex: this,
				QueueBehavior: queue,
				ShowEffects: showEffects
			});
		},
		UnitTaunt: function (queue, showEffects) {
			return Player.PrepareOrders({
				OrderType: dotaunitorder_t.DOTA_UNIT_ORDER_TAUNT,
				UnitIndex: this,
				QueueBehavior: queue,
				ShowEffects: showEffects
			});
		},
		ItemFromStash: function (item, queue, showEffects) {
			return Player.PrepareOrders({
				OrderType: dotaunitorder_t.DOTA_UNIT_ORDER_EJECT_ITEM_FROM_STASH,
				UnitIndex: this,
				AbilityIndex: item,
				QueueBehavior: queue,
				ShowEffects: showEffects
			});
		},
		CastRune: function (runeItem, queue, showEffects) {
			return Player.PrepareOrders({
				OrderType: dotaunitorder_t.DOTA_UNIT_ORDER_CAST_RUNE,
				UnitIndex: this,
				TargetIndex: runeItem,
				QueueBehavior: queue,
				ShowEffects: showEffects
			});
		},
		PingAbility: function (ability, queue, showEffects) {
			return Player.PrepareOrders({
				OrderType: dotaunitorder_t.DOTA_UNIT_ORDER_PING_ABILITY,
				UnitIndex: this,
				AbilityIndex: ability,
				QueueBehavior: queue,
				ShowEffects: showEffects
			});
		},
		MoveToDirection: function (position, queue, showEffects) {
			return Player.PrepareOrders({
				OrderType: dotaunitorder_t.DOTA_UNIT_ORDER_MOVE_TO_DIRECTION,
				UnitIndex: this,
				Position: position,
				QueueBehavior: queue,
				ShowEffects: showEffects
			});
		},
		Patrol: function (position, queue, showEffects) {
			return Player.PrepareOrders({
				OrderType: dotaunitorder_t.DOTA_UNIT_ORDER_PATROL,
				UnitIndex: this,
				Position: position,
				QueueBehavior: queue,
				ShowEffects: showEffects
			});
		},
		VectorTargetPosition: function (ability, direction, queue, showEffects) {
			return Player.PrepareOrders({
				OrderType: dotaunitorder_t.DOTA_UNIT_ORDER_VECTOR_TARGET_POSITION,
				UnitIndex: this,
				AbilityIndex: ability,
				Position: direction,
				QueueBehavior: queue,
				ShowEffects: showEffects
			});
		},
		CastVectorTargetPosition: function (ability, position, direction, queue, showEffects) {

			if (position instanceof Entity)
				position = position.Position;

			return this.VectorTargetPosition(ability, direction, queue, showEffects)
				&& this.CastPosition(ability, position, queue, showEffects);
		},
		ItemLock: function (item, state, queue, showEffects) {
			return Player.PrepareOrders({
				OrderType: dotaunitorder_t.DOTA_UNIT_ORDER_SET_ITEM_COMBINE_LOCK,
				UnitIndex: this,
				AbilityIndex: item,
				TargetIndex: +state,
				QueueBehavior: queue,
				ShowEffects: showEffects
			});
		},
		OrderContinue: function (item, queue, showEffects) {
			return Player.PrepareOrders({
				OrderType: dotaunitorder_t.DOTA_UNIT_ORDER_CONTINUE,
				UnitIndex: this,
				AbilityIndex: item,
				QueueBehavior: queue,
				ShowEffects: showEffects
			});
		},
		VectorTargetCanceled: function (position, queue, showEffects) {
			return Player.PrepareOrders({
				OrderType: dotaunitorder_t.DOTA_UNIT_ORDER_VECTOR_TARGET_CANCELED,
				UnitIndex: this,
				Position: position,
				QueueBehavior: queue,
				ShowEffects: showEffects
			});
		},

		/* ==== Geometry ==== */
		Distance: function (vec) {
			if (vec instanceof Entity)
				return this.Position.Distance(vec.Position);
			return this.Position.Distance(vec);
		},
		Distance2D: function (vec) {
			if (vec instanceof Entity)
				return this.Position.Distance2D(vec.Position);
			return this.Position.Distance2D(vec);
		},
		DistanceSquared: function (vec) {
			if (vec instanceof Entity)
				vec = vec.Position;
			return this.Position.DistanceSqr(vec);
		},
		DistanceSquared2D: function (vec) {
			if (vec instanceof Entity)
				vec = vec.Position;
			return this.Position.DistanceSqr2D(vec);
		},
		AngleBetweenFaces: function (front) {
			return this.Forward.AngleBetweenFaces(front)
		},
		InFront: function (distance) {
			return this.Position.RotationForThis(this.Forward, distance);
		},
		InFrontFromAngle: function (delta, distance) {
			return this.Position.InFrontFromAngle(this.Forward.Angle + delta, distance);
		},
		FindRotationAngle: function (vec) {
			if (vec instanceof Entity)
				vec = vec.Position

			return this.Position.FindRotationAngle(vec, this.RotationRad)
		},
		IsInRange: function (ent, range) {
			return this.DistanceSquared2D(ent) < range * range
		},
		Closest: function (ents) {
			var thisPos = this.Position

			var entity
			var distance = Number.POSITIVE_INFINITY

			ARRAY_FOREACH(ents, function (ent) {
				var tempDist = ent.Distance(thisPos)
				if (tempDist < distance) {
					distance = tempDist
					entity = ent
				}
			});

			return entity;
		},

		/* ==== Items | Spells ==== */
		HasAbility: function (name) {
			return Entities.GetAbilityByName(this.Index, name) !== -1;
		},
		ItemByName: function (name, includeBackpack) {
			if (!this.HasItemInInventory(name))
				return;

			var item, len = includeBackpack ? 9 : 6;

			for (var i = 0; i < len; i++) {
				item = this.Item(i);
				if (item !== undefined && item.Name === name)
					return item;
			}
		},
		HasItem: function (name, includeBackpack) {
			if (!this.HasItemInInventory(name))
				return;

			var item, len = includeBackpack ? 9 : 6;

			for (var i = 0; i < len; i++) {
				item = this.Item(i);
				if (item !== undefined && item.Name === name)
					return true;
			}
			return false;
		},
		GetItems: function (start, end) {
			start = Math.min(start, MAX_ITEMS)
			end = Math.min(end, MAX_ITEMS)

			var items = [];

			if (start > end)
				return items;

			for (var i = start; i < end; i++) {
				var item = this.Item(i);
				if (item !== undefined)
					items.push(item)
			}
			return items;
		},
		GetFreeSlots: function (start, end) {
			start = Math.min(start, MAX_ITEMS)
			end = Math.min(end, MAX_ITEMS)

			var items = [];

			if (start > end)
				return items;

			for (var i = start; i <= end; i++) {
				if (Entities.GetItemInSlot(this.Index, i) !== -1)
					items.push(i);
			}
			return items;
		},
		HasFreeSlots: function (start, end, howMany) {
			howMany = typeof howMany === "number" ? howMany : 0;

			if (start > MAX_ITEMS && start > end)
				return false;

			var many = 0
			for (var i = end + 1; i-- > start;) {
				if (i > MAX_ITEMS)
					break;

				if (Entities.GetItemInSlot(this.Index, i) === -1) {
					many++;
					if (many >= howMany)
						return true;
				}
			}
			return false;
		},
		CountItemByOtherPlayer: function (unit) {
			var count = 0;
			var item;

			for (var i = 0; i < 14; i++) {

				item = this.Item(i);
				if (item !== undefined && item.Owner === unit)
					count++;
			}
			return count;
		},
		BuffByName: function (name) {
			var buff;

			for (var i = 0, len = this.NumBuffs; i < len; i++) {
				buff = this.Buff(i);

				if (buff.Name === name)
					return buff;
			}
		},
		HasBuff: function (name) {
			for (var i = 0, len = this.NumBuffs; i < len; i++)
				if (this.Buff(i).Name === name)
					return true;
			return false;
		},
		/* ==== Other ==== */
		IsEnemyTo: function (entity) {
			return this.TeamNumber !== entity.TeamNumber;
		},
		AttackRangeBonus: function (entity) {
			return this.AttackRange + this.HullRadius + (entity ? entity.HullRadius : 0);
		},
		IsBlockingAbilities: function (checkReflecting) {
			if (checkReflecting === true && this.IsReflectingAbilities)
				return true;

			return this.IsLinkensProtected || this.IsAeonProtected;
		},
		/** 
		 * @param {Entity} target 
		 */
		CanAttack: function (target) {
			if (!this.IsAlive || this.IsDisarmed || this.IsInvulnerable || this.IsStunned
				|| this.IsAttackImmune)
				return false;

			if (target === undefined || !target.IsAlive || target.IsInvulnerable || target.IsDormant
				|| target.IsUnselectable || target.IsAttackImmune)
				return false;

			if (target.TeamNumber === Me.TeamNumber) {
				if (target.IsCreep)
					return target.HPPercent < 0.5

				if (target.IsHero)
					return target.IsDeniable && target.HPPercent < 0.25

				if (target.IsBuilding)
					return target.HPPercent < 0.1
			}

			return true
		},

		toString: function () {
			return this.Name;
		}
	}

	return Entity;
})();