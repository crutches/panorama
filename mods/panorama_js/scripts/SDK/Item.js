// REQUIRED: Ability

function Item(index) {
	Ability.call(this, index);

	this.AbilityTextureSF = Items.GetAbilityTextureSF(this.Index);
	this.Cost = Items.GetCost(this.Index);
	this.IsRecipe = Items.IsRecipe(this.Index);
	this.IsSellable = Items.IsSellable(this.Index);
	this.IsStackable = Items.IsStackable(this.Index);

	this.IsBottle = this.Name === "item_bottle";
	this.IsTango = this.Name.indexOf("item_tango") === 0;
	this.IsDagon = this.Name.indexOf("item_dagon") === 0;
}

Item.prototype = Object.create(Ability.prototype, {
	/* ============= GETTERS ============= */

	/* ====== DOTA ====== */
	AlwaysDisplayCharges: {
		get: function () {
			return Items.AlwaysDisplayCharges(this.Index);
		},
		enumerable: true
	},
	CanBeExecuted: {
		get: function () {
			return Items.CanBeExecuted(this.Index);
		},
		enumerable: true
	},
	CanBeSoldByLocalPlayer: {
		get: function () {
			return Items.CanBeSoldByLocalPlayer(this.Index);
		},
		enumerable: true
	},
	CanDoubleTapCast: {
		get: function () {
			return Items.CanDoubleTapCast(this.Index);
		},
		enumerable: true
	},
	ForceHideCharges: {
		get: function () {
			return Items.ForceHideCharges(this.Index);
		},
		enumerable: true
	},
	AssembledTime: {
		get: function () {
			return Items.GetAssembledTime(this.Index);
		},
		enumerable: true
	},
	CurrentCharges: {
		get: function () {
			return Items.GetCurrentCharges(this.Index);
		},
		enumerable: true
	},
	DisplayedCharges: {
		get: function () {
			return Items.GetDisplayedCharges(this.Index);
		},
		enumerable: true
	},
	InitialCharges: {
		get: function () {
			return Items.GetInitialCharges(this.Index);
		},
		enumerable: true
	},
	ItemColor: {
		get: function () {
			return Items.GetItemColor(this.Index);
		},
		enumerable: true
	},
	PurchaseTime: {
		get: function () {
			return Items.GetPurchaseTime(this.Index);
		},
		enumerable: true
	},
	SecondaryCharges: {
		get: function () {
			return Items.GetSecondaryCharges(this.Index);
		},
		enumerable: true
	},
	Shareability: {
		get: function () {
			return Items.GetShareability(this.Index);
		},
		enumerable: true
	},
	IsAlertableItem: {
		get: function () {
			return Items.IsAlertableItem(this.Index);
		},
		enumerable: true
	},
	IsCastOnPickup: {
		get: function () {
			return Items.IsCastOnPickup(this.Index);
		},
		enumerable: true
	},
	IsDisassemblable: {
		get: function () {
			return Items.IsDisassemblable(this.Index);
		},
		enumerable: true
	},
	IsDroppable: {
		get: function () {
			return Items.IsDroppable(this.Index);
		},
		enumerable: true
	},
	IsInnatelyDisassemblable: {
		get: function () {
			return Items.IsInnatelyDisassemblable(this.Index);
		},
		enumerable: true
	},
	IsKillable: {
		get: function () {
			return Items.IsKillable(this.Index);
		},
		enumerable: true
	},
	IsMutedItem: {
		get: function () {
			return Items.IsMuted(this.Index);
		},
		enumerable: true
	},
	IsPermanent: {
		get: function () {
			return Items.IsPermanent(this.Index);
		},
		enumerable: true
	},
	IsPurchasable: {
		get: function () {
			return Items.IsPurchasable(this.Index);
		},
		enumerable: true
	},
	IsRecipeGenerated: {
		get: function () {
			return Items.IsRecipeGenerated(this.Index);
		},
		enumerable: true
	},
	ProRatesChargesWhenSelling: {
		get: function () {
			return Items.ProRatesChargesWhenSelling(this.Index);
		},
		enumerable: true
	},
	RequiresCharges: {
		get: function () {
			return Items.RequiresCharges(this.Index);
		},
		enumerable: true
	},
	ShouldDisplayCharges: {
		get: function () {
			return Items.ShouldDisplayCharges(this.Index);
		},
		enumerable: true
	},
	ShowSecondaryCharges: {
		get: function () {
			return Items.ShowSecondaryCharges(this.Index);
		},
		enumerable: true
	},

	/* ====== CUSTOM ====== */

	Purchaser: {
		get: function () {

			if (this._Purchaser !== undefined)
				return this._Purchaser;

			Object.defineProperty(this, "_Purchaser", {
				value: EntityManager.EntityByIndex(Items.GetPurchaser(this.Index), Entity),
				configurable: false,
				writable: false,
				enumerable: false
			});

			return this._Purchaser;
		},
		enumerable: true
	},
	IsEmptyBottle: {
		get: function () {
			return this.IsBottle && this.CurrentCharges === 0;
		},
		enumerable: true
	},
	IsEmptyBottleRune: {
		get: function () {
			return this.IsBottle && (this.CurrentCharges < 3 || this.AbilityTextureSF === "bottle")
		},
		enumerable: true
	},

	/* ============= METHODS ============= */

	/* ====== DOTA ====== */

	/* ====== CUSTOM ====== */
	CanBeCasted: {
		value: function () {

			if (!Ability.prototype.CanBeCasted.apply(this, arguments))
				return false;

			return !this.IsMuted && this.RequiresCharges && this.CurrentCharges < 1;
		}
	},
});
Item.prototype.constructor = Item;