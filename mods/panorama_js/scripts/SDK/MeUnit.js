// REQUIRED: Entity
// REQUIRED: Events

var Me;

(function () {

	InGameEvents.on("AssignedHero", function (player, entity) {
		if (player.PlayerID === Game.GetLocalPlayerID())
			Me = new LocalUnit(player, entity.Index);
	});

	Events.on("GameEnded", function () {
		Me = undefined;
	})

	function LocalUnit(player, index) {
		Entity.call(this, index);

		Object.defineProperty(this, "_Player", {
			value: player,
			configurable: false,
			writable: false,
			enumerable: false
		});
		Object.defineProperty(this, "_PlayerID", {
			value: player.PlayerID,
			configurable: false,
			writable: false,
			enumerable: false
		});
		Object.defineProperty(this, "Entity", {
			value: EntityManager.EntityByIndex(index),
			configurable: false,
			writable: false,
			enumerable: false
		});
	}

	LocalUnit.prototype = Object.create(Entity.prototype, {
		ActiveAbility: {
			get: function () {
				return EntityManager.EntityByIndex(
					Abilities.GetLocalPlayerActiveAbility(this.Index), Item);
			},
			enumerable: true
		},
		HasItemByDefinition: {
			value: function (item) {
				return LocalInventory.HasItemByDefinition(item instanceof Item ? item.Index : item);
			}
		},
		// test
		SelectionEntities: {
			value: function () {
				var entities = [];

				var selection = Entities.GetSelectionEntities(this.Index);

				if (selection === undefined)
					return entities;

				ARRAY_FOREACH(selection, function (entIndex) {

					var entity = EntityManager.EntityByIndex(entIndex);

					if (entity !== undefined)
						entities.push(entity);
				});

				return entities;
			}
		},
		SelectUnit: {
			value: function (ent, addGroup) {
				ent = ent || this;
				GameUI.SelectUnit(ent instanceof Entity ? ent.Index : ent, addGroup || false);
			}
		},
		SelectUnits: {
			value: function (ents, addGroup) {
				for (var i = 0, len = ents.length; i < len; i++)
					this.SelectUnit(ents[i], addGroup || i === 0 ? false : true);

				return;
			}
		},
		/* DisassembleItem: {
			value: function (item, queue, showEffects) {
				
				if (queue === undefined && showEffects === undefined)
					return Items.LocalPlayerDisassembleItem(
						item instanceof Ability ? item.Index : item);
				
				Entity.prototype.DisassembleItem.apply(this, arguments);
			}
		}, */
		DropItemFromStash: {
			value: function (item) {
				var selected = this.Player.SelectedEntities;
				this.SelectUnit(this);

				var issued = Items.LocalPlayerDropItemFromStash(
					item instanceof Ability ? item.Index : item);

				this.SelectUnits(selected);

				return issued;
			}
		},
		// test (abilities too?)
		ItemAlertAllies: {
			value: function (item, queue, showEffects) {

				if (item instanceof Item && queue === undefined && showEffects === undefined)
					return Items.LocalPlayerItemAlertAllies(item.Handle);

				Entity.prototype.PingAbility.apply(this, arguments);
			}
		},
		MoveItemToStash: {
			value: function (item) {
				return Items.LocalPlayerMoveItemToStash(
					item instanceof Ability ? item.Handle : item);
			}
		},
		/* SellItem: {
			value: function (item, queue, showEffects) {
				
				if (queue === undefined && showEffects === undefined)
					return Items.LocalPlayerSellItem(item instanceof Ability ? item.Handle : item);
				
				Entity.prototype.SellItem.apply(this, arguments);
			}
		} */
	});
	LocalUnit.prototype.constructor = LocalUnit;
})();