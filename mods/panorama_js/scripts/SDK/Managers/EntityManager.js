// REQUIRED: Entity
// REQUIRED: MeUnit
// REQUIRED: Player
// REQUIRED: Events

var EntityManager = (function () {

	/** @type {Entity[]} */
	var AllEntities = [];
	/** @type {Map<number, Entity>} */
	var AllEntitiesAsMap = new Map();

	/** @type {Buff[]} */
	var AllBuffs = [];
	/** @type {Map<number, Buff>} */
	var AllBuffsAsMap = new Map();

	/** @type {Player[]} */
	var AllPlayers = [];
	/** @type {Map<number, Player>} */
	var AllPlayersAsMap = new Map();

	/** @type {number[]} */
	var allKilled = [];

	/**
	 * @param {Function} className 
	 */
	function AddToCache(index, className, lastEntity) {

		if (lastEntity !== undefined) {
			ARRAY_REMOVE(AllEntities, lastEntity);
			InGameEvents.emit("EntityKilled", false, lastEntity);
		}

		var entity;

		if (className !== undefined)
			entity = new className(index);
		else
			entity = DotaToSDKClassed(index);

		AllEntities.push(entity);
		AllEntitiesAsMap.set(index, entity);

		InGameEvents.emit("EntitySpawned", false, entity);

		return entity;
	}

	function DotaToSDKClassed(index) {

		var abilName = Abilities.GetAbilityName(index);

		if (abilName !== "") {

			if (Abilities.IsItem(index))
				return new Item(index);

			return new Ability(index);
		}

		return new Entity(index);
	}

	function RemoveFromCache(index) {

		var entity = AllEntitiesAsMap.get(index);

		if (entity === undefined)
			return undefined;

		AllEntitiesAsMap.delete(index);
		ARRAY_REMOVE(AllEntities, entity);

		InGameEvents.emit("EntityKilled", false, entity);

		return entity;
	}

	function AddToCachePlayer(playerID) {
		var player = new Player(playerID);

		AllPlayersAsMap.set(playerID, player);
		AllPlayers.push(player);

		InGameEvents.emit("PlayerLoaded", false, player);

		if (!player.IsSpectator) {
			Events.on("Tick", function WaitValidAssignHero() {

				var entityID = Players.GetPlayerHeroEntityIndex(playerID);

				if (entityID === -1)
					return;

				Events.removeListener("Tick", WaitValidAssignHero);

				EntityManager.EntityByIndex(entityID, Entity);
			});
		}

		return player;
	}

	NativeEvents.on("EntitySpawned", function (event) {
		var entID = event.entindex;

		var entity = AllEntitiesAsMap.get(entID);

		if (entity !== undefined) {
			EntityManager.EntityByIndex(entID);
			return;
		}

		InGameEvents.emit("EntitySpawned", false, EntityManager.EntityByIndex(entID));
	});

	NativeEvents.on("EntityKilled", function (event) {
		var entity = RemoveFromCache(event.entindex_killed);

		allKilled.push(event.entindex_killed);

		InGameEvents.emit("EntityKilled", false, entity);
	});

	NativeEvents.on("AssignedHero", function (event) {
		var player = EntityManager.GetPlayerByID(event.playerid)

		InGameEvents.emit("AssignedHero", false, player, player.HeroEntity);
	});

	Events.on("Tick", function () {

		var allEntities = Entities.GetAllEntities();

		{ // Update Entities

			ARRAY_FOREACH(AllEntities, function (ent) {
				ent.IsVisible = false;
				ent.IsDormant = true;
			});

			var gameTime = Game.GetGameTime();

			ARRAY_FOREACH(allEntities, function (entID) {

				if (ARRAY_INCLUDE(allKilled, entID))
					return;

				var entity = EntityManager.EntityByIndex(entID);

				entity.IsVisible = true;
				entity.IsDormant = false;

				entity.LastVisibleTime = gameTime;
			});
			ARRAY_FOREACH(allKilled, function (id, index) {
				if (id === undefined)
					return;

				if (!Entities.IsValidEntity(id))
					allKilled.splice(index, 1);
			})

		}

		{ // Update Players & assigned unit

			var allPlayerIDs = Game.GetAllPlayerIDs();

			if (AllPlayers.length !== allPlayerIDs.length) {

				ARRAY_FOREACH_PLUS(allPlayerIDs, function (playerID) {

					var player = AllPlayersAsMap.get(playerID);

					if (player !== undefined)
						return;

					AddToCachePlayer(playerID);
				});
			}
		}

		{ // Update Buffs

			ARRAY_FOREACH(AllBuffs, function (buff) {
				var owner = buff.Owner,
					ownerIndex = owner.Index;

				if (!owner.IsValid) {
					var entity = RemoveFromCache(ownerIndex);

					if (entity !== undefined)
						InGameEvents.emit("EntityKilled", false, entity);
				}

				if (!AllEntitiesAsMap.has(ownerIndex) || owner.Buffs.indexOf(buff) === -1) {
					ARRAY_REMOVE(AllBuffs, buff);
					AllBuffsAsMap.delete(HASH_PAIR(ownerIndex, buff.Index));

					InGameEvents.emit("BuffDestroyed", false, buff);
					return;
				}

				var stackCount = Buffs.GetStackCount(ownerIndex, buff.Index);

				if (buff.StackCount !== stackCount) {
					buff.StackCount = stackCount;
					InGameEvents.emit("BuffStackCountChanged", false, buff, stackCount);
				}
			});
		}
	});

	Events.on("GameStarted", function () {
		//
	});

	Events.on("GameEnded", function () {
		ARRAY_FOREACH(AllBuffs, function (buff) {
			InGameEvents.emit("BuffDestroyed", false, buff);
		});
		ARRAY_CLEAN(AllBuffs);
		AllBuffsAsMap.clear();

		ARRAY_FOREACH(AllEntities, function (ent) {
			InGameEvents.emit("EntityKilled", false, ent);
		});
		ARRAY_CLEAN(AllEntities);
		AllEntitiesAsMap.clear();

		ARRAY_CLEAN(AllPlayers);
		AllPlayersAsMap.clear();
	});

	return {
		get AllEntities() {
			return ARRAY_COPY(AllEntities);
		},
		/** @returns {Player} */
		get LocalPlayer() {
			return Me !== undefined ? Me.Player : undefined;
		},
		/** @returns {Entity} */
		get LocalHero() {
			return Me;
		},
		/** @returns {Player[]} */
		Players: function (team) {
			if (team === undefined)
				return ARRAY_COPY(AllPlayers);

			return ARRAY_FILTER_PLUS(AllPlayers, function (player) {
				return player.Team === team;
			});
		},
		/** @returns {Entity[]} */
		Heroes: function (team) {

			var heroes = [];

			if (team === undefined) {
				ARRAY_FOREACH_PLUS(AllPlayers, function (player) {
					var hero = player.HeroEntity;
					if (hero !== undefined)
						heroes.push(hero);
				});
			}
			else {
				ARRAY_FOREACH_PLUS(AllPlayers, function (player) {
					if (player.Team !== team)
						return;

					var hero = player.HeroEntity;
					if (hero !== undefined)
						heroes.push(hero);
				})
			}

			return heroes;
		},
		/** @returns {Entity} */
		EntityByIndex: function (index, className) {
			if (index <= -1)
				return undefined;

			var entity = AllEntitiesAsMap.get(index);

			if (entity === undefined)
				entity = AddToCache(index, className);

			else if (entity instanceof Ability) {
				if (entity.Name !== Abilities.GetAbilityName(index)) {
					entity = AddToCache(index, className, entity);
				}
			}
			else if (entity.Name !== Entities.GetUnitName(index)) {
				entity = AddToCache(index, className, entity);
			}

			return entity;
		},
		BuffByIndex: function (index, owner) {
			if (index <= -1)
				return undefined;

			var pair = HASH_PAIR(owner instanceof Entity ? owner.Index : owner, index);

			var buff = AllBuffsAsMap.get(pair);
			if (buff !== undefined) {
				if (buff.Name !== Buffs.GetName(owner instanceof Entity ? owner.Index : owner, index))
					$.Msg("BUFF CHANGED NAME: ", buff.Name, " => ", Buffs.GetName(owner instanceof Entity ? owner.Index : owner, index));

				return buff;
			}

			buff = new Buff(index, owner instanceof Entity ? owner : this.EntityByIndex(owner));

			AllBuffsAsMap.set(pair, buff);
			AllBuffs.push(buff);

			return buff;
		},
		/** @returns {Player} */
		GetPlayerByID: function (playerID) {
			if (playerID === -1)
				return undefined;

			var player = AllPlayersAsMap.get(playerID);
			if (player !== undefined)
				return player;

			return AddToCachePlayer(playerID);
		},

		AllByClass: function (className, classSDK) {

			var entities = [];

			if (Array.isArray(className)) {
				ARRAY_FOREACH_PLUS(className, function (className) {
					ARRAY_FOREACH(Entities.GetAllEntitiesByClassname(className), function (entIndex) {
						entities.push(EntityManager.EntityByIndex(entIndex, classSDK));
					});
				});
			}
			else {
				ARRAY_FOREACH(Entities.GetAllEntitiesByClassname(className), function (entIndex) {
					entities.push(EntityManager.EntityByIndex(entIndex, classSDK));
				});
			}

			return entities;
		},
		AllByName: function (className, classSDK) {

			var entities = [];

			if (Array.isArray(className)) {
				ARRAY_FOREACH_PLUS(className, function (className) {
					ARRAY_FOREACH(Entities.GetAllEntitiesByName(className), function (entIndex) {
						entities.push(EntityManager.EntityByIndex(entIndex, classSDK));
					});
				});
			}
			else {
				ARRAY_FOREACH(Entities.GetAllEntitiesByName(className), function (entIndex) {
					entities.push(EntityManager.EntityByIndex(entIndex, classSDK));
				});
			}

			return entities;
		},
		get AllRunes() {
			return this.AllByClass("dota_item_rune");
		},
		get AllBuildings() {
			var entities = [];

			ARRAY_FOREACH(Entities.GetAllBuildingEntities(), function (entIndex) {
				entities.push(EntityManager.EntityByIndex(entIndex));
			});

			return entities;
		},
	}
})();

(function () {

	var currentlyHero;
	var currentlyHeroIndex = -1;

	/** @type {MenuTitle} */
	var currentlyHeroTitle;
	/** @type {MenuTitle} */
	var allHeroesTitle;

	InGameEvents.on("AssignedHero", function (player, unit) {
		if (player.PlayerID !== Game.GetLocalPlayerID())
			return;

		currentlyHero = Menu.FindControl("Heroes", unit.NameJSON);

		if (currentlyHero === undefined) {
			currentlyHeroTitle.Remove();
			allHeroesTitle.Remove();
			return;
		}

		currentlyHeroIndex = currentlyHero.Index;

		var heroes = Menu.Factory("Heroes");

		currentlyHeroTitle.ChangeParentTo(heroes);
		currentlyHeroTitle.Index = 0;

		allHeroesTitle.ChangeParentTo(heroes);
		allHeroesTitle.Index = 1;

		currentlyHero.Index = 1;
	});

	Events.on("GameEnded", function () {
		currentlyHeroTitle.Remove();
		allHeroesTitle.Remove();

		if (currentlyHero === undefined)
			return;

		currentlyHero.Index = currentlyHeroIndex;
		currentlyHero = undefined;
	})

	Events.on("ReloadScriptsEnded", function () {
		currentlyHeroTitle = Menu.FindControl("Heroes", "Current Hero");
		allHeroesTitle = Menu.FindControl("Heroes", "All Heroes");

		currentlyHeroTitle.Remove();
		allHeroesTitle.Remove();
	});
})();