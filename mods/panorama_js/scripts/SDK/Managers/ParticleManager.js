// REQUIRED: Particle

var ParticleManager = (function () {
	
	/** @type {Map<string|number, Particle>} */
	var allParticles = new Map();
	
	const PATHS = {
		RANGE: "panorama/crutches/particles/range_display/range_display_",
		LINE: "panorama/crutches/particles/range_line/",
		TARGET: "panorama/crutches/particles/target/range_finder_tower_aoe.vpcf"
	};
	
	function DrawCircleSwitchRender(render) {
		switch (render) {
			default:
			case 0: return "normal";
			case 1: return "rope";
			case 2: return "animation";
		}
	}
	function DrawBoundingAreaRender(render) {
		switch (render) {
			default:
			case 0: return "normal";
			case 1: return "rope";
		}
	}
	
	Events.on("ReloadScripts", function () {
		allParticles.forEach(function (particle) {
			particle.Destroy();
		});
	});
	
	Events.on("GameEnded", function () {
		allParticles.clear();
	});
	
	return {
		AddOrUpdate: function (name, unit, path, attachment) {
			
			if (name instanceof Entity)
				name = name.Index;
			
			var particle = allParticles.get(name);
			
			if (particle === undefined) {
				
				var particle = Object.create(Particle.prototype);
				Particle.apply(particle, arguments);
				allParticles.set(name, particle)
			}
			else {
				
				if (particle.Entity !== unit || particle.Path !== path || particle.Attachment !== attachment) {
				
					particle.Destroy();
					var particle = Object.create(Particle.prototype);
					Particle.apply(particle, arguments);
					allParticles.set(name, particle)
					return particle;
				}
				
				var particlesControls = Array.prototype.slice.call(
					ARGUMENTS_INLINE_SLICE(arguments),
					this.AddOrUpdate.length);
				
				var hash = Particle.ControlPointsHashCode(particlesControls);
				
				if (hash !== particle.GetControlPointsHashCode())
					particle.SetControlPoints.apply(particle, particlesControls);
			}
			
			return particle;
		},
		Destroy: function (name) {
			
			if (name instanceof Entity)
				name = name.Index;
			
			var particle = allParticles.get(name);
			
			if (particle === undefined)
				return;
			
			particle.Destroy();
			allParticles.delete(name);
		},
		
		DrawCircle: function (name, unit, range, options) {
			
			if (options !== undefined) {
				return this.AddOrUpdate(name, unit,
					PATHS.RANGE + DrawCircleSwitchRender(options.Render || 0) + ".vpcf",
					options.Attachment || ParticleAttachment_t.PATTACH_ABSORIGIN_FOLLOW,
					0, options.Position || unit, 			// Position
					1, range || 50,							// Range
					2, options.Color || [0, 255, 255],		// Color
					3, options.Width || 10,					// Width
					4, options.Alpha || 255					// Alpha
				);
			}
			
			return this.AddOrUpdate(name, unit,
				PATHS.RANGE + DrawCircleSwitchRender(0) + ".vpcf",
				ParticleAttachment_t.PATTACH_ABSORIGIN_FOLLOW,
				0, unit, 				// Position
				1, range || 50,			// Range
				2, [0, 255, 255],		// Color
				3, 10,					// Width
				4, 255					// Alpha
			);
			
		},
		DrawSelectedRing: function (name, unit, range, position, color) {
			
			return this.AddOrUpdate(unit, name,
				"particles/ui_mouseactions/drag_selected_ring.vpcf",
				ParticleAttachment_t.PATTACH_ABSORIGIN,
				0, position || unit,
				1, color || [0, 255, 255],
				2, range * 1.1 || 50
			);	
		},
		DrawLine: function (name, unit, endPosition, options) {

			if (options !== undefined) {
				return this.AddOrUpdate(name, unit,
					PATHS.LINE + "line.vpcf",
					ParticleAttachment_t.PATTACH_ABSORIGIN,
					0, options.Position || unit,			// Position
					1, endPosition, 						// End Position
					2, options.Color || [0, 255, 255],		// Color
					3, options.Width || 10,					// Width
					4, options.Alpha || 255					// Alpha
				);
			}
			
			return this.AddOrUpdate(name, unit,
				PATHS.LINE + "line.vpcf",
				ParticleAttachment_t.PATTACH_ABSORIGIN,
				0, unit,				// Position
				1, endPosition, 		// End Position
				2, [0, 255, 255],		// Color
				3, 10,					// Width
				4, 255					// Alpha
			);
		},
		DrawRangeLine: function (name, unit, endPosition, red, startPos) {

			return this.AddOrUpdate(name, unit,
				"particles/ui_mouseactions/range_finder_line.vpcf",
				ParticleAttachment_t.PATTACH_ABSORIGIN,
				0, startPos || unit.Position,						// Position
				1, !red ? startPos || unit.Position : endPosition, 	// Red ?
				2, endPos											// End Position
			);
		},
		DrawLineToTarget: function (name, unit, target, color) {

			if (color instanceof Color)
				color.SetR(Math.max(color.x, 1));

			return this.AddOrUpdate(name, target,
				ParticlesPath.TARGET + "range_finder_tower_aoe.vpcf",
				ParticleAttachment_t.PATTACH_INVALID,
				2, unit,
				6, color || 255,
				7, target
			);
		},
		DrawBoundingArea: function (name, unit, endPosition, startPos, options) {
			
			if (options !== undefined) {

				return this.AddOrUpdate(name, unit,
					PATHS.LINE + "bounding_area_view_" + DrawBoundingAreaRender(options.Render) + ".vpcf",
					ParticleAttachment_t.PATTACH_ABSORIGIN,
					0, options.Position || unit,			// Position
					1, endPosition,		 					// End Position
					2, options.Color || [0, 255, 0],		// Color
					3, options.Width || 10,					// Width
					4, options.Alpha || 255					// Alpha
				);
			}
			
			return this.AddOrUpdate(name, unit,
				PATHS.LINE + "bounding_area_view_" + DrawBoundingAreaRender(0) + ".vpcf",
				ParticleAttachment_t.PATTACH_ABSORIGIN,
				0, startPos || unit,	// Position
				1, endPosition,			// End Position
				2, [0, 255, 0],			// Color
				3, 10,					// Width
				4, 255					// Alpha
			);
		}
	}
})();