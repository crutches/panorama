// REQUIRED: Entity

function Ability(index) {
	Entity.call(this, index);

	this.TextureName = Abilities.GetAbilityTextureName(this.Index);
	this.IsAttributeBonus = Abilities.IsAttributeBonus(this.Index);
	this.IsItem = Abilities.IsItem(this.Index);
	this.Name = Abilities.GetAbilityName(this.Index);

	this.IsActivated = Abilities.IsActivated(this.Index);
	this.IsStolen = Abilities.IsStolen(this.Index);

	this.AbilitySlot = -1;
}
Ability.prototype = Object.create(Entity.prototype, {
	/* ============= GETTERS ============= */

	/* ====== DOTA ====== */
	AbilityReady: {
		get: function () {
			return Abilities.AbilityReady(this.Index);
		},
		enumerable: true
	},
	// test
	CanAbilityBeUpgraded: {
		get: function () {
			return Abilities.CanAbilityBeUpgraded(this.Index);
		},
		enumerable: true
	},
	CanBeExecuted: {
		get: function () {
			return Abilities.CanBeExecuted(this.Index);
		},
		enumerable: true
	},
	CanLearn: {
		get: function () {
			return Abilities.CanLearn(this.Index);
		},
		enumerable: true
	},
	Radius: {
		get: function () {
			return Abilities.GetAOERadius(this.Index);
		},
		enumerable: true
	},
	Damage: {
		get: function () {
			return Abilities.GetAbilityDamage(this.Index);
		},
		enumerable: true
	},
	DamageType: {
		get: function () {
			return Abilities.GetAbilityDamageType(this.Index);
		},
		enumerable: true
	},
	TargetFlags: {
		get: function () {
			return Abilities.GetAbilityTargetFlags(this.Index);
		},
		enumerable: true
	},
	TargetTeam: {
		get: function () {
			return Abilities.GetAbilityTargetTeam(this.Index);
		},
		enumerable: true
	},
	TargetType: {
		get: function () {
			return Abilities.GetAbilityTargetType(this.Index);
		},
		enumerable: true
	},
	Type: {
		get: function () {
			return Abilities.GetAbilityType(this.Index);
		},
		enumerable: true
	},
	// test
	AssociatedPrimaryAbilities: {
		get: function () {
			return Abilities.GetAssociatedPrimaryAbilities(this.Index);
		},
		enumerable: true
	},
	// test
	AssociatedSecondaryAbilities: {
		get: function () {
			return Abilities.GetAssociatedSecondaryAbilities(this.Index);
		},
		enumerable: true
	},
	AutoCastState: {
		get: function () {
			return Abilities.GetAutoCastState(this.Index);
		},
		enumerable: true
	},
	BackswingTime: {
		get: function () {
			return Abilities.GetBackswingTime(this.Index);
		},
		enumerable: true
	},
	Behavior: {
		get: function () {
			return Abilities.GetBehavior(this.Index);
		},
		enumerable: true
	},
	CastPoint: {
		get: function () {
			return Abilities.GetCastPoint(this.Index);
		},
		enumerable: true
	},
	BaseCastRange: {
		get: function () {
			return Abilities.GetCastRange(this.Index);
		},
		enumerable: true
	},
	ChannelStartTime: {
		get: function () {
			return Abilities.GetChannelStartTime(this.Index);
		},
		enumerable: true
	},
	ChannelTime: {
		get: function () {
			return Abilities.GetChannelTime(this.Index);
		},
		enumerable: true
	},
	ChannelledManaCostPerSecond: {
		get: function () {
			return Abilities.GetChannelledManaCostPerSecond(this.Index);
		},
		enumerable: true
	},
	Cooldown: {
		get: function () {
			return Abilities.GetCooldown(this.Index);
		},
		enumerable: true
	},
	CooldownLength: {
		get: function () {
			return Abilities.GetCooldownLength(this.Index);
		},
		enumerable: true
	},
	CooldownTime: {
		get: function () {
			return Abilities.GetCooldownTime(this.Index);
		},
		enumerable: true
	},
	CooldownTimeRemaining: {
		get: function () {
			return Abilities.GetCooldownTimeRemaining(this.Index);
		},
		enumerable: true
	},
	CurrentCharges: {
		get: function () {
			return Abilities.GetCurrentCharges(this.Index);
		},
		enumerable: true
	},
	Duration: {
		get: function () {
			return Abilities.GetDuration(this.Index);
		},
		enumerable: true
	},
	EffectiveLevel: {
		get: function () {
			return Abilities.GetEffectiveLevel(this.Index);
		},
		enumerable: true
	},
	HeroLevelRequiredToUpgrade: {
		get: function () {
			return Abilities.GetHeroLevelRequiredToUpgrade(this.Index);
		},
		enumerable: true
	},
	HotkeyOverride: {
		get: function () {
			return Abilities.GetHotkeyOverride(this.Index);
		},
		enumerable: true
	},
	IntrinsicModifierName: {
		get: function () {
			return Abilities.GetIntrinsicModifierName(this.Index);
		},
		enumerable: true
	},
	Keybind: {
		get: function () {
			return Abilities.GetKeybind(this.Index);
		},
		enumerable: true
	},
	Level: {
		get: function () {
			return Abilities.GetLevel(this.Index);
		},
		enumerable: true
	},
	ManaCost: {
		get: function () {
			return Abilities.GetManaCost(this.Index);
		},
		enumerable: true
	},
	MaxLevel: {
		get: function () {
			return Abilities.GetMaxLevel(this.Index);
		},
		enumerable: true
	},
	SharedCooldownName: {
		get: function () {
			return Abilities.GetSharedCooldownName(this.Index);
		},
		enumerable: true
	},
	ToggleState: {
		get: function () {
			return Abilities.GetToggleState(this.Index);
		},
		enumerable: true
	},
	UpgradeBlend: {
		get: function () {
			return Abilities.GetUpgradeBlend(this.Index);
		},
		enumerable: true
	},
	HasScepterUpgradeTooltip: {
		get: function () {
			return Abilities.HasScepterUpgradeTooltip(this.Index);
		},
		enumerable: true
	},
	IsActivatedChanging: {
		get: function () {
			return Abilities.IsActivatedChanging(this.Index);
		},
		enumerable: true
	},
	IsAutocast: {
		get: function () {
			return Abilities.IsAutocast(this.Index);
		},
		enumerable: true
	},
	// test (native vs this.Cooldown === 0)
	IsCooldownReady: {
		get: function () {
			return Abilities.IsCooldownReady(this.Index);
		},
		enumerable: true
	},
	IsDisplayedAbility: {
		get: function () {
			return Abilities.IsDisplayedAbility(this.Index);
		},
		enumerable: true
	},
	IsHidden: {
		get: function () {
			return Abilities.IsHidden(this.Index);
		},
		enumerable: true
	},
	IsHiddenWhenStolen: {
		get: function () {
			return Abilities.IsHiddenWhenStolen(this.Index);
		},
		enumerable: true
	},
	IsInAbilityPhase: {
		get: function () {
			return Abilities.IsInAbilityPhase(this.Index);
		},
		enumerable: true
	},
	IsMarkedAsDirty: {
		get: function () {
			return Abilities.IsMarkedAsDirty(this.Index);
		},
		enumerable: true
	},
	IsMuted: {
		get: function () {
			return Abilities.IsMuted(this.Index);
		},
		enumerable: true
	},
	IsOnCastbar: {
		get: function () {
			return Abilities.IsOnCastbar(this.Index);
		},
		enumerable: true
	},
	IsOnLearnbar: {
		get: function () {
			return Abilities.IsOnLearnbar(this.Index);
		},
		enumerable: true
	},
	IsOwnersGoldEnough: {
		get: function () {
			return Abilities.IsOwnersGoldEnough(this.Index);
		},
		enumerable: true
	},
	IsOwnersGoldEnoughForUpgrade: {
		get: function () {
			return Abilities.IsOwnersGoldEnoughForUpgrade(this.Index);
		},
		enumerable: true
	},
	IsOwnersManaEnough: {
		get: function () {
			return Abilities.IsOwnersManaEnough(this.Index);
		},
		enumerable: true
	},
	IsPassive: {
		get: function () {
			return Abilities.IsPassive(this.Index);
		},
		enumerable: true
	},
	IsRecipe: {
		get: function () {
			return Abilities.IsRecipe(this.Index);
		},
		enumerable: true
	},
	IsSharedWithTeammates: {
		get: function () {
			return Abilities.IsSharedWithTeammates(this.Index);
		},
		enumerable: true
	},
	IsStealable: {
		get: function () {
			return Abilities.IsStealable(this.Index);
		},
		enumerable: true
	},
	IsToggle: {
		get: function () {
			return Abilities.IsToggle(this.Index);
		},
		enumerable: true
	},

	/* ====== CUSTOM ====== */
	IsShow: {
		get: function () {
			if (this.Owner === undefined || this.IsAttributeBonus || this.AbilitySlot >= 10)
				return false;

			switch (this.Owner.Name) {

				case "npc_dota_hero_invoker":
					if (!this.IsDisplayedAbility
						&& this.HasBehavior(DOTA_ABILITY_BEHAVIOR.DOTA_ABILITY_BEHAVIOR_NOT_LEARNABLE))
						return;

					return this.IsActivated && this.IsDisplayedAbility;

				case "npc_dota_hero_doom_bringer":
				case "npc_dota_hero_rubick":
					return this.IsDisplayedAbility
						&& !this.HasBehavior(DOTA_ABILITY_BEHAVIOR.DOTA_ABILITY_BEHAVIOR_NOT_LEARNABLE);

				default: return this.IsActivated && this.IsDisplayedAbility;
			}
		},
		enumerable: true
	},
	Owner: {
		get: function () {

			if (this._Owner !== undefined)
				return this._Owner;

			Object.defineProperty(this, "_Owner", {
				value: EntityManager.EntityByIndex(Abilities.GetCaster(this.Index), Entity),
				configurable: false,
				writable: false,
				enumerable: false
			});

			return this._Owner;
		},
		enumerable: true
	},
	// test (|| => &&)
	IsChanneling: {
		get: function () {
			return (this.IsInAbilityPhase || this.ChannelStartTime > 0);
		},
		enumerable: true
	},
	CastRange: {
		get: function () {
			var owner = this.Owner,
				castrange = this.BaseCastRange;

			switch (this.Name) {
				case "skywrath_mage_concussive_shot": {
					var unique = owner.AbilityByName("special_bonus_unique_skywrath_4");
					if (unique !== undefined && unique.Level > 0)
						return Number.MAX_SAFE_INTEGER;

					break;
				}
				case "gyrocopter_call_down": {
					var unique = owner.AbilityByName("special_bonus_unique_gyrocopter_5");
					if (unique !== undefined && unique.Level > 0)
						return Number.MAX_SAFE_INTEGER;

					break;
				}
				case 'lion_impale': {
					var unique = owner.AbilityByName("special_bonus_unique_lion_2");
					if (unique !== undefined && unique.Level > 0)
						castrange += unique.SpecialValueFor("value")

					break;
				}
			}

			return castrange + (owner !== undefined ? owner.CastRangeBonus : 0);
		},
		enumerable: true
	},

	/* ============= METHODS ============= */

	/* ====== DOTA ====== */
	AttemptToUpgrade: {
		value: function () {
			return Abilities.AttemptToUpgrade(this.Index);
		}
	},
	CreateDoubleTapCastOrder: {
		value: function () {

			var selected = Me.Player.SelectedEntities;

			Me.SelectUnit(this.Owner);

			Abilities.CreateDoubleTapCastOrder(this.Index, this.Owner.Index);

			Me.SelectUnits(selected);
		}
	},
	ExecuteAbility: {
		value: function (quickCast) {
			return Abilities.ExecuteAbility(this.Index, this.Owner.Index, quickCast || false);
		}
	},
	CustomValueFor: {
		value: function (name) {
			return Abilities.GetCustomValueFor(this.Index, name);
		}
	},
	IsCosmetic: {
		value: function (target) {
			return Abilities.IsCosmetic(this.Index, target instanceof Entity ? target.Index : target);
		}
	},
	LevelSpecialValueFor: {
		value: function (name, lvl) {
			if (this._LevelSpecial === undefined) {
				Object.defineProperty(this, "_LevelSpecial", {
					value: new Map(),
					writable: false,
					configurable: false,
					enumerable: false
				});
			}

			var cache = this._LevelSpecial.get(name);

			if (cache === undefined) {
				cache = this._LevelSpecial[name] = [];

				for (var i = 0; i < this.MaxLevel; i++)
					cache[i] = Abilities.GetLevelSpecialValueFor(this.Index, name, i);

				this._LevelSpecial.set(name, cache);
			}

			return cache[lvl - 1];
		}
	},
	SpecialValueFor: {
		value: function (name) {
			if (this._Special === undefined) {
				Object.defineProperty(this, "_Special", {
					value: {},
					writable: false,
					configurable: false,
					enumerable: false
				});
			}

			return this._Special[name] || (this._Special[name] = Abilities.GetSpecialValueFor(this.Index, name));
		}
	},
	PingAbility: {
		value: function () {
			Abilities.PingAbility(this.Index);
			// this.Owner.PingAbility(this);
		}
	},

	/* ====== CUSTOM ====== */
	UseAbility: {
		value: function (target, checkToggled, queue, showEffects) {
			return this.Owner.UseSmartAbility(this, target, checkToggled, queue, showEffects);
		}
	},
	UpgradeAbility: {
		value: function () {
			return this.Owner.TrainAbility(this);
		}
	},
	HasBehavior: {
		value: function (flag) {
			return HAS_MASK(this.Behavior, flag);
		}
	},
	HasTargetFlags: {
		value: function (flag) {
			return HAS_MASK(this.TargetFlags, flag);
		}
	},
	HasTargetTeam: {
		value: function (flag) {
			return HAS_MASK(this.TargetTeam, flag);
		}
	},
	HasTargetType: {
		value: function (flag) {
			return HAS_MASK(this.TargetType, flag);
		}
	},
	IsManaEnough: {
		value: function (bonusMana) {
			return (this.Owner.Mana + bonusMana) >= this.ManaCost
		}
	},
	CanBeCasted: {
		value: function (bonusMana) {
			bonusMana = bonusMana || 0;

			if (this.HasBehavior(DOTA_ABILITY_BEHAVIOR.DOTA_ABILITY_BEHAVIOR_TOGGLE) && this.ToggleState)
				return false;

			return this.Level > 0
				&& !this.Owner.IsSilenced
				&& this.IsManaEnough(bonusMana)
				&& this.IsCooldownReady
		}
	}
});
Ability.prototype.constructor = Ability;