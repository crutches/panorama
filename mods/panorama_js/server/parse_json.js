// @ts-check
const {
	existsSync,
	readFileSync,
	ensureFileSync,
	writeFileSync,
} = require("fs-extra");
const { join, basename, extname, normalize } = require('path');
const VPK = require("vpk");
const vdf = require('@node-steam/vdf');

const { DOTA_PATH } = LoadConfig(join(process.cwd(), "config.json"));

const PATH_TO_DOTADIR = join(DOTA_PATH, "game", "dota");
const PATH_TO_VPK = join(PATH_TO_DOTADIR, "pak01_dir.vpk");

let filesVPK = [
	"scripts/npc/neutral_items.txt",
	"scripts/npc/items.txt"
]

let fileDota = [
	//"/scripts/npc/npc_abilities.txt",
	"/scripts/npc/npc_heroes.txt",
	"/scripts/npc/npc_units.txt"
]

function LoadConfig(src) {
	try {

		if (!existsSync(src))
			throw Error();

		return JSON.parse(readFileSync(src).toString());

	} catch (err) {
		throw console.error(`Config '${src}' is not found! Try 'npm i' again`);
	}
}

function ParseFromVPK(output) {

	let vpk = new VPK(PATH_TO_VPK);

	vpk.load();

	filesVPK.forEach(file => {

		let txtFile = vpk.getFile(file);

		if (!txtFile)
			return console.warn(`${file} not found`);

		let fileName = basename(file);

		let jsonFile = vdf.parse(txtFile.toString());

		saveJSONToFile(output, fileName, jsonFile);
	});
}

function ParseFromDota(output) {
	fileDota.forEach(file => {

		var fullPath = join(PATH_TO_DOTADIR, normalize(file));

		var txtFile = readFileSync(fullPath);

		if (!txtFile)
			return console.warn(`${file} not found`);

		let fileName = basename(file);

		let jsonFile = vdf.parse(txtFile.toString());

		saveJSONToFile(output, fileName, jsonFile);
	});
}


function saveJSONToFile(output, fileName, data) {

	var cleanData;

	for (var name in data) {
		cleanData = data[name];
		break;
	}

	delete cleanData.Version;

	var fullOutput = join(output, basename(fileName, extname(fileName)) + '.json');

	ensureFileSync(fullOutput);
	writeFileSync(fullOutput, JSON.stringify(cleanData, null, "\t"));

	console.log(`parsed: ${fileName}`);
}

module.exports = function (output) {

	console.log("parsing JSON files..");

	ParseFromVPK(output);
	ParseFromDota(output);
}