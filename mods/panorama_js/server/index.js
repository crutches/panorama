// @ts-check
const {
	readJSONSync,
	readJSON,
	ensureFile,
	readFile,
	outputJson,
	pathExists,
	readdir,
	lstat,
} = require('fs-extra');
const { join, basename, normalize } = require('path');
const glob = require('fast-glob');

const CONFIG_SERVER = readJSONSync(join(__dirname, "config.json"));

const DIR_SCRIPTS = join(__dirname, normalize(CONFIG_SERVER.scripts_src));

const PATH_SETTINGS = join(__dirname, "user", "settings.json");

const JSON_DIR = join(__dirname, "user", "json");

var userConfig = readJSONSync(join(__dirname, "user", "config.json"));

/* ===========  =========== */

const REQUEST_TYPE = {
	GET: 1,
	SAVE_CONFIG: 2,
	CHECK_VERSION: 3,
	UPDATE_ONLINE: 4,
	CHECK_LAUNCHER: 5,
	CHECK_SERVER: 6
}

const RESPONSE_TYPE = {
	OK: 1,
	ERROR: 2,
	BADVERSION: 3,
	CONFIG: 4,
	SERVER_UPDATING: 5,
	SERVER_OFFLINE: 6
}

/* =========== Get JSON =========== */

require("./parse_json")(JSON_DIR);

/* ===========  =========== */

/*
	data: {
		type: REQUEST_TYPE
		version
		settings
		steam_id
		player_info: 
	}
*/
function pack(str) {
	var bytes = [];
	for (var i = 0, n = str.length; i < n; i++) {
		var char = str.charCodeAt(i);
		bytes.push(char >>> 3, char & 0xFF);
	}
	return bytes;
}

async function FindScripts() {

	let scripts = {
		sdk: {}
	};
	
	const files = await glob("**/*.js", { cwd: DIR_SCRIPTS, absolute: true });
	
	let promises = files.map(async file => {
		var fileBuffer = await readFile(file);

		var name = basename(file, ".js");
		var packScript = pack(fileBuffer.toString());

		if (file.includes("/SDK"))
			scripts.sdk[name] = packScript;
		else
			scripts[name] = packScript;
	});

	await Promise.all(promises);
	
	return scripts;
}

async function FindJSON() {
	
	const files = await glob("**/*.json", { cwd: JSON_DIR, absolute: true });
	
	var json = {};
	
	let promises = files.map(async file => {
		var fileBuffer = await readFile(file);

		var name = basename(file, ".json");

		json[name] = fileBuffer.toString();
	});

	await Promise.all(promises);

	return json;
}

module.exports = async (req, res) => {

	let headers = req.headers;

	if (headers === undefined
		|| headers["user-agent"] === undefined
		|| !headers["user-agent"].includes("Valve/Steam HTTP Client")) {

		return res.sendStatus(404)
	}

	var data = req.body;

	console.log(data);

	try {

		switch (parseInt(data.type)) {
			case REQUEST_TYPE.GET:
				let existsSettings = await pathExists(PATH_SETTINGS);

				let settings = {};

				if (existsSettings)
					settings = await readJSON(PATH_SETTINGS, { throws: false }) || {};
			
				let scripts = await FindScripts();
				let json = await FindJSON();
				
				res.json({
					type: RESPONSE_TYPE.OK,
					scripts, json, settings,
					os: process.platform,
					arch: process.arch
				});
				break;

			case REQUEST_TYPE.SAVE_CONFIG:
				outputJson(PATH_SETTINGS, JSON.parse(data.settings), { spaces: "\t" })
				res.json({ type: RESPONSE_TYPE.OK });
				break;

			case REQUEST_TYPE.CHECK_VERSION:

				if (userConfig.version !== parseInt(data.version)) {

					res.json({
						type: RESPONSE_TYPE.BADVERSION,
						version: userConfig.version
					});
					return;
				}

				res.json({ type: RESPONSE_TYPE.OK });
				break;

			case REQUEST_TYPE.UPDATE_ONLINE:
			case REQUEST_TYPE.CHECK_LAUNCHER:
			case REQUEST_TYPE.CHECK_SERVER:
				res.json({ type: RESPONSE_TYPE.OK });
				break;

			default:
				res.sendStatus(404);
				break;
		}

	} catch (e) {
		console.warn(e);

		res.json({
			type: RESPONSE_TYPE.ERROR,
			message: e.stack || e.message
		});
	}
}