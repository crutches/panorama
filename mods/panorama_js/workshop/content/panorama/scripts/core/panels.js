var Drawing = (function () {

	var GameSize_Width = 0,
		GameSize_Height = 0;

	/** @type {Panel} */
	var dotaHud;

	var MAP_LEFT = -8068;
	var MAP_BOTTOM = -7521;
	var MAP_RIGHT = 7933;
	var MAP_TOP = 7679;

	var MAP_WIDTH = Math.abs(MAP_LEFT - MAP_RIGHT);
	var MAP_HEIGHT = Math.abs(MAP_BOTTOM - MAP_TOP);

	var MiniMapScreenPosition = new Vector();
	var MiniMapSize = new Vector();
	var MiniMapScale = new Vector();
	var MiniMapRight = false;

	new Thread(3, function () {

		var GameSize_Width_New = Game.GetScreenWidth();

		var GameSize_Height_New = Game.GetScreenHeight();

		if (GameSize_Width !== GameSize_Width_New || GameSize_Height !== GameSize_Height_New) {
			GameSize_Width = GameSize_Width_New;
			GameSize_Height = GameSize_Height_New;
			Events.emit("ResolutionChanges", false, GameSize_Width, GameSize_Height)
		}

		if (Game.IsInGame) {

			dotaHud = Game.InGameDotaHUD.FindChildTraverse("Hud");
			var miniMapContainer = Game.InGameDotaHUD.FindChildTraverse("minimap_container");

			if (Panels.IsValid(dotaHud) && Panels.IsValid(miniMapContainer)) {
				var miniMapBlock = Game.InGameDotaHUD.FindChildTraverse("minimap_block");

				MiniMapSize.SetVector(
					miniMapBlock.actuallayoutwidth,
					miniMapBlock.actuallayoutheight);

				MiniMapRight = dotaHud.BHasClass("HUDFlipped");

				MiniMapScreenPosition.SetVector(
					MiniMapRight ? GameSize_Width - MiniMapSize.x : 0,
					GameSize_Height - MiniMapSize.y);

				MiniMapScale.SetVector(MiniMapSize.x / MAP_WIDTH, MiniMapSize.y / MAP_HEIGHT);
			}
		}

	}, true).Start();

	return {
		get GameWidth() {
			return GameSize_Width;
		},
		get GameHeight() {
			return GameSize_Height;
		},
		get GameSize() {
			return new Vector(GameSize_Width, GameSize_Height);
		},

		VectorToScreen: function (vec) {
			/*
			this.x = this.x / GameSize_Width * 100;
			this.y = this.y / GameSize_Height * 100;
			this.z = 0;
			*/
			return vec.DivideScalarX(GameSize_Width)
				.DivideScalarY(GameSize_Height)
				.MultiplyScalarForThis(100)
				.SetZ(0);
		},
		VectorToPanelPercent: function (vec, panel, changeVisible) {
			if (vec.x < 0 || vec.y < 0 || vec.x > 100 || vec.y > 100 || !isFinite(vec.x) || !isFinite(vec.y)) {
				if (changeVisible)
					panel.visible = false;
				return;
			}

			if (!panel.visible && changeVisible)
				panel.visible = true;

			panel.style.position = vec.x + "% " + vec.y + "% " + vec.z + "%";
		},
		VectorToPanelPx: function (vec, panel, changeVisible) {
			if (vec.x < 0 || vec.y < 0 || vec.x > GameSize_Width || vec.y > GameSize_Height || !isFinite(vec.x) || !isFinite(vec.y)) {
				if (changeVisible)
					panel.visible = false;
				return
			}

			if (!panel.visible && changeVisible)
				panel.visible = true;

			panel.style.position = vec.x + "px " + vec.y + "px " + vec.z + "px";
		},
		WorldToScreenXY: function (sourceVec, offsetVec) {

			var _sourceVecX = sourceVec.x;

			if (offsetVec !== undefined) {

				sourceVec.x = Game.WorldToScreenX(
					_sourceVecX + offsetVec.x,
					sourceVec.y + offsetVec.y,
					sourceVec.z + offsetVec.z
				);

				sourceVec.y = Game.WorldToScreenY(
					_sourceVecX + offsetVec.x,
					sourceVec.y + offsetVec.y,
					sourceVec.z + offsetVec.z
				);
			}
			else {
				sourceVec.x = Game.WorldToScreenX(_sourceVecX, sourceVec.y, sourceVec.z);
				sourceVec.y = Game.WorldToScreenY(_sourceVecX, sourceVec.y, sourceVec.z);
			}

			return Drawing.VectorToScreen(sourceVec);
		},
		WorldToMiniMap: function (worldPos, self) {

			var x = Math.min(Math.max((worldPos.x - MAP_LEFT) * MiniMapScale.x, 0), MiniMapSize.x);
			var y = Math.min(Math.max((worldPos.y - MAP_BOTTOM) * MiniMapScale.y, 0), MiniMapSize.y);

			if (MiniMapRight)
				x = GameSize_Width + x - MiniMapSize.x;

			x = Math.floor(x);
			y = Math.floor(GameSize_Height - y);

			return self ? worldPos.SetVector(x, y) : new Vector(x, y);
		},
		Notify: function (panel, delayRemove) {
			if (!panel)
				throw new Error("panel in not defined");

			var notifyPanel = Game.InGameHUD.FindChildTraverse('NotifyPanel');

			if (!notifyPanel) {
				notifyPanel = $.CreatePanel('Panel', Game.InGameHUD, 'NotifyPanel');
				notifyPanel.BLoadLayout("file://{resources}/layout/custom_game/core/notify.xml", false, false);
				notifyPanel.BLoadLayoutSnippet("MainPanel");
			}

			var count = notifyPanel.GetChildCount();

			if (count >= 5) {

				count -= 5;

				if (count > 0) {
					for (var i = count; i--;)
						notifyPanel.GetChild(i).DeleteAsync(0);
				} else
					notifyPanel.GetChild(0).DeleteAsync(0);
			}

			var noty = $.CreatePanel('Panel', notifyPanel, count + 1);

			noty.BLoadLayoutSnippet("ChildPanel");

			noty.BCreateChildren(panel);

			noty.style.opacity = "1.0";
			noty.style.transform = "none";

			var children = noty.Children();

			var notify = {
				get Panel() {
					return noty;
				},
				get Children() {
					return children;
				},
				Remove: function () {

					if (notifyPanel.IsValid() && notifyPanel.GetChildCount() === 1) {
						notifyPanel.DeleteAsync(0);
						return;
					}

					if (noty.IsValid())
						noty.DeleteAsync(0);
				}
			}

			if (delayRemove !== false)
				$.Schedule(typeof delayRemove === "number" ? delayRemove : 5,
					notify.Remove);

			return notify;
		},
		TextNearMouse: (function () {

			/** @type {Panel} */
			var textAroundMousePanel = undefined;

			var started = false;

			var mousePos = new Vector();
			var lastPos = new Vector();

			var defaultColor = new Color(255, 171, 15);
			var lastColor = Color.CopyFrom(defaultColor);

			function UpdateCursor() {

				if (!textAroundMousePanel.IsValid())
					return;

				mousePos.CopyFromArray(GameUI.GetCursorPosition());

				mousePos.AddScalarX(30).AddScalarY(15);

				mousePos.x = Math.min(Math.max(mousePos.x, 0), GameSize_Width - textAroundMousePanel.actuallayoutwidth);
				mousePos.y = Math.min(Math.max(mousePos.y, 0), GameSize_Height - textAroundMousePanel.actuallayoutheight);

				Drawing.VectorToScreen(mousePos);

				if (!lastPos.Equals(mousePos)) {
					lastPos.CopyFrom(mousePos);

					textAroundMousePanel.style.position = mousePos.x + "% " + mousePos.y + "% " + mousePos.z + "%";
				}
			}

			function Delete() {
				if (textAroundMousePanel === undefined || !textAroundMousePanel.IsValid())
					return;

				Events.removeListener("Update", UpdateCursor);

				if (textAroundMousePanel.IsValid())
					textAroundMousePanel.DeleteAsync(0);

				textAroundMousePanel = undefined;
				started = false;
				lastPos.Invalidate();
				lastColor.CopyFrom(defaultColor);
			}

			Events.on("ReloadScripts", Delete, true);

			return {
				CreateOrUpdate: function (text, color) {

					if (textAroundMousePanel === undefined || !textAroundMousePanel.IsValid()) {
						textAroundMousePanel = $.CreatePanel("Label", Game.InGameHUD, "PanelsAroundMouse");

						textAroundMousePanel.BLoadLayoutFromString("\
							<root>\
								<Label hittest='false' html='true' style='font-size:16px;font-weight:bold;'/>\
							</root>\
						", false, false);
					}

					if (color === undefined || !lastColor.Equals(color)) {

						if (color === undefined)
							color = Color.CopyFrom(defaultColor);

						textAroundMousePanel.style.color = "rgb(" + color.r + "," + color.g + "," + color.b + ")";

						lastColor.CopyFrom(color);
					}

					textAroundMousePanel.text = text;

					if (started)
						return;

					Events.on("Update", UpdateCursor, true);
					started = true;
				},
				Delete: Delete
			}
		})()
	};
})();


var Panels = (function () {

	function getRandomIDPanel() {
		var rand = RANDOM_INTEGER(1, 999999);

		return Game.InGameHUD.FindChildTraverse(rand) !== null
			? getRandomIDPanel()
			: rand;
	}

	return {
		/** @argument {Panel} panel */
		IsValid: function (panel) {
			return panel.IsValid()
				&& panel.actualxoffset <= Number.MAX_SAFE_INTEGER
				&& panel.actualyoffset <= Number.MAX_SAFE_INTEGER
				&& panel.actualxoffset <= Number.MAX_SAFE_INTEGER
				&& panel.actualyoffset <= Number.MAX_SAFE_INTEGER;
		},
		DeletePanels: function (panels) {
			if (panels === undefined)
				return;

			if (panels.IsValid !== undefined) {
				if (panels.IsValid())
					panels.DeleteAsync(0.0);
			}
			// In scripts Map instanceof Map is true, but not here..
			// FUCKING VALVE.
			else if (panels.constructor !== undefined && panels.constructor.name === "Map") {
				panels.forEach(function (panel) {
					Panels.DeletePanels(panel);
				});
			}
			else if (typeof panels === "object" || Array.isArray(panels)) {
				for (var i in panels)
					Panels.DeletePanels(panels[i]);
			}
		},
		MovePanel: function (panel, type, panelMove) {

			var events = new EventEmitter();

			var positionPanel = new Vector();
			var lastPosition = new Vector();
			var mouseInPanel = new Vector();
			var mousePos = new Vector();

			var mouseOut = true;

			function MovePanel() {

				var isDown = GameUI.IsMouseDown(0);

				if (!panel.IsValid() || (type === 1 && !isDown) || (type !== 1 && isDown)) {

					Events.removeListener("Update", MovePanel);

					events.emit("MoveEnded", false, positionPanel);

					if (type !== 1) {
						mouseOut = false;
						$.Schedule(0.5, function () {
							mouseOut = true
						})
					}
					// @ts-ignore
					else Events.on("Update", MovePanelWaitOver, true);

					return;
				}

				mousePos.CopyFromArray(GameUI.GetCursorPosition());

				positionPanel.CopyFrom(mousePos.SubtractForThis(mouseInPanel));

				positionPanel.x = Math.min(Math.max(positionPanel.x, 0), Drawing.GameWidth - panel.actuallayoutwidth);
				positionPanel.y = Math.min(Math.max(positionPanel.y, 0), Drawing.GameHeight - panel.actuallayoutheight);

				Drawing.VectorToScreen(positionPanel);

				positionPanel.x = TRUNCATED(positionPanel.x, 2);
				positionPanel.y = TRUNCATED(positionPanel.y, 2);

				if (positionPanel.IsValid && !lastPosition.Equals(positionPanel)) {

					lastPosition.CopyFrom(positionPanel);

					panel.style.position = positionPanel.x + "% " + positionPanel.y + "% " + positionPanel.z + "%";

					events.emit("Move", false, positionPanel);
				}
			}

			function Start() {
				if (!panel.visible || !mouseOut)
					return;

				mouseOut = true;

				positionPanel.SetVector(panel.actualxoffset, panel.actualyoffset);

				events.emit("MoveStart", false, positionPanel);

				mousePos.CopyFromArray(GameUI.GetCursorPosition());

				mouseInPanel.CopyFrom(mousePos.SubtractForThis(positionPanel));
				// @ts-ignore
				Events.on("Update", MovePanel, true);
			}

			if (type === 1) {

				function MovePanelWaitOver() {
					if (!GameUI.IsMouseDown(0))
						return;

					Events.removeListener("Update", MovePanelWaitOver);
					Start();
				}

				(panelMove || panel).SetPanelEvent('onmouseover', function () {
					// @ts-ignore
					Events.on("Update", MovePanelWaitOver, true);
				});
				(panelMove || panel).SetPanelEvent('onmouseout', function () {
					Events.removeListener("Update", MovePanelWaitOver);
					Events.removeListener("Update", MovePanel);
				});
			}
			else {
				panel.SetPanelEvent('onmouseactivate', function () {
					if (type === 2 && !GameUI.IsControlDown())
						return;
					Start();
				});
			}

			return events;
		},
		WorldToScreen: function (pos, offset, panel, changeVisible) {
			Drawing.WorldToScreenXY(pos, offset);
			Drawing.VectorToPanelPercent(pos, panel, changeVisible);
		},
		MiniMap: function (image, pos, size, delayRemove) {

			if (size === undefined)
				size = new Vector(22, 22);

			else if (typeof size === "number")
				size = new Vector(size, size);

			var panel = $.CreatePanel("Image", Game.InGameHUD, getRandomIDPanel());

			panel.BLoadLayoutFromString("<root>\
				<Image style='visibility: collapse;width: " + size.x + "px;height:" + size.y + "px;background-repeat: no-repeat;' />\
			</root>", false, false);

			panel.SetImage("s2r://panorama/images/" + image + ".vtex");

			var visible = false;

			var miniMap = {
				get IsValid() {
					return panel !== undefined && panel.IsValid();
				},
				get Position() {
					return pos;
				},
				set Position(value) {
					if (panel === undefined || !panel.IsValid())
						return;

					pos = value;

					Drawing.WorldToMiniMap(pos, true);
					pos.SubtractScalarX(size.x / 2).SubtractScalarY(size.y / 2);
					Drawing.VectorToScreen(pos);

					if (!pos.IsValid)
						return;

					panel.style.position = pos.x + "% " + pos.y + "% " + pos.z + "%";

					if (visible === false)
						panel.visible = visible = true;
				},
				Remove: function () {
					if (panel === undefined || !panel.IsValid())
						return;

					panel.DeleteAsync(0);
					panel = undefined;
				}
			}

			if (typeof delayRemove === "number")
				$.Schedule(delayRemove, miniMap.Remove);

			if (pos !== undefined)
				miniMap.Position = pos.Clone();

			return miniMap;
		},
		/**
		 * @param {Object} parent
		 * @param {string} func Name function to run
		 */
		RunFunc: function (func, parent) {
			parent = parent || $.GetContextPanel();

			parent.BCreateChildren('<Panel id="DOTARunFuncHidden" onload="' + func + '" />');
			parent.FindChildTraverse('DOTARunFuncHidden').DeleteAsync(0.0);
		}
	}
})();