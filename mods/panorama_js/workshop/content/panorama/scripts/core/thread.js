function Thread(delay, callback, isCoreContext) {

	delay = typeof delay !== "number" ? 1 / 30 : delay;

	isCoreContext = isCoreContext || false;

	var canStart = true;
	var updating = false;
	var started = false;

	var _this = this;

	function Update() {
		if (!updating || !canStart) {
			started = false;

			if (canStart)
				_this.emit("Stop");
			return;
		}

		_this.emit("Update");

		$.Schedule(delay, Update);
	}

	Object.defineProperty(this, 'IsStarted', {
		get: function () {
			return started;
		},
		enumerable: true
	});

	this.Start = function () {
		if (!canStart)
			return;
		updating = true;

		if (!started) {
			_this.emit("Start");

			Update();
		}
		return _this;
	}

	this.Stop = function () {
		updating = false;
		return _this;
	}

	this.UpdateState = function (state) {
		return state ? _this.Start() : _this.Stop();
	}

	this.SetDelay = function (value) {
		delay = value;
		return _this;
	}

	EventEmitter.call(this);

	if (typeof callback === 'function')
		this.on("Update", callback, isCoreContext);

	if (!isCoreContext) {
		Events.on("ReloadScripts", function ReloadRemove() {
			Events.removeListener("ReloadRemove", ReloadRemove);
			canStart = false;
		}, true);
	}
}

Thread.prototype = Object.create(EventEmitter.prototype);
Thread.prototype.constructor = Thread;

function ThreadCallback(delay, callback, startAfterDelay) {
	var delay = delay === undefined ? 1 / 30 : delay;

	var canStart = true;
	var updating = false;
	var started = false;

	var _this = this;

	function Update() {
		if (!updating || !canStart) {
			started = false;
			return;
		}

		callback()
			.then(function (newDelay) {
				if (newDelay !== undefined && delay !== newDelay)
					delay = newDelay;

				$.Schedule(delay, Update);
			})
			.catch(function (err) {
				$.Msg(err);
				_this.Stop();
			});
	}

	Object.defineProperty(this, 'IsStarted', {
		get: function () {
			return updating;
		},
		enumerable: true
	});

	this.Start = function () {
		updating = true;

		if (!started) {

			if (startAfterDelay) {
				$.Schedule(delay, function () {
					started = true;
					Update();
				})
			}
			else {
				started = true;
				Update();
			}
		}
		return _this;
	}

	this.Stop = function () {
		updating = false;
		return _this;
	}

	this.UpdateState = function (state) {
		return state ? _this.Start() : _this.Stop();
	}

	this.SetDelay = function (value) {
		delay = value;
		return _this;
	}
}