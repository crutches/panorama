var Color = (function () {

	function Color(r, g, b, a) {
		this.r = r || 0;
		this.g = g || 0;
		this.b = b || 0;
		this.a = a || 255;
	}

	Color.prototype = {
		
		/* ================== Getters ================== */
		get IsValid() {
			var x = this.x,
				y = this.y,
				z = this.z

			return !Number.isNaN(x) && Number.isFinite(x)
				&& !Number.isNaN(y) && Number.isFinite(y)
				&& !Number.isNaN(z) && Number.isFinite(z)
		},
		
		/* ================== Methods ================== */
		Equals: function (col) {
			return this.r === col.r
				&& this.g === col.g
				&& this.b === col.b
				&& this.a === col.a;
		},
		Invalidate: function () {
			this.x = this.y = this.z = NaN
			return this
		},
		/**
		 * Set Color by numbers
		 */
		SetColor: function (r, g, b, a) {
			this.r = r;
			this.g = g;
			this.b = b;
			this.a = a;
			return this;
		},
		/**
		 * Set R of color by number
		 */
		SetR: function (r) {
			this.r = r;
			return this;
		},
		/**
		 * Set G of color by number
		 */
		SetG: function (g) {
			this.g = g;
			return this;
		},
		/**
		 * Set B of color by number
		 */
		SetB: function (b) {
			this.b = b;
			return this;
		},
		/**
		 * Set A of color by number
		 */
		SetA: function (a) {
			this.a = a;
			return this;
		},

		CopyTo: function (color) {
			color.r = this.r;
			color.g = this.g;
			color.b = this.b;
			color.a = this.a;
			return color;
		},
		CopyFrom: function (color) {
			this.r = color.r;
			this.g = color.g;
			this.b = color.b;
			this.a = color.a;
			return this;
		},
		Clone: function () {
			return new Color(this.r, this.g, this.b, this.a);
		},
		/* ================== To ================== */
		GetHashCode: function () {
			var hash = this.r;
			hash = (hash * 397) ^ this.g;
			hash = (hash * 397) ^ this.b;
			hash = (hash * 397) ^ this.a;
			return hash;
		},

		/**
		 * Color to String Color
		 * @return Color(r,g,b,a)
		 */
		toString: function () {
			return "Color(" + this.r + "," + this.g + "," + this.b + "," + this.a + ")";
		},
		/**
		 * @return [r, g, b, a]
		 */
		toArray: function (count) {
			if (count === undefined)
				return [this.r, this.g, this.b];

			if (typeof count !== "number" || count <= 0)
				return [];

			if (count === 1)
				return [this.r];

			if (count === 2)
				return [this.r, this.g];

			if (count === 3)
				return [this.r, this.g, this.b];

			return [this.r, this.g, this.b, this.a];
		}
	}

	Object.defineProperties(Color, {
		CopyFrom: {
			value: function (color) {
				return new Color(color.r, color.g, color.b, color.a);
			}
		},

		Black: {
			get: function () {
				return new Color(0, 0, 0)
			}
		},
		Red: {
			get: function () {
				return new Color(255);
			}
		},
		Green: {
			get: function () {
				return new Color(0, 255);
			}
		},
		Blue: {
			get: function () {
				return new Color(0, 0, 255);
			}
		},
		Yellow: {
			get: function () {
				return new Color(255, 255);
			}
		},
		Orange: {
			get: function () {
				return new Color(255, 128);
			}
		},
		Fuchsia: {
			get: function () {
				return new Color(255, 0, 255);
			}
		},
		Aqua: {
			get: function () {
				return new Color(0, 255, 255);
			}
		},
		White: {
			get: function () {
				return new Color(255, 255, 255);
			}
		}
	});

	return Color;
})();

