/* ================================== DOTA ================================== */

Game.AutoAcceptMatch = false;
Game.AutoAcceptMatchTime = 0;

if ($.Schedule_original === undefined) {
	$.Schedule_original = $.Schedule
	$.Schedule = function (delay, callback) {
		if (isNaN(delay) || !isFinite(delay) || delay === undefined)
			throw "$.Schedule -> invalid delay: " + delay;
		return $.Schedule_original(delay, callback)
	}
}

if (Game.ExecuteCmd === undefined) {
	Object.defineProperty(Game, "ExecuteCmd", {
		value: function (conVar) {
			Game.CreateCustomKeyBind("", conVar);
		},
		configurable: false,
		writable: false
	});
}
if (Game.Commands === undefined) {
	Object.defineProperty(Game, "Commands", {
		value: {},
		configurable: false,
		writable: false
	});
}

if (Game.global === undefined) {
	var global = this;
	Object.defineProperty(Game, "global", {
		get: function () {
			return OBJECT_ASSIGN({}, global)
		},
		set: function () { },
		configurable: false
	});
}

if (Game.IsInGame === undefined) {
	Object.defineProperty(Game, "IsInGame", {
		get: function () {
			var gameState = Game.GetState();
			return gameState === DOTA_GameState.DOTA_GAMERULES_STATE_GAME_IN_PROGRESS
				|| gameState === DOTA_GameState.DOTA_GAMERULES_STATE_PRE_GAME
				|| gameState === DOTA_GameState.DOTA_GAMERULES_STATE_POST_GAME;
		},
		set: function () { },
		configurable: false
	})
}

if (Game.CursorToWorld === undefined) {
	(function () {
		var vector;
		Object.defineProperty(Game, "CursorToWorld", {
			value: function () {
				if (vector === undefined)
					vector = new Vector();

				var pos = GameUI.GetScreenWorldPosition(GameUI.GetCursorPosition());
				if (pos !== null)
					vector.CopyFromArray(pos);

				return vector;
			},
			configurable: false,
			writable: false
		});


	})();
}

if (Game.NeutralSpots === undefined) {
	(function () {
		var spots = [];
		Object.defineProperty(Game, "NeutralSpots", {
			get: function () {

				if (spots.length === 0) {
					spots = [
						/* ====== Rediant ====== */

						// Top
						[new Vector(-5166, 106), new Vector(-4289, -635)],
						[new Vector(-4154, 1085), new Vector(-3475, 197)],
						[new Vector(-3363, -197), new Vector(-2464, -1018)],   // Ancient
						// Bot
						[new Vector(-2225, -3803), new Vector(-1473, -4637)],
						[new Vector(-274, -1548), new Vector(606, -2378)],
						[new Vector(179, -4123), new Vector(1115, -5087)],
						[new Vector(2783, -4026), new Vector(3699, -4905)],
						[new Vector(4220, -3800), new Vector(5310, -4826)],
						[new Vector(-613, -3015), new Vector(264, -3759)],     // Ancient

						/* ====== Dire ====== */

						//Top
						[new Vector(-4749, 3953), new Vector(-3922, 3255)],
						[new Vector(-3137, 5199), new Vector(-2141, 4378)],
						[new Vector(-2118, 4666), new Vector(-1389, 4039)],
						[new Vector(-994, 2713), new Vector(-288, 1974)],
						[new Vector(766, 3718), new Vector(1791, 3122)],
						[new Vector(-755, 3767), new Vector(114, 3118)],       // Ancient

						//Bot
						[new Vector(2252, 405), new Vector(3039, -152)],
						[new Vector(4017, 1095), new Vector(4741, 518)],
						[new Vector(3467, -28), new Vector(4519, -751)]  		// Ancient   
					];
				}

				return spots;
			},
			set: function () { },
			configurable: false,
		});
	})();
}

/* ================================== Console ================================== */

var GameConsole = (function () {

	var started = false;
	var consoleMsg = [];

	function doMsg() {

		Game.ServerCmd(consoleMsg[0]);

		$.Schedule(0.3, function () {
			consoleMsg.splice(0, 1);

			if (consoleMsg.length === 0) {
				started = false;
				return;
			}

			doMsg();
		});
	}

	function msgHandler() {
		if (consoleMsg.length === 0) {
			started = false;
			return;
		}

		if (started)
			return;

		started = true;
		doMsg();
	}

	return {
		Sound: function (soundName, volume) {
			Game.ExecuteCmd("playvol " + soundName + " " + ((volume / 100) || "0.5"));
		},
		/**
		 * Say to all chat
		 */
		SayAll: function () {
			var args = ARGUMENTS_INLINE_SLICE(arguments);
			if (args !== undefined) {
				args = args.map(function (val) {
					if (val !== undefined && val.toString !== undefined)
						return val.toString();

					if (typeof val === "object")
						return JSON.stringify(val)

					return val;
				});
			}
			consoleMsg.push("say " + args.join(' '));
			msgHandler();
		},
		/**
		 * Say to team chat
		 */
		SayTeam: function () {
			var args = ARGUMENTS_INLINE_SLICE(arguments);
			if (args !== undefined) {
				args = args.map(function (val) {
					if (val !== undefined && val.toString !== undefined)
						return val.toString();

					if (typeof val === "object")
						return JSON.stringify(val)

					return val;
				});
			}
			consoleMsg.push("say_team " + args.join(' '));
			msgHandler();
		},
		/**
		 * Chat wheel
		 * 
		 * Only for Supported OS
		 */
		ChatWheel: function (id) {
			Game.ExecuteCmd("chatwheel_say " + id);
		},
		/**
		 * Laugh to chat (ex: lol, rofl). Timeout 15 sec
		 * 
		 * Only for Supported OS
		 */
		Laugh: function () {
			consoleMsg.push("say /laugh");
			msgHandler();
		},
		/**
		 * Only for Supported OS
		 */
		Taunt: function () {
			Game.ExecuteCmd("use_item_client current_hero taunt");
		}
	}
})();


/* ================================== FUNCTIONS ================================== */

function ShowMessagePopup(title, message) {
	$.DispatchEvent(
		"UIShowCustomLayoutPopupParameters",
		"CustomPopupTest",
		"file://{resources}/layout/custom_game/popups/popup_common_alert.xml",
		(title ? "popup_title=" + title : "") + "&" + (message ? "message=" + message : "")
	);
}

/* ================================== Math ==================================  */

function ROUND_PLUS(x, n) {
	if (isNaN(x) || isNaN(n))
		return x;

	var m = Math.pow(10, n)
	return Math.round(x * m) / m
}

function ROUND_COUNT(x, n) {
	if (isNaN(x) || isNaN(n))
		return x;

	var b = x % n;
	b && (x = x - b + n);
	return x;
}

function TRUNCATED(x, n) {
	var numPowerConverter = Math.pow(10, n);
	return ~~(x * numPowerConverter) / numPowerConverter;
}

function LOG_BASE(x, y) {
	return Math.log(x) / Math.log(y)
}

/* ================================== Object ==================================  */

function OBJECT_ASSIGN(target) {
	'use strict';
	if (target === undefined || target === null) {
		throw new TypeError('Cannot convert first argument to object');
	}

	var to = Object(target);
	for (var i = 1; i < arguments.length; i++) {
		var nextSource = arguments[i];
		if (nextSource === undefined || nextSource === null) {
			continue;
		}

		var keysArray = Object.keys(Object(nextSource));
		for (var nextIndex = 0, len = keysArray.length; nextIndex < len; nextIndex++) {
			var nextKey = keysArray[nextIndex];
			var desc = Object.getOwnPropertyDescriptor(nextSource, nextKey);
			if (desc !== undefined && desc.enumerable) {
				to[nextKey] = nextSource[nextKey];
			}
		}
	}
	return to;
}

/* ================================== String ==================================  */

function STRING_STARTWITH(source, searchString, position) {
	position = position || 0;
	return source.indexOf(searchString, position) === position;
}

function STRING_REPEAT(source, count) {
	'use strict';
	if (source == null) {
		throw new TypeError('can\'t convert ' + source + ' to object');
	}
	var str = '' + source;
	count = +count;
	if (count != count) {
		count = 0;
	}
	if (count < 0) {
		throw new RangeError('repeat count must be non-negative');
	}
	if (count == Infinity) {
		throw new RangeError('repeat count must be less than infinity');
	}
	count = Math.floor(count);
	if (str.length == 0 || count == 0) {
		return '';
	}
	if (str.length * count >= 1 << 28) {
		throw new RangeError('repeat count must not overflow maximum string size');
	}
	var rpt = '';
	for (var i = 0; i < count; i++) {
		rpt += str;
	}
	return rpt;
}

/* ================================== Number ==================================  */

function RANDOM_INTEGER(min, max) {
	return Math.floor(min + Math.random() * (max + 1 - min));
}

/* ================================== Array ==================================  */


function ARGUMENTS_INLINE_SLICE(sourceArgs) {
	var i = sourceArgs.length;
	if (i === 0)
		return;

	var args = [];
	while (i--) args[i] = sourceArgs[i];
	return args
}

function ARRAY_FOREACH(source, callback) {
	for (var i = source.length; i--;)
		callback(source[i], i, source)
}

function ARRAY_FOREACH_PLUS(source, callback) {
	for (var i = 0, len = source.length; i < len; i++)
		callback(source[i], i, source)
}

function ARRAY_SOME(source, callback) {
	for (var i = source.length; i--;)
		if (callback(source[i], i, source) === true)
			return true;

	return false;
}

function ARRAY_SOME_PLUS(source, callback) {
	for (var i = 0, len = source.length; i < len; i++)
		if (callback(source[i], i, source) === true)
			return true;

	return false;
}

function ARRAY_FIND(source, callback) {
	for (var i = source.length; i--;)
		if (callback(source[i], i, source) === true)
			return source[i];
}

function ARRAY_FIND_PLUS(source, callback) {
	for (var i = 0, len = source.length; i < len; i++)
		if (callback(source[i], i, source) === true)
			return source[i];
}

function ARRAY_FILTER(source, callback) {
	var newArray = [];
	for (var i = source.length; i--;) {
		if (callback(source[i], i, source) === true)
			newArray.push(source[i]);
	}
	return newArray;
}
function ARRAY_FILTER_PLUS(source, callback) {
	var newArray = [];
	for (var i = 0, len = source.length; i < len; i++)
		if (callback(source[i], i, source) === true)
			newArray.push(source[i]);
	return newArray;
}

function ARRAY_REMOVE(source, obj) {
	var idx = source.indexOf(obj);
	if (idx > -1)
		source.splice(idx, 1);
	return idx > -1;
}

function ARRAY_REMOVE_AS_DELETE(source, obj) {
	var idx = source.indexOf(obj);
	if (idx > -1)
		delete source[idx];
	return idx > -1;
}

function ARRAY_REMOVE_ALL(source, obj) {
	var removed = false;
	var i = 0;
	while ((i = source.indexOf(obj)) > -1) {
		source.splice(i, 1);
		removed = true;
	}
	return removed;
}

function ARRAY_SWAP(source, x, y) {
	var b = source[x];
	source[x] = source[y];
	source[y] = b;
	return source;
}

function ARRAY_INCLUDE(source, obj) {
	return source.indexOf(obj) !== -1;
}

function ARRAY_TO_STRING(source) {
	var strings = [];
	for (var i = 0, len = source.length; i < len; i++)
		strings.push(source[i] + '');
	return strings;
}

function ARRAY_SORT(source, index, invert) {
	return source.sort(function (a, b) {
		return invert
			? a[index] < b[index]
				? 1 : a[index] > b[index]
					? -1 : 0
			: a[index] > b[index]
				? 1 : a[index] < b[index]
					? -1 : 0;
	})
}

var ARRAY_FROM = (function () {
	var toStr = Object.prototype.toString;
	var isCallable = function (fn) {
		return typeof fn === 'function' || toStr.call(fn) === '[object Function]';
	};
	var toInteger = function (value) {
		var number = Number(value);
		if (isNaN(number)) { return 0; }
		if (number === 0 || !isFinite(number)) { return number; }
		return (number > 0 ? 1 : -1) * Math.floor(Math.abs(number));
	};
	var maxSafeInteger = Math.pow(2, 53) - 1;
	var toLength = function (value) {
		var len = toInteger(value);
		return Math.min(Math.max(len, 0), maxSafeInteger);
	};

	return function from(arrayLike) {
		var C = this;

		var items = Object(arrayLike);

		if (arrayLike == null) {
			throw new TypeError('Array.from requires an array-like object - not null or undefined');
		}
		var mapFn = arguments.length > 1 ? arguments[1] : void undefined;
		var T;
		if (typeof mapFn !== 'undefined') {
			if (!isCallable(mapFn)) {
				throw new TypeError('Array.from: when provided, the second argument must be a function');
			}
			if (arguments.length > 2) {
				T = arguments[2];
			}
		}

		var len = toLength(items.length);

		var A = isCallable(C) ? Object(new C(len)) : new Array(len);

		var k = 0;
		var kValue;
		while (k < len) {
			kValue = items[k];
			if (mapFn) {
				A[k] = typeof T === 'undefined' ? mapFn(kValue, k) : mapFn.call(T, kValue, k);
			} else {
				A[k] = kValue;
			}
			k += 1;
		}
		A.length = len;
		return A;
	};
}());

function ARRAY_COPY(source) {
	var copy = new Array(source.length);
	for (var i = 0, l = source.length; i < l; i++)
		copy[i] = source[i];
	return copy;
}

function ARRAY_CLEAN(source) {
	while (source.length > 0)
		source.pop();
}

function HAS_BIT(num, bit) {
	return ((num >> bit) & 1) === 1
}

function HAS_MASK(num, mask) {
	return (num & mask) === mask;
}

var MASK_TO_ARRAY = (function () {
	var masksNumber = new Array(64);

	for (var i = 64; i--;)
		masksNumber[i] = 1 << i;

	return function (num) {
		return masksNumber.map(function (mask) {
			return num & mask;
		}).filter(function (masked) {
			return masked !== 0;
		})
	}
})();

function HASH_PAIR(x, y) {
	return (x >= y) ? (x * x + x + y) : (y * y + x);
}

function HASH_UNPAIR(z) {
	var q = Math.floor(Math.sqrt(z));
	var l = z - q * q;
	return l < q ? [l, q] : [q, l - q];
}

/* ================================== Panels ==================================  */

var MOVE_PANEL_TYPE = {
	NONE: 0,
	ONLY_PRESSED: 1,
	CTRL_NEED: 2
}

/* ================================== Request ==================================  */

var REQUEST_TYPE = {
	GET: 1,
	SAVE_CONFIG: 2,
	CHECK_VERSION: 3,
	UPDATE_ONLINE: 4,
	CHECK_LAUNCHER: 5,
	CHECK_SERVER: 6
}

var RESPONSE_TYPE = {
	OK: 1,
	ERROR: 2,
	BADVERSION: 3,
	CONFIG: 4,
	SERVER_UPDATING: 5,
	SERVER_OFFLINE: 6
}

/* ================================== PARTICLES ==================================  */

var PARTICLES_RENDER_NORMAL = { en: "Standart", ru: "Стандарт" };
var PARTICLES_RENDER_ROPE = { en: "Rope", ru: "Пунктир" };
var PARTICLES_RENDER_ANIMATION = { en: "Animation", ru: "Анимация" };

/* ================================== IN GAME ==================================  */

var MAX_ITEMS = 16;
var MAX_SKILLS = 24;

/* ================================== DEBUG==================================  */

var ClassLog = (function () {

	function dumpClass(obj, fields, recursiveCount, alreadyCount) {
		var fieledIsArray = fields && Array.isArray(fields);

		for (var name in obj) {
			if (fieledIsArray && !ARRAY_INCLUDE(fields, name))
				continue;

			if (typeof obj[name] === "function")
				continue;

			var value = obj[name];

			$.Msg(STRING_REPEAT("\t", alreadyCount), "| ", name, ": ", Array.isArray(value) ? ARRAY_TO_STRING(value) : value);

			if (typeof value === "object" && !Array.isArray(value)) {
				if (recursiveCount > 0)
					dumpClass(value, fields, recursiveCount - 1, alreadyCount + 1);
			}
		}
	}

	return function (obj, fields, recursiveCount) {
		$.Msg("-------");
		dumpClass(obj, fields, recursiveCount || 0, 1);
		$.Msg("-------");
	}
})();