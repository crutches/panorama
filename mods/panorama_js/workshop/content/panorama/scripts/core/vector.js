function Vector(x, y, z) {
	this.x = x || 0;
	this.y = y || 0;
	this.z = z || 0;
}

Vector.prototype = {

	/* ================== Getters ================== */

	get IsValid() {
		var x = this.x,
			y = this.y,
			z = this.z

		return !Number.isNaN(x) && Number.isFinite(x)
			&& !Number.isNaN(y) && Number.isFinite(y)
			&& !Number.isNaN(z) && Number.isFinite(z)
	},
	get LengthSqr() {
		return (this.x * this.x) + (this.y * this.y)/*  + (this.z * this.z); */
	},
	get Length() {
		return Math.sqrt(this.LengthSqr)
	},
	get Angle() {
		return Math.atan2(this.y, this.x)
	},
	get Polar() {

		if (Math.abs(this.x - 0) <= 1e-9)
			return this.y > 0 ? 90 : this.y < 0 ? 270 : 0;

		var theta = Math.atan(this.y / this.x) * (180 / Math.PI)

		if (this.x < 0)
			theta += 180

		if (theta < 0)
			theta += 360

		return theta
	},

	/* ================== Methods ================== */

	Equals: function (vec) {
		return this.x === vec.x
			&& this.y === vec.y
			&& this.z === vec.z
	},
	IsZero: function (tolerance) {
		tolerance = tolerance || 0.01;

		var x = this.x,
			y = this.y,
			z = this.z

		return x > -tolerance && x < tolerance
			&& y > -tolerance && y < tolerance
			&& z > -tolerance && z < tolerance
	},
	IsLengthGreaterThan: function (val) {
		return this.LengthSqr > val * val;
	},
	Invalidate: function () {
		this.x = this.y = this.z = NaN
		return this
	},
	toZero: function () {
		this.x = this.y = this.z = 0
		return this
	},
	Negate: function () {
		this.x *= -1
		this.y *= -1
		this.z *= -1
		return this
	},
	Random: function (minVal, maxVal) {
		this.x = Math.random() * (maxVal - minVal) + minVal
		this.y = Math.random() * (maxVal - minVal) + minVal
		this.z = Math.random() * (maxVal - minVal) + minVal
		return this
	},
	Min: function (vec) {
		return new Vector(
			Math.min(this.x, vec.x),
			Math.min(this.y, vec.y),
			Math.min(this.z, vec.z)
		);
	},
	Max: function (vec) {
		return new Vector(
			Math.max(this.x, vec.x),
			Math.max(this.y, vec.y),
			Math.max(this.z, vec.z)
		);
	},
	Abs: function () {
		return new Vector(
			Math.abs(this.x),
			Math.abs(this.y),
			Math.abs(this.z)
		);
	},
	SquareRoot: function () {
		return new Vector(
			Math.sqrt(this.x),
			Math.sqrt(this.y),
			Math.sqrt(this.z)
		);
	},
	SetVector: function (x, y, z) {
		this.x = x || 0;
		this.y = y || 0;
		this.z = z || 0;
		return this
	},
	CopyTo: function (vec) {
		vec.x = this.x;
		vec.y = this.y;
		vec.z = this.z;
		return vec;
	},
	CopyFrom: function (vec) {
		this.x = vec.x;
		this.y = vec.y;
		this.z = vec.z;
		return this;
	},
	CopyFromArray: function (array) {
		this.x = array[0] || 0;
		this.y = array[1] || 0;
		this.z = array[2] || 0;
		return this;
	},
	Clone: function () {
		return new Vector(this.x, this.y, this.z);
	},
	SetX: function (num) {
		this.x = num
		return this
	},
	SetY: function (num) {
		this.y = num
		return this
	},
	SetZ: function (num) {
		this.z = num
		return this
	},
	Normalize: function (scalar) {
		var length = this.Length

		if (length !== 0)
			this.DivideScalarForThis(scalar !== undefined ? length * scalar : length)

		return this
	},
	Cross: function (vec) {
		return new Vector(
			this.y * vec.z - this.z * vec.y,
			this.z * vec.x - this.x * vec.z,
			this.x * vec.y - this.y * vec.x
		);
	},
	Dot: function (vec) {
		return this.x * vec.x + this.y * vec.y + this.z * vec.z
	},
	ScaleTo: function (scalar) {
		var length = this.Length

		if (length === 0) {
			this.x = 0
			this.y = 0
			this.z = 0
		} else
			this.MultiplyScalar(scalar / length)

		return this
	},
	DivideTo: function (scalar) {
		var length = this.Length

		if (length === 0)
			this.toZero()
		else
			this.DivideScalar(scalar / length)

		return this
	},
	Clamp: function (min, max) {
		return new Vector(
			Math.min((this.x > max.x) ? max.x : this.x, min.x),
			Math.min((this.y > max.y) ? max.y : this.y, min.y),
			Math.min((this.z > max.z) ? max.z : this.z, min.z))
	},

	/* ======== Add ======== */

	Add: function (vec) {
		return new Vector(
			this.x + vec.x,
			this.y + vec.y,
			this.z + vec.z
		);
	},
	AddForThis: function (vec) {
		this.x += vec.x
		this.y += vec.y
		this.z += vec.z
		return this
	},
	AddScalar: function (scalar) {
		return new Vector(
			this.x + scalar,
			this.y + scalar,
			this.z + scalar
		);
	},
	AddScalarForThis: function (scalar) {
		this.x += scalar
		this.y += scalar
		this.z += scalar
		return this
	},
	AddScalarX: function (scalar) {
		this.x += scalar
		return this
	},
	AddScalarY: function (scalar) {
		this.y += scalar
		return this
	},
	AddScalarZ: function (scalar) {
		this.z += scalar
		return this
	},

	/* ======== Subtract ======== */

	Subtract: function (vec) {
		return new Vector(
			this.x - vec.x,
			this.y - vec.y,
			this.z - vec.z
		);
	},
	SubtractForThis: function (vec) {
		this.x -= vec.x
		this.y -= vec.y
		this.z -= vec.z
		return this;
	},
	SubtractScalar: function (scalar) {
		return new Vector(
			this.x - scalar,
			this.y - scalar,
			this.z - scalar
		);
	},
	SubtractScalarForThis: function (scalar) {
		this.x -= scalar
		this.y -= scalar
		this.z -= scalar
		return this;
	},
	SubtractScalarX: function (scalar) {
		this.x -= scalar
		return this
	},
	SubtractScalarY: function (scalar) {
		this.y -= scalar
		return this
	},
	SubtractScalarZ: function (scalar) {
		this.z -= scalar
		return this
	},

	/* ======== Multiply ======== */

	Multiply: function (vec) {
		return new Vector(
			this.x * vec.x,
			this.y * vec.y,
			this.z * vec.z
		);
	},
	MultiplyForThis: function (vec) {
		this.x *= vec.x
		this.y *= vec.y
		this.z *= vec.z
		return this;
	},
	MultiplyScalar: function (scalar) {
		return new Vector(
			this.x * scalar,
			this.y * scalar,
			this.z * scalar
		);
	},
	MultiplyScalarForThis: function (scalar) {
		this.x *= scalar
		this.y *= scalar
		this.z *= scalar
		return this;
	},
	MultiplyScalarX: function (scalar) {
		this.x *= scalar
		return this
	},
	MultiplyScalarY: function (scalar) {
		this.y *= scalar
		return this
	},
	MultiplyScalarZ: function (scalar) {
		this.z *= scalar
		return this
	},

	/* ======== Divide ======== */

	Divide: function (vec) {
		return new Vector(
			this.x / vec.x,
			this.y / vec.y,
			this.z / vec.z
		);
	},
	DivideForThis: function (vec) {
		this.x /= vec.x
		this.y /= vec.y
		this.z /= vec.z
		return this;
	},
	DivideScalar: function (scalar) {
		return new Vector(
			this.x / scalar,
			this.y / scalar,
			this.z / scalar
		);
	},
	DivideScalarForThis: function (scalar) {
		this.x /= scalar
		this.y /= scalar
		this.z /= scalar
		return this
	},
	DivideScalarX: function (scalar) {
		this.x /= scalar
		return this
	},
	DivideScalarY: function (scalar) {
		this.y /= scalar
		return this
	},
	DivideScalarZ: function (scalar) {
		this.z /= scalar
		return this
	},

	MultiplyAdd: function (vec2, scalar) {
		return this.Add(vec2).MultiplyScalar(scalar)
	},
	MultiplyAddForThis: function (vec2, scalar) {
		return this.AddForThis(vec2).MultiplyScalarForThis(scalar)
	},

	/* ======== Distance ======== */

	DistanceSqr: function (vec) {
		var x = (this.x - vec.x);
		var y = (this.y - vec.y);
		var z = (this.z - vec.z);

		return x * x + y * y + z * z;
	},
	DistanceSqr2D: function (vec) {
		var x = (this.x - vec.x);
		var y = (this.y - vec.y);

		return x * x + y * y;
	},
	Distance: function (vec) {
		return Math.sqrt(this.DistanceSqr(vec))
	},
	Distance2D: function (vec) {
		return Math.sqrt(this.DistanceSqr2D(vec))
	},

	/* ================== Geometric ================== */

	Perpendicular: function (is_x) {
		return (is_x === undefined ? true : is_x)
			? new Vector(-this.y, this.x, this.z)
			: new Vector(this.y, -this.x, this.z)
	},
	PolarAngle: function (radian) {
		if (radian === true)
			return this.Angle;

		return this.Angle * (180 / Math.PI);
	},
	Rotated: function (angle) {
		var cos = Math.cos(angle),
			sin = Math.sin(angle)

		return new Vector(
			(this.x * cos) - (this.y * sin),
			(this.y * cos) + (this.x * sin)
		)
	},
	Rotation: function (rotation, distance) {
		return new Vector(
			this.x + rotation.x * distance,
			this.y + rotation.y * distance,
			this.z + rotation.z * distance
		);
	},
	RotationForThis: function (rotation, distance) {
		this.x = this.x + rotation.x * distance;
		this.y = this.y + rotation.y * distance;
		this.z = this.z + rotation.z * distance;
		return this;
	},
	RotationRad: function (rotation, distance) {
		return this.Rotation(rotation.DegreesToRadians(), distance);
	},
	InFrontFromAngle: function (angle, distance) {
		return this.Rotation(Vector.FromAngle(angle), distance);
	},
	FindRotationAngle: function (vec, vecAngleRadian) {
		var angle = Math.abs(Math.atan2(vec.y - this.y, vec.x - this.x) - vecAngleRadian);

		if (angle > Math.PI)
			angle = Math.abs((Math.PI * 2) - angle)

		return angle
	},
	RotationTime: function (rot_speed) {
		return this.Angle / (30 * rot_speed)
	},
	AngleBetweenVectors: function (vec) {
		var theta = this.Polar - vec.Polar;
		if (theta < 0) {
			theta = theta + 360;
		}

		if (theta > 180) {
			theta = 360 - theta;
		}

		return theta;
	},
	AngleBetweenFaces: function (front) {
		return Math.acos((this.x * front.x) + (this.y * front.y))
	},
	Extend: function (vec, distance) {
		return vec.Subtract(this).Normalize().MultiplyScalarForThis(distance).AddForThis(this); // this + (distance * (vec - this).Normalize())
	},
	IsInRange: function (vec, range) {
		return this.DistanceSqr(vec) < range * range
	},
	Closest: function (vecs) {

		var minVec = new Vector();
		var distance = Number.POSITIVE_INFINITY;

		ARRAY_FOREACH(vecs, function (vec) {
			var tempDist = this.Distance(vec);
			if (tempDist < distance) {
				distance = tempDist;
				minVec = vec;
			}
		});
		return minVec;
	},
	IsUnderRectangle: function (x, y, width, height) {
		return this.x > x && this.x < (x + width) && this.y > y && this.y < (y + height)
	},
	RadiansToDegrees: function () {
		return this.MultiplyScalar(180).DivideScalar(Math.PI);
	},
	DegreesToRadians: function () {
		return this.MultiplyScalar(Math.PI).DivideScalar(180);
	},

	/* ================== To ================== */
	GetHashCode: function () {
		var hash = this.x;
		hash = (hash * 397) ^ this.y;
		hash = (hash * 397) ^ this.z;
		return hash;
	},
	toString: function () {
		return "Vector(" + this.x + "," + this.y + "," + this.z + ")"
	},
	toArray: function (count) {
		if (count === undefined)
			return [this.x, this.y, this.z];

		if (typeof count !== "number" || count <= 0)
			return [];

		if (count === 1)
			return [this.x];

		if (count === 2)
			return [this.x, this.y];

		return [this.x, this.y, this.z];
	}
}

/* ================== Static ================== */

Vector.fromArray = function (array) {
	return new Vector(array[0] || 0, array[1] || 0, array[2] || 0)
}
Vector.FromAngle = function (angle) {
	return new Vector(Math.cos(angle), Math.sin(angle))
}
Vector.FromPolarCoordinates = function (radial, polar) {
	return new Vector(Math.cos(polar) * radial, Math.sin(polar) * radial)
}
Vector.GetCenterType = function (array, callback) {

	var newVec = new Vector();

	ARRAY_FOREACH(array, function (vec) {
		newVec.AddForThis(callback(vec))
	})

	return newVec.DivideScalarForThis(array.length);
}
Vector.GetCenter = function (array) {

	var newVec = new Vector();

	ARRAY_FOREACH(array, function (vec) {
		newVec.AddForThis(vec);
	})

	return newVec.DivideScalarForThis(array.length);
}
Vector.CopyFrom = function (vec) {
	return new Vector(vec.x, vec.y, vec.z);
}