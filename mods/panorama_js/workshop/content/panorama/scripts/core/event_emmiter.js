function EventEmitter() {

	var events = {};
	var scriptsContext = [];
	var _this = this;

	this.on = function (name, listener, isCoreContext) {
		if (listener === undefined)
			throw new Error(name + ": listener is not defined").stack;

		var listeners = events[name]

		if (listeners === undefined)
			events[name] = listeners = []

		if (isCoreContext !== true)
			scriptsContext.push(listener);

		listeners.unshift(listener)
		return _this;
	}

	this.removeListener = function (name, listener) {
		var listeners = events[name]

		if (listeners !== undefined)
			ARRAY_REMOVE_ALL(listeners, listener);

		return _this
	}

	this.emit = function (name, cancellable) {
		var listeners = events[name];

		if (listeners === undefined)
			return true;

		if (arguments.length > 2)
			var args = Array.prototype.slice.call(ARGUMENTS_INLINE_SLICE(arguments),
				_this.emit.length);

		return !ARRAY_SOME(listeners, function (listener) {
			if (listener === undefined)
				return false;
			try {
				return (args === undefined
					? listener()
					: listener.apply(this, args)) === false && cancellable;
			} catch (e) {
				$.Msg(listener.name, ": ", e.stack || new Error(e).stack)
				return false
			}
		});
	}


	this.removeAllListeners = function () {
		ARRAY_FOREACH(Object.keys(events), function (name) {
			ARRAY_CLEAN(events[name])
		});
		return this;
	}

	this.once = function (name, listener, isCoreContext) {
		function once_listener() {
			_this.removeListener(name, once_listener);

			var args = ARGUMENTS_INLINE_SLICE(arguments);

			if (args === undefined)
				listener();
			else
				listener.apply(null, args);
		}
		return _this.on(name, once_listener, isCoreContext);
	}

	var globalEvent = Events instanceof EventEmitter ? Events : this;

	globalEvent.on("RemoveEvents", function () {
		ARRAY_FOREACH(scriptsContext, function (listener) {
			for (var name in events) {
				_this.removeListener(name, listener);
			}
		});
	}, true);
}

var Events = new EventEmitter();
var NativeEvents = new EventEmitter();
var MouseEvents = new EventEmitter();

(function () {

	{ // Update & Tick

		function OnUpdate() {
			$.Schedule(0, function () {
				Events.emit("Update");
				OnUpdate();
			});
		}
		OnUpdate();

		function OnTick() {
			$.Schedule(1 / 30, function () {
				Events.emit("Tick");
				OnTick();
			});
		}
		OnTick();
	}

	{ // MouseEvents

		GameUI.SetMouseCallback(function (eventName, arg) {
			return MouseEvents.emit(eventName, true, arg) === false;
		});

		if (Game.Events === undefined) {
			Object.defineProperty(Game, "Events", {
				value: Events,
				configurable: false,
				writable: false
			});

		}

		if (Game.MouseEvents === undefined) {
			Object.defineProperty(Game, "MouseEvents", {
				value: MouseEvents,
				configurable: false,
				writable: false
			});
		}
	}

	{
		GameEvents.Subscribe("dota_player_update_assigned_hero", function (event) {
			NativeEvents.emit("AssignedHero", false, event);
		});
		GameEvents.Subscribe("npc_spawned", function (event) {
			NativeEvents.emit("npc_spawned", false, event);
		});
		GameEvents.Subscribe("entity_killed", function (event) {
			NativeEvents.emit("EntityKilled", false, event);
		});
		GameEvents.Subscribe("dota_player_update_selected_unit", function () {
			NativeEvents.emit("SelectedUnitsUpdate");
		});
		GameEvents.Subscribe("dota_player_update_query_unit", function () {
			NativeEvents.emit("QueryUnitsUpdate");
		});
		GameEvents.Subscribe("dota_hero_swap", function (event) {
			NativeEvents.emit("HeroSwap", false, event);
		});
	}
})();