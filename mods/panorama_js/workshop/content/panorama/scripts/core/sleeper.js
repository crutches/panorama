function SleeperBase() {
	/**
	 * @protected
	 * @type {Map<string, number>}
	 */
	this.SleepDB = new Map();
}

SleeperBase.prototype = {
	setTime: function (id, time) {
		this.SleepDB.set(id, time)
		return time;
	},
	updateTime: function (id, timeNow, timeExtend) {
		var value = this.SleepDB.get(id);

		if (value === undefined || value <= timeExtend)
			return false;

		this.setTime(id, timeNow += timeExtend);
		return true
	}
}

function Sleeper() {
	SleeperBase.call(this);
}
Sleeper.prototype = Object.create(SleeperBase.prototype, {
	Sleep: {
		value: function (ms, id, extend) {
			if (typeof ms !== "number")
				return this.setTime(id, Date.now());

			if (extend === true && this.updateTime(id, Date.now(), ms))
				return;

			return this.setTime(id, Date.now() + ms);
		}
	},
	Sleeping: {
		value: function (id) {
			var sleepID = this.SleepDB.get(id);
			return sleepID !== undefined && Date.now() < sleepID;
		}
	},
	Reset: {
		value: function (id) {
			this.SleepDB.delete(id);
			return this;
		}
	},
	FullReset: {
		value: function () {
			this.SleepDB.clear();
			return this;
		}
	}
});
Sleeper.prototype.constructor = Sleeper;

function GameSleeper() {
	SleeperBase.call(this);
}
GameSleeper.prototype = Object.create(SleeperBase.prototype, {
	Sleep: {
		value: function (ms, id, extend) {
			if (typeof ms !== "number")
				return this.setTime(id, Game.GetGameTime());

			if (extend === true && this.updateTime(id, Game.GetGameTime(), ms / 1000))
				return;

			return this.setTime(id, Game.GetGameTime() + (ms / 1000));
		}
	},
	Sleeping: {
		value: function (id) {
			var sleepID = this.SleepDB.get(id);
			return sleepID !== undefined && Game.GetGameTime() < sleepID;
		}
	},
	Reset: {
		value: function (id) {
			this.SleepDB.delete(id);
			return this;
		}
	},
	FullReset: {
		value: function () {
			this.SleepDB.clear();
			return this;
		}
	}
});
GameSleeper.prototype.constructor = GameSleeper;