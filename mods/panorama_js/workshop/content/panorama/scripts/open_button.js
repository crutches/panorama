var InGameOpenButton = (function () {

	/**
	 * @type {Panel}
	 */
	var buttonPanel = null;

	function Create() {
		var showIcon = Menu.FindSettings(["Settings", "Ingame Icon"]);

		if (showIcon === false)
			return;

		buttonPanel = $.CreatePanel("Panel", Game.InGameHUD, "InGameOpenButton");

		buttonPanel.BLoadLayout("file://{resources}/layout/custom_game/open_button.xml", false, false)

		buttonPanel.SetPanelEvent("onactivate", function () {
			MenuPanel.SendToInGame();
		});

		var x = Menu.FindSettings(["Settings", "Ingame Icon", "Position: X"]);
		var y = Menu.FindSettings(["Settings", "Ingame Icon", "Position: Y"]);

		buttonPanel.style.position = (x !== undefined ? x + "% " : "150px ")
			+ (y !== undefined ? y + "% " : "10.5px ") + "0";
	}

	function ChangeVision(visibility) {

		buttonPanel = Game.InGameHUD.FindChildTraverse("InGameOpenButton");

		if (buttonPanel !== null) {

			if (buttonPanel.IsValid())
				buttonPanel.DeleteAsync(0.0);

			buttonPanel = null;
		}

		if (!visibility)
			return;

		var gameState = Game.GetState();

		if ((gameState !== DOTA_GameState.DOTA_GAMERULES_STATE_GAME_IN_PROGRESS
			&& gameState !== DOTA_GameState.DOTA_GAMERULES_STATE_PRE_GAME
			&& gameState !== DOTA_GameState.DOTA_GAMERULES_STATE_STRATEGY_TIME
			&& gameState !== DOTA_GameState.DOTA_GAMERULES_STATE_HERO_SELECTION))
			return;

		Create();
	}

	Events.on("GameLoaded", function () {
		ChangeVision(true);
	}, true);

	return {
		ChangeVision: ChangeVision,
		Move: function (vector) {
			if (buttonPanel === null || !buttonPanel.IsValid())
				return;

			Drawing.VectorToPanelPercent(vector, buttonPanel);
		}
	}
})();