function MenuSwitch(parent, name, values, value) {

	ARRAY_FOREACH(values, function (name, index) {

		var nameNormalize = {
			en: undefined,
			ru: undefined
		}

		if (typeof name === "object") {

			if (name.en === undefined)
				throw Error("Name English is not defined").stack;

			nameNormalize = name;
		}
		else nameNormalize.en = nameNormalize.ru = name;

		values[index] = nameNormalize;
	});

	Object.defineProperty(this, "values", {
		get: function () {
			return ARRAY_COPY(values);
		},
		set: function () {},
		configurable: false,
		enumerable: true
	});

	MenuBase.call(this, parent, name, value || 0);
}

MenuSwitch.prototype = Object.create(MenuBase.prototype, {
	ChangeToLeft: {
		value: function () {
			this.value = this.value - 1 < 0 ? this.values.length - 1 : this.value - 1;
			return this;
		}
	},
	ChangeToRight: {
		value: function () {
			this.value = this.value + 1 > this.values.length - 1 ? 0 : this.value + 1
			return this;
		}
	},
});
MenuSwitch.prototype.constructor = MenuSwitch;