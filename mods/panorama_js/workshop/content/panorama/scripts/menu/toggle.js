function MenuToggle(parent, name, value) {
	var _this = this;

	value = !!value;
	Object.defineProperty(this, "value", {
		set: function (newValue) {

			if (!!newValue === value)
				return;

			value = !!newValue;

			_this.emit("Value", false, value, _this);

			if (value)
				_this.emit("Activated", false, _this)
			else
				_this.emit("Deactivated", false, _this);
		},
		get: function () {
			return value;
		},
		configurable: false,
		enumerable: true
	});

	MenuBase.call(this, parent, name, value);

	var settsValue = Menu.FindSettings(_this);

	if (settsValue !== undefined && typeof settsValue === typeof value)
		value = settsValue;
}

MenuToggle.prototype = Object.create(MenuBase.prototype, {
	ChangeReverse: {
		value: function () {
			this.value = !this.value;
			return this;
		}
	}
});
MenuToggle.prototype.constructor = MenuToggle;