function MenuButton(parent, name) {
	Object.defineProperty(this, "value", {
		value: false,
		configurable: false,
		writable: false
	});

	MenuBase.call(this, parent, name, false);
}

MenuButton.prototype = Object.create(MenuBase.prototype);
MenuButton.prototype.constructor = MenuButton;