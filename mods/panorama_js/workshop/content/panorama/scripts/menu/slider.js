function MenuSlider(parent, name, value, min, max, isFloat) {

	min = Math.max(min, 0) || 0;
	max = Math.max(min, max) || min + 1;

	Object.defineProperty(this, 'IsFloat', {
		value: !!isFloat,
		configurable: false,
		writable: false
	});

	Object.defineProperty(this, "min", {
		value: min,
		configurable: false,
		writable: false
	});

	Object.defineProperty(this, "max", {
		value: max,
		configurable: false,
		writable: false
	});

	MenuBase.call(this, parent, name, value || 0);
}

MenuSlider.prototype = Object.create(MenuBase.prototype);
MenuSlider.prototype.constructor = MenuSlider;