
var MenuTree = (function () {

	function CheckSameName(entries, name) {
		return ARRAY_FIND(entries, function (entry) {
			return (typeof name === "object"
				? (name.en === entry.name.en || name.ru === entry.name.ru)
				: entry.name.en === name);
		});
	}

	function MenuTree(parent, name, isToggle, value) {

		var nameControl = typeof name === "object" ? name.en : name;

		if (nameControl.indexOf("_toggle", nameControl.length) !== -1)
			throw Error("That name is reserved. Try without '_toggle'");

		var _this = this;

		var entries = [];
		Object.defineProperty(this, "entries", {
			value: entries,
			configurable: false,
			writable: false
		});

		Object.defineProperty(this, "IsTreeToogle", {
			value: typeof isToggle === "boolean" ? isToggle : false,
			configurable: false,
			writable: false
		});

		if (isToggle === true) {

			value = !!value;

			Object.defineProperty(this, "value", {
				set: function (newValue) {

					if (!!newValue === value)
						return;

					value = !!newValue;

					_this.emit("Value", false, value, _this);

					if (value)
						_this.emit("Activated", false, _this)
					else
						_this.emit("Deactivated", false, _this);
				},
				get: function () {
					return value;
				},
				configurable: false,
				enumerable: true
			});
		} else {
			Object.defineProperty(this, "value", {
				value: false,
				configurable: false,
				writable: false,
				enumerable: true
			});
		}

		MenuBase.call(this, parent, name, value);

		if (isToggle === true) {

			var settsValue = Menu.FindSettings(_this);

			if (settsValue !== undefined && typeof settsValue === typeof value)
				value = settsValue;
		}
	}

	MenuTree.prototype = Object.create(MenuBase.prototype, {
		ChangeValue: {
			value: function (value) {
				if (this.IsTreeToogle)
					this.value = value;
				return this;
			}
		},
		ChangeReverse: {
			value: function () {
				if (this.IsTreeToogle)
					this.value = !this.value;
				return this;
			}
		},
		ChangeToDefault: {
			value: function () {
				if (this.IsTreeToogle)
					this.value = this.default;
				return this;
			}
		},
		AddControl: {
			value: function (ctrl, index) {

				if (CheckSameName(this.entries, ctrl.name) !== undefined)
					throw Error("Need unique controller or controller name");

				ctrl.parent = this;

				if (index !== undefined)
					ctrl.Index = index;

				return this;
			}
		},
		HasControl: {
			value: function (ctrl) {
				return this.entries.indexOf(ctrl) !== -1;
			}
		},
		GetControl: {
			value: function (name) {
				return CheckSameName(this.entries, name);
			}
		},
		ChangeIndexControl: {
			value: function (ctrl, index) {
				var find = ARRAY_FIND(this.entries, function (entry) {
					return entry === ctrl;
				});

				if (find !== undefined)
					find.Index = index;

				return this;
			}
		},
		RemoveControl: {
			value: function (ctrl) {
				var find = ARRAY_FIND(this.entries, function (entry) {
					return entry === ctrl;
				});

				if (find !== undefined)
					find.parent = undefined;

				return this;
			}
		},
		AddTree: {
			value: function (name, isToggle, value) {

				var find = CheckSameName(this.entries, name);

				if (find !== undefined)
					return find;

				return new MenuTree(this, name, isToggle, value);
			}
		},
		AddToggle: {
			value: function (name, value) {

				var find = CheckSameName(this.entries, name);

				if (find !== undefined)
					return find;

				return new MenuToggle(this, name, value);
			}
		},
		AddSlider: {
			value: function (name, value, minValue, maxValue, isFloat) {

				var find = CheckSameName(this.entries, name);

				if (find !== undefined)
					return find;

				return new MenuSlider(this, name, value, minValue, maxValue, isFloat);
			}
		},
		AddSwitch: {
			value: function (name, values, value) {

				var find = CheckSameName(this.entries, name);

				if (find !== undefined)
					return find;

				return new MenuSwitch(this, name, values, value);
			}
		},
		AddButton: {
			value: function (name) {

				var find = CheckSameName(this.entries, name);

				if (find !== undefined)
					return find;

				return new MenuButton(this, name);
			}
		},
		AddTitle: {
			value: function (name) {

				var find = CheckSameName(this.entries, name);

				if (find !== undefined)
					return find;

				return new MenuTitle(this, name);
			}
		},
		AddSpells: {
			value: function (name, values, value, dragAndDrop) {

				var find = CheckSameName(this.entries, name);

				if (find !== undefined)
					return find;

				return new MenuSelectorSpells(this, name, values, value, dragAndDrop);
			}
		},
		AddItems: {
			value: function (name, values, value, dragAndDrop) {

				var find = CheckSameName(this.entries, name);

				if (find !== undefined)
					return find;

				return new MenuSelectorItems(this, name, values, value, dragAndDrop);
			}
		},
		AddHeroes: {
			value: function (name, values, value, dragAndDrop) {

				var find = CheckSameName(this.entries, name);

				if (find !== undefined)
					return find;

				return new MenuSelectorHeroes(this, name, values, value, dragAndDrop);
			}
		},
	});
	MenuTree.prototype.constructor = MenuTree;

	return MenuTree;
})();