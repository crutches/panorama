
var MenuBase = (function () {

	function menuManagerUpdate(node) {
		if (node instanceof MenuCategory)
			return node;

		node = node.parent;

		if (node === undefined)
			return;

		if (node instanceof MenuCategory)
			return node;

		return menuManagerUpdate(node);
	}

	function CheckSameName(entries, name) {
		return ARRAY_FIND(entries, function (entry) {
			return (typeof name === "object"
				? (name.en === entry.name.en || name.ru === entry.name.ru)
				: entry.name.en === name);
		});
	}

	function MenuBase(parent, ctrlName, value) {

		if (parent !== undefined && CheckSameName(parent.entries, ctrlName))
			throw Error("That control " +
				(typeof ctrlName === "object" ? ctrlName.en : ctrlName)
				+ " already exist in " + parent.name.en + ". Use 'tree.AddControl' or unique name");

		EventEmitter.call(this);

		var _this = this;
		var hasParent = false;

		Object.defineProperty(this, "parent", {
			set: function (newParent) {

				if (newParent !== undefined && CheckSameName(newParent.entries, _this.name))
					throw Error("That control " + _this.name.en + " already exist in "
						+ newParent.name.en + ". Use 'tree.AddControl' or unique name");

				if (parent !== undefined) {
					ARRAY_REMOVE(parent.entries, _this);
					var top = menuManagerUpdate(_this);
					if (top !== undefined)
						top.Update(_this);
				}

				if (newParent === undefined) {
					parent = undefined;
					return;
				}

				if (newParent.entries.indexOf(_this) === -1)
					newParent.entries.push(_this);

				parent = newParent;

				var top = menuManagerUpdate(_this);
				if (top !== undefined)
					top.Update(_this);

				if (_this.value !== undefined) {
					var settsValue = Menu.FindSettings(_this);

					if (settsValue !== undefined && typeof settsValue === typeof _this.value && _this.value !== settsValue) {
						if (hasParent) {
							_this.value = settsValue
						}
						else {
							hasParent = true;
							value = settsValue;
						}
					}
				}
			},
			get: function () {
				return parent;
			},
			configurable: false,
			enumerable: true
		});

		var name = {
			en: undefined,
			ru: undefined
		}

		if (typeof ctrlName === "object") {

			if (ctrlName.en === undefined)
				throw Error("Name English is not defined").stack;

			name = ctrlName;
		}
		else name.en = name.ru = ctrlName;

		Object.defineProperty(this, "name", {
			value: name,
			configurable: false,
			writable: false
		});

		var tooltip = {
			ru: undefined,
			en: undefined
		};

		Object.defineProperty(this, "tooltip", {
			set: function (newName) {

				var ttEn = newName.en || newName;
				var ttRu = newName.ru || ttEn;

				if (ttEn === tooltip.en && ttRu === tooltip.ru)
					return;

				tooltip.en = ttEn;
				tooltip.ru = ttRu;

				_this.emit("ToolTipChanged", false, _this);
			},
			get: function () {
				return tooltip;
			},
			configurable: false,
			enumerable: true
		});

		var logo = undefined;

		Object.defineProperty(this, "logo", {
			set: function (path) {
				if (path === logo)
					return;

				logo = path;

				_this.emit("LogoChanged", false, _this);
			},
			get: function () {
				return logo;
			},
			configurable: false,
			enumerable: true
		});

		var logoSize = new Vector(18, 18);

		Object.defineProperty(this, "LogoSize", {
			set: function (newSize) {
				if (!(newSize instanceof Vector) || newSize.Equals(logoSize))
					return;

				logoSize.CopyFrom(newSize);

				_this.emit("LogoChanged", false, _this);
			},
			get: function () {
				return logoSize.Clone();
			},
			configurable: false,
			enumerable: true
		});

		Object.defineProperty(this, "default", {
			value: value,
			configurable: false,
			writable: false
		});

		if (this.value === undefined) {

			var settsValue = Menu.FindSettings(_this);

			if (settsValue !== undefined && typeof settsValue === typeof value)
				value = settsValue;

			Object.defineProperty(this, "value", {
				set: function (newValue) {

					if (newValue === value)
						return;

					value = newValue;

					_this.emit("Value", false, value, _this);
				},
				get: function () {
					return value;
				},
				configurable: false,
				enumerable: true
			});
		}

		var save = true;
		Object.defineProperty(this, "save", {
			set: function (saving) {
				if (save === saving)
					return;

				save = !!saving;

				save ? _this.on("Value", Save) : _this.removeListener("Value", Save);
			},
			get: function () {
				return save;
			},
			configurable: false,
			enumerable: true
		});

		function Save() {
			var top = menuManagerUpdate(_this);
			if (top !== undefined)
				top.Save();
		}
		this.on("Value", Save);

		if (parent !== undefined) {
			if (parent.entries.indexOf(_this) === -1)
				this.parent = parent;
		}
	}

	MenuBase.prototype = Object.create(EventEmitter.prototype, {
		toString: {
			value: function () {
				return this.name.en;
			}
		},
		Index: {
			get: function () {
				if (this.parent === undefined)
					return -1;

				return this.parent.entries.indexOf(this);
			},
			set: function (value) {
				if (this.parent === undefined)
					return;

				ARRAY_REMOVE(this.parent.entries, this);

				this.parent.entries.splice(value, 0, this)

				var top = menuManagerUpdate(this);
				if (top !== undefined)
					top.Update(this);
			},
			enumerable: true
		},
		Remove: {
			value: function () {
				var parent = this.parent;
				if (parent !== undefined)
					parent.RemoveControl(this);

				return this;
			}
		},
		ChangeParentTo: {
			value: function (parent) {
				this.parent = parent;
				return this;
			}
		},
		SetToolTip: {
			value: function (text) {
				this.tooltip = text
				return this;
			}
		},
		Saving: {
			value: function (saving) {
				this.save = saving;
				return this;
			}
		},
		AddLogo: {
			value: function (path, logoSize) {
				if (typeof logoSize === "number")
					logoSize = new Vector(logoSize, logoSize);
				this.LogoSize = logoSize;
				this.logo = path;
				return this;
			}
		},
		ChangeValue: {
			value: function (value) {
				this.value = value;
				return this;
			}
		},
		ChangeToDefault: {
			value: function () {
				this.value = this.default;
			}
		}
	});
	MenuBase.prototype.constructor = MenuBase;

	return MenuBase;
})();

function MenuTitle(parent, name) {
	MenuBase.call(this, parent, name, false);
}

MenuTitle.prototype = Object.create(MenuBase.prototype);
MenuTitle.prototype.constructor = MenuTitle;