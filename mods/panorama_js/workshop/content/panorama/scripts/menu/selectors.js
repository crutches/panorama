var MenuSelector = (function () {

	function menuManagerUpdate(node) {
		if (node instanceof MenuCategory)
			return node;

		node = node.parent;

		if (node === undefined)
			return;

		if (node instanceof MenuCategory)
			return node;

		return menuManagerUpdate(node);
	}

	function MenuSelector(parent, name, values, turnValues, dragAndDrop) {

		var _this = this;

		Object.defineProperty(this, "DragAndDrop", {
			value: !!dragAndDrop,
			configurable: false,
			writable: false
		});

		values = values || [];
		Object.defineProperty(this, "values", {
			set: function (newValues) {

				newValues = newValues || [];

				var lastValueLength = values.length;

				values = newValues;

				var top = menuManagerUpdate(_this);
				if (top === undefined)
					return;

				top.Save();

				if (lastValueLength !== newValues.length)
					top.Update(_this);
			},
			get: function () {
				return ARRAY_COPY(values);
			},
			configurable: false,
			enumerable: true
		});

		if (typeof turnValues === "boolean")
			turnValues = turnValues ? this.values : [];

		turnValues = turnValues || [];
		Object.defineProperty(this, "value", {
			set: function (newValue) {

				if (newValue === undefined)
					newValue = [];

				else if (typeof newValue === "boolean")
					newValue = newValue ? _this.values : [];

				else if (!Array.isArray(newValue))
					throw new Error("value is not a array");

				turnValues = ARRAY_COPY(newValue);

				_this.emit("Value", false, turnValues, _this);
			},
			get: function () {
				return ARRAY_COPY(turnValues);
			},
			configurable: false,
			enumerable: true
		});

		MenuBase.call(this, parent, name, turnValues);

		var settsValue = Menu.FindSettings(_this);

		if (settsValue !== undefined && Array.isArray(settsValue)) {
			var tempValues = [];
			var tempValue = [];
			ARRAY_FOREACH_PLUS(settsValue, function (val) {

				var name = val[0];
				var isOn = val[1];

				if (values.indexOf(name) === -1)
					return;

				tempValues.push(name);

				if (isOn)
					tempValue.push(name);
			});
			values = tempValues;
			turnValues = tempValue;
		}
	}
	MenuSelector.prototype = Object.create(MenuBase.prototype, {
		ChangeToAll: {
			value: function () {
				this.value = true;
				return this;
			}
		},
		ChangeToEmpty: {
			value: function () {
				this.value = false;
				return this;
			}
		},
		ChangeValues: {
			value: function (values) {
				this.values = values;
				return this;
			}
		}
	});
	MenuSelector.prototype.constructor = MenuSelector;

	return MenuSelector;
})();

function MenuSelectorSpells(parent, name, values, turnValues, dragAndDrop) {
	MenuSelector.call(this, parent, name, values, turnValues, dragAndDrop);
}
MenuSelectorSpells.prototype = Object.create(MenuSelector.prototype);
MenuSelectorSpells.prototype.constructor = MenuSelectorSpells;


function MenuSelectorItems(parent, name, values, turnValues, dragAndDrop) {
	MenuSelector.call(this, parent, name, values, turnValues, dragAndDrop);
}
MenuSelectorItems.prototype = Object.create(MenuSelector.prototype);
MenuSelectorItems.prototype.constructor = MenuSelectorItems;


function MenuSelectorHeroes(parent, name, values, turnValues, dragAndDrop) {
	MenuSelector.call(this, parent, name, values, turnValues, dragAndDrop);
}
MenuSelectorHeroes.prototype = Object.create(MenuSelector.prototype);
MenuSelectorHeroes.prototype.constructor = MenuSelectorHeroes;