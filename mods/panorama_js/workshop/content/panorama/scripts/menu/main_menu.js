(function (global, manager) {

	/**
	 * @type {Panel}
	 */
	var mainPanel = null;
	var mainPanelPosition = new Vector().SetX(NaN).SetY(NaN);
	/**
	 * @type {Panel}
	 */
	var overlayPanel = null;
	/**
	 * @type {Panel}
	 */
	var loadingPanel = null;

	var menuIsOpened = false;
	var isDashboard = true;

	var menuManager = manager({
		get MainPanel() {
			return mainPanel;
		},
		get OverlayPanel() {
			return overlayPanel;
		},
		get IsDashboard() {
			return isDashboard;
		}
	});

	function Create(parent) {
		mainPanel = $.CreatePanel("Panel", parent || Game.DashboardHUD, "CrutchesMenu");

		mainPanel.BLoadLayout("file://{resources}/layout/custom_game/menu/main_panel.xml", true, false);

		SetPosition();

		ChildCreate();
	}

	function SetPosition() {
		mainPanelPosition.IsValid
			? Drawing.VectorToPanelPercent(mainPanelPosition, mainPanel)
			: mainPanel.style.position = "30px 120px 0";
	}

	function ChildCreate() {
		overlayPanel = mainPanel.FindChildTraverse("overlay");

		mainPanel.FindChildTraverse("panel-close")
			.SetPanelEvent('onactivate', function () {
				ChangeVision(false);
			});

		Panels.MovePanel(mainPanel, MOVE_PANEL_TYPE.ONLY_PRESSED, mainPanel.GetChild(0))
			.on("MoveStart", menuManager.RemoveOpenedControls, true)
			.on("MoveEnded", function (pos) {
				mainPanelPosition.CopyFrom(pos);

				var settings = OBJECT_ASSIGN({}, Crutches.Settings);

				settings["Menu Position: X"] = pos.x;
				settings["Menu Position: Y"] = pos.y;

				Crutches.Settings = settings;
			}, true);
	}

	function AddLoading() {
		overlayPanel.RemoveAndDeleteChildren();

		loadingPanel = $.CreatePanel("Panel", overlayPanel, "loading");

		loadingPanel.BLoadLayoutSnippet("Loading");
	}

	function ChangeVision(state) {
		if (typeof state === "boolean" ? state : !menuIsOpened) {

			mainPanel.AddClass("Opened");
			menuManager.MenuOpened();
			menuIsOpened = true;
		}
		else {
			menuManager.RemoveOpenedControls(true, function () {
				mainPanel.RemoveClass("Opened");
				menuIsOpened = false;
			});
		}
	}

	// Panel.SetParent is broken. Crash by any background (color/image) when showing in another DotaHUD
	// so need delete full panel (or childs) and create again on new HUD. FUCKING VALVE.
	function ChangeParent(parent) {
		mainPanel.RemoveAndDeleteChildren();

		mainPanel.SetParent(parent);

		mainPanel.BLoadLayout("file://{resources}/layout/custom_game/menu/main_panel.xml", true, false);
		ChildCreate();

		menuManager.MenuChangeHud();
	}

	function SendToDashboard(state) {
		if (mainPanel === null || !mainPanel.IsValid())
			Create();

		if (isDashboard)
			return ChangeVision(state);

		menuManager.RemoveOpenedControls(true, function () {
			ChangeParent(Game.DashboardHUD);

			isDashboard = true;

			ChangeVision(menuIsOpened);
		});
	}

	function SendToInGame(state) {
		if (mainPanel === null || !mainPanel.IsValid())
			Create();

		if (!isDashboard)
			return ChangeVision(state);

		menuManager.RemoveOpenedControls(true, function () {
			ChangeParent(Game.InGameHUD);

			isDashboard = false;
			ChangeVision(menuIsOpened);
		});
	}

	function RegisterCommand() {
		if (Game.Commands.OpenMenu === undefined) {
			Game.AddCommand("OpenMenu", ChangeVision, "", 0);
			Game.Commands.OpenMenu = "Open menu";
		}
		if (Game.Commands.OpenMenu === undefined) {
			Game.AddCommand("ReloadCrutches", function () {
				Crutches.ReloadScripts();
			}, "", 0);
			Game.Commands.OpenMenu = "ReloadCrutches";
		}
	}

	if (!Game.IsInToolsMode())
		RegisterCommand();

	$.RegisterKeyBind(Game.DashboardDotaHUD, "key_HOME", function () {
		SendToDashboard();
	});

	Events.on("ResolutionChanges", function () {
		menuManager.RemoveOpenedControls(true, function () {
			if (menuIsOpened)
				ChangeVision(true);
		});
	}, true);

	Events.on("DashboardCreated", function () {
		Create();
		AddLoading();
	}, true);

	Events.on("GameEnded", function () {
		for (var command in Game.Commands)
			Game.Commands[command] = undefined;

		RegisterCommand();

		SendToDashboard();
	}, true);

	Events.on("GameLoaded", function () {
		RegisterCommand();

		SendToInGame();
	}, true);

	Events.on("ReloadScripts", function () {
		menuIsOpened = false;
		AddLoading();
	}, true);

	Events.on("ReloadScriptsPreEnded", function () {

		mainPanelPosition
			.SetX(Menu.FindSettings("Menu Position: X"))
			.SetY(Menu.FindSettings("Menu Position: Y"));

		SetPosition();

		if (loadingPanel !== null) {

			if (loadingPanel.IsValid())
				loadingPanel.DeleteAsync(0.0);

			loadingPanel = null;
		}
	}, true)

	global.MenuPanel = {
		SendToDashboard: SendToDashboard,
		SendToInGame: SendToInGame
	}
	global.MenuCategory = menuManager.MenuCategory;
	global.Menu = menuManager.Manager;

})(this, function (menu) {

	/**
	 * @type {MenuBase[]}
	 */
	var mainCategories = [];
	/**
	 * @type {MenuBase[]}
	 */
	var openedMenu = [];


	/* === functions === */

	function recurSettingsByMenu(entries) {
		var settings = {};

		ARRAY_FOREACH_PLUS(entries, function (entry) {
			if (entry instanceof MenuButton)
				return;

			var value = entry.value;

			if (entry instanceof MenuTree) {
				settings[entry.name.en + "_toggle"] = value;
				settings[entry.name.en] = recurSettingsByMenu(entry.entries);
				return;
			}

			if (entry instanceof MenuSelector) {
				var saveVal = [];
				ARRAY_FOREACH_PLUS(entry.values, function (val) {
					saveVal.push([val, entry.value.indexOf(val) !== -1]);
				});
				settings[entry.name.en] = saveVal;
				return;
			}

			settings[entry.name.en] = value;
		});

		return settings;
	}

	function recurSettings(object, findArray) {
		var find = findArray.splice(0, 1)[0];

		var value = object[find + "_toggle"];

		if (value !== undefined && findArray.length === 0)
			return value;

		value = object[find];

		if (value === undefined) {
			if (!Array.isArray(object) && typeof object === "object")
				return undefined;
			return object;
		}

		return recurSettings(value, findArray);
	}

	function getNamesParents(node, names) {
		if (node instanceof MenuCategory)
			return names;

		node = node.parent;

		if (node === undefined)
			return names;

		names.unshift(node.name.en);

		return getNamesParents(node, names);
	}

	function findLastControl(last, name) {
		if (last === undefined)
			return undefined;

		var find = ARRAY_FIND(last, function (base) {
			return typeof name === "object"
				? (name.en === base.name.en || name.en === base.name.ru)
				: base.name.en === name;
		});

		if (find === undefined)
			return;

		if (!(find instanceof MenuTree))
			throw "First argument require MenuTree or Array of MenuTree";

		return find.entries;
	}

	{ // controls events
		function TreeToggleClass(control, toggleButton) {
			control.value ? toggleButton.AddClass("on") : toggleButton.RemoveClass("on");
		}

		function ToggleClass(control, controlPanel, toggleButton) {
			if (control.value) {
				controlPanel.AddClass("Active");
				toggleButton.AddClass("on")
			}
			else {
				controlPanel.RemoveClass("Active");
				toggleButton.RemoveClass("on");
			}
		}

		function UpdateSlider(control, newValue, sliderValue, sliderTextValue) {
			if (control.value === newValue)
				return;

			control.value = newValue;
			sliderValue.text = newValue;
			sliderTextValue.text = newValue;
		}

		function SliderClass(control, sliderControl, sliderValue, sliderTextValue, controlPanel) {
			control.value !== control.default
				? controlPanel.AddClass("Active")
				: controlPanel.RemoveClass("Active");

			sliderValue.text = control.value;
			sliderControl.value = control.value;
			sliderTextValue.text = control.value;
		}

		function SelectorClass(control, controlPanel, icon, iconPanel) {

			var isNotDefault = ARRAY_SOME(control.value, function (value, index) {
				return value !== control.default[index];
			});

			isNotDefault
				? controlPanel.AddClass("Active")
				: controlPanel.RemoveClass("Active");

			if (ARRAY_INCLUDE(control.value, icon)) {
				iconPanel.checked = true;
				iconPanel.AddClass("Active");
			}
			else {
				iconPanel.checked = false;
				iconPanel.RemoveClass("Active");
			}
		}

		function ShowToolTip(control, controlPanel) {
			if (control.tooltip.en === undefined || ARRAY_INCLUDE(openedMenu, control))
				return;

			var lang = Crutches.Language.toLowerCase().substring(0, 2);

			$.DispatchEvent("DOTAShowTextTooltip", controlPanel, control.tooltip[lang] || control.tooltip.en)
		}
	}

	/**
	 * @param {MenuBase} control 
	 * @param {Panel} panel 
	 */
	function AddControl(control, panel) {

		var controlPanel = $.CreatePanel("Button", panel, "control-" + control.name.en);

		Language(Crutches.Language);

		if (!(control instanceof MenuTree) && !(control instanceof MenuSelector))
			controlPanel.BLoadLayoutSnippet(control.constructor.name);

		if (control instanceof MenuTree) {

			controlPanel.BLoadLayoutSnippet("MenuTree");

			if (control.IsTreeToogle) {
				var toggleButton = controlPanel.FindChildTraverse("toggle-button")

				toggleButton.RemoveClass("non-visible");

				toggleButton.SetPanelEvent("onactivate", function () {
					control.ChangeReverse();
				});

				control.on("Value", function OnValue() {
					if (!controlPanel.IsValid())
						return control.removeListener("Value", OnValue);

					TreeToggleClass(control, toggleButton);
				}, true);

				TreeToggleClass(control, toggleButton);
			}

			controlPanel.SetPanelEvent("onactivate", function () {

				RemoveOpenedControls(control, function (include) {
					if (include) {
						ShowToolTip(control, controlPanel);
						return;
					}

					controlPanel.AddClass("Active");

					if (control.entries.length > 0) {

						var controlsPanel = CreateControlPanel(control, controlPanel, panel, openedMenu.length);

						ARRAY_FOREACH_PLUS(control.entries, function (ctrl) {
							AddControl(ctrl, controlsPanel)
						});
					}

					openedMenu.push(control);
					$.DispatchEvent("DOTAHideTextTooltip", controlPanel);
				});
			});

			if (ARRAY_INCLUDE(openedMenu, control))
				controlPanel.AddClass("Active");
		}
		else if (control instanceof MenuToggle) {

			var toggleButton = controlPanel.FindChildTraverse("toggle-button")

			toggleButton.SetPanelEvent("onactivate", function () {
				control.ChangeReverse();
			});

			control.on("Value", function OnValue() {
				if (!controlPanel.IsValid())
					return control.removeListener("Value", OnValue);

				ToggleClass(control, controlPanel, toggleButton);
			}, true);

			ToggleClass(control, controlPanel, toggleButton);
		}
		else if (control instanceof MenuSlider) {

			var defaultButton = controlPanel.FindChildTraverse("default-button")
			var namePanel = controlPanel.FindChildTraverse("name");

			var lengthValue = control.max.toString().length;
			if (control.IsFloat)
				lengthValue += 2;

			namePanel.style.marginRight = (lengthValue * 10) + "px";

			defaultButton.SetPanelEvent("onactivate", function () {
				control.ChangeToDefault();
			});
			defaultButton.SetPanelEvent("onmouseover", function () {
				$.DispatchEvent("DOTAShowTextTooltip", controlPanel, Game.Language.get("set_default"));
			})

			var sliderControl = controlPanel.FindChildTraverse("slider-control");
			var sliderValue = controlPanel.FindChildTraverse("slider-value");
			var sliderHand = controlPanel.FindChildTraverse("hand-button");
			var sliderTextValue = controlPanel.FindChildTraverse("slider-text-value");

			sliderHand.SetPanelEvent("onactivate", function () {
				var visible = sliderTextValue.visible;

				sliderValue.visible = visible;
				sliderControl.enabled = visible;
				sliderTextValue.visible = !visible;
			});

			sliderHand.SetPanelEvent("onmouseover", function () {
				$.DispatchEvent("DOTAShowTextTooltip", controlPanel, Game.Language.get("set_by_hand"));
			});


			sliderTextValue.SetPanelEvent("oninputsubmit", function () {
				var value = parseFloat(sliderTextValue.text);

				if (value !== NaN) {
					value = Math.max(Math.min(value, control.max), control.min);

					value = control.IsFloat ? ROUND_PLUS(value, 2) : Math.floor(value)

					sliderControl.value = value;
					UpdateSlider(control, value, sliderValue, sliderTextValue);
				}

				sliderControl.enabled = true;
				sliderValue.visible = true;
				sliderTextValue.visible = false;
			});
			sliderTextValue.SetPanelEvent("oncancel", function () {
				sliderControl.enabled = true;
				sliderValue.visible = true;
				sliderTextValue.visible = false;
			});

			sliderControl.min = control.min;
			sliderControl.max = control.max;

			Events.on("Update", function OnUpdate() {
				if (!controlPanel.IsValid())
					return Events.removeListener("Update", OnUpdate);

				UpdateSlider(control,
					control.IsFloat ? ROUND_PLUS(sliderControl.value, 2) : Math.floor(sliderControl.value),
					sliderValue, sliderTextValue);
			}, true);

			control.on("Value", function OnValue() {
				if (!controlPanel.IsValid())
					return control.removeListener("Value", OnValue);

				SliderClass(control, sliderControl, sliderValue, sliderTextValue, controlPanel);
			}, true);

			SliderClass(control, sliderControl, sliderValue, sliderTextValue, controlPanel);
		}
		else if (control instanceof MenuSwitch) {

			var leftArrow = controlPanel.FindChildTraverse("arrow-left");
			var rightArrow = controlPanel.FindChildTraverse("arrow-right");

			leftArrow.SetPanelEvent("onactivate", function () {
				control.ChangeToLeft();
			});
			rightArrow.SetPanelEvent("onactivate", function () {
				control.ChangeToRight();
			});

			control.on("Value", function OnValue() {
				if (!controlPanel.IsValid())
					return control.removeListener("Value", OnValue);

				control.value !== control.default
					? controlPanel.AddClass("Active")
					: controlPanel.RemoveClass("Active");

				var lang = Crutches.Language.toLowerCase().substring(0, 2);

				controlPanel.SetDialogVariable("value", control.values[control.value][lang]);
			}, true);

			control.value !== control.default
				? controlPanel.AddClass("Active")
				: controlPanel.RemoveClass("Active");
		}
		else if (control instanceof MenuButton) {

			var buttonControl = controlPanel.FindChildTraverse("button-control");

			var scheduled = null;

			function SetDeActivate() {
				scheduled = null;
				if (controlPanel.IsValid())
					controlPanel.RemoveClass("Active");
			}

			buttonControl.SetPanelEvent("onactivate", function () {
				control.emit("Pressed");

				controlPanel.AddClass("Active");

				if (scheduled !== null) {
					$.CancelScheduled(scheduled, SetDeActivate);
					scheduled = null;
				}

				scheduled = $.Schedule(0.5, SetDeActivate);
			});
		}
		else if (control instanceof MenuSelector) {

			controlPanel.BLoadLayoutSnippet("MenuSelector");

			var panelName = "DOTAAbilityImage";
			var panelImageName = "abilityname";

			if (control instanceof MenuSelectorItems) {

				panelName = "DOTAItemImage";
				panelImageName = "itemname";
			}
			else if (control instanceof MenuSelectorHeroes) {

				panelName = "DOTAHeroImage";
				panelImageName = "heroname";
			}

			/** @type {Panel} */
			var tempImage;

			var imagePanel = controlPanel.FindChildTraverse("image-panel");

			var tempHas = [];
			ARRAY_FOREACH_PLUS(control.values, function (value) {

				if (ARRAY_INCLUDE(tempHas, value))
					return;

				tempHas.push(value);

				var iconPanel = $.CreatePanel(panelName, imagePanel, value);
				iconPanel.BLoadLayoutFromString("<root><Panel scaling='stretch-to-cover-preserve-aspect'/></root>", false, false)

				iconPanel[panelImageName] = value;

				iconPanel.SetPanelEvent("onmouseover", function () {
					$.DispatchEvent("DOTAShowTextTooltip", controlPanel, value);
				});
				iconPanel.SetPanelEvent("onmouseout", function () {
					$.DispatchEvent("DOTAHideTextTooltip", controlPanel);
				});

				iconPanel.SetPanelEvent("onactivate", function () {
					if (iconPanel.checked) {
						control.value = ARRAY_FILTER(control.value, function (val) {
							return val !== value;
						});
					}
					else {
						var newValue = control.value;
						newValue.push(value);
						control.value = newValue;
					}
				});

				if (control.DragAndDrop) {
					iconPanel.SetDraggable(true);

					$.RegisterEventHandler('DragEnter', iconPanel, function (enterPanel, draggedPanel) {
						var Childs = imagePanel.Children(),
							nowInex = imagePanel.GetChildIndex(iconPanel),
							draggedIndex = draggedPanel.Index;

						if (nowInex - 1 < 0)
							imagePanel.MoveChildBefore(Childs[draggedIndex], Childs[0]);

						else if (nowInex + 1 > Childs.length - 1)
							imagePanel.MoveChildAfter(Childs[draggedIndex], Childs[Childs.length - 1]);

						else if (nowInex < draggedIndex)
							imagePanel.MoveChildAfter(Childs[draggedIndex], Childs[nowInex - 1]);

						else
							imagePanel.MoveChildBefore(Childs[draggedIndex], Childs[nowInex + 1]);

						draggedPanel.Index = nowInex;

						return true;
					});
					$.RegisterEventHandler('DragDrop', iconPanel, function (panelId, draggedPanel) {
						iconPanel.RemoveClass("dragged");
						draggedPanel.DeleteAsync(0);
						UpdateValue();

						return true;
					});
					$.RegisterEventHandler('DragStart', iconPanel, function (panelId, dragCallbacks) {

						iconPanel.AddClass("dragged");

						var displayPanel = $.CreatePanel(panelName, menu.IsDashboard ? Game.DashboardHUD : Game.InGameHUD, "dragImage");
						displayPanel[panelImageName] = value;

						displayPanel.style.width = ((iconPanel.actuallayoutwidth / iconPanel.actualuiscale_x) * 2) + "px";
						displayPanel.style.height = ((iconPanel.actuallayoutheight / iconPanel.actualuiscale_y) * 2) + "px";
						displayPanel.style.borderRadius = "25%";
						displayPanel.style.border = "3px solid #D1D1D12F";

						displayPanel.Index = imagePanel.GetChildIndex(iconPanel);

						dragCallbacks.displayPanel = displayPanel;
						dragCallbacks.offsetX = dragCallbacks.offsetY = 0;

						return true;
					});
					$.RegisterEventHandler('DragEnd', iconPanel, function (panelId, draggedPanel) {
						iconPanel.RemoveClass("dragged");
						draggedPanel.DeleteAsync(0);
						UpdateValue();

						return true;
					});
				}

				control.on("Value", function OnValue() {
					if (!controlPanel.IsValid())
						return control.removeListener("Value", OnValue);

					SelectorClass(control, controlPanel, value, iconPanel);
				}, true)

				SelectorClass(control, controlPanel, value, iconPanel);

				if (tempImage === undefined) {

					Events.on("Update", function WaitValid() {
						if (!controlPanel.IsValid() || tempImage !== undefined)
							return Events.removeListener("Update", WaitValid);

						if (!Panels.IsValid(iconPanel))
							return;

						// DOTAHeroImage haven't correctly contentwidth (and etc.) after creating.
						// so need wait until value will be lower or higher then 2. FUCKING VALVE
						if (panelName === "DOTAHeroImage" &&
							(iconPanel.actuallayoutwidth === 2 || iconPanel.actuallayoutheight === 2))
							return;

						tempImage = iconPanel;
					}, true);
				}
			});

			Events.on("Update", function WaitValid() {
				if (!controlPanel.IsValid())
					return Events.removeListener("Update", WaitValid);

				if (tempImage === undefined || !Panels.IsValid(panel))
					return;

				Events.removeListener("Update", WaitValid);

				var widthMainPanel = panel.actuallayoutwidth / panel.actualuiscale_x;
				var widthIcon = tempImage.actuallayoutwidth / tempImage.actualuiscale_x;

				var widthAllIcons = (widthIcon + 2) * (imagePanel.GetChildCount() - (panelName === "DOTAAbilityImage" ? 3 : 1));

				var maxWidth = 250 + widthAllIcons; // standart width + (size icon + margin) * count;

				var widthStyle = panel.style.width;

				if (widthStyle !== null)
					widthMainPanel = parseFloat(widthStyle);

				if (maxWidth > widthMainPanel)
					panel.style.width = maxWidth + "px";
			}, true);

			function UpdateValue() {
				var values = [];
				var value = ARRAY_COPY(control.value);

				ARRAY_FOREACH_PLUS(imagePanel.Children(), function (child, index) {
					var name = child[panelImageName]
					values.push(name);

					if (child.checked)
						value.splice(index, 0, name);
				});

				control.value = value;
				control.values = values;
			}
		}

		function ChangeLogo() {
			if (!controlPanel.IsValid())
				return control.removeListener("LogoChanged", ChangeLogo);

			var logoPanel = controlPanel.FindChildTraverse("logo");

			if (logoPanel === null)
				return;

			if (control.logo === undefined) {
				logoPanel.style.width = 0;
				logoPanel.style.height = 0;
				logoPanel.RemoveClass("show");
				return;
			}

			var SizeWidth = control.LogoSize.x;
			var SizeHeight = control.LogoSize.y;

			if (control.logo.indexOf("://") === -1) {

				var isDefaultSize = SizeHeight === 18 && SizeWidth === 18;

				if (control.logo.indexOf("npc_dota_hero") !== -1) {

					if (logoPanel.paneltype !== "DOTAHeroImage") {
						var logoNew = $.CreatePanel("DOTAHeroImage", controlPanel, "logo");

						controlPanel.MoveChildAfter(logoNew, logoPanel);
						logoPanel.DeleteAsync(0);
						logoPanel = logoNew;
					}

					logoPanel.heroname = control.logo;

					if (isDefaultSize) {
						SizeHeight = 25;
						SizeWidth = SizeHeight * 1.83;
					}
				}
				else if (control.logo.indexOf("item_") !== -1) {

					if (logoPanel.paneltype !== "DOTAItemImage") {
						var logoNew = $.CreatePanel("DOTAItemImage", controlPanel, "logo");

						controlPanel.MoveChildAfter(logoNew, logoPanel);
						logoPanel.DeleteAsync(0);
						logoPanel = logoNew;
					}

					logoPanel.itemname = control.logo;

					if (isDefaultSize) {
						SizeHeight = 25;
						SizeWidth = SizeHeight * 1.31;
					}
				}
				else {

					if (logoPanel.paneltype !== "DOTAAbilityImage") {
						var logoNew = $.CreatePanel("DOTAAbilityImage", controlPanel, "logo");

						controlPanel.MoveChildAfter(logoNew, logoPanel);
						logoPanel.DeleteAsync(0);
						logoPanel = logoNew;
					}

					logoPanel.abilityname = control.logo;

					if (isDefaultSize)
						SizeWidth = SizeHeight = 25;
				}
			}
			else {

				if (control.logo.indexOf("emoticons") !== -1 && logoPanel.paneltype !== "DOTAEmoticon") {
					var logoNew = $.CreatePanel("DOTAEmoticon", controlPanel, "logo");

					controlPanel.MoveChildAfter(logoNew, logoPanel);
					logoPanel.DeleteAsync(0);
					logoPanel = logoNew;
				}

				logoPanel.SetImage(control.logo);
			}

			if (!logoPanel.BHasClass("show"))
				logoPanel.AddClass("show");

			logoPanel.style.width = SizeWidth + "px";
			logoPanel.style.height = SizeHeight + "px";

			// just for remove default tooltip (ex. DOTAItemImage)
			logoPanel.SetPanelEvent("onmouseover", function () { });
		}

		ChangeLogo();
		control.on("LogoChanged", ChangeLogo);

		controlPanel.SetPanelEvent("onmouseover", function () {
			ShowToolTip(control, controlPanel);
		});

		function Language(language) {
			if (!controlPanel.IsValid()) {
				Events.removeListener("Language", Language);
				return;
			}

			var lang = language.toLowerCase().substring(0, 2);

			if (control instanceof MenuSwitch)
				controlPanel.SetDialogVariable("value", control.values[control.value][lang]);

			controlPanel.SetDialogVariable("name", control.name[lang] || control.name.en);
		}

		Events.on("Language", Language, true);

		if (control instanceof MenuCategory)
			controlPanel.visible = control.entries.length > 0;

		return controlPanel;
	}

	function CreateControlPanel(ctrl, controlPanel, parentPanel, index) {

		var hudPanel = menu.IsDashboard ? Game.DashboardHUD : Game.InGameHUD;

		var controlsPanel = $.CreatePanel("Panel", hudPanel, "tree-" + ctrl.name.en + "-" + (index + 1));

		controlsPanel.BLoadLayout("file://{resources}/layout/custom_game/menu/controls.xml", false, false)

		var x_px = parentPanel.actuallayoutwidth;
		var y_px = parentPanel.actualyoffset + controlPanel.actualyoffset;

		if (index === 0) {
			x_px += menu.MainPanel.actualxoffset + 4; // width of capsule in menu
			y_px += menu.MainPanel.actualyoffset;
		}
		else {
			x_px += parentPanel.actualxoffset - 2;
		}

		var x_prc = x_px / Drawing.GameWidth * 100;
		var y_prc = y_px / Drawing.GameHeight * 100;

		controlsPanel.style.position = x_prc + "% " + y_prc + "% 0";

		return controlsPanel;
	}

	function RemoveOpenedControls(control, callback) {

		if (!menu.MainPanel.BHasClass("Opened")) {
			if (callback !== undefined)
				callback(false);
			return;
		}

		var mainPanel = menu.IsDashboard ? Game.DashboardHUD : Game.InGameHUD;

		var indexControl = openedMenu.indexOf(control);

		var entries = control instanceof MenuTree && control.parent !== undefined
			? control.parent.entries : mainCategories;

		/** @type {Panel[]} */
		var removing = [];

		ARRAY_FOREACH(entries, function RemoveOpenedCtrl(ctrl) {

			var index = openedMenu.indexOf(ctrl);

			if (index === -1)
				return;

			if (ctrl instanceof MenuTree)
				ARRAY_FOREACH(ctrl.entries, RemoveOpenedCtrl);

			var callControls;

			if (ctrl.parent === undefined) {

				callControls = menu.OverlayPanel
					.FindChildTraverse("control-" + ctrl.name.en)
					.GetParent();

			} else
				callControls = mainPanel.FindChildTraverse("tree-" + ctrl.parent.name.en + "-" + index);


			if (callControls !== null && callControls.IsValid())
				callControls.FindChildTraverse("control-" + ctrl.name.en).RemoveClass("Active");

			var controlsPanel = mainPanel.FindChildTraverse("tree-" + ctrl.name.en + "-" + (index + 1));

			if (controlsPanel !== null) {
				controlsPanel.DeleteAsync(0.0);
				removing.push(controlsPanel);
			}

			if (control !== true)
				openedMenu.splice(index, 1);
		});

		Events.on("Update", function WaitToRemove() {

			if (ARRAY_SOME(removing, function (panel) { return panel.IsValid(); }))
				return;

			Events.removeListener("Update", WaitToRemove);

			if (callback !== undefined)
				callback(indexControl !== -1);
		}, true);
	}

	function ShowOpened(ctrl, index) {

		var mainPanel = menu.IsDashboard ? Game.DashboardHUD : Game.InGameHUD;

		/** @type {Panel} */
		var panel;
		/** @type {Panel} */
		var parentPanel;

		if (ctrl.parent === undefined) {

			parentPanel = menu.OverlayPanel;
			panel = parentPanel.FindChildTraverse("control-" + ctrl.name.en);

		} else {

			var parentIndex = openedMenu.indexOf(ctrl.parent) + 1;

			parentPanel = mainPanel
				.FindChildTraverse("tree-" + ctrl.parent.name.en + "-" + parentIndex)

			panel = parentPanel.FindChildTraverse("control-" + ctrl.name.en);
		}

		panel.AddClass("Active");

		if (ctrl.entries.length > 0) {

			Events.on("Update", function WaitLoadPanel() {
				// Panel haven't correctly position after creating
				// so need wait until value will be lower then MAX_SAFE_INTEGER. FUCKING VALVE
				if (!Panels.IsValid(parentPanel))
					return;

				Events.removeListener("Update", WaitLoadPanel);

				var controlsPanel = CreateControlPanel(ctrl, panel, parentPanel, index);

				ARRAY_FOREACH_PLUS(ctrl.entries, function (ctrl) {
					AddControl(ctrl, controlsPanel)
				});

				if (++index < openedMenu.length)
					ShowOpened(openedMenu[index], index);
			}, true);
		}
	}

	function UpdateControl(control, nodePanel, index) {

		var controlPanel = AddControl(control, nodePanel);

		if (nodePanel.GetChildCount() > 1) {

			var nearChild = nodePanel.GetChild(index);

			if (index <= 1)
				nodePanel.MoveChildBefore(controlPanel, nearChild);
			else
				nodePanel.MoveChildAfter(controlPanel, nearChild);
		}
	}

	/* === Menu Manager === */

	function MenuCategory(name) {
		if (ARRAY_SOME(mainCategories, function (base) {
			return typeof name === "object"
				? (name.en === base.name.en || name.en === base.name.ru)
				: base.name.en === name;
		}))
			throw Error("That category already exist. Use 'Menu.Factory' or unique name");

		MenuTree.call(this, undefined, name);

		mainCategories.push(this);
		this.Update();
	}

	MenuCategory.prototype = Object.create(MenuTree.prototype, {
		Update: {
			value: function (control) {

				control = control || this;
				// check in Main Categories
				if (mainCategories.indexOf(control) !== -1) {

					var panel = menu.OverlayPanel.FindChildTraverse("control-" + this.name.en)
					if (panel !== null)
						panel.DeleteAsync(0.0);

					UpdateControl(this, menu.OverlayPanel, mainCategories.indexOf(this))
					return;
				}

				var node = control.parent;

				if (mainCategories.indexOf(node) !== -1)
					menu.OverlayPanel.FindChildTraverse("control-" + node.name.en).visible = node.entries.length > 0;

				var mainPanel = menu.IsDashboard ? Game.DashboardHUD : Game.InGameHUD;

				// check in Trees
				ARRAY_SOME(openedMenu, function (ctrl, index) {

					if (ctrl !== node)
						return false;

					var nodePanel = mainPanel.FindChildTraverse("tree-" + node.name.en + "-" + (index + 1));

					if (nodePanel === null) {
						var parentNode = node.parent;

						if (parentNode === undefined)
							return false;

						var controlParentPanel = mainPanel.FindChildTraverse("tree-" + parentNode.name.en + "-" + (index));

						var controlParent = controlParentPanel.FindChildTraverse("control-" + node.name.en);

						nodePanel = CreateControlPanel(node, controlParent, controlParent.GetParent(), index);
					}
					else {

						var controlPanel = nodePanel.FindChildTraverse("control-" + control.name.en);

						if (controlPanel !== null)
							controlPanel.DeleteAsync(0.0);
					}

					var indexInMenu = node.entries.indexOf(control);

					if (indexInMenu === -1) {

						if (control instanceof MenuTree) {
							ARRAY_FOREACH(control.entries, function RemoveOpenedCtrl(ctrl) {
								var index = openedMenu.indexOf(ctrl.parent);

								if (index === -1)
									return;

								var ctrlTree = mainPanel.FindChildTraverse("tree-" + ctrl.parent.name.en + "-" + (index + 1));

								if (ctrlTree !== null)
									ctrlTree.DeleteAsync(0.0);

								if (ctrl instanceof MenuTree)
									ARRAY_FOREACH(ctrl.entries, RemoveOpenedCtrl);

								openedMenu.splice(index, 1);
							});
						}
						return true;
					}

					UpdateControl(control, nodePanel, indexInMenu);

					return true;
				});
			}
		},
		Save: {
			value: function () {

				var settings = OBJECT_ASSIGN({}, Crutches.Settings);

				settings[this.name.en] = recurSettingsByMenu(this.entries);

				Crutches.Settings = settings;
			}
		}
	});
	MenuCategory.prototype.constructor = MenuCategory;

	Events.on("ReloadScripts", function () {
		RemoveOpenedControls();
		ARRAY_CLEAN(mainCategories);
		ARRAY_CLEAN(openedMenu);
	}, true);

	return {
		MenuCategory: MenuCategory,
		MenuOpened: function () {
			if (openedMenu.length > 0)
				ShowOpened(openedMenu[0], 0);
		},
		MenuChangeHud: function () {
			menu.OverlayPanel.RemoveAndDeleteChildren();

			ARRAY_FOREACH_PLUS(mainCategories, function (category) {
				AddControl(category, menu.OverlayPanel);
			})
		},
		RemoveOpenedControls: RemoveOpenedControls,
		Manager: {
			get ToolTipOSWarning() {
				var fullyUnsupported = !Crutches.IsOSSuppoted && Crutches.OS !== "Windows";

				return {
					en: "TODO",
					ru: "ВНИМАНИЕ!<br/><br/> Ваша операционная система (" + (Crutches.OS + " " + Crutches.Arch) + ") "
						+ (fullyUnsupported
							? "не поддерживается. <br/><br/>Данная функция не будет работать."
							: "поддерживается не полностью. <br/><br/>Данная функция не будет работать корректно.")
				}
			},
			FindControl: function (trees, controlName) {
				var lastControls;

				if (Array.isArray(trees)) {

					for (var i = 0; i < trees.length; i++)
						lastControls = findLastControl(lastControls ? lastControls.entries : mainCategories, trees[i]);
				}
				else
					lastControls = findLastControl(lastControls ? lastControls.entries : mainCategories, trees);

				if (lastControls === undefined)
					return undefined;

				return ARRAY_FIND(lastControls, function (base) {
					return (typeof controlName === "object"
						? (controlName.en === base.name.en || controlName.ru === base.name.ru)
						: base.name.en === controlName);
				});
			},
			FindSettings: function (trees) {

				if (trees instanceof MenuBase) {
					var name = trees instanceof MenuTree ? trees.name.en + "_toggle" : trees.name.en;
					trees = getNamesParents(trees, [name]);
				}

				return Array.isArray(trees)
					? recurSettings(Crutches.Settings, trees)
					: Crutches.Settings[trees];
			},
			Factory: function (categoryName) {
				var find = ARRAY_FIND(mainCategories, function (base) {
					return typeof categoryName === "object"
						? (categoryName.en === base.name.en || categoryName.en === base.name.ru)
						: base.name.en === categoryName;
				});

				if (find !== undefined)
					return find;

				return new MenuCategory(categoryName);
			},
			CreateXY: function (parent, name, vector, minVector, maxVector, isToggleTree, valueToggle, isFloat) {

				var tree = new MenuTree(parent, name, isToggleTree, valueToggle);

				if (typeof minVector === "number")
					minVector = new Vector(minVector, minVector);

				if (!(minVector instanceof Vector))
					minVector = new Vector(0, 0);

				if (typeof maxVector === "number")
					maxVector = new Vector(maxVector, maxVector);

				if (!(maxVector instanceof Vector))
					maxVector = new Vector(95, 95);

				var X = tree.AddSlider({ en: "Position: X", ru: "Позиция: X" }, vector.x, minVector.x, maxVector.x, isFloat);
				var Y = tree.AddSlider({ en: "Position: Y", ru: "Позиция: Y" }, vector.y, minVector.y, maxVector.y, isFloat);

				return {
					tree: tree,
					X: X, Y: Y,
					get Vector() {
						return new Vector(X.value, Y.value);
					}
				}
			},
			CreateXYZ: function (parent, name, vector, minVector, maxVector, isToggleTree, valueToggle, isFloat) {

				var tree = new MenuTree(parent, name, isToggleTree, valueToggle);

				if (typeof minVector === "number")
					minVector = new Vector(minVector, minVector, minVector);

				if (!(minVector instanceof Vector))
					minVector = new Vector(0, 0, 0);

				if (typeof maxVector === "number")
					maxVector = new Vector(maxVector, maxVector, maxVector);

				if (!(maxVector instanceof Vector))
					maxVector = new Vector(95, 95, 95);

				var X = tree.AddSlider({ en: "Position: X", ru: "Позиция: X" }, vector.x, minVector.x, maxVector.x, isFloat);
				var Y = tree.AddSlider({ en: "Position: Y", ru: "Позиция: Y" }, vector.y, minVector.y, maxVector.y, isFloat);
				var Z = tree.AddSlider({ en: "Position: Z", ru: "Позиция: Z" }, vector.z, minVector.z, maxVector.z, isFloat);

				return {
					tree: tree,
					X: X, Y: Y, Z: Z,
					get Vector() {
						return new Vector(X.value, Y.value, Z.value);
					}
				}
			},
			CreateRGB: function (parent, name, color, isToggleTree, valueToggle) {

				var tree = new MenuTree(parent, name, isToggleTree, valueToggle);

				if (typeof color === "number")
					color = new Color(color, color, color);

				if (!(color instanceof Color))
					color = new Color(0, 255);

				var R = tree.AddSlider({ en: "Color: R (red)", ru: "Цвет: R (красный)" }, color.r, 0, 255);
				var G = tree.AddSlider({ en: "Color: G (green)", ru: "Цвет: G (зеленый)" }, color.g, 0, 255);
				var B = tree.AddSlider({ en: "Color: B (blue)", ru: "Цвет: B (синий)" }, color.b, 0, 255);

				return {
					tree: tree,
					R: R, G: G, B: B,
					get Color() {
						return new Color(R.value, G.value, B.value);
					}
				}
			},
			CreateRGBA: function (parent, name, color, isToggleTree, valueToggle) {

				var tree = new MenuTree(parent, name, isToggleTree, valueToggle);

				if (typeof color === "number")
					color = new Color(color, color, color);

				if (!(color instanceof Color))
					color = new Color(0, 255);

				var R = tree.AddSlider({ en: "Color: R (red)", ru: "Цвет: R (красный)" }, color.r, 0, 255);
				var G = tree.AddSlider({ en: "Color: G (green)", ru: "Цвет: G (зеленый)" }, color.g, 0, 255);
				var B = tree.AddSlider({ en: "Color: B (blue)", ru: "Цвет: B (синий)" }, color.b, 0, 255);
				var A = tree.AddSlider({ en: "Opacity (alpha)", ru: "Прозрачность" }, color.a, 0, 100);

				return {
					tree: tree,
					R: R, G: G, B: B, A: A,
					get Color() {
						return new Color(R.value, G.value, B.value, A.value);
					}
				}
			},
			CreateParticle: function (parent, name, color, render, renderValue, isToggleTree, valueToggle) {

				var tree = new MenuTree(parent, name, isToggleTree, valueToggle);

				if (typeof color === "number")
					color = new Color(color, color, color);

				if (!(color instanceof Color))
					color = new Color(0, 255);

				if (!Array.isArray(render))
					render = [PARTICLES_RENDER_NORMAL, PARTICLES_RENDER_ROPE, PARTICLES_RENDER_ANIMATION];

				var R = tree.AddSlider({ en: "Color: R (red)", ru: "Цвет: R (красный)" }, color.r, 0, 255);
				var G = tree.AddSlider({ en: "Color: G (green)", ru: "Цвет: G (зеленый)" }, color.g, 0, 255);
				var B = tree.AddSlider({ en: "Color: B (blue)", ru: "Цвет: B (синий)" }, color.b, 0, 255);
				var A = tree.AddSlider({ en: "Opacity (alpha)", ru: "Прозрачность" }, color.a, 1, 255);

				var Width = tree.AddSlider({ en: "Width", ru: "Ширина" }, 15, 1, 150);
				var Style = tree.AddSwitch({ en: "Style", ru: "Стиль" }, render, renderValue);

				return {
					tree: tree,
					R: R, G: G, B: B, A: A,
					Width: Width,
					Style: Style,
					get Color() {
						return new Color(R.value, G.value, B.value, A.value);
					}
				}
			}
		}
	};
});

(function () {

	Events.on("ReloadScriptsPreEnded", function () {

		{ // Visual
			var visual = Menu.Factory({ en: "Visual", ru: "Визульное" });
			visual.AddLogo("s2r://panorama/images/control_icons/eye_png.vtex");

		}

		{ // Auto Use
			var autoUse = Menu.Factory({ en: "Auto Using", ru: "Автоматическое" })
				.AddLogo("s2r://panorama/images/hud/reborn/icon_speed_psd.vtex")

			autoUse.SetToolTip({
				en: "Auto using items or spells",
				ru: "Автоматическое использование предметов или способностей"
			});

		}

		{ // Heroes
			var heroes = Menu.Factory({ en: "Heroes", ru: "Герои" });
			heroes.AddLogo("s2r://panorama/images/control_icons/hero_progress_icon_png.vtex");

			heroes.AddTitle({ en: "Current Hero", ru: "Выбранный герой" });
			heroes.AddTitle({ en: "All Heroes", ru: "Все герои" });
		}

		{ // Utility
			var utility = Menu.Factory({ en: "Utility", ru: "Утилиты" });
			utility.AddLogo("s2r://panorama/images/hud/reborn/icon_combat_log_psd.vtex");

			var camera = utility.AddTree({ en: "Camera", ru: "Камера" }, true, true)
				.SetToolTip({
					en: "Change camera distance",
					ru: "Смена дистанции камеры"
				})
				.on("Value", CameraCallback);

			if (!Crutches.IsOSSuppoted)
				camera.SetToolTip(Menu.ToolTipOSWarning);

			var cameraDistance = camera.AddSlider({ en: "Distance", ru: "Дистанция" }, 1200, 1, 10000)
				.on("Value", CameraDistanceCallback);

			var cameraWheel = camera.AddTree({ en: "Mouse wheel", ru: "Колесико мыши" }, true, true)
				.SetToolTip({ en: "TODO", ru: "Позволяет изменять дальность камеры с помощью колеса мыши" })
				.on("Activated", function () {
					if (!Game.IsInGame)
						return;

					MouseEvents.on("wheeled", ChangeDistanceWheel);
				})
				.on("Deactivated", function () {
					MouseEvents.removeListener("wheeled", ChangeDistanceWheel);
				});

			var cameraWheelCtrl = camera.AddToggle({ en: "Only 'CTRL' is down", ru: "Если нажат 'CTRL'" })
				.SetToolTip({ en: "TODO", ru: "Изменять только если нажата кнопка 'CTRL'" });

			var cameraWheelStep = cameraWheel.AddSlider({ en: "Step change", ru: "Шаг изменения" }, 50, 5, 500)
				.SetToolTip({ en: "Step to change the camera distance", ru: "Шаг для изменения камеры во время прокрутки" })

			var hiddenSpells = utility.AddToggle({ en: "Hidden spells", ru: "Скрытые способности" }, true)
				.on("Value", HiddenSpellsCallback);

			var weather = utility.AddSwitch({ en: "Weather", ru: "Погода" }, [
				{ en: "Default", ru: "Стандартная" },
				{ en: "Snow", ru: "Снег" },
				{ en: "Rain", ru: "Дождь" },
				{ en: "Moonbeam", ru: "Лунная" },
				{ en: "Pestilence", ru: "Чума" },
				{ en: "Harvest", ru: "Жатва" },
				{ en: "Sirocco", ru: "Пустыня" },
				{ en: "Spring", ru: "Весна" },
				{ en: "Ash", ru: "Жара" },
				{ en: "Aurora", ru: "Аврора" }
			]).on("Value", WeatherChange);

			if (!Crutches.IsOSSuppoted) {
				var tooltip = Menu.ToolTipOSWarning;

				if (Crutches.OS === "Windows") {
					tooltip.en += "TODO";
					tooltip.ru += "<br/><br/>НО вы можете воспользоваться консольными командами";
				}

				hiddenSpells.SetToolTip(tooltip);
				weather.SetToolTip(tooltip);
			}

			function CameraCallback(value) {
				Game.ExecuteCmd("fog_enable " + (!value + 0));

				CameraDistanceCallback();
			}

			function CameraDistanceCallback() {
				if (camera.value) {
					GameUI.SetCameraDistance(cameraDistance.value);
					Game.ExecuteCmd("r_farz " + (cameraDistance.value * 2));
				}
				else {
					GameUI.SetCameraDistance(-1);
					Game.ExecuteCmd("r_farz -1");
				}
			}

			function ChangeDistanceWheel(arg) {
				if (!camera.value)
					return;

				if (cameraWheelCtrl.value && !GameUI.IsControlDown())
					return;

				cameraDistance.value = arg === 1
					? Math.max(cameraDistance.value - cameraWheelStep.value, cameraDistance.min)
					: Math.min(cameraDistance.value + cameraWheelStep.value, cameraDistance.max);
			}

			function HiddenSpellsCallback(value) {
				Game.ExecuteCmd("dota_use_particle_fow " + (!value + 0));
			}

			function WeatherChange(value) {
				Game.ExecuteCmd("cl_weather " + value);
			}
		}

		{ // Settings
			var settings = Menu.Factory({ en: "Settings", ru: "Настройки" });
			settings.AddLogo("s2r://panorama/images/control_icons/gear_small_png.vtex");

			{ // Auto Accept
				var autoAccept = settings.AddTree({ en: "Auto Accept game", ru: "Автопринятие игры" }, true, true)
					.on("Value", function (value) {
						Game.AutoAcceptMatch = value;
					});

				Game.AutoAcceptMatch = autoAccept.value;

				var autoAcceptTime = autoAccept.AddSlider({ en: "Delay", ru: "Задержка" }, 0, 0, 44)
					.SetToolTip({
						en: "Auto accept delay (in seconds)",
						ru: "Задержка перед принятием игры (в секундах)"
					})
					.on("Value", function (value) {
						Game.AutoAcceptMatchTime = value;
					});

				Game.AutoAcceptMatchTime = autoAcceptTime.value;
			}

			{ // Language
				var language = settings.AddSwitch({ en: "Language", ru: "Язык" }, ["English", "Русский"])
					.on("Value", function (value) {
						Crutches.Language = value;
					});

				Events.on("Language", function (value) {
					switch (value) {
						default:
						case "english":
							language.value = 0;
							break;
						case "russian":
							language.value = 1;
							break;
					}
				}, true);
			}

			{ // Ingame Icon

				var iconPos = Menu.CreateXY(settings,
					{ en: "Ingame Icon", ru: "Иконка в игре" },
					new Vector(7.8, 1), 0, 98, true, true, true);

				var iconTree = iconPos.tree;

				iconTree.SetToolTip({
					en: "Ingame Icon for open menu: Hide / Show",
					ru: "Внутриигровая иконка для открытия меню: Скрыть / Показать"
				});

				iconTree.on("Value", function (value) {
					InGameOpenButton.ChangeVision(value);
				});

				iconPos.X.on("Value", function () { InGameOpenButton.Move(iconPos.Vector); });
				iconPos.Y.on("Value", function () { InGameOpenButton.Move(iconPos.Vector); });
			}

			settings.AddButton({ en: "Reset settings", ru: "Сбросить настройки" })
				.SetToolTip({
					en: "Reset your all setting",
					ru: "Сбросить все ваши настройки"
				})
				.on("Pressed", function () {
					Crutches.WebRequest({
						type: REQUEST_TYPE.SAVE_CONFIG,
						settings: JSON.stringify({})
					}, Crutches.ReloadScripts);
				});

			settings.AddButton("Reload Scripts")
				.on("Pressed", Crutches.ReloadScripts);
		}

		Events.on("GameStarted", function () {
			Game.ExecuteCmd("sv_cheats 1");

			Game.ExecuteCmd("demo_recordcommands false");
			Game.ExecuteCmd("dota_unit_orders_rate 512");

			CameraCallback(camera.value);
			HiddenSpellsCallback(hiddenSpells.value);
			WeatherChange(weather.value);

			if (cameraWheel.value)
				MouseEvents.on("wheeled", ChangeDistanceWheel);
		});
	}, true);

	/** @type {Notify} */
	var NotitfyDemo;

	function ChangeLanguageDemo() {
		NotitfyDemo.Panel.SetDialogVariable("text", Game.Language.get("demo_map_error"));
	}

	Events.on("GameLoaded", function () {
		if (Game.GetMapInfo().map_display_name === "hero_demo_main") {
			NotitfyDemo = Drawing.Notify('<Label html="true" text="{s:text}" style="margin: 10px 15px;vertical-align: center; text-align: left; color: #b7d9f5; font-size: 20px;" />', false);
			Events.on("Language", ChangeLanguageDemo);
			ChangeLanguageDemo();
		}
	}, true);
	Events.on("GameEnded", function () {
		if (NotitfyDemo !== undefined) {
			Events.removeListener("Language", ChangeLanguageDemo);
			NotitfyDemo.Remove();
			NotitfyDemo = undefined;
		}
	}, true);

	Events.on("GameStarted", function () {
		Game.ExecuteCmd("sv_cheats 1");
		Game.ExecuteCmd("fog_enable 0");
		Game.ExecuteCmd("r_farz 18000");
	});
})();