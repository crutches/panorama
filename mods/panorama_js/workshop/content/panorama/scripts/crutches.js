var Crutches = (function (global, webRequest) {

	var userSteamID = "";
	var Language = "";

	var privateCrutches = {
		OS: "win32",
		Arch: "x64",
		JSON: undefined,
		Settings: undefined
	};

	var WebRequest = webRequest(privateCrutches);

	var Crutches = {
		get VersionCore() {
			return 4;
		},
		get Contacts() {
			return {
				VK: "vk.com/crutches_official",
				Site: "MyCrutches.ru"
			}
		},
		get OS() {
			return privateCrutches.OS;
		},
		get Arch() {
			return privateCrutches.Arch;
		},
		get JSON() {
			return privateCrutches.JSON;
		},
		get IsOSSuppoted() {
			return Crutches.OS === "Windows" && Crutches.Arch === "x64";
		},

		WebRequest: WebRequest,

		get SteamID() {
			if (userSteamID === "") {
				var info = Game.GetLocalPlayerInfo();

				userSteamID = info !== undefined
					? info.player_steamid : "";
			}
			return userSteamID;
		},

		get Settings() {
			return privateCrutches.Settings || {};
		},
		set Settings(value) {

			if (JSON.stringify(privateCrutches.Settings) === JSON.stringify(value) || typeof value !== "object")
				return;

			privateCrutches.Settings = value;

			WebRequest({
				type: REQUEST_TYPE.SAVE_CONFIG,
				settings: JSON.stringify(value)
			}, function () {
				$.Msg("Config save succeed");
			});
		},
		get Language() {
			return Language;
		},
		set Language(value) {

			if (typeof value === "number") {

				switch (value) {
					default:
					case 0:
						Crutches.Language = "english";
						break;
					case 1:
						Crutches.Language = "russian";
						break;
				}
				return;
			}

			if (Language === value || typeof value !== "string")
				return;

			Language = value;
			Game.LanguageChange(value);
			Events.emit("Language", false, value);
		}
	};

	Events.on("ReloadScriptsPreEnded", function () {
		Crutches.Language = Menu.FindSettings(["Settings", "Language"]) || $.Language();
	}, true)

	return Crutches;

})(this, function (privateCrutches) {

	function unpack(bytes) {
		var chars = [];
		for (var i = 0, n = bytes.length; i < n;) {
			chars.push(((bytes[i++] & 0xFF) << 3) | (bytes[i++] & 0xFF));
		}
		return String.fromCharCode.apply(null, chars);
	}

	var queueReqs = [];
	var queueStarted = false;

	var countTrying = 5;
	var AsyncWebRequestFailed = false;

	function DoRequest() {

		var data = queueReqs[0].data;
		var callback = queueReqs[0].callback

		var playerInfo = Game.GetLocalPlayerInfo();

		OBJECT_ASSIGN(data, {
			steam_id: Crutches.SteamID,
			player_info: playerInfo !== undefined ? playerInfo : ""
		});

		//$.Msg("Req: ", data);

		try {
			$.AsyncWebRequest("http://localhost:8085", {
				type: 'POST',
				data: data,
				timeout: 5000, // 3 sec
				success: function (response) {
					AsyncWebRequestFailed = false;
					//$.Msg(response);

					DashboardPanelUpdate(response.type);

					switch (response.type) {
						case RESPONSE_TYPE.OK: {

							if (response.os !== undefined) {
								switch (response.os) {
									case 'darwin': privateCrutches.OS = "MacOS"; break;
									case 'freebsd': privateCrutches.OS = "FreeBSD"; break;
									case 'sunos': privateCrutches.OS = "SunOS"; break;
									case 'win32': privateCrutches.OS = "Windows"; break;
									case 'linux': privateCrutches.OS = "Linux"; break;
									default: privateCrutches.OS = "Unknown"; break;
								}
							}
							if (response.arch !== undefined) {
								switch (response.arch) {
									case 'arm': privateCrutches.Arch = "arm"; break;
									case 'ia32': privateCrutches.Arch = "x32"; break;
									case 'x64': privateCrutches.Arch = "x64"; break;
									default: privateCrutches.Arch = "Unknown"; break;
								}
							}

							if (response.settings !== undefined)
								privateCrutches.Settings = response.settings;

							if (response.json !== undefined) {
								privateCrutches.JSON = response.json;
								for (var name in privateCrutches.JSON)
									privateCrutches.JSON[name] = JSON.parse(privateCrutches.JSON[name]);
							}

							if (response.scripts !== undefined) {
								for (var name in response.scripts) {
									if (name === "sdk") {
										for (var nameSDK in response.scripts.sdk)
											response.scripts.sdk[nameSDK] = unpack(response.scripts.sdk[nameSDK]);
									}
									else response.scripts[name] = unpack(response.scripts[name]);
								}
							}

							break;
						}
						case RESPONSE_TYPE.ERROR: {
							$.Msg("Error in Launcher. Response:", response.message);
							ShowMessagePopup("popup_launcher_error_title", "popup_launcher_error_message");
							queueStarted = false;
							return;
						}
						case RESPONSE_TYPE.BADVERSION: {

							$.DispatchEvent(
								"UIShowCustomLayoutPopupParameters",
								"CustomPopupTest",
								"file://{resources}/layout/custom_game/popups/popup_update.xml",
								"now=" + Crutches.VersionCore + "&new=" + response.version
							);
							break;
						}
						case RESPONSE_TYPE.SERVER_UPDATING:
							ShowMessagePopup("popup_server_updating_title", "popup_server_updating_message");
							break;
						case RESPONSE_TYPE.SERVER_OFFLINE:
							ShowMessagePopup("popup_server_offline_title", "popup_server_offline_message");
							break;
						default: break;
					}

					try {
						if (callback !== undefined)
							callback(response);
					} catch (e) { $.Msg("Error: succeed callback:\n", e.stack || e.message || e); }

					queueReqs.splice(0, 1);
					countTrying = 5;

					queueReqs = queueReqs.filter(function (req) {
						return req.data.type < 5;
					});

					var scheduleTime = 1;

					if (data.type === REQUEST_TYPE.SAVE_CONFIG && queueReqs.length > 0
						&& queueReqs[0].data.type === REQUEST_TYPE.SAVE_CONFIG) {
						scheduleTime = 2;
					}


					$.Schedule(scheduleTime, function () {
						if (queueReqs.length === 0) {
							queueStarted = false;
							return;
						}

						DoRequest();
					});
				},
				error: function (response) {
					AsyncWebRequestFailed = false;

					$.Msg("Can't connect to loader. Response:", response);

					DashboardPanelUpdate(0, countTrying);

					if (countTrying === 0) {
						queueStarted = false;
						countTrying = 5;
						ShowMessagePopup("popup_launcher_offline_title", "popup_launcher_offline_message");
						return;
					}

					$.Msg("Trying again");

					countTrying--;
					$.Schedule(1, DoRequest);
				}
			});
		}
		catch (e) {

			var message = e.message || e

			if (AsyncWebRequestFailed !== message) {
				$.Msg("AsyncWebRequest: ", message);
				DashboardPanelUpdate(99);
				AsyncWebRequestFailed = message;
			}

			$.Msg(message);
			$.Schedule(1, DoRequest);
		}

	}

	function QueueHandler() {
		if (queueReqs.length === 0) {
			queueStarted = false;
			return;
		}

		if (queueStarted === true)
			return;

		queueStarted = true;

		DoRequest();
	}

	return function (data, callback) {
		queueReqs.push({
			data: data,
			callback: callback
		});

		if (data.type === REQUEST_TYPE.SAVE_CONFIG && queueReqs.length > 1)
			queueReqs.splice(0, queueReqs.length - 1);

		QueueHandler();
	};
});