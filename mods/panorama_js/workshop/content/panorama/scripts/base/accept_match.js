var contex = $.GetContextPanel();
var openned = 0;

contex.SetPanelEvent("onload", function () {
	openned = Date.now();
	WaitAcceptOn();
});

function WaitAcceptOn() {
	if (Game.AutoAcceptMatch !== true) {
		if (contex.IsValid())
			$.Schedule(0, WaitAcceptOn);
		return;
	}
	var time = (Game.AutoAcceptMatchTime) - ((Date.now() - openned) / 1000);
	if (time < 0)
		return Accept();

	$.Schedule(time, function () {
		if (!contex.IsValid())
			return;

		contex.BCreateChildren('<Panel id="AcceptMatchPanel" onload="DOTAPlayAcceptMatch()" />');
		$('#AcceptMatchPanel').DeleteAsync(0.0);
	});
}

function Accept() {
	if (!contex.IsValid())
		return;

	contex.BCreateChildren('<Panel id="AcceptMatchPanel" onload="DOTAPlayAcceptMatch()" />');
	$('#AcceptMatchPanel').DeleteAsync(0.0);
}

