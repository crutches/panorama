var DashboardPanelUpdate = (function () {

	/**
	 * @type {Panel}
	 */
	var dashPanel = null;

	/**
	 * @type {Panel}
	 */
	var versionInfo = null;
	/**
	 * @type {Panel}
	 */
	var newsButton = null;
	/**
	 * @type {Panel}
	 */
	var warningPanel = null;
	/**
	 * @type {Panel}
	 */
	var refreshButton = null;

	var warningInfo = "";
	var warningInfoTooltip = "";
	var tooltipPanelRefresh = "";

	var countTryingToConnect = 0;
	var responseType = undefined;

	function Create() {

		dashPanel = Game.DashboardDotaHUD.FindChildTraverse("CrutchesDashboardPanel");

		if (dashPanel !== null)
			dashPanel.DeleteAsync(0);

		var leftColumn = Game.DashboardDotaHUD.FindChildTraverse("LeftColumn");

		if (leftColumn === null)
			throw "LeftColumn is not defined. Pls, send screenshot to group VK: " + Crutches.Contacts.VK;

		var homeProfile = leftColumn.FindChild("HomeProfileContainer");

		if (homeProfile === null)
			throw "Friend Panel is not defined. Pls, send screenshot to group VK: " + Crutches.Contacts.VK;

		dashPanel = $.CreatePanel("Panel", leftColumn, "CrutchesDashboardPanel");

		dashPanel.SetDialogVariable("now_version", Crutches.VersionCore);

		leftColumn.MoveChildAfter(dashPanel, homeProfile);

		dashPanel.BLoadLayout("file://{resources}/layout/custom_game/dashboard_panel.xml", false, false);

		versionInfo = dashPanel.FindChildTraverse("info-version");
		newsButton = dashPanel.FindChildTraverse("news-button");
		warningPanel = dashPanel.FindChildTraverse("warning-panel");
		refreshButton = dashPanel.FindChildTraverse("refresh-icon");

		versionInfo.SetPanelEvent("onmouseover", function () {
			if (!versionInfo.BHasClass("old"))
				return;

			$.DispatchEvent("DOTAShowTextTooltip", versionInfo, Game.Language.get("panel_version_old_message"));
		})

		dashPanel.FindChildTraverse("open-button-icon").SetPanelEvent("onactivate", function () {
			MenuPanel.SendToDashboard();
		});

		newsButton.SetPanelEvent("onactivate", function () {

			const newsUrl = "https://www.mycts.ru/forum/forums/News_"
				+ Crutches.Language.toLocaleLowerCase().substr(0, 2) + "/";

			Panels.RunFunc("ExternalBrowserGoToURL(" + newsUrl + ")", newsButton)
		});

		refreshButton.SetPanelEvent("onactivate", function () {

			dashPanel.enabled = false;

			refreshButton.AddClass("Active");

			warningInfo = "panel_connecting";
			warningInfoTooltip = "panel_connecting_message";
			tooltipPanelRefresh = "panel_connecting_message";

			ChangeLanguage();

			Crutches.WebRequest({
				type: responseType === 0 ? REQUEST_TYPE.CHECK_LAUNCHER : REQUEST_TYPE.CHECK_SERVER
			})
		})

		UpdateInfo();
	}

	function UpdateInfo() {
		if (dashPanel === null)
			return;

		if (countTryingToConnect > 0)
			return;

		tooltipPanelRefresh = "tooltip_panel_refresh";

		dashPanel.enabled = true;
		refreshButton.RemoveClass("Active");

		responseType === RESPONSE_TYPE.BADVERSION
			? versionInfo.AddClass("old")
			: versionInfo.RemoveClass("old");

		switch (responseType) {
			case 0: {
				newsButton.visible = false;
				warningPanel.visible = true;
				refreshButton.visible = true;

				warningInfo = "popup_launcher_offline_title";
				warningInfoTooltip = "panel_launcher_offline_message";
				break;
			}
			case RESPONSE_TYPE.CONFIG:
			case RESPONSE_TYPE.OK: {
				newsButton.visible = true;
				warningPanel.visible = false;
				break;
			}
			case RESPONSE_TYPE.ERROR: {
				newsButton.visible = false;
				warningPanel.visible = true;
				refreshButton.visible = true;

				warningInfo = "popup_launcher_error_title";
				warningInfoTooltip = "panel_launcher_error_message";
				break;
			}
			case RESPONSE_TYPE.BADVERSION: {
				newsButton.visible = false;
				warningPanel.visible = true;
				refreshButton.visible = false;

				warningInfo = "popup_need_update_title";
				warningInfoTooltip = "panel_need_update_message";
				break;
			}
			case RESPONSE_TYPE.SERVER_UPDATING: {
				newsButton.visible = false;
				warningPanel.visible = true;
				refreshButton.visible = true;

				warningInfo = "popup_server_updating_title";
				warningInfoTooltip = "panel_server_updating_message";
				break;
			}
			case RESPONSE_TYPE.SERVER_OFFLINE: {
				newsButton.visible = false;
				warningPanel.visible = true;
				refreshButton.visible = true;

				warningInfo = "popup_server_offline_title";
				warningInfoTooltip = "panel_server_offline_message";
				break;
			}
			case 99: {
				warningPanel.visible = true;
				refreshButton.visible = false;

				warningInfo = "panel_connecting";
				warningInfoTooltip = "panel_connecting_message";
				break;
			}
			case undefined:
				warningPanel.visible = true;
				refreshButton.visible = false;

				warningInfo = "loading";
				warningInfoTooltip = "loading";
				break;

			default: break;
		}

		ChangeLanguage();
	}

	function ChangeLanguage() {
		if (dashPanel === null)
			return;

		dashPanel.SetDialogVariable("warning_info", Game.Language.get(warningInfo));
		dashPanel.SetDialogVariable("warning_info_tooltip", Game.Language.get(warningInfoTooltip));
		dashPanel.SetDialogVariable("tooltip_panel_refresh", Game.Language.get(tooltipPanelRefresh));
		dashPanel.SetDialogVariable("tooltip_menu", Game.Language.get("tooltip_menu"));
	}
	Events.on("DashboardCreated", Create, true);
	Events.on("Language", ChangeLanguage, true);

	return function (resType, countTrying) {
		responseType = resType;
		countTryingToConnect = countTrying || 0;

		UpdateInfo();
	};
})();

(function () {

	var globalContext = $.GetContextPanel();
	var parent = globalContext;

	while (parent !== null) {
		globalContext = parent;

		parent = globalContext.GetParent();
	}

	Game.DashboardDotaHUD = globalContext;
	Game.DashboardHUD = globalContext.FindChildTraverse("Crutches");

	Game.DashboardHUD.RemoveAndDeleteChildren();
})();