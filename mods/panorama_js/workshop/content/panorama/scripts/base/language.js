(function () {

	var Language = new Map();

	function LanguageChange(lang) {

		switch (lang || $.Language()) {
			default:
			case "english": {

				Language.set("loading", "Loading...");
				Language.set("version", "Version");
				Language.set("version_your", "Your version");
				Language.set("version_new", "New version");
				Language.set("launcher", "Launcher");

				Language.set("open", "Open");
				Language.set("close", "Close");

				// Popups
				Language.set("popup_title", "Attention!");

				Language.set("popup_need_update_title", "Update requared!");
				Language.set("popup_need_update_message", "Please, update Panorama JS in Launcher.<br/><br/>If that is not done - scripts won't work!<br/><br/><span class=\"popups-text-small\">In case of problems, contact our technical support in VK group - </span><a href=\"event:ExternalBrowserGoToURL('https://vk.com/crutches_official')\"><span class=\"LabelLink popups-text-link\">HERE</span></a>.");

				Language.set("popup_server_updating_title", "Server updating!");
				Language.set("popup_server_updating_message", "We're updating server at the moment.<br/><br/>Unfortunately, you can't play with scripts.<br/><br/><br/><span class=\"popups-text-small\">Look for availability in our <a href=\"event:ExternalBrowserGoToURL('https://vk.com/crutches_official')\"><span class=\"LabelLink popups-text-link\">VK group</span></a>, or on our <a href=\"event:ExternalBrowserGoToURL('https://www.mycts.ru/forum/')\"><span class=\"LabelLink popups-text-link\">forum</span></a>!</span>");

				Language.set("popup_server_offline_title", "Server currently offline!");
				Language.set("popup_server_offline_message", "Our server temporarily unavailable at the moment.<br/><br/>Unfortunately, you can't play with scripts.<br/><br/><br/><span class=\"popups-text-small\">Look for availability in our <a href=\"event:ExternalBrowserGoToURL('https://vk.com/crutches_official')\"><span class=\"LabelLink popups-text-link\">VK group</span></a>, or on our <a href=\"event:ExternalBrowserGoToURL('https://www.mycts.ru/forum/')\"><span class=\"LabelLink popups-text-link\">forum</span></a>!</span>");

				Language.set("popup_launcher_offline_title", "Launcher is not running!");
				Language.set("popup_launcher_offline_message", "Failed to connect to Launcher<br/><br/>Make sure Launcher is open and try again.<br/><br/><br/><span class=\"popups-text-small\">In case of problems, contact our technical support in VK group - </span><a href=\"event:ExternalBrowserGoToURL('https://vk.com/crutches_official')\"><span class=\"LabelLink popups-text-link\">HERE</span></a>.");

				Language.set("popup_launcher_error_title", "Error in Launcher!");
				Language.set("popup_launcher_error_message", "Возникла проблема в Лаунчере.<br/><br/>Подробности описаны во всплывающем окне окне в Лаунчере<br/><br/><br/><span class=\"popups-text-small\">In case of problems, contact our technical support in VK group - </span><a href=\"event:ExternalBrowserGoToURL('https://vk.com/crutches_official')\"><span class=\"LabelLink popups-text-link\">HERE</span></a>.");

				// Panel
				Language.set("panel_version_old_message", "Your version outdate!");
				Language.set("panel_need_update_message", "New version available!<br/><br/>Please, update Panorama JS in Launcher.<br/><br/>If that is not done - scripts won't work!");
				Language.set("panel_server_updating_message", "We're updating server at the moment.<br/><br/>Unfortunately, you can't play with scripts.");
				Language.set("panel_server_offline_message", "Our server temporarily unavailable at the moment.<br/><br/>Unfortunately, you can't play with scripts.");
				Language.set("panel_launcher_offline_message", "Failed to connect to Launcher.<br/><br/>Make sure Launcher is open and try again.");
				Language.set("panel_launcher_error_message", "Возникла проблема в Лаунчере.<br/><br/>Подробности описаны во всплывающем окне в Лаунчере.");
				Language.set("panel_connecting_message", "Connecting, please wait...");


				Language.set("panel_connecting", "Connecting...");
				Language.set("tooltip_panel_refresh", "Check availability");
				Language.set("panel_news_info", "What's new?");
				Language.set("tooltip_panel_news_info", "Find out what's new in the last update");

				Language.set("tooltip_menu", "Crutches Menu");

				Language.set("set_default", "Set default");
				Language.set("set_by_hand", "TODO");

				Language.set("demo_map_error", "TODO");

				break;
			}
			case "russian": {

				Language.set("loading", "Загрузка...");
				Language.set("version", "Версия");
				Language.set("version_your", "Ваша версия");
				Language.set("version_new", "Новая версия");
				Language.set("launcher", "Лаунчер");

				Language.set("open", "Открыть");
				Language.set("close", "Закрыть");

				// Popups
				Language.set("popup_title", "Внимание!");

				Language.set("popup_need_update_title", "Требуется обновление!");
				Language.set("popup_need_update_message", "Пожалуйста, обновите Panorama JS в Лаунчере.<br/><br/>Если этого не сделать - скрипты работать не будут!<br/><br/><span class=\"popups-text-small\">В случае возникновения проблем обратитесь в нашу тех. поддержку в группе в ВК - </span><a href=\"event:ExternalBrowserGoToURL('https://vk.com/crutches_official')\"><span class=\"LabelLink popups-text-link\">ЗДЕСЬ</span></a>.");

				Language.set("popup_server_updating_title", "Обновление сервера!");
				Language.set("popup_server_updating_message", "На данный момент у нас проходит обновление сервера.<br/><br/>К сожалению, поиграть со скриптами не получится.<br/><br/><span class=\"popups-text-small\">Вы можете следить за доступностью в нашей <a href=\"event:ExternalBrowserGoToURL('https://vk.com/crutches_official')\"><span class=\"LabelLink popups-text-link\">группе в ВК</span></a>, или на нашем <a href=\"event:ExternalBrowserGoToURL('https://www.mycts.ru/forum/')\"><span class=\"LabelLink popups-text-link\">форуме</span></a>!</span>");

				Language.set("popup_server_offline_title", "Cервер недоступен!");
				Language.set("popup_server_offline_message", "На данный момент наш сервер временно не доступен.<br/><br/>К сожалению, поиграть со скриптами не получится.<br/><br/><span class=\"popups-text-small\">Вы можете следить за доступностью в нашей <a href=\"event:ExternalBrowserGoToURL('https://vk.com/crutches_official')\"><span class=\"LabelLink popups-text-link\">группе в ВК</span></a>, или на нашем <a href=\"event:ExternalBrowserGoToURL('https://www.mycts.ru/forum/')\"><span class=\"LabelLink popups-text-link\">форуме</span></a>!</span>");

				Language.set("popup_launcher_offline_title", "Лаунчер не запущен!");
				Language.set("popup_launcher_offline_message", "Не удается подключиться к Лаунчеру.<br/><br/>Убедитесь что Лаунчер запущен и попробуйте снова.<br/><br/><span class=\"popups-text-small\">В случае возникновения проблем обратитесь в нашу тех. поддержку в группе в ВК - </span><a href=\"event:ExternalBrowserGoToURL('https://vk.com/crutches_official')\"><span class=\"LabelLink popups-text-link\">ЗДЕСЬ</span></a>.");

				Language.set("popup_launcher_error_title", "Error in Launcher!");
				Language.set("popup_launcher_error_message", "Возникла проблема в Лаунчере.<br/><br/>Подробности описаны во всплывающем окне в Лаунчере<br/><br/><br/><span class=\"popups-text-small\">In case of problems, contact our technical support in VK group - </span><a href=\"event:ExternalBrowserGoToURL('https://vk.com/crutches_official')\"><span class=\"LabelLink popups-text-link\">HERE</span></a>.");

				// Panel
				Language.set("panel_version_old_message", "Ваша версия устарела!");
				Language.set("panel_need_update_message", "Доступна новая версия!<br/><br/>Пожалуйста, обновите Panorama JS в Лаунчере.<br/><br/>Если этого не сделать - скрипты работать не будут!");
				Language.set("panel_server_updating_message", "На данный момент у нас проходит обновление сервера.<br/><br/>К сожалению, поиграть со скриптами не получится!");
				Language.set("panel_server_offline_message", "На данный момент наш сервер временно не доступен.<br/><br/>К сожалению, поиграть со скриптами не получится!");
				Language.set("panel_launcher_offline_message", "Не удается подключится к Лаунчеру.<br/><br/>Убедитесь что Лаунчер запущен и попробуйте снова.");
				Language.set("panel_launcher_error_message", "Возникла проблема в Лаунчере.<br/><br/>Подробности описаны во всплывающем окне в Лаунчере.");
				Language.set("panel_connecting_message", "Идет подключение, пожалуйста подождите");


				Language.set("panel_connecting", "Подключение...");
				Language.set("tooltip_panel_refresh", "Проверить доступность");
				Language.set("panel_news_info", "Что нового?");
				Language.set("tooltip_panel_news_info", "Узнайте что нового в последнем обновлении");

				Language.set("tooltip_menu", "Crutches Меню");

				Language.set("set_default", "По умолчанию");
				Language.set("set_by_hand", "Написать вручную");

				Language.set("demo_map_error", "Внимание!<br/><br/>В Демо некоторые функции будут работать НЕ корректно или НЕ БУДУТ работать совсем!<br/><br/>Для полноценной работы всех функций лучше тестировать на стандартной карте");
				break;
			}
		}
	}
	LanguageChange();

	if (Game.Language === undefined) {
		Object.defineProperty(Game, "Language", {
			value: Language,
			configurable: false,
			writable: false
		});
	}

	if (Game.LanguageChange === undefined) {
		Object.defineProperty(Game, "LanguageChange", {
			value: LanguageChange,
			configurable: false,
			writable: false
		});
	}
})();