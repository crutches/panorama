(function () {

	var globalContext = $.GetContextPanel();
	var parent = globalContext;

	while (parent !== null) {
		globalContext = parent;

		parent = globalContext.GetParent();
	}

	Game.InGameDotaHUD = globalContext;
	Game.InGameHUD = globalContext.FindChildTraverse("Crutches");

	Game.InGameHUD.RemoveAndDeleteChildren();
})();