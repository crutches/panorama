/* ================================== REWRITE ================================== */


if ($.Schedule_original === undefined) {
	$.Schedule_original = $.Schedule
	$.Schedule = function (delay, callback) {
		if (isNaN(delay) || !isFinite(delay) || delay === undefined)
			throw "$.Schedule -> invalid delay: " + delay;
		return $.Schedule_original(delay, callback)
	}
}

/* ================================== Global ================================== */

for (var name in Game.global) {
	if (this[name] !== undefined)
		continue;
	this[name] = Game.global[name];
}

/* ================================== Inject ================================== */

const ALL_SCRIPTS_UNSORTED = JSON.parse($.GetContextPanel().GetAttributeString("scripts", ""));

const REQUIRED_REGEXP = /^\/\/\s+REQUIRED:\s+(\w+)$/gm;

const ALL_SCRIPTS = {};

ARRAY_FOREACH_PLUS(Object.keys(ALL_SCRIPTS_UNSORTED).sort(), function (key) {
	ALL_SCRIPTS[key] = ALL_SCRIPTS_UNSORTED[key];
});

$.Msg("SDK:");

const SORTED_SDK = [];

for (var NameSDK in ALL_SCRIPTS.sdk)
	FindRequire(NameSDK, SORTED_SDK, true);

for (var i = 0, length = SORTED_SDK.length; i < length; i++) {
	var MODULE_SDK = SORTED_SDK[i];
	try {
		eval(MODULE_SDK.code);

		$.Msg("\t| ", MODULE_SDK.name);
	} catch (e) {
		$.Msg("\t| ", MODULE_SDK.name, " | error: ", e.stack || e.message || e || "unknown");
	}
}

$.Msg("Scripts:");

const SORTED_SCRIPTS = [];

for (var NameSDK in ALL_SCRIPTS) {
	if (NameSDK === "sdk")
		continue;
	FindRequire(NameSDK, SORTED_SCRIPTS);
}

ARRAY_FOREACH_PLUS(SORTED_SCRIPTS, function (script, index) {
	try {
		(new Function(script.code)());

		$.Msg("\t| ", index + 1, "\t| ", script.name);
	} catch (e) {
		$.Msg("\t| ", script.name, " | error: ", e.stack || e.message || e || "unknown");
	}
});


function FindRequire(name, sorted, sdk) {

	if (ARRAY_SOME(sorted, function (module) { return module.name === name }))
		return;

	const code = sdk ? ALL_SCRIPTS.sdk[name] : ALL_SCRIPTS[name];

	const match = code.match(REQUIRED_REGEXP);

	if (match !== null) {

		ARRAY_FOREACH_PLUS(match, function (module) {
			const req = REQUIRED_REGEXP.exec(module);
			// RegExp is broken. Need execute twice for good result. Facepalm. FUCKING VALVE.
			REQUIRED_REGEXP.exec(module)
			FindRequire(req[1], sorted, sdk);
		});
	}

	if (ARRAY_SOME(sorted, function (module) { return module.name === name }))
		return;

	sorted.push({ name: name, code: code });
}