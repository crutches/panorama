(function () {

	var globalContext = $.GetContextPanel();
	var parent = globalContext;

	while (parent !== null) {
		globalContext = parent;

		parent = globalContext.GetParent();
	}

	if (globalContext.FindChildTraverse("Crutches") !== null)
		return;

	var crutchesPanel = $.CreatePanel("Panel", globalContext, "Crutches");

	crutchesPanel.BLoadLayoutFromString("\
		<root>\
			<Panel hittest='false' style='width: 100%; height: 100%;'/>\
		</root>\
	", false, false);

})();