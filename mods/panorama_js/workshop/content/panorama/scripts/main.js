(function (preStart) {

	preStart();

	var ContextPanel = Game.DashboardDotaHUD;
	var GameStarted = false;
	var eventByStartGame = false;

	function CheckUpdate(callback) {

		Crutches.WebRequest({
			type: REQUEST_TYPE.CHECK_VERSION,
			version: Crutches.VersionCore
		}, function (response) {

			if (response.type === RESPONSE_TYPE.BADVERSION)
				return;

			callback();
		})
	}

	var reloading = false;
	Object.defineProperty(Crutches, "ReloadScripts", {
		value: ReloadScripts,
		configurable: false,
		writable: false
	});

	function ReloadScripts() {

		if (reloading)
			return;

		reloading = true;

		Events.emit("ReloadScripts");

		Events.emit("RemoveEvents");

		var scriptsPanel = Game.DashboardHUD.FindChildTraverse("Scripts");

		if (scriptsPanel !== null)
			scriptsPanel.DeleteAsync(0.0);

		var childsCrutchesHud = Game.InGameHUD.Children();
		childsCrutchesHud.concat(Game.DashboardHUD.Children());

		ARRAY_FOREACH(childsCrutchesHud, function (panel) {
			if (panel.id !== "CrutchesMenu" && panel.IsValid())
				panel.DeleteAsync(0);
		})

		Crutches.WebRequest({
			type: REQUEST_TYPE.GET
		}, function (res) {

			Events.emit("ReloadScriptsPreEnded");

			try {
				var scriptsPanel = $.CreatePanel("Panel", Game.DashboardHUD, "Scripts");

				scriptsPanel.SetAttributeString("scripts", JSON.stringify(res.scripts));

				scriptsPanel.BLoadLayout("file://{resources}/layout/custom_game/inject_scripts.xml", false, false);
			}
			catch (e) { }

			Events.emit("ReloadScriptsEnded");

			ReloadEvents();

			$.Schedule(1, function () {
				reloading = false;
			});
		});
	}

	function ReloadEvents() {
		waitLoadGame.Stop();
		waitStartGame.Stop();
		waitEndGame.Stop();

		waitLoadGame.Start();
	}

	function RegisterCommand() {
		if (Game.Commands.ReloadScripts === undefined) {
			Game.AddCommand("ReloadScripts", ReloadScripts, "", 0);
			Game.Commands.ReloadScripts = "Reload scripts";
		}
	}

	if (!Game.IsInToolsMode())
		RegisterCommand();

	{ // HomePage

		var waitHomePage = new Thread(0, function () {

			if (ContextPanel.FindChildTraverse("TodayPages") === null)
				return;

			waitHomePage.Stop();

			Events.emit("DashboardCreated");
		}, true);
	}

	{ // Update connect

		var checkUpdateThread = new ThreadCallback(60, function () {
			return new Promise(function (resolve) {
				CheckUpdate(resolve);
			});
		}, true);

		var updateUser = new ThreadCallback(30, function () {
			return new Promise(function (resolve) {

				if (Crutches.SteamID === "") {
					resolve(1);
					return;
				}
				Crutches.WebRequest({ type: REQUEST_TYPE.UPDATE_ONLINE }, function () { resolve(30); });
			})
		});
	}

	{ // Load Game

		var waitLoadGame = new Thread(0, function () {

			var gameState = Game.GetState();

			if ((gameState !== DOTA_GameState.DOTA_GAMERULES_STATE_POST_GAME 
				&& gameState !== DOTA_GameState.DOTA_GAMERULES_STATE_GAME_IN_PROGRESS
				&& gameState !== DOTA_GameState.DOTA_GAMERULES_STATE_PRE_GAME
				&& gameState !== DOTA_GameState.DOTA_GAMERULES_STATE_STRATEGY_TIME
				&& gameState !== DOTA_GameState.DOTA_GAMERULES_STATE_HERO_SELECTION))
				return;

			waitLoadGame.Stop();

			waitEndGame.Start();

			RegisterCommand();

			Events.emit("GameLoaded");

			waitStartGame.Start();
		}, true);
	}

	{ // Start Game

		var waitStartGame = new Thread(0, function () {

			if (!Game.IsInGame)
				return;

			waitStartGame.Stop();

			if (GameStarted) {
				eventByStartGame = true;
				ARRAY_FOREACH_PLUS(assignedHeroes, function (event) {
					NativeEvents.emit("AssignedHero", false, event);
				});
				ARRAY_FOREACH_PLUS(allEntities, function (event) {
					NativeEvents.emit("EntitySpawned", false, event);
				});
				eventByStartGame = false;
			}

			Events.emit("GameStarted");

			GameStarted = true;
		}, true);
	}

	{ // In-game callback caching

		var assignedHeroes = [];
		NativeEvents.on("AssignedHero", function (event) {
			if (eventByStartGame)
				return;
			assignedHeroes.push(event);
		}, true);

		var allEntities = [];
		NativeEvents.on("npc_spawned", function (event) {
			allEntities.push(event);
		}, true);
		NativeEvents.on("EntityKilled", function (event) {
			ARRAY_SOME(allEntities, function (ev, index) {
				if (ev.entindex !== event.entindex_killed)
					return false;

				allEntities.splice(index, 1);
				return true;
			})
		}, true);
	}

	{ // End Game

		var waitEndGame = new Thread(0, function () {
			var gameState = Game.GetState();

			if (gameState !== DOTA_GameState.DOTA_GAMERULES_STATE_DISCONNECT
				&& gameState !== DOTA_GameState.DOTA_GAMERULES_STATE_INIT)
				return;

			GameStarted = false;
			ARRAY_CLEAN(assignedHeroes);
			ARRAY_CLEAN(allEntities);

			Events.emit("GameEnded");

			ReloadEvents();
			RegisterCommand();
		}, true);
	}

	Events.on("DashboardCreated", function () {
		CheckUpdate(function () {
			checkUpdateThread.Start();
			updateUser.Start();
			ReloadScripts();
		});
	}, true);

	waitHomePage.Start();

})(function () {

	Events.on("ReloadScripts", function () { $.Msg("ReloadScripts"); }, true);
	Events.on("ReloadScriptsEnded", function () { $.Msg("ReloadScriptsEnded"); }, true);
	Events.on("Language", function (language) { $.Msg("Language: ", language); }, true);

	Events.on("ResolutionChanges", function (width, height) { $.Msg("ResolutionChanges: ", width, "x", height); }, true);
	Events.on("DashboardCreated", function () { $.Msg("DashboardCreated"); Game.ExecuteCmd("map dota"); }, true);

	Events.on("GameLoaded", function () { $.Msg("GameLoaded"); }, true);
	Events.on("PlayerLoaded", function (player) { $.Msg("PlayerLoaded: ", player); }, true);
	Events.on("GameStarted", function () { $.Msg("GameStarted"); }, true);
	Events.on("GameEnded", function () { $.Msg("GameEnded"); }, true);
});